package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableIgnoreElements<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  public FlowableIgnoreElements(Publisher<T> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new IgnoreElementsSubscriber(paramSubscriber));
  }
  
  static final class IgnoreElementsSubscriber<T>
    implements QueueSubscription<T>, Subscriber<T>
  {
    final Subscriber<? super T> actual;
    Subscription s;
    
    IgnoreElementsSubscriber(Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void clear() {}
    
    public boolean isEmpty()
    {
      return true;
    }
    
    public boolean offer(T paramT)
    {
      throw new UnsupportedOperationException("Should not be called!");
    }
    
    public boolean offer(T paramT1, T paramT2)
    {
      throw new UnsupportedOperationException("Should not be called!");
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT) {}
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public T poll()
    {
      return null;
    }
    
    public void request(long paramLong) {}
    
    public int requestFusion(int paramInt)
    {
      return paramInt & 0x2;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableIgnoreElements.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */