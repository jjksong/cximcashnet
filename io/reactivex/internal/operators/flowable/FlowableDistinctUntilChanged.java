package io.reactivex.internal.operators.flowable;

import io.reactivex.functions.BiPredicate;
import io.reactivex.functions.Function;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.subscribers.BasicFuseableConditionalSubscriber;
import io.reactivex.internal.subscribers.BasicFuseableSubscriber;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableDistinctUntilChanged<T, K>
  extends AbstractFlowableWithUpstream<T, T>
{
  final BiPredicate<? super K, ? super K> comparer;
  final Function<? super T, K> keySelector;
  
  public FlowableDistinctUntilChanged(Publisher<T> paramPublisher, Function<? super T, K> paramFunction, BiPredicate<? super K, ? super K> paramBiPredicate)
  {
    super(paramPublisher);
    this.keySelector = paramFunction;
    this.comparer = paramBiPredicate;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if ((paramSubscriber instanceof ConditionalSubscriber))
    {
      paramSubscriber = (ConditionalSubscriber)paramSubscriber;
      this.source.subscribe(new DistinctUntilChangedConditionalSubscriber(paramSubscriber, this.keySelector, this.comparer));
    }
    else
    {
      this.source.subscribe(new DistinctUntilChangedSubscriber(paramSubscriber, this.keySelector, this.comparer));
    }
  }
  
  static final class DistinctUntilChangedConditionalSubscriber<T, K>
    extends BasicFuseableConditionalSubscriber<T, T>
  {
    final BiPredicate<? super K, ? super K> comparer;
    boolean hasValue;
    final Function<? super T, K> keySelector;
    K last;
    
    DistinctUntilChangedConditionalSubscriber(ConditionalSubscriber<? super T> paramConditionalSubscriber, Function<? super T, K> paramFunction, BiPredicate<? super K, ? super K> paramBiPredicate)
    {
      super();
      this.keySelector = paramFunction;
      this.comparer = paramBiPredicate;
    }
    
    public void onNext(T paramT)
    {
      if (!tryOnNext(paramT)) {
        this.s.request(1L);
      }
    }
    
    public T poll()
      throws Exception
    {
      for (;;)
      {
        Object localObject2 = this.qs.poll();
        if (localObject2 == null) {
          return null;
        }
        Object localObject1 = this.keySelector.apply(localObject2);
        if (!this.hasValue)
        {
          this.hasValue = true;
          this.last = localObject1;
          return (T)localObject2;
        }
        if (!this.comparer.test(this.last, localObject1))
        {
          this.last = localObject1;
          return (T)localObject2;
        }
        this.last = localObject1;
        if (this.sourceMode != 1) {
          this.s.request(1L);
        }
      }
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
    
    public boolean tryOnNext(T paramT)
    {
      if (this.done) {
        return false;
      }
      if (this.sourceMode != 0) {
        return this.actual.tryOnNext(paramT);
      }
      try
      {
        Object localObject = this.keySelector.apply(paramT);
        if (this.hasValue)
        {
          boolean bool = this.comparer.test(this.last, localObject);
          this.last = localObject;
          if (bool) {
            return false;
          }
        }
        else
        {
          this.hasValue = true;
          this.last = localObject;
        }
        this.actual.onNext(paramT);
        return true;
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
      return true;
    }
  }
  
  static final class DistinctUntilChangedSubscriber<T, K>
    extends BasicFuseableSubscriber<T, T>
    implements ConditionalSubscriber<T>
  {
    final BiPredicate<? super K, ? super K> comparer;
    boolean hasValue;
    final Function<? super T, K> keySelector;
    K last;
    
    DistinctUntilChangedSubscriber(Subscriber<? super T> paramSubscriber, Function<? super T, K> paramFunction, BiPredicate<? super K, ? super K> paramBiPredicate)
    {
      super();
      this.keySelector = paramFunction;
      this.comparer = paramBiPredicate;
    }
    
    public void onNext(T paramT)
    {
      if (!tryOnNext(paramT)) {
        this.s.request(1L);
      }
    }
    
    public T poll()
      throws Exception
    {
      for (;;)
      {
        Object localObject2 = this.qs.poll();
        if (localObject2 == null) {
          return null;
        }
        Object localObject1 = this.keySelector.apply(localObject2);
        if (!this.hasValue)
        {
          this.hasValue = true;
          this.last = localObject1;
          return (T)localObject2;
        }
        if (!this.comparer.test(this.last, localObject1))
        {
          this.last = localObject1;
          return (T)localObject2;
        }
        this.last = localObject1;
        if (this.sourceMode != 1) {
          this.s.request(1L);
        }
      }
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
    
    public boolean tryOnNext(T paramT)
    {
      if (this.done) {
        return false;
      }
      if (this.sourceMode != 0)
      {
        this.actual.onNext(paramT);
        return true;
      }
      try
      {
        Object localObject = this.keySelector.apply(paramT);
        if (this.hasValue)
        {
          boolean bool = this.comparer.test(this.last, localObject);
          this.last = localObject;
          if (bool) {
            return false;
          }
        }
        else
        {
          this.hasValue = true;
          this.last = localObject;
        }
        this.actual.onNext(paramT);
        return true;
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
      return true;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDistinctUntilChanged.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */