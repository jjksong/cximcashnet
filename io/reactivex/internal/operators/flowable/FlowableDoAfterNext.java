package io.reactivex.internal.operators.flowable;

import io.reactivex.annotations.Experimental;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.subscribers.BasicFuseableConditionalSubscriber;
import io.reactivex.internal.subscribers.BasicFuseableSubscriber;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

@Experimental
public final class FlowableDoAfterNext<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Consumer<? super T> onAfterNext;
  
  public FlowableDoAfterNext(Publisher<T> paramPublisher, Consumer<? super T> paramConsumer)
  {
    super(paramPublisher);
    this.onAfterNext = paramConsumer;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if ((paramSubscriber instanceof ConditionalSubscriber)) {
      this.source.subscribe(new DoAfterConditionalSubscriber((ConditionalSubscriber)paramSubscriber, this.onAfterNext));
    } else {
      this.source.subscribe(new DoAfterSubscriber(paramSubscriber, this.onAfterNext));
    }
  }
  
  static final class DoAfterConditionalSubscriber<T>
    extends BasicFuseableConditionalSubscriber<T, T>
  {
    final Consumer<? super T> onAfterNext;
    
    DoAfterConditionalSubscriber(ConditionalSubscriber<? super T> paramConditionalSubscriber, Consumer<? super T> paramConsumer)
    {
      super();
      this.onAfterNext = paramConsumer;
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
      if (this.sourceMode == 0) {
        try
        {
          this.onAfterNext.accept(paramT);
        }
        catch (Throwable paramT)
        {
          fail(paramT);
        }
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.qs.poll();
      if (localObject != null) {
        this.onAfterNext.accept(localObject);
      }
      return (T)localObject;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
    
    public boolean tryOnNext(T paramT)
    {
      boolean bool = this.actual.tryOnNext(paramT);
      try
      {
        this.onAfterNext.accept(paramT);
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
      return bool;
    }
  }
  
  static final class DoAfterSubscriber<T>
    extends BasicFuseableSubscriber<T, T>
  {
    final Consumer<? super T> onAfterNext;
    
    DoAfterSubscriber(Subscriber<? super T> paramSubscriber, Consumer<? super T> paramConsumer)
    {
      super();
      this.onAfterNext = paramConsumer;
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      this.actual.onNext(paramT);
      if (this.sourceMode == 0) {
        try
        {
          this.onAfterNext.accept(paramT);
        }
        catch (Throwable paramT)
        {
          fail(paramT);
        }
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.qs.poll();
      if (localObject != null) {
        this.onAfterNext.accept(localObject);
      }
      return (T)localObject;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDoAfterNext.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */