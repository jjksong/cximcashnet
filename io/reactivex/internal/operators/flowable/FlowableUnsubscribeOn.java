package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableUnsubscribeOn<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Scheduler scheduler;
  
  public FlowableUnsubscribeOn(Publisher<T> paramPublisher, Scheduler paramScheduler)
  {
    super(paramPublisher);
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new UnsubscribeSubscriber(paramSubscriber, this.scheduler));
  }
  
  static final class UnsubscribeSubscriber<T>
    extends AtomicBoolean
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = 1015244841293359600L;
    final Subscriber<? super T> actual;
    Subscription s;
    final Scheduler scheduler;
    
    UnsubscribeSubscriber(Subscriber<? super T> paramSubscriber, Scheduler paramScheduler)
    {
      this.actual = paramSubscriber;
      this.scheduler = paramScheduler;
    }
    
    public void cancel()
    {
      if (compareAndSet(false, true)) {
        this.scheduler.scheduleDirect(new Runnable()
        {
          public void run()
          {
            FlowableUnsubscribeOn.UnsubscribeSubscriber.this.s.cancel();
          }
        });
      }
    }
    
    public void onComplete()
    {
      if (!get()) {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (get())
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (!get()) {
        this.actual.onNext(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableUnsubscribeOn.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */