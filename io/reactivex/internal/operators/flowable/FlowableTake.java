package io.reactivex.internal.operators.flowable;

import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicBoolean;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableTake<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final long limit;
  
  public FlowableTake(Publisher<T> paramPublisher, long paramLong)
  {
    super(paramPublisher);
    this.limit = paramLong;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new TakeSubscriber(paramSubscriber, this.limit));
  }
  
  static final class TakeSubscriber<T>
    extends AtomicBoolean
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -5636543848937116287L;
    final Subscriber<? super T> actual;
    boolean done;
    final long limit;
    long remaining;
    Subscription subscription;
    
    TakeSubscriber(Subscriber<? super T> paramSubscriber, long paramLong)
    {
      this.actual = paramSubscriber;
      this.limit = paramLong;
      this.remaining = paramLong;
    }
    
    public void cancel()
    {
      this.subscription.cancel();
    }
    
    public void onComplete()
    {
      if (!this.done)
      {
        this.done = true;
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (!this.done)
      {
        this.done = true;
        this.subscription.cancel();
        this.actual.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (!this.done)
      {
        long l = this.remaining;
        this.remaining = (l - 1L);
        if (l > 0L)
        {
          int i;
          if (this.remaining == 0L) {
            i = 1;
          } else {
            i = 0;
          }
          this.actual.onNext(paramT);
          if (i != 0)
          {
            this.subscription.cancel();
            onComplete();
          }
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.subscription, paramSubscription))
      {
        this.subscription = paramSubscription;
        if (this.limit == 0L)
        {
          paramSubscription.cancel();
          this.done = true;
          EmptySubscription.complete(this.actual);
        }
        else
        {
          this.actual.onSubscribe(this);
        }
      }
    }
    
    public void request(long paramLong)
    {
      if (!SubscriptionHelper.validate(paramLong)) {
        return;
      }
      if ((!get()) && (compareAndSet(false, true)) && (paramLong >= this.limit))
      {
        this.subscription.request(Long.MAX_VALUE);
        return;
      }
      this.subscription.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableTake.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */