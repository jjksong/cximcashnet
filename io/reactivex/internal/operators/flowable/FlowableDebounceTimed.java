package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableDebounceTimed<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Scheduler scheduler;
  final long timeout;
  final TimeUnit unit;
  
  public FlowableDebounceTimed(Publisher<T> paramPublisher, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    super(paramPublisher);
    this.timeout = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new DebounceTimedSubscriber(new SerializedSubscriber(paramSubscriber), this.timeout, this.unit, this.scheduler.createWorker()));
  }
  
  static final class DebounceEmitter<T>
    extends AtomicReference<Disposable>
    implements Runnable, Disposable
  {
    private static final long serialVersionUID = 6812032969491025141L;
    final long idx;
    final AtomicBoolean once = new AtomicBoolean();
    final FlowableDebounceTimed.DebounceTimedSubscriber<T> parent;
    final T value;
    
    DebounceEmitter(T paramT, long paramLong, FlowableDebounceTimed.DebounceTimedSubscriber<T> paramDebounceTimedSubscriber)
    {
      this.value = paramT;
      this.idx = paramLong;
      this.parent = paramDebounceTimedSubscriber;
    }
    
    public void dispose()
    {
      DisposableHelper.dispose(this);
    }
    
    void emit()
    {
      if (this.once.compareAndSet(false, true)) {
        this.parent.emit(this.idx, this.value, this);
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void run()
    {
      emit();
    }
    
    public void setResource(Disposable paramDisposable)
    {
      DisposableHelper.replace(this, paramDisposable);
    }
  }
  
  static final class DebounceTimedSubscriber<T>
    extends AtomicLong
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -9102637559663639004L;
    final Subscriber<? super T> actual;
    boolean done;
    volatile long index;
    Subscription s;
    final long timeout;
    final SequentialDisposable timer = new SequentialDisposable();
    final TimeUnit unit;
    final Scheduler.Worker worker;
    
    DebounceTimedSubscriber(Subscriber<? super T> paramSubscriber, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker)
    {
      this.actual = paramSubscriber;
      this.timeout = paramLong;
      this.unit = paramTimeUnit;
      this.worker = paramWorker;
    }
    
    public void cancel()
    {
      DisposableHelper.dispose(this.timer);
      this.worker.dispose();
      this.s.cancel();
    }
    
    void emit(long paramLong, T paramT, FlowableDebounceTimed.DebounceEmitter<T> paramDebounceEmitter)
    {
      if (paramLong == this.index) {
        if (get() != 0L)
        {
          this.actual.onNext(paramT);
          BackpressureHelper.produced(this, 1L);
          paramDebounceEmitter.dispose();
        }
        else
        {
          cancel();
          this.actual.onError(new MissingBackpressureException("Could not deliver value due to lack of requests"));
        }
      }
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      Object localObject = (Disposable)this.timer.get();
      if (!DisposableHelper.isDisposed((Disposable)localObject))
      {
        localObject = (FlowableDebounceTimed.DebounceEmitter)localObject;
        if (localObject != null) {
          ((FlowableDebounceTimed.DebounceEmitter)localObject).emit();
        }
        DisposableHelper.dispose(this.timer);
        this.worker.dispose();
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      DisposableHelper.dispose(this.timer);
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      long l = this.index + 1L;
      this.index = l;
      Disposable localDisposable = (Disposable)this.timer.get();
      if (localDisposable != null) {
        localDisposable.dispose();
      }
      paramT = new FlowableDebounceTimed.DebounceEmitter(paramT, l, this);
      if (this.timer.replace(paramT)) {
        paramT.setResource(this.worker.schedule(paramT, this.timeout, this.unit));
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        BackpressureHelper.add(this, paramLong);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDebounceTimed.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */