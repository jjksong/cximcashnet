package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.functions.Action;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import java.util.concurrent.atomic.AtomicLong;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableOnBackpressureBuffer<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final int bufferSize;
  final boolean delayError;
  final Action onOverflow;
  final boolean unbounded;
  
  public FlowableOnBackpressureBuffer(Publisher<T> paramPublisher, int paramInt, boolean paramBoolean1, boolean paramBoolean2, Action paramAction)
  {
    super(paramPublisher);
    this.bufferSize = paramInt;
    this.unbounded = paramBoolean1;
    this.delayError = paramBoolean2;
    this.onOverflow = paramAction;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new BackpressureBufferSubscriber(paramSubscriber, this.bufferSize, this.unbounded, this.delayError, this.onOverflow));
  }
  
  static final class BackpressureBufferSubscriber<T>
    extends BasicIntQueueSubscription<T>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -2514538129242366402L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    final boolean delayError;
    volatile boolean done;
    Throwable error;
    final Action onOverflow;
    boolean outputFused;
    final SimplePlainQueue<T> queue;
    final AtomicLong requested = new AtomicLong();
    Subscription s;
    
    BackpressureBufferSubscriber(Subscriber<? super T> paramSubscriber, int paramInt, boolean paramBoolean1, boolean paramBoolean2, Action paramAction)
    {
      this.actual = paramSubscriber;
      this.onOverflow = paramAction;
      this.delayError = paramBoolean2;
      if (paramBoolean1) {
        paramSubscriber = new SpscLinkedArrayQueue(paramInt);
      } else {
        paramSubscriber = new SpscArrayQueue(paramInt);
      }
      this.queue = paramSubscriber;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        if (getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Subscriber<? super T> paramSubscriber)
    {
      if (this.cancelled)
      {
        this.queue.clear();
        return true;
      }
      if (paramBoolean1)
      {
        Throwable localThrowable;
        if (this.delayError)
        {
          if (paramBoolean2)
          {
            localThrowable = this.error;
            if (localThrowable != null) {
              paramSubscriber.onError(localThrowable);
            } else {
              paramSubscriber.onComplete();
            }
            return true;
          }
        }
        else
        {
          localThrowable = this.error;
          if (localThrowable != null)
          {
            this.queue.clear();
            paramSubscriber.onError(localThrowable);
            return true;
          }
          if (paramBoolean2)
          {
            paramSubscriber.onComplete();
            return true;
          }
        }
      }
      return false;
    }
    
    public void clear()
    {
      this.queue.clear();
    }
    
    void drain()
    {
      if (getAndIncrement() == 0)
      {
        SimplePlainQueue localSimplePlainQueue = this.queue;
        Subscriber localSubscriber = this.actual;
        int i = 1;
        int j;
        do
        {
          if (checkTerminated(this.done, localSimplePlainQueue.isEmpty(), localSubscriber)) {
            return;
          }
          long l2 = this.requested.get();
          for (long l1 = 0L; l1 != l2; l1 += 1L)
          {
            boolean bool2 = this.done;
            Object localObject = localSimplePlainQueue.poll();
            boolean bool1;
            if (localObject == null) {
              bool1 = true;
            } else {
              bool1 = false;
            }
            if (checkTerminated(bool2, bool1, localSubscriber)) {
              return;
            }
            if (bool1) {
              break;
            }
            localSubscriber.onNext(localObject);
          }
          if ((l1 == l2) && (checkTerminated(this.done, localSimplePlainQueue.isEmpty(), localSubscriber))) {
            return;
          }
          if ((l1 != 0L) && (l2 != Long.MAX_VALUE)) {
            this.requested.addAndGet(-l1);
          }
          j = addAndGet(-i);
          i = j;
        } while (j != 0);
      }
    }
    
    public boolean isEmpty()
    {
      return this.queue.isEmpty();
    }
    
    public void onComplete()
    {
      this.done = true;
      if (this.outputFused) {
        this.actual.onComplete();
      } else {
        drain();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.error = paramThrowable;
      this.done = true;
      if (this.outputFused) {
        this.actual.onError(paramThrowable);
      } else {
        drain();
      }
    }
    
    public void onNext(T paramT)
    {
      if (!this.queue.offer(paramT))
      {
        this.s.cancel();
        MissingBackpressureException localMissingBackpressureException = new MissingBackpressureException("Buffer is full");
        try
        {
          this.onOverflow.run();
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          localMissingBackpressureException.initCause(paramT);
        }
        onError(localMissingBackpressureException);
        return;
      }
      if (this.outputFused) {
        this.actual.onNext(null);
      } else {
        drain();
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
    
    public T poll()
      throws Exception
    {
      return (T)this.queue.poll();
    }
    
    public void request(long paramLong)
    {
      if ((!this.outputFused) && (SubscriptionHelper.validate(paramLong)))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        this.outputFused = true;
        return 2;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableOnBackpressureBuffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */