package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.fuseable.FuseToFlowable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableAllSingle<T>
  extends Single<Boolean>
  implements FuseToFlowable<Boolean>
{
  final Predicate<? super T> predicate;
  final Publisher<T> source;
  
  public FlowableAllSingle(Publisher<T> paramPublisher, Predicate<? super T> paramPredicate)
  {
    this.source = paramPublisher;
    this.predicate = paramPredicate;
  }
  
  public Flowable<Boolean> fuseToFlowable()
  {
    return RxJavaPlugins.onAssembly(new FlowableAll(this.source, this.predicate));
  }
  
  protected void subscribeActual(SingleObserver<? super Boolean> paramSingleObserver)
  {
    this.source.subscribe(new AllSubscriber(paramSingleObserver, this.predicate));
  }
  
  static final class AllSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final SingleObserver<? super Boolean> actual;
    boolean done;
    final Predicate<? super T> predicate;
    Subscription s;
    
    AllSubscriber(SingleObserver<? super Boolean> paramSingleObserver, Predicate<? super T> paramPredicate)
    {
      this.actual = paramSingleObserver;
      this.predicate = paramPredicate;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onSuccess(Boolean.valueOf(true));
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      try
      {
        boolean bool = this.predicate.test(paramT);
        if (!bool)
        {
          this.done = true;
          this.s.cancel();
          this.s = SubscriptionHelper.CANCELLED;
          this.actual.onSuccess(Boolean.valueOf(false));
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        this.s = SubscriptionHelper.CANCELLED;
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableAllSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */