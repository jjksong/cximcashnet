package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.MaybeObserver;
import io.reactivex.MaybeSource;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AtomicThrowable;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableFlatMapMaybe<T, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final boolean delayErrors;
  final Function<? super T, ? extends MaybeSource<? extends R>> mapper;
  final int maxConcurrency;
  
  public FlowableFlatMapMaybe(Publisher<T> paramPublisher, Function<? super T, ? extends MaybeSource<? extends R>> paramFunction, boolean paramBoolean, int paramInt)
  {
    super(paramPublisher);
    this.mapper = paramFunction;
    this.delayErrors = paramBoolean;
    this.maxConcurrency = paramInt;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    this.source.subscribe(new FlatMapMaybeSubscriber(paramSubscriber, this.mapper, this.delayErrors, this.maxConcurrency));
  }
  
  static final class FlatMapMaybeSubscriber<T, R>
    extends AtomicInteger
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = 8600231336733376951L;
    final AtomicInteger active;
    final Subscriber<? super R> actual;
    volatile boolean cancelled;
    final boolean delayErrors;
    final AtomicThrowable errors;
    final Function<? super T, ? extends MaybeSource<? extends R>> mapper;
    final int maxConcurrency;
    final AtomicReference<SpscLinkedArrayQueue<R>> queue;
    final AtomicLong requested;
    Subscription s;
    final CompositeDisposable set;
    
    FlatMapMaybeSubscriber(Subscriber<? super R> paramSubscriber, Function<? super T, ? extends MaybeSource<? extends R>> paramFunction, boolean paramBoolean, int paramInt)
    {
      this.actual = paramSubscriber;
      this.mapper = paramFunction;
      this.delayErrors = paramBoolean;
      this.maxConcurrency = paramInt;
      this.requested = new AtomicLong();
      this.set = new CompositeDisposable();
      this.errors = new AtomicThrowable();
      this.active = new AtomicInteger(1);
      this.queue = new AtomicReference();
    }
    
    public void cancel()
    {
      this.cancelled = true;
      this.s.cancel();
      this.set.dispose();
    }
    
    void clear()
    {
      SpscLinkedArrayQueue localSpscLinkedArrayQueue = (SpscLinkedArrayQueue)this.queue.get();
      if (localSpscLinkedArrayQueue != null) {
        localSpscLinkedArrayQueue.clear();
      }
    }
    
    void drain()
    {
      if (getAndIncrement() == 0) {
        drainLoop();
      }
    }
    
    void drainLoop()
    {
      Subscriber localSubscriber = this.actual;
      AtomicInteger localAtomicInteger = this.active;
      AtomicReference localAtomicReference = this.queue;
      int i = 1;
      int j;
      do
      {
        long l2 = this.requested.get();
        int m;
        Object localObject;
        int k;
        for (long l1 = 0L;; l1 += 1L)
        {
          m = 0;
          if (l1 == l2) {
            break;
          }
          if (this.cancelled)
          {
            clear();
            return;
          }
          if ((!this.delayErrors) && ((Throwable)this.errors.get() != null))
          {
            localObject = this.errors.terminate();
            clear();
            localSubscriber.onError((Throwable)localObject);
            return;
          }
          if (localAtomicInteger.get() == 0) {
            j = 1;
          } else {
            j = 0;
          }
          localObject = (SpscLinkedArrayQueue)localAtomicReference.get();
          if (localObject != null) {
            localObject = ((SpscLinkedArrayQueue)localObject).poll();
          } else {
            localObject = null;
          }
          if (localObject == null) {
            k = 1;
          } else {
            k = 0;
          }
          if ((j != 0) && (k != 0))
          {
            localObject = this.errors.terminate();
            if (localObject != null) {
              localSubscriber.onError((Throwable)localObject);
            } else {
              localSubscriber.onComplete();
            }
            return;
          }
          if (k != 0) {
            break;
          }
          localSubscriber.onNext(localObject);
        }
        if (l1 == l2)
        {
          if (this.cancelled)
          {
            clear();
            return;
          }
          if ((!this.delayErrors) && ((Throwable)this.errors.get() != null))
          {
            localObject = this.errors.terminate();
            clear();
            localSubscriber.onError((Throwable)localObject);
            return;
          }
          if (localAtomicInteger.get() == 0) {
            j = 1;
          } else {
            j = 0;
          }
          localObject = (SpscLinkedArrayQueue)localAtomicReference.get();
          if (localObject != null)
          {
            k = m;
            if (!((SpscLinkedArrayQueue)localObject).isEmpty()) {}
          }
          else
          {
            k = 1;
          }
          if ((j != 0) && (k != 0))
          {
            localObject = this.errors.terminate();
            if (localObject != null) {
              localSubscriber.onError((Throwable)localObject);
            } else {
              localSubscriber.onComplete();
            }
            return;
          }
        }
        if (l1 != 0L)
        {
          BackpressureHelper.produced(this.requested, l1);
          if (this.maxConcurrency != Integer.MAX_VALUE) {
            this.s.request(l1);
          }
        }
        j = addAndGet(-i);
        i = j;
      } while (j != 0);
    }
    
    SpscLinkedArrayQueue<R> getOrCreateQueue()
    {
      SpscLinkedArrayQueue localSpscLinkedArrayQueue;
      do
      {
        localSpscLinkedArrayQueue = (SpscLinkedArrayQueue)this.queue.get();
        if (localSpscLinkedArrayQueue != null) {
          return localSpscLinkedArrayQueue;
        }
        localSpscLinkedArrayQueue = new SpscLinkedArrayQueue(Flowable.bufferSize());
      } while (!this.queue.compareAndSet(null, localSpscLinkedArrayQueue));
      return localSpscLinkedArrayQueue;
    }
    
    void innerComplete(FlatMapMaybeSubscriber<T, R>.InnerObserver paramFlatMapMaybeSubscriber)
    {
      this.set.delete(paramFlatMapMaybeSubscriber);
      if (get() == 0)
      {
        int i = 1;
        if (compareAndSet(0, 1))
        {
          if (this.active.decrementAndGet() != 0) {
            i = 0;
          }
          paramFlatMapMaybeSubscriber = (SpscLinkedArrayQueue)this.queue.get();
          if ((i != 0) && ((paramFlatMapMaybeSubscriber == null) || (paramFlatMapMaybeSubscriber.isEmpty())))
          {
            paramFlatMapMaybeSubscriber = this.errors.terminate();
            if (paramFlatMapMaybeSubscriber != null) {
              this.actual.onError(paramFlatMapMaybeSubscriber);
            } else {
              this.actual.onComplete();
            }
            return;
          }
          if (decrementAndGet() == 0) {
            return;
          }
          drainLoop();
          return;
        }
      }
      this.active.decrementAndGet();
      drain();
    }
    
    void innerError(FlatMapMaybeSubscriber<T, R>.InnerObserver paramFlatMapMaybeSubscriber, Throwable paramThrowable)
    {
      this.set.delete(paramFlatMapMaybeSubscriber);
      if (this.errors.addThrowable(paramThrowable))
      {
        if (!this.delayErrors)
        {
          this.s.cancel();
          this.set.dispose();
        }
        this.active.decrementAndGet();
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    void innerSuccess(FlatMapMaybeSubscriber<T, R>.InnerObserver arg1, R paramR)
    {
      this.set.delete(???);
      if (get() == 0)
      {
        int i = 1;
        if (compareAndSet(0, 1))
        {
          if (this.active.decrementAndGet() != 0) {
            i = 0;
          }
          if (this.requested.get() != 0L)
          {
            this.actual.onNext(paramR);
            ??? = (SpscLinkedArrayQueue)this.queue.get();
            if ((i != 0) && ((??? == null) || (???.isEmpty())))
            {
              ??? = this.errors.terminate();
              if (??? != null) {
                this.actual.onError(???);
              } else {
                this.actual.onComplete();
              }
              return;
            }
            BackpressureHelper.produced(this.requested, 1L);
            if (this.maxConcurrency == Integer.MAX_VALUE) {
              break label171;
            }
            this.s.request(1L);
          }
          synchronized (getOrCreateQueue())
          {
            ???.offer(paramR);
            label171:
            if (decrementAndGet() == 0) {
              return;
            }
          }
        }
      }
      synchronized (getOrCreateQueue())
      {
        ???.offer(paramR);
        this.active.decrementAndGet();
        if (getAndIncrement() != 0) {
          return;
        }
        drainLoop();
        return;
      }
    }
    
    public void onComplete()
    {
      this.active.decrementAndGet();
      drain();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.active.decrementAndGet();
      if (this.errors.addThrowable(paramThrowable))
      {
        if (!this.delayErrors) {
          this.set.dispose();
        }
        drain();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      try
      {
        paramT = (MaybeSource)ObjectHelper.requireNonNull(this.mapper.apply(paramT), "The mapper returned a null MaybeSource");
        this.active.getAndIncrement();
        InnerObserver localInnerObserver = new InnerObserver();
        if (this.set.add(localInnerObserver)) {
          paramT.subscribe(localInnerObserver);
        }
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        this.s.cancel();
        onError(paramT);
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        int i = this.maxConcurrency;
        if (i == Integer.MAX_VALUE) {
          paramSubscription.request(Long.MAX_VALUE);
        } else {
          paramSubscription.request(i);
        }
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        drain();
      }
    }
    
    final class InnerObserver
      extends AtomicReference<Disposable>
      implements MaybeObserver<R>, Disposable
    {
      private static final long serialVersionUID = -502562646270949838L;
      
      InnerObserver() {}
      
      public void dispose()
      {
        DisposableHelper.dispose(this);
      }
      
      public boolean isDisposed()
      {
        return DisposableHelper.isDisposed((Disposable)get());
      }
      
      public void onComplete()
      {
        FlowableFlatMapMaybe.FlatMapMaybeSubscriber.this.innerComplete(this);
      }
      
      public void onError(Throwable paramThrowable)
      {
        FlowableFlatMapMaybe.FlatMapMaybeSubscriber.this.innerError(this, paramThrowable);
      }
      
      public void onSubscribe(Disposable paramDisposable)
      {
        DisposableHelper.setOnce(this, paramDisposable);
      }
      
      public void onSuccess(R paramR)
      {
        FlowableFlatMapMaybe.FlatMapMaybeSubscriber.this.innerSuccess(this, paramR);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFlatMapMaybe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */