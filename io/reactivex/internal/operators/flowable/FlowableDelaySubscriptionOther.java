package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableDelaySubscriptionOther<T, U>
  extends Flowable<T>
{
  final Publisher<? extends T> main;
  final Publisher<U> other;
  
  public FlowableDelaySubscriptionOther(Publisher<? extends T> paramPublisher, Publisher<U> paramPublisher1)
  {
    this.main = paramPublisher;
    this.other = paramPublisher1;
  }
  
  public void subscribeActual(final Subscriber<? super T> paramSubscriber)
  {
    final SubscriptionArbiter localSubscriptionArbiter = new SubscriptionArbiter();
    paramSubscriber.onSubscribe(localSubscriptionArbiter);
    paramSubscriber = new Subscriber()
    {
      boolean done;
      
      public void onComplete()
      {
        if (this.done) {
          return;
        }
        this.done = true;
        FlowableDelaySubscriptionOther.this.main.subscribe(new Subscriber()
        {
          public void onComplete()
          {
            FlowableDelaySubscriptionOther.1.this.val$child.onComplete();
          }
          
          public void onError(Throwable paramAnonymous2Throwable)
          {
            FlowableDelaySubscriptionOther.1.this.val$child.onError(paramAnonymous2Throwable);
          }
          
          public void onNext(T paramAnonymous2T)
          {
            FlowableDelaySubscriptionOther.1.this.val$child.onNext(paramAnonymous2T);
          }
          
          public void onSubscribe(Subscription paramAnonymous2Subscription)
          {
            FlowableDelaySubscriptionOther.1.this.val$serial.setSubscription(paramAnonymous2Subscription);
          }
        });
      }
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        if (this.done)
        {
          RxJavaPlugins.onError(paramAnonymousThrowable);
          return;
        }
        this.done = true;
        paramSubscriber.onError(paramAnonymousThrowable);
      }
      
      public void onNext(U paramAnonymousU)
      {
        onComplete();
      }
      
      public void onSubscribe(final Subscription paramAnonymousSubscription)
      {
        localSubscriptionArbiter.setSubscription(new Subscription()
        {
          public void cancel()
          {
            paramAnonymousSubscription.cancel();
          }
          
          public void request(long paramAnonymous2Long) {}
        });
        paramAnonymousSubscription.request(Long.MAX_VALUE);
      }
    };
    this.other.subscribe(paramSubscriber);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDelaySubscriptionOther.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */