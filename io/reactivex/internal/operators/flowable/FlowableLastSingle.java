package io.reactivex.internal.operators.flowable;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.NoSuchElementException;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableLastSingle<T>
  extends Single<T>
{
  final T defaultItem;
  final Publisher<T> source;
  
  public FlowableLastSingle(Publisher<T> paramPublisher, T paramT)
  {
    this.source = paramPublisher;
    this.defaultItem = paramT;
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new LastSubscriber(paramSingleObserver, this.defaultItem));
  }
  
  static final class LastSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final SingleObserver<? super T> actual;
    final T defaultItem;
    T item;
    Subscription s;
    
    LastSubscriber(SingleObserver<? super T> paramSingleObserver, T paramT)
    {
      this.actual = paramSingleObserver;
      this.defaultItem = paramT;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.s = SubscriptionHelper.CANCELLED;
      Object localObject = this.item;
      if (localObject != null)
      {
        this.item = null;
        this.actual.onSuccess(localObject);
      }
      else
      {
        localObject = this.defaultItem;
        if (localObject != null) {
          this.actual.onSuccess(localObject);
        } else {
          this.actual.onError(new NoSuchElementException());
        }
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.s = SubscriptionHelper.CANCELLED;
      this.item = null;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.item = paramT;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableLastSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */