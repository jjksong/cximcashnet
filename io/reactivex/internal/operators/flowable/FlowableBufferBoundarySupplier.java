package io.reactivex.internal.operators.flowable;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimplePlainQueue;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.internal.subscribers.QueueDrainSubscriber;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.QueueDrainHelper;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.subscribers.DisposableSubscriber;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.Collection;
import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableBufferBoundarySupplier<T, U extends Collection<? super T>, B>
  extends AbstractFlowableWithUpstream<T, U>
{
  final Callable<? extends Publisher<B>> boundarySupplier;
  final Callable<U> bufferSupplier;
  
  public FlowableBufferBoundarySupplier(Publisher<T> paramPublisher, Callable<? extends Publisher<B>> paramCallable, Callable<U> paramCallable1)
  {
    super(paramPublisher);
    this.boundarySupplier = paramCallable;
    this.bufferSupplier = paramCallable1;
  }
  
  protected void subscribeActual(Subscriber<? super U> paramSubscriber)
  {
    this.source.subscribe(new BufferBoundarySupplierSubscriber(new SerializedSubscriber(paramSubscriber), this.bufferSupplier, this.boundarySupplier));
  }
  
  static final class BufferBoundarySubscriber<T, U extends Collection<? super T>, B>
    extends DisposableSubscriber<B>
  {
    boolean once;
    final FlowableBufferBoundarySupplier.BufferBoundarySupplierSubscriber<T, U, B> parent;
    
    BufferBoundarySubscriber(FlowableBufferBoundarySupplier.BufferBoundarySupplierSubscriber<T, U, B> paramBufferBoundarySupplierSubscriber)
    {
      this.parent = paramBufferBoundarySupplierSubscriber;
    }
    
    public void onComplete()
    {
      if (this.once) {
        return;
      }
      this.once = true;
      this.parent.next();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.once)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.once = true;
      this.parent.onError(paramThrowable);
    }
    
    public void onNext(B paramB)
    {
      if (this.once) {
        return;
      }
      this.once = true;
      cancel();
      this.parent.next();
    }
  }
  
  static final class BufferBoundarySupplierSubscriber<T, U extends Collection<? super T>, B>
    extends QueueDrainSubscriber<T, U, U>
    implements Subscriber<T>, Subscription, Disposable
  {
    final Callable<? extends Publisher<B>> boundarySupplier;
    U buffer;
    final Callable<U> bufferSupplier;
    final AtomicReference<Disposable> other = new AtomicReference();
    Subscription s;
    
    BufferBoundarySupplierSubscriber(Subscriber<? super U> paramSubscriber, Callable<U> paramCallable, Callable<? extends Publisher<B>> paramCallable1)
    {
      super(new MpscLinkedQueue());
      this.bufferSupplier = paramCallable;
      this.boundarySupplier = paramCallable1;
    }
    
    public boolean accept(Subscriber<? super U> paramSubscriber, U paramU)
    {
      this.actual.onNext(paramU);
      return true;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.s.cancel();
        disposeOther();
        if (enter()) {
          this.queue.clear();
        }
      }
    }
    
    public void dispose()
    {
      this.s.cancel();
      disposeOther();
    }
    
    void disposeOther()
    {
      DisposableHelper.dispose(this.other);
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.other.get() == DisposableHelper.DISPOSED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    /* Error */
    void next()
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 44	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:bufferSupplier	Ljava/util/concurrent/Callable;
      //   4: invokeinterface 113 1 0
      //   9: ldc 115
      //   11: invokestatic 121	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   14: checkcast 53	java/util/Collection
      //   17: astore_1
      //   18: aload_0
      //   19: getfield 46	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:boundarySupplier	Ljava/util/concurrent/Callable;
      //   22: invokeinterface 113 1 0
      //   27: ldc 123
      //   29: invokestatic 121	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   32: checkcast 125	org/reactivestreams/Publisher
      //   35: astore_2
      //   36: new 127	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySubscriber
      //   39: dup
      //   40: aload_0
      //   41: invokespecial 130	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySubscriber:<init>	(Lio/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber;)V
      //   44: astore_3
      //   45: aload_0
      //   46: getfield 42	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:other	Ljava/util/concurrent/atomic/AtomicReference;
      //   49: invokevirtual 101	java/util/concurrent/atomic/AtomicReference:get	()Ljava/lang/Object;
      //   52: checkcast 11	io/reactivex/disposables/Disposable
      //   55: astore 4
      //   57: aload_0
      //   58: getfield 42	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:other	Ljava/util/concurrent/atomic/AtomicReference;
      //   61: aload 4
      //   63: aload_3
      //   64: invokevirtual 134	java/util/concurrent/atomic/AtomicReference:compareAndSet	(Ljava/lang/Object;Ljava/lang/Object;)Z
      //   67: ifne +4 -> 71
      //   70: return
      //   71: aload_0
      //   72: monitorenter
      //   73: aload_0
      //   74: getfield 136	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:buffer	Ljava/util/Collection;
      //   77: astore 4
      //   79: aload 4
      //   81: ifnonnull +6 -> 87
      //   84: aload_0
      //   85: monitorexit
      //   86: return
      //   87: aload_0
      //   88: aload_1
      //   89: putfield 136	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:buffer	Ljava/util/Collection;
      //   92: aload_0
      //   93: monitorexit
      //   94: aload_2
      //   95: aload_3
      //   96: invokeinterface 140 2 0
      //   101: aload_0
      //   102: aload 4
      //   104: iconst_0
      //   105: aload_0
      //   106: invokevirtual 144	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:fastPathEmitMax	(Ljava/lang/Object;ZLio/reactivex/disposables/Disposable;)V
      //   109: return
      //   110: astore_1
      //   111: aload_0
      //   112: monitorexit
      //   113: aload_1
      //   114: athrow
      //   115: astore_1
      //   116: aload_1
      //   117: invokestatic 150	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   120: aload_0
      //   121: iconst_1
      //   122: putfield 70	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:cancelled	Z
      //   125: aload_0
      //   126: getfield 72	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:s	Lorg/reactivestreams/Subscription;
      //   129: invokeinterface 74 1 0
      //   134: aload_0
      //   135: getfield 60	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:actual	Lorg/reactivestreams/Subscriber;
      //   138: aload_1
      //   139: invokeinterface 153 2 0
      //   144: return
      //   145: astore_1
      //   146: aload_1
      //   147: invokestatic 150	io/reactivex/exceptions/Exceptions:throwIfFatal	(Ljava/lang/Throwable;)V
      //   150: aload_0
      //   151: invokevirtual 154	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:cancel	()V
      //   154: aload_0
      //   155: getfield 60	io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier$BufferBoundarySupplierSubscriber:actual	Lorg/reactivestreams/Subscriber;
      //   158: aload_1
      //   159: invokeinterface 153 2 0
      //   164: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	165	0	this	BufferBoundarySupplierSubscriber
      //   17	72	1	localCollection	Collection
      //   110	4	1	localObject1	Object
      //   115	24	1	localThrowable1	Throwable
      //   145	14	1	localThrowable2	Throwable
      //   35	60	2	localPublisher	Publisher
      //   44	52	3	localBufferBoundarySubscriber	FlowableBufferBoundarySupplier.BufferBoundarySubscriber
      //   55	48	4	localObject2	Object
      // Exception table:
      //   from	to	target	type
      //   73	79	110	finally
      //   84	86	110	finally
      //   87	94	110	finally
      //   111	113	110	finally
      //   18	36	115	java/lang/Throwable
      //   0	18	145	java/lang/Throwable
    }
    
    public void onComplete()
    {
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection == null) {
          return;
        }
        this.buffer = null;
        this.queue.offer(localCollection);
        this.done = true;
        if (enter()) {
          QueueDrainHelper.drainMaxLoop(this.queue, this.actual, false, this, this);
        }
        return;
      }
      finally {}
    }
    
    public void onError(Throwable paramThrowable)
    {
      cancel();
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      try
      {
        Collection localCollection = this.buffer;
        if (localCollection == null) {
          return;
        }
        localCollection.add(paramT);
        return;
      }
      finally {}
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (!SubscriptionHelper.validate(this.s, paramSubscription)) {
        return;
      }
      this.s = paramSubscription;
      Subscriber localSubscriber = this.actual;
      try
      {
        Object localObject = (Collection)ObjectHelper.requireNonNull(this.bufferSupplier.call(), "The buffer supplied is null");
        this.buffer = ((Collection)localObject);
        try
        {
          localObject = (Publisher)ObjectHelper.requireNonNull(this.boundarySupplier.call(), "The boundary publisher supplied is null");
          FlowableBufferBoundarySupplier.BufferBoundarySubscriber localBufferBoundarySubscriber = new FlowableBufferBoundarySupplier.BufferBoundarySubscriber(this);
          this.other.set(localBufferBoundarySubscriber);
          localSubscriber.onSubscribe(this);
          if (!this.cancelled)
          {
            paramSubscription.request(Long.MAX_VALUE);
            ((Publisher)localObject).subscribe(localBufferBoundarySubscriber);
          }
          return;
        }
        catch (Throwable localThrowable1)
        {
          Exceptions.throwIfFatal(localThrowable1);
          this.cancelled = true;
          paramSubscription.cancel();
          EmptySubscription.error(localThrowable1, localSubscriber);
          return;
        }
        return;
      }
      catch (Throwable localThrowable2)
      {
        Exceptions.throwIfFatal(localThrowable2);
        this.cancelled = true;
        paramSubscription.cancel();
        EmptySubscription.error(localThrowable2, localSubscriber);
      }
    }
    
    public void request(long paramLong)
    {
      requested(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableBufferBoundarySupplier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */