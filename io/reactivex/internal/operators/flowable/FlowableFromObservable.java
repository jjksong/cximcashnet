package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableFromObservable<T>
  extends Flowable<T>
{
  private final Observable<T> upstream;
  
  public FlowableFromObservable(Observable<T> paramObservable)
  {
    this.upstream = paramObservable;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.upstream.subscribe(new SubscriberObserver(paramSubscriber));
  }
  
  static class SubscriberObserver<T>
    implements Observer<T>, Subscription
  {
    private Disposable d;
    private final Subscriber<? super T> s;
    
    SubscriberObserver(Subscriber<? super T> paramSubscriber)
    {
      this.s = paramSubscriber;
    }
    
    public void cancel()
    {
      this.d.dispose();
    }
    
    public void onComplete()
    {
      this.s.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.s.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.s.onNext(paramT);
    }
    
    public void onSubscribe(Disposable paramDisposable)
    {
      this.d = paramDisposable;
      this.s.onSubscribe(this);
    }
    
    public void request(long paramLong) {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableFromObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */