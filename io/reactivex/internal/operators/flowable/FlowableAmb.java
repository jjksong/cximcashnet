package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableAmb<T>
  extends Flowable<T>
{
  final Publisher<? extends T>[] sources;
  final Iterable<? extends Publisher<? extends T>> sourcesIterable;
  
  public FlowableAmb(Publisher<? extends T>[] paramArrayOfPublisher, Iterable<? extends Publisher<? extends T>> paramIterable)
  {
    this.sources = paramArrayOfPublisher;
    this.sourcesIterable = paramIterable;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    Object localObject2 = this.sources;
    int j;
    if (localObject2 == null)
    {
      Object localObject1 = new Publisher[8];
      try
      {
        Iterator localIterator = this.sourcesIterable.iterator();
        int i = 0;
        for (;;)
        {
          localObject2 = localObject1;
          j = i;
          if (!localIterator.hasNext()) {
            break;
          }
          Publisher localPublisher = (Publisher)localIterator.next();
          if (localPublisher == null)
          {
            localObject1 = new java/lang/NullPointerException;
            ((NullPointerException)localObject1).<init>("One of the sources is null");
            EmptySubscription.error((Throwable)localObject1, paramSubscriber);
            return;
          }
          localObject2 = localObject1;
          if (i == localObject1.length)
          {
            localObject2 = new Publisher[(i >> 2) + i];
            System.arraycopy(localObject1, 0, localObject2, 0, i);
          }
          localObject2[i] = localPublisher;
          i++;
          localObject1 = localObject2;
        }
        j = localObject2.length;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        EmptySubscription.error(localThrowable, paramSubscriber);
        return;
      }
    }
    if (j == 0)
    {
      EmptySubscription.complete(paramSubscriber);
      return;
    }
    if (j == 1)
    {
      localObject2[0].subscribe(paramSubscriber);
      return;
    }
    new AmbCoordinator(paramSubscriber, j).subscribe((Publisher[])localObject2);
  }
  
  static final class AmbCoordinator<T>
    implements Subscription
  {
    final Subscriber<? super T> actual;
    final FlowableAmb.AmbInnerSubscriber<T>[] subscribers;
    final AtomicInteger winner = new AtomicInteger();
    
    AmbCoordinator(Subscriber<? super T> paramSubscriber, int paramInt)
    {
      this.actual = paramSubscriber;
      this.subscribers = new FlowableAmb.AmbInnerSubscriber[paramInt];
    }
    
    public void cancel()
    {
      if (this.winner.get() != -1)
      {
        this.winner.lazySet(-1);
        FlowableAmb.AmbInnerSubscriber[] arrayOfAmbInnerSubscriber = this.subscribers;
        int j = arrayOfAmbInnerSubscriber.length;
        for (int i = 0; i < j; i++) {
          arrayOfAmbInnerSubscriber[i].cancel();
        }
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        int i = this.winner.get();
        if (i > 0)
        {
          this.subscribers[(i - 1)].request(paramLong);
        }
        else if (i == 0)
        {
          FlowableAmb.AmbInnerSubscriber[] arrayOfAmbInnerSubscriber = this.subscribers;
          int j = arrayOfAmbInnerSubscriber.length;
          for (i = 0; i < j; i++) {
            arrayOfAmbInnerSubscriber[i].request(paramLong);
          }
        }
      }
    }
    
    public void subscribe(Publisher<? extends T>[] paramArrayOfPublisher)
    {
      FlowableAmb.AmbInnerSubscriber[] arrayOfAmbInnerSubscriber = this.subscribers;
      int m = arrayOfAmbInnerSubscriber.length;
      int j = 0;
      int k;
      for (int i = 0; i < m; i = k)
      {
        k = i + 1;
        arrayOfAmbInnerSubscriber[i] = new FlowableAmb.AmbInnerSubscriber(this, k, this.actual);
      }
      this.winner.lazySet(0);
      this.actual.onSubscribe(this);
      for (i = j; i < m; i++)
      {
        if (this.winner.get() != 0) {
          return;
        }
        paramArrayOfPublisher[i].subscribe(arrayOfAmbInnerSubscriber[i]);
      }
    }
    
    public boolean win(int paramInt)
    {
      int j = this.winner.get();
      int i = 0;
      if ((j == 0) && (this.winner.compareAndSet(0, paramInt)))
      {
        FlowableAmb.AmbInnerSubscriber[] arrayOfAmbInnerSubscriber = this.subscribers;
        int k = arrayOfAmbInnerSubscriber.length;
        while (i < k)
        {
          j = i + 1;
          if (j != paramInt) {
            arrayOfAmbInnerSubscriber[i].cancel();
          }
          i = j;
        }
        return true;
      }
      return false;
    }
  }
  
  static final class AmbInnerSubscriber<T>
    extends AtomicReference<Subscription>
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -1185974347409665484L;
    final Subscriber<? super T> actual;
    final int index;
    final AtomicLong missedRequested = new AtomicLong();
    final FlowableAmb.AmbCoordinator<T> parent;
    boolean won;
    
    AmbInnerSubscriber(FlowableAmb.AmbCoordinator<T> paramAmbCoordinator, int paramInt, Subscriber<? super T> paramSubscriber)
    {
      this.parent = paramAmbCoordinator;
      this.index = paramInt;
      this.actual = paramSubscriber;
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this);
    }
    
    public void onComplete()
    {
      if (this.won)
      {
        this.actual.onComplete();
      }
      else if (this.parent.win(this.index))
      {
        this.won = true;
        this.actual.onComplete();
      }
      else
      {
        ((Subscription)get()).cancel();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.won)
      {
        this.actual.onError(paramThrowable);
      }
      else if (this.parent.win(this.index))
      {
        this.won = true;
        this.actual.onError(paramThrowable);
      }
      else
      {
        ((Subscription)get()).cancel();
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.won)
      {
        this.actual.onNext(paramT);
      }
      else if (this.parent.win(this.index))
      {
        this.won = true;
        this.actual.onNext(paramT);
      }
      else
      {
        ((Subscription)get()).cancel();
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      SubscriptionHelper.deferredSetOnce(this, this.missedRequested, paramSubscription);
    }
    
    public void request(long paramLong)
    {
      SubscriptionHelper.deferredRequest(this, this.missedRequested, paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableAmb.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */