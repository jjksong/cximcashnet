package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.fuseable.FuseToFlowable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.NoSuchElementException;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableSingleSingle<T>
  extends Single<T>
  implements FuseToFlowable<T>
{
  final T defaultValue;
  final Publisher<T> source;
  
  public FlowableSingleSingle(Publisher<T> paramPublisher, T paramT)
  {
    this.source = paramPublisher;
    this.defaultValue = paramT;
  }
  
  public Flowable<T> fuseToFlowable()
  {
    return RxJavaPlugins.onAssembly(new FlowableSingle(this.source, this.defaultValue));
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    this.source.subscribe(new SingleElementSubscriber(paramSingleObserver, this.defaultValue));
  }
  
  static final class SingleElementSubscriber<T>
    implements Subscriber<T>, Disposable
  {
    final SingleObserver<? super T> actual;
    final T defaultValue;
    boolean done;
    Subscription s;
    T value;
    
    SingleElementSubscriber(SingleObserver<? super T> paramSingleObserver, T paramT)
    {
      this.actual = paramSingleObserver;
      this.defaultValue = paramT;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.s = SubscriptionHelper.CANCELLED;
      Object localObject2 = this.value;
      this.value = null;
      Object localObject1 = localObject2;
      if (localObject2 == null) {
        localObject1 = this.defaultValue;
      }
      if (localObject1 != null) {
        this.actual.onSuccess(localObject1);
      } else {
        this.actual.onError(new NoSuchElementException());
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.value != null)
      {
        this.done = true;
        this.s.cancel();
        this.s = SubscriptionHelper.CANCELLED;
        this.actual.onError(new IllegalArgumentException("Sequence contains more than one element!"));
        return;
      }
      this.value = paramT;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableSingleSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */