package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableReduce<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final BiFunction<T, T, T> reducer;
  
  public FlowableReduce(Publisher<T> paramPublisher, BiFunction<T, T, T> paramBiFunction)
  {
    super(paramPublisher);
    this.reducer = paramBiFunction;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new ReduceSubscriber(paramSubscriber, this.reducer));
  }
  
  static final class ReduceSubscriber<T>
    extends DeferredScalarSubscription<T>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -4663883003264602070L;
    final BiFunction<T, T, T> reducer;
    Subscription s;
    
    ReduceSubscriber(Subscriber<? super T> paramSubscriber, BiFunction<T, T, T> paramBiFunction)
    {
      super();
      this.reducer = paramBiFunction;
    }
    
    public void cancel()
    {
      super.cancel();
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public void onComplete()
    {
      if (this.s == SubscriptionHelper.CANCELLED) {
        return;
      }
      this.s = SubscriptionHelper.CANCELLED;
      Object localObject = this.value;
      if (localObject != null) {
        complete(localObject);
      } else {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.s == SubscriptionHelper.CANCELLED)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      if (this.s == SubscriptionHelper.CANCELLED) {
        return;
      }
      Object localObject = this.value;
      if (localObject == null) {
        this.value = paramT;
      } else {
        try
        {
          this.value = ObjectHelper.requireNonNull(this.reducer.apply(localObject, paramT), "The reducer returned a null value");
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          this.s.cancel();
          onError(paramT);
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableReduce.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */