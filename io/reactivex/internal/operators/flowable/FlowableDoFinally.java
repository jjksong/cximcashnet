package io.reactivex.internal.operators.flowable;

import io.reactivex.annotations.Experimental;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

@Experimental
public final class FlowableDoFinally<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Action onFinally;
  
  public FlowableDoFinally(Publisher<T> paramPublisher, Action paramAction)
  {
    super(paramPublisher);
    this.onFinally = paramAction;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if ((paramSubscriber instanceof ConditionalSubscriber)) {
      this.source.subscribe(new DoFinallyConditionalSubscriber((ConditionalSubscriber)paramSubscriber, this.onFinally));
    } else {
      this.source.subscribe(new DoFinallySubscriber(paramSubscriber, this.onFinally));
    }
  }
  
  static final class DoFinallyConditionalSubscriber<T>
    extends BasicIntQueueSubscription<T>
    implements ConditionalSubscriber<T>
  {
    private static final long serialVersionUID = 4109457741734051389L;
    final ConditionalSubscriber<? super T> actual;
    final Action onFinally;
    QueueSubscription<T> qs;
    Subscription s;
    boolean syncFused;
    
    DoFinallyConditionalSubscriber(ConditionalSubscriber<? super T> paramConditionalSubscriber, Action paramAction)
    {
      this.actual = paramConditionalSubscriber;
      this.onFinally = paramAction;
    }
    
    public void cancel()
    {
      this.s.cancel();
      runFinally();
    }
    
    public void clear()
    {
      this.qs.clear();
    }
    
    public boolean isEmpty()
    {
      return this.qs.isEmpty();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
      runFinally();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
      runFinally();
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        if ((paramSubscription instanceof QueueSubscription)) {
          this.qs = ((QueueSubscription)paramSubscription);
        }
        this.actual.onSubscribe(this);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.qs.poll();
      if ((localObject == null) && (this.syncFused)) {
        runFinally();
      }
      return (T)localObject;
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
    
    public int requestFusion(int paramInt)
    {
      QueueSubscription localQueueSubscription = this.qs;
      if ((localQueueSubscription != null) && ((paramInt & 0x4) == 0))
      {
        paramInt = localQueueSubscription.requestFusion(paramInt);
        if (paramInt != 0)
        {
          boolean bool = true;
          if (paramInt != 1) {
            bool = false;
          }
          this.syncFused = bool;
        }
        return paramInt;
      }
      return 0;
    }
    
    void runFinally()
    {
      if (compareAndSet(0, 1)) {
        try
        {
          this.onFinally.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
    
    public boolean tryOnNext(T paramT)
    {
      return this.actual.tryOnNext(paramT);
    }
  }
  
  static final class DoFinallySubscriber<T>
    extends BasicIntQueueSubscription<T>
    implements Subscriber<T>
  {
    private static final long serialVersionUID = 4109457741734051389L;
    final Subscriber<? super T> actual;
    final Action onFinally;
    QueueSubscription<T> qs;
    Subscription s;
    boolean syncFused;
    
    DoFinallySubscriber(Subscriber<? super T> paramSubscriber, Action paramAction)
    {
      this.actual = paramSubscriber;
      this.onFinally = paramAction;
    }
    
    public void cancel()
    {
      this.s.cancel();
      runFinally();
    }
    
    public void clear()
    {
      this.qs.clear();
    }
    
    public boolean isEmpty()
    {
      return this.qs.isEmpty();
    }
    
    public void onComplete()
    {
      this.actual.onComplete();
      runFinally();
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
      runFinally();
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        if ((paramSubscription instanceof QueueSubscription)) {
          this.qs = ((QueueSubscription)paramSubscription);
        }
        this.actual.onSubscribe(this);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.qs.poll();
      if ((localObject == null) && (this.syncFused)) {
        runFinally();
      }
      return (T)localObject;
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
    
    public int requestFusion(int paramInt)
    {
      QueueSubscription localQueueSubscription = this.qs;
      if ((localQueueSubscription != null) && ((paramInt & 0x4) == 0))
      {
        paramInt = localQueueSubscription.requestFusion(paramInt);
        if (paramInt != 0)
        {
          boolean bool = true;
          if (paramInt != 1) {
            bool = false;
          }
          this.syncFused = bool;
        }
        return paramInt;
      }
      return 0;
    }
    
    void runFinally()
    {
      if (compareAndSet(0, 1)) {
        try
        {
          this.onFinally.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          RxJavaPlugins.onError(localThrowable);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDoFinally.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */