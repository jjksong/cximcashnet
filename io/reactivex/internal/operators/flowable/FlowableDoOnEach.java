package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.fuseable.ConditionalSubscriber;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.subscribers.BasicFuseableConditionalSubscriber;
import io.reactivex.internal.subscribers.BasicFuseableSubscriber;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;

public final class FlowableDoOnEach<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final Action onAfterTerminate;
  final Action onComplete;
  final Consumer<? super Throwable> onError;
  final Consumer<? super T> onNext;
  
  public FlowableDoOnEach(Publisher<T> paramPublisher, Consumer<? super T> paramConsumer, Consumer<? super Throwable> paramConsumer1, Action paramAction1, Action paramAction2)
  {
    super(paramPublisher);
    this.onNext = paramConsumer;
    this.onError = paramConsumer1;
    this.onComplete = paramAction1;
    this.onAfterTerminate = paramAction2;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if ((paramSubscriber instanceof ConditionalSubscriber)) {
      this.source.subscribe(new DoOnEachConditionalSubscriber((ConditionalSubscriber)paramSubscriber, this.onNext, this.onError, this.onComplete, this.onAfterTerminate));
    } else {
      this.source.subscribe(new DoOnEachSubscriber(paramSubscriber, this.onNext, this.onError, this.onComplete, this.onAfterTerminate));
    }
  }
  
  static final class DoOnEachConditionalSubscriber<T>
    extends BasicFuseableConditionalSubscriber<T, T>
  {
    final Action onAfterTerminate;
    final Action onComplete;
    final Consumer<? super Throwable> onError;
    final Consumer<? super T> onNext;
    
    DoOnEachConditionalSubscriber(ConditionalSubscriber<? super T> paramConditionalSubscriber, Consumer<? super T> paramConsumer, Consumer<? super Throwable> paramConsumer1, Action paramAction1, Action paramAction2)
    {
      super();
      this.onNext = paramConsumer;
      this.onError = paramConsumer1;
      this.onComplete = paramAction1;
      this.onAfterTerminate = paramAction2;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      try
      {
        this.onComplete.run();
        this.done = true;
        this.actual.onComplete();
        try
        {
          this.onAfterTerminate.run();
        }
        catch (Throwable localThrowable1)
        {
          Exceptions.throwIfFatal(localThrowable1);
          RxJavaPlugins.onError(localThrowable1);
        }
        return;
      }
      catch (Throwable localThrowable2)
      {
        fail(localThrowable2);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      int i = 1;
      this.done = true;
      try
      {
        this.onError.accept(paramThrowable);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
        i = 0;
      }
      if (i != 0) {
        this.actual.onError(paramThrowable);
      }
      try
      {
        this.onAfterTerminate.run();
      }
      catch (Throwable paramThrowable)
      {
        Exceptions.throwIfFatal(paramThrowable);
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.sourceMode != 0)
      {
        this.actual.onNext(null);
        return;
      }
      try
      {
        this.onNext.accept(paramT);
        this.actual.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.qs.poll();
      if (localObject != null) {}
      try
      {
        this.onNext.accept(localObject);
        this.onAfterTerminate.run();
      }
      finally
      {
        this.onAfterTerminate.run();
      }
      this.onComplete.run();
      this.onAfterTerminate.run();
      return ?;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
    
    public boolean tryOnNext(T paramT)
    {
      if (this.done) {
        return false;
      }
      try
      {
        this.onNext.accept(paramT);
        return this.actual.tryOnNext(paramT);
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
      return false;
    }
  }
  
  static final class DoOnEachSubscriber<T>
    extends BasicFuseableSubscriber<T, T>
  {
    final Action onAfterTerminate;
    final Action onComplete;
    final Consumer<? super Throwable> onError;
    final Consumer<? super T> onNext;
    
    DoOnEachSubscriber(Subscriber<? super T> paramSubscriber, Consumer<? super T> paramConsumer, Consumer<? super Throwable> paramConsumer1, Action paramAction1, Action paramAction2)
    {
      super();
      this.onNext = paramConsumer;
      this.onError = paramConsumer1;
      this.onComplete = paramAction1;
      this.onAfterTerminate = paramAction2;
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      try
      {
        this.onComplete.run();
        this.done = true;
        this.actual.onComplete();
        try
        {
          this.onAfterTerminate.run();
        }
        catch (Throwable localThrowable1)
        {
          Exceptions.throwIfFatal(localThrowable1);
          RxJavaPlugins.onError(localThrowable1);
        }
        return;
      }
      catch (Throwable localThrowable2)
      {
        fail(localThrowable2);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      int i = 1;
      this.done = true;
      try
      {
        this.onError.accept(paramThrowable);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(new CompositeException(new Throwable[] { paramThrowable, localThrowable }));
        i = 0;
      }
      if (i != 0) {
        this.actual.onError(paramThrowable);
      }
      try
      {
        this.onAfterTerminate.run();
      }
      catch (Throwable paramThrowable)
      {
        Exceptions.throwIfFatal(paramThrowable);
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (this.done) {
        return;
      }
      if (this.sourceMode != 0)
      {
        this.actual.onNext(null);
        return;
      }
      try
      {
        this.onNext.accept(paramT);
        this.actual.onNext(paramT);
        return;
      }
      catch (Throwable paramT)
      {
        fail(paramT);
      }
    }
    
    public T poll()
      throws Exception
    {
      Object localObject = this.qs.poll();
      if (localObject != null) {}
      try
      {
        this.onNext.accept(localObject);
        this.onAfterTerminate.run();
      }
      finally
      {
        this.onAfterTerminate.run();
      }
      this.onComplete.run();
      this.onAfterTerminate.run();
      return ?;
    }
    
    public int requestFusion(int paramInt)
    {
      return transitiveBoundaryFusion(paramInt);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDoOnEach.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */