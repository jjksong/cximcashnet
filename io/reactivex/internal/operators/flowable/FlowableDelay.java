package io.reactivex.internal.operators.flowable;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.concurrent.TimeUnit;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableDelay<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final long delay;
  final boolean delayError;
  final Scheduler scheduler;
  final TimeUnit unit;
  
  public FlowableDelay(Publisher<T> paramPublisher, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, boolean paramBoolean)
  {
    super(paramPublisher);
    this.delay = paramLong;
    this.unit = paramTimeUnit;
    this.scheduler = paramScheduler;
    this.delayError = paramBoolean;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if (!this.delayError) {
      paramSubscriber = new SerializedSubscriber(paramSubscriber);
    }
    Scheduler.Worker localWorker = this.scheduler.createWorker();
    this.source.subscribe(new DelaySubscriber(paramSubscriber, this.delay, this.unit, localWorker, this.delayError));
  }
  
  static final class DelaySubscriber<T>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super T> actual;
    final long delay;
    final boolean delayError;
    Subscription s;
    final TimeUnit unit;
    final Scheduler.Worker w;
    
    DelaySubscriber(Subscriber<? super T> paramSubscriber, long paramLong, TimeUnit paramTimeUnit, Scheduler.Worker paramWorker, boolean paramBoolean)
    {
      this.actual = paramSubscriber;
      this.delay = paramLong;
      this.unit = paramTimeUnit;
      this.w = paramWorker;
      this.delayError = paramBoolean;
    }
    
    public void cancel()
    {
      this.w.dispose();
      this.s.cancel();
    }
    
    public void onComplete()
    {
      this.w.schedule(new Runnable()
      {
        public void run()
        {
          try
          {
            FlowableDelay.DelaySubscriber.this.actual.onComplete();
            return;
          }
          finally
          {
            FlowableDelay.DelaySubscriber.this.w.dispose();
          }
        }
      }, this.delay, this.unit);
    }
    
    public void onError(final Throwable paramThrowable)
    {
      Scheduler.Worker localWorker = this.w;
      paramThrowable = new Runnable()
      {
        public void run()
        {
          try
          {
            FlowableDelay.DelaySubscriber.this.actual.onError(paramThrowable);
            return;
          }
          finally
          {
            FlowableDelay.DelaySubscriber.this.w.dispose();
          }
        }
      };
      long l;
      if (this.delayError) {
        l = this.delay;
      } else {
        l = 0L;
      }
      localWorker.schedule(paramThrowable, l, this.unit);
    }
    
    public void onNext(final T paramT)
    {
      this.w.schedule(new Runnable()
      {
        public void run()
        {
          FlowableDelay.DelaySubscriber.this.actual.onNext(paramT);
        }
      }, this.delay, this.unit);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDelay.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */