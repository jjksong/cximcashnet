package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.fuseable.HasUpstreamPublisher;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.queue.SpscArrayQueue;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowablePublish<T>
  extends ConnectableFlowable<T>
  implements HasUpstreamPublisher<T>
{
  static final long CANCELLED = Long.MIN_VALUE;
  final int bufferSize;
  final AtomicReference<PublishSubscriber<T>> current;
  final Publisher<T> onSubscribe;
  final Publisher<T> source;
  
  private FlowablePublish(Publisher<T> paramPublisher1, Publisher<T> paramPublisher2, AtomicReference<PublishSubscriber<T>> paramAtomicReference, int paramInt)
  {
    this.onSubscribe = paramPublisher1;
    this.source = paramPublisher2;
    this.current = paramAtomicReference;
    this.bufferSize = paramInt;
  }
  
  public static <T> ConnectableFlowable<T> create(Flowable<T> paramFlowable, final int paramInt)
  {
    AtomicReference localAtomicReference = new AtomicReference();
    RxJavaPlugins.onAssembly(new FlowablePublish(new Publisher()
    {
      public void subscribe(Subscriber<? super T> paramAnonymousSubscriber)
      {
        FlowablePublish.InnerSubscriber localInnerSubscriber = new FlowablePublish.InnerSubscriber(paramAnonymousSubscriber);
        paramAnonymousSubscriber.onSubscribe(localInnerSubscriber);
        do
        {
          FlowablePublish.PublishSubscriber localPublishSubscriber;
          do
          {
            localPublishSubscriber = (FlowablePublish.PublishSubscriber)this.val$curr.get();
            if (localPublishSubscriber != null)
            {
              paramAnonymousSubscriber = localPublishSubscriber;
              if (!localPublishSubscriber.isDisposed()) {
                break;
              }
            }
            paramAnonymousSubscriber = new FlowablePublish.PublishSubscriber(this.val$curr, paramInt);
          } while (!this.val$curr.compareAndSet(localPublishSubscriber, paramAnonymousSubscriber));
        } while (!paramAnonymousSubscriber.add(localInnerSubscriber));
        if (localInnerSubscriber.get() == Long.MIN_VALUE) {
          paramAnonymousSubscriber.remove(localInnerSubscriber);
        } else {
          localInnerSubscriber.parent = paramAnonymousSubscriber;
        }
        paramAnonymousSubscriber.dispatch();
      }
    }, paramFlowable, localAtomicReference, paramInt));
  }
  
  public void connect(Consumer<? super Disposable> paramConsumer)
  {
    PublishSubscriber localPublishSubscriber2;
    PublishSubscriber localPublishSubscriber1;
    do
    {
      localPublishSubscriber2 = (PublishSubscriber)this.current.get();
      if (localPublishSubscriber2 != null)
      {
        localPublishSubscriber1 = localPublishSubscriber2;
        if (!localPublishSubscriber2.isDisposed()) {
          break;
        }
      }
      localPublishSubscriber1 = new PublishSubscriber(this.current, this.bufferSize);
    } while (!this.current.compareAndSet(localPublishSubscriber2, localPublishSubscriber1));
    boolean bool = localPublishSubscriber1.shouldConnect.get();
    int i = 1;
    if ((bool) || (!localPublishSubscriber1.shouldConnect.compareAndSet(false, true))) {
      i = 0;
    }
    try
    {
      paramConsumer.accept(localPublishSubscriber1);
      if (i != 0) {
        this.source.subscribe(localPublishSubscriber1);
      }
      return;
    }
    catch (Throwable paramConsumer)
    {
      Exceptions.throwIfFatal(paramConsumer);
      throw ExceptionHelper.wrapOrThrow(paramConsumer);
    }
  }
  
  public Publisher<T> source()
  {
    return this.source;
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.onSubscribe.subscribe(paramSubscriber);
  }
  
  static final class InnerSubscriber<T>
    extends AtomicLong
    implements Subscription
  {
    private static final long serialVersionUID = -4453897557930727610L;
    final Subscriber<? super T> child;
    volatile FlowablePublish.PublishSubscriber<T> parent;
    
    InnerSubscriber(Subscriber<? super T> paramSubscriber)
    {
      this.child = paramSubscriber;
    }
    
    public void cancel()
    {
      if ((get() != Long.MIN_VALUE) && (getAndSet(Long.MIN_VALUE) != Long.MIN_VALUE))
      {
        FlowablePublish.PublishSubscriber localPublishSubscriber = this.parent;
        if (localPublishSubscriber != null)
        {
          localPublishSubscriber.remove(this);
          localPublishSubscriber.dispatch();
        }
      }
    }
    
    public long produced(long paramLong)
    {
      return BackpressureHelper.producedCancel(this, paramLong);
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.addCancel(this, paramLong);
        FlowablePublish.PublishSubscriber localPublishSubscriber = this.parent;
        if (localPublishSubscriber != null) {
          localPublishSubscriber.dispatch();
        }
      }
    }
  }
  
  static final class PublishSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>, Disposable
  {
    static final FlowablePublish.InnerSubscriber[] EMPTY = new FlowablePublish.InnerSubscriber[0];
    static final FlowablePublish.InnerSubscriber[] TERMINATED = new FlowablePublish.InnerSubscriber[0];
    private static final long serialVersionUID = -202316842419149694L;
    final int bufferSize;
    final AtomicReference<PublishSubscriber<T>> current;
    volatile SimpleQueue<T> queue;
    final AtomicReference<Subscription> s = new AtomicReference();
    final AtomicBoolean shouldConnect;
    int sourceMode;
    final AtomicReference<FlowablePublish.InnerSubscriber[]> subscribers = new AtomicReference(EMPTY);
    volatile Object terminalEvent;
    
    PublishSubscriber(AtomicReference<PublishSubscriber<T>> paramAtomicReference, int paramInt)
    {
      this.current = paramAtomicReference;
      this.shouldConnect = new AtomicBoolean();
      this.bufferSize = paramInt;
    }
    
    boolean add(FlowablePublish.InnerSubscriber<T> paramInnerSubscriber)
    {
      FlowablePublish.InnerSubscriber[] arrayOfInnerSubscriber1;
      FlowablePublish.InnerSubscriber[] arrayOfInnerSubscriber2;
      do
      {
        arrayOfInnerSubscriber1 = (FlowablePublish.InnerSubscriber[])this.subscribers.get();
        if (arrayOfInnerSubscriber1 == TERMINATED) {
          return false;
        }
        int i = arrayOfInnerSubscriber1.length;
        arrayOfInnerSubscriber2 = new FlowablePublish.InnerSubscriber[i + 1];
        System.arraycopy(arrayOfInnerSubscriber1, 0, arrayOfInnerSubscriber2, 0, i);
        arrayOfInnerSubscriber2[i] = paramInnerSubscriber;
      } while (!this.subscribers.compareAndSet(arrayOfInnerSubscriber1, arrayOfInnerSubscriber2));
      return true;
    }
    
    boolean checkTerminated(Object paramObject, boolean paramBoolean)
    {
      int j = 0;
      int i = 0;
      if (paramObject != null) {
        if (NotificationLite.isComplete(paramObject))
        {
          if (paramBoolean)
          {
            this.current.compareAndSet(this, null);
            paramObject = (FlowablePublish.InnerSubscriber[])this.subscribers.getAndSet(TERMINATED);
            j = paramObject.length;
            while (i < j)
            {
              paramObject[i].child.onComplete();
              i++;
            }
            return true;
          }
        }
        else
        {
          paramObject = NotificationLite.getError(paramObject);
          this.current.compareAndSet(this, null);
          FlowablePublish.InnerSubscriber[] arrayOfInnerSubscriber = (FlowablePublish.InnerSubscriber[])this.subscribers.getAndSet(TERMINATED);
          if (arrayOfInnerSubscriber.length != 0)
          {
            int k = arrayOfInnerSubscriber.length;
            for (i = j; i < k; i++) {
              arrayOfInnerSubscriber[i].child.onError((Throwable)paramObject);
            }
          }
          RxJavaPlugins.onError((Throwable)paramObject);
          return true;
        }
      }
      return false;
    }
    
    void dispatch()
    {
      if (getAndIncrement() != 0) {
        return;
      }
      int i = 1;
      for (;;)
      {
        Object localObject1 = this.terminalEvent;
        SimpleQueue localSimpleQueue = this.queue;
        boolean bool;
        if ((localSimpleQueue != null) && (!localSimpleQueue.isEmpty())) {
          bool = false;
        } else {
          bool = true;
        }
        if (checkTerminated(localObject1, bool)) {
          return;
        }
        if (!bool)
        {
          FlowablePublish.InnerSubscriber[] arrayOfInnerSubscriber = (FlowablePublish.InnerSubscriber[])this.subscribers.get();
          int n = arrayOfInnerSubscriber.length;
          int i1 = arrayOfInnerSubscriber.length;
          long l1 = Long.MAX_VALUE;
          int m = 0;
          int j = 0;
          long l2;
          int k;
          while (m < i1)
          {
            long l3 = arrayOfInnerSubscriber[m].get();
            if (l3 >= 0L)
            {
              l2 = Math.min(l1, l3);
              k = j;
            }
            else
            {
              k = j;
              l2 = l1;
              if (l3 == Long.MIN_VALUE)
              {
                k = j + 1;
                l2 = l1;
              }
            }
            m++;
            j = k;
            l1 = l2;
          }
          Object localObject4;
          Object localObject2;
          if (n == j)
          {
            localObject4 = this.terminalEvent;
            try
            {
              localObject1 = localSimpleQueue.poll();
            }
            catch (Throwable localThrowable1)
            {
              Exceptions.throwIfFatal(localThrowable1);
              ((Subscription)this.s.get()).cancel();
              localObject4 = NotificationLite.error(localThrowable1);
              this.terminalEvent = localObject4;
              localObject2 = null;
            }
            if (localObject2 == null) {
              bool = true;
            } else {
              bool = false;
            }
            if (checkTerminated(localObject4, bool)) {
              return;
            }
            if (this.sourceMode != 1) {
              ((Subscription)this.s.get()).request(1L);
            }
          }
          else
          {
            for (j = 0;; j++)
            {
              l2 = j;
              if (l2 >= l1) {
                break;
              }
              localObject4 = this.terminalEvent;
              try
              {
                localObject2 = localSimpleQueue.poll();
              }
              catch (Throwable localThrowable2)
              {
                Exceptions.throwIfFatal(localThrowable2);
                ((Subscription)this.s.get()).cancel();
                localObject4 = NotificationLite.error(localThrowable2);
                this.terminalEvent = localObject4;
                localObject3 = null;
              }
              if (localObject3 == null) {
                bool = true;
              } else {
                bool = false;
              }
              if (checkTerminated(localObject4, bool)) {
                return;
              }
              if (bool) {
                break;
              }
              Object localObject3 = NotificationLite.getValue(localObject3);
              m = arrayOfInnerSubscriber.length;
              for (k = 0; k < m; k++)
              {
                localObject4 = arrayOfInnerSubscriber[k];
                if (((FlowablePublish.InnerSubscriber)localObject4).get() > 0L)
                {
                  ((FlowablePublish.InnerSubscriber)localObject4).child.onNext(localObject3);
                  ((FlowablePublish.InnerSubscriber)localObject4).produced(1L);
                }
              }
            }
            if (j > 0) {
              if (this.sourceMode != 1) {
                ((Subscription)this.s.get()).request(l2);
              } else {}
            }
            if ((l1 != 0L) && (!bool)) {
              continue;
            }
          }
        }
        i = addAndGet(-i);
        if (i == 0) {
          return;
        }
      }
    }
    
    public void dispose()
    {
      Object localObject = this.subscribers.get();
      FlowablePublish.InnerSubscriber[] arrayOfInnerSubscriber = TERMINATED;
      if ((localObject != arrayOfInnerSubscriber) && ((FlowablePublish.InnerSubscriber[])this.subscribers.getAndSet(arrayOfInnerSubscriber) != TERMINATED))
      {
        this.current.compareAndSet(this, null);
        SubscriptionHelper.cancel(this.s);
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.subscribers.get() == TERMINATED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      if (this.terminalEvent == null)
      {
        this.terminalEvent = NotificationLite.complete();
        dispatch();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.terminalEvent == null)
      {
        this.terminalEvent = NotificationLite.error(paramThrowable);
        dispatch();
      }
      else
      {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if ((this.sourceMode == 0) && (!this.queue.offer(paramT)))
      {
        onError(new MissingBackpressureException("Prefetch queue is full?!"));
        return;
      }
      dispatch();
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.setOnce(this.s, paramSubscription))
      {
        if ((paramSubscription instanceof QueueSubscription))
        {
          QueueSubscription localQueueSubscription = (QueueSubscription)paramSubscription;
          int i = localQueueSubscription.requestFusion(3);
          if (i == 1)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            this.terminalEvent = NotificationLite.complete();
            dispatch();
            return;
          }
          if (i == 2)
          {
            this.sourceMode = i;
            this.queue = localQueueSubscription;
            paramSubscription.request(this.bufferSize);
            return;
          }
        }
        this.queue = new SpscArrayQueue(this.bufferSize);
        paramSubscription.request(this.bufferSize);
      }
    }
    
    void remove(FlowablePublish.InnerSubscriber<T> paramInnerSubscriber)
    {
      FlowablePublish.InnerSubscriber[] arrayOfInnerSubscriber2;
      FlowablePublish.InnerSubscriber[] arrayOfInnerSubscriber1;
      do
      {
        arrayOfInnerSubscriber2 = (FlowablePublish.InnerSubscriber[])this.subscribers.get();
        int m = arrayOfInnerSubscriber2.length;
        if (m == 0) {
          break;
        }
        int k = -1;
        int j;
        for (int i = 0;; i++)
        {
          j = k;
          if (i >= m) {
            break;
          }
          if (arrayOfInnerSubscriber2[i].equals(paramInnerSubscriber))
          {
            j = i;
            break;
          }
        }
        if (j < 0) {
          return;
        }
        if (m == 1)
        {
          arrayOfInnerSubscriber1 = EMPTY;
        }
        else
        {
          arrayOfInnerSubscriber1 = new FlowablePublish.InnerSubscriber[m - 1];
          System.arraycopy(arrayOfInnerSubscriber2, 0, arrayOfInnerSubscriber1, 0, j);
          System.arraycopy(arrayOfInnerSubscriber2, j + 1, arrayOfInnerSubscriber1, j, m - j - 1);
        }
      } while (!this.subscribers.compareAndSet(arrayOfInnerSubscriber2, arrayOfInnerSubscriber1));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowablePublish.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */