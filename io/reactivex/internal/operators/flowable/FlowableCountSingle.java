package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.fuseable.FuseToFlowable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableCountSingle<T>
  extends Single<Long>
  implements FuseToFlowable<Long>
{
  final Publisher<T> source;
  
  public FlowableCountSingle(Publisher<T> paramPublisher)
  {
    this.source = paramPublisher;
  }
  
  public Flowable<Long> fuseToFlowable()
  {
    return RxJavaPlugins.onAssembly(new FlowableCount(this.source));
  }
  
  protected void subscribeActual(SingleObserver<? super Long> paramSingleObserver)
  {
    this.source.subscribe(new CountSubscriber(paramSingleObserver));
  }
  
  static final class CountSubscriber
    implements Subscriber<Object>, Disposable
  {
    final SingleObserver<? super Long> actual;
    long count;
    Subscription s;
    
    CountSubscriber(SingleObserver<? super Long> paramSingleObserver)
    {
      this.actual = paramSingleObserver;
    }
    
    public void dispose()
    {
      this.s.cancel();
      this.s = SubscriptionHelper.CANCELLED;
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (this.s == SubscriptionHelper.CANCELLED) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onSuccess(Long.valueOf(this.count));
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.s = SubscriptionHelper.CANCELLED;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(Object paramObject)
    {
      this.count += 1L;
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
        paramSubscription.request(Long.MAX_VALUE);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableCountSingle.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */