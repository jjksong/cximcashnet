package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.subscribers.SerializedSubscriber;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableWithLatestFrom<T, U, R>
  extends AbstractFlowableWithUpstream<T, R>
{
  final BiFunction<? super T, ? super U, ? extends R> combiner;
  final Publisher<? extends U> other;
  
  public FlowableWithLatestFrom(Publisher<T> paramPublisher, BiFunction<? super T, ? super U, ? extends R> paramBiFunction, Publisher<? extends U> paramPublisher1)
  {
    super(paramPublisher);
    this.combiner = paramBiFunction;
    this.other = paramPublisher1;
  }
  
  protected void subscribeActual(Subscriber<? super R> paramSubscriber)
  {
    paramSubscriber = new SerializedSubscriber(paramSubscriber);
    final WithLatestFromSubscriber localWithLatestFromSubscriber = new WithLatestFromSubscriber(paramSubscriber, this.combiner);
    paramSubscriber.onSubscribe(localWithLatestFromSubscriber);
    this.other.subscribe(new Subscriber()
    {
      public void onComplete() {}
      
      public void onError(Throwable paramAnonymousThrowable)
      {
        localWithLatestFromSubscriber.otherError(paramAnonymousThrowable);
      }
      
      public void onNext(U paramAnonymousU)
      {
        localWithLatestFromSubscriber.lazySet(paramAnonymousU);
      }
      
      public void onSubscribe(Subscription paramAnonymousSubscription)
      {
        if (localWithLatestFromSubscriber.setOther(paramAnonymousSubscription)) {
          paramAnonymousSubscription.request(Long.MAX_VALUE);
        }
      }
    });
    this.source.subscribe(localWithLatestFromSubscriber);
  }
  
  static final class WithLatestFromSubscriber<T, U, R>
    extends AtomicReference<U>
    implements Subscriber<T>, Subscription
  {
    private static final long serialVersionUID = -312246233408980075L;
    final Subscriber<? super R> actual;
    final BiFunction<? super T, ? super U, ? extends R> combiner;
    final AtomicReference<Subscription> other = new AtomicReference();
    final AtomicLong requested = new AtomicLong();
    final AtomicReference<Subscription> s = new AtomicReference();
    
    WithLatestFromSubscriber(Subscriber<? super R> paramSubscriber, BiFunction<? super T, ? super U, ? extends R> paramBiFunction)
    {
      this.actual = paramSubscriber;
      this.combiner = paramBiFunction;
    }
    
    public void cancel()
    {
      SubscriptionHelper.cancel(this.s);
      SubscriptionHelper.cancel(this.other);
    }
    
    public void onComplete()
    {
      SubscriptionHelper.cancel(this.other);
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      SubscriptionHelper.cancel(this.other);
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      Object localObject = get();
      if (localObject != null) {
        try
        {
          paramT = ObjectHelper.requireNonNull(this.combiner.apply(paramT, localObject), "The combiner returned a null value");
          this.actual.onNext(paramT);
        }
        catch (Throwable paramT)
        {
          Exceptions.throwIfFatal(paramT);
          cancel();
          this.actual.onError(paramT);
          return;
        }
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      SubscriptionHelper.deferredSetOnce(this.s, this.requested, paramSubscription);
    }
    
    public void otherError(Throwable paramThrowable)
    {
      SubscriptionHelper.cancel(this.s);
      this.actual.onError(paramThrowable);
    }
    
    public void request(long paramLong)
    {
      SubscriptionHelper.deferredRequest(this.s, this.requested, paramLong);
    }
    
    public boolean setOther(Subscription paramSubscription)
    {
      return SubscriptionHelper.setOnce(this.other, paramSubscription);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableWithLatestFrom.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */