package io.reactivex.internal.operators.flowable;

import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BooleanSupplier;
import io.reactivex.internal.subscriptions.SubscriptionArbiter;
import java.util.concurrent.atomic.AtomicInteger;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableRepeatUntil<T>
  extends AbstractFlowableWithUpstream<T, T>
{
  final BooleanSupplier until;
  
  public FlowableRepeatUntil(Publisher<T> paramPublisher, BooleanSupplier paramBooleanSupplier)
  {
    super(paramPublisher);
    this.until = paramBooleanSupplier;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    SubscriptionArbiter localSubscriptionArbiter = new SubscriptionArbiter();
    paramSubscriber.onSubscribe(localSubscriptionArbiter);
    new RepeatSubscriber(paramSubscriber, this.until, localSubscriptionArbiter, this.source).subscribeNext();
  }
  
  static final class RepeatSubscriber<T>
    extends AtomicInteger
    implements Subscriber<T>
  {
    private static final long serialVersionUID = -7098360935104053232L;
    final Subscriber<? super T> actual;
    final SubscriptionArbiter sa;
    final Publisher<? extends T> source;
    final BooleanSupplier stop;
    
    RepeatSubscriber(Subscriber<? super T> paramSubscriber, BooleanSupplier paramBooleanSupplier, SubscriptionArbiter paramSubscriptionArbiter, Publisher<? extends T> paramPublisher)
    {
      this.actual = paramSubscriber;
      this.sa = paramSubscriptionArbiter;
      this.source = paramPublisher;
      this.stop = paramBooleanSupplier;
    }
    
    public void onComplete()
    {
      try
      {
        boolean bool = this.stop.getAsBoolean();
        if (bool) {
          this.actual.onComplete();
        } else {
          subscribeNext();
        }
        return;
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.actual.onError(localThrowable);
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(T paramT)
    {
      this.actual.onNext(paramT);
      this.sa.produced(1L);
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      this.sa.setSubscription(paramSubscription);
    }
    
    void subscribeNext()
    {
      if (getAndIncrement() == 0)
      {
        int i = 1;
        int j;
        do
        {
          this.source.subscribe(this);
          j = addAndGet(-i);
          i = j;
        } while (j != 0);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableRepeatUntil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */