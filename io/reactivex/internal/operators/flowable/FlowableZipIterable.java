package io.reactivex.internal.operators.flowable;

import io.reactivex.Flowable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.BiFunction;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Iterator;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableZipIterable<T, U, V>
  extends Flowable<V>
{
  final Iterable<U> other;
  final Publisher<? extends T> source;
  final BiFunction<? super T, ? super U, ? extends V> zipper;
  
  public FlowableZipIterable(Publisher<? extends T> paramPublisher, Iterable<U> paramIterable, BiFunction<? super T, ? super U, ? extends V> paramBiFunction)
  {
    this.source = paramPublisher;
    this.other = paramIterable;
    this.zipper = paramBiFunction;
  }
  
  public void subscribeActual(Subscriber<? super V> paramSubscriber)
  {
    try
    {
      Iterator localIterator = (Iterator)ObjectHelper.requireNonNull(this.other.iterator(), "The iterator returned by other is null");
      try
      {
        boolean bool = localIterator.hasNext();
        if (!bool)
        {
          EmptySubscription.complete(paramSubscriber);
          return;
        }
        this.source.subscribe(new ZipIterableSubscriber(paramSubscriber, localIterator, this.zipper));
        return;
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        EmptySubscription.error(localThrowable1, paramSubscriber);
        return;
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      EmptySubscription.error(localThrowable2, paramSubscriber);
    }
  }
  
  static final class ZipIterableSubscriber<T, U, V>
    implements Subscriber<T>, Subscription
  {
    final Subscriber<? super V> actual;
    boolean done;
    final Iterator<U> iterator;
    Subscription s;
    final BiFunction<? super T, ? super U, ? extends V> zipper;
    
    ZipIterableSubscriber(Subscriber<? super V> paramSubscriber, Iterator<U> paramIterator, BiFunction<? super T, ? super U, ? extends V> paramBiFunction)
    {
      this.actual = paramSubscriber;
      this.iterator = paramIterator;
      this.zipper = paramBiFunction;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    void error(Throwable paramThrowable)
    {
      Exceptions.throwIfFatal(paramThrowable);
      this.done = true;
      this.s.cancel();
      this.actual.onError(paramThrowable);
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    /* Error */
    public void onNext(T paramT)
    {
      // Byte code:
      //   0: aload_0
      //   1: getfield 53	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:done	Z
      //   4: ifeq +4 -> 8
      //   7: return
      //   8: aload_0
      //   9: getfield 34	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:iterator	Ljava/util/Iterator;
      //   12: invokeinterface 72 1 0
      //   17: ldc 74
      //   19: invokestatic 80	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   22: astore_3
      //   23: aload_0
      //   24: getfield 36	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:zipper	Lio/reactivex/functions/BiFunction;
      //   27: aload_1
      //   28: aload_3
      //   29: invokeinterface 86 3 0
      //   34: ldc 88
      //   36: invokestatic 80	io/reactivex/internal/functions/ObjectHelper:requireNonNull	(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
      //   39: astore_1
      //   40: aload_0
      //   41: getfield 32	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:actual	Lorg/reactivestreams/Subscriber;
      //   44: aload_1
      //   45: invokeinterface 90 2 0
      //   50: aload_0
      //   51: getfield 34	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:iterator	Ljava/util/Iterator;
      //   54: invokeinterface 94 1 0
      //   59: istore_2
      //   60: iload_2
      //   61: ifne +26 -> 87
      //   64: aload_0
      //   65: iconst_1
      //   66: putfield 53	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:done	Z
      //   69: aload_0
      //   70: getfield 42	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:s	Lorg/reactivestreams/Subscription;
      //   73: invokeinterface 44 1 0
      //   78: aload_0
      //   79: getfield 32	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:actual	Lorg/reactivestreams/Subscriber;
      //   82: invokeinterface 59 1 0
      //   87: return
      //   88: astore_1
      //   89: aload_0
      //   90: aload_1
      //   91: invokevirtual 96	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:error	(Ljava/lang/Throwable;)V
      //   94: return
      //   95: astore_1
      //   96: aload_0
      //   97: aload_1
      //   98: invokevirtual 96	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:error	(Ljava/lang/Throwable;)V
      //   101: return
      //   102: astore_1
      //   103: aload_0
      //   104: aload_1
      //   105: invokevirtual 96	io/reactivex/internal/operators/flowable/FlowableZipIterable$ZipIterableSubscriber:error	(Ljava/lang/Throwable;)V
      //   108: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	109	0	this	ZipIterableSubscriber
      //   0	109	1	paramT	T
      //   59	2	2	bool	boolean
      //   22	7	3	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   50	60	88	java/lang/Throwable
      //   23	40	95	java/lang/Throwable
      //   8	23	102	java/lang/Throwable
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableZipIterable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */