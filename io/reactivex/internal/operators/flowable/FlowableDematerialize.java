package io.reactivex.internal.operators.flowable;

import io.reactivex.Notification;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class FlowableDematerialize<T>
  extends AbstractFlowableWithUpstream<Notification<T>, T>
{
  public FlowableDematerialize(Publisher<Notification<T>> paramPublisher)
  {
    super(paramPublisher);
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    this.source.subscribe(new DematerializeSubscriber(paramSubscriber));
  }
  
  static final class DematerializeSubscriber<T>
    implements Subscriber<Notification<T>>, Subscription
  {
    final Subscriber<? super T> actual;
    boolean done;
    Subscription s;
    
    DematerializeSubscriber(Subscriber<? super T> paramSubscriber)
    {
      this.actual = paramSubscriber;
    }
    
    public void cancel()
    {
      this.s.cancel();
    }
    
    public void onComplete()
    {
      if (this.done) {
        return;
      }
      this.done = true;
      this.actual.onComplete();
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (this.done)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.done = true;
      this.actual.onError(paramThrowable);
    }
    
    public void onNext(Notification<T> paramNotification)
    {
      if (this.done)
      {
        if (paramNotification.isOnError()) {
          RxJavaPlugins.onError(paramNotification.getError());
        }
        return;
      }
      if (paramNotification.isOnError())
      {
        this.s.cancel();
        onError(paramNotification.getError());
      }
      else if (paramNotification.isOnComplete())
      {
        this.s.cancel();
        onComplete();
      }
      else
      {
        this.actual.onNext(paramNotification.getValue());
      }
    }
    
    public void onSubscribe(Subscription paramSubscription)
    {
      if (SubscriptionHelper.validate(this.s, paramSubscription))
      {
        this.s = paramSubscription;
        this.actual.onSubscribe(this);
      }
    }
    
    public void request(long paramLong)
    {
      this.s.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/operators/flowable/FlowableDematerialize.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */