package io.reactivex.internal.disposables;

import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public final class ListCompositeDisposable
  implements Disposable, DisposableContainer
{
  volatile boolean disposed;
  List<Disposable> resources;
  
  public ListCompositeDisposable() {}
  
  public ListCompositeDisposable(Iterable<? extends Disposable> paramIterable)
  {
    ObjectHelper.requireNonNull(paramIterable, "resources is null");
    this.resources = new LinkedList();
    Iterator localIterator = paramIterable.iterator();
    while (localIterator.hasNext())
    {
      paramIterable = (Disposable)localIterator.next();
      ObjectHelper.requireNonNull(paramIterable, "Disposable item is null");
      this.resources.add(paramIterable);
    }
  }
  
  public ListCompositeDisposable(Disposable... paramVarArgs)
  {
    ObjectHelper.requireNonNull(paramVarArgs, "resources is null");
    this.resources = new LinkedList();
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++)
    {
      Disposable localDisposable = paramVarArgs[i];
      ObjectHelper.requireNonNull(localDisposable, "Disposable item is null");
      this.resources.add(localDisposable);
    }
  }
  
  public boolean add(Disposable paramDisposable)
  {
    ObjectHelper.requireNonNull(paramDisposable, "d is null");
    if (!this.disposed) {
      try
      {
        if (!this.disposed)
        {
          List localList = this.resources;
          Object localObject = localList;
          if (localList == null)
          {
            localObject = new java/util/LinkedList;
            ((LinkedList)localObject).<init>();
            this.resources = ((List)localObject);
          }
          ((List)localObject).add(paramDisposable);
          return true;
        }
      }
      finally {}
    }
    paramDisposable.dispose();
    return false;
  }
  
  public boolean addAll(Disposable... paramVarArgs)
  {
    ObjectHelper.requireNonNull(paramVarArgs, "ds is null");
    boolean bool = this.disposed;
    int i = 0;
    if (!bool) {
      try
      {
        if (!this.disposed)
        {
          Object localObject2 = this.resources;
          Object localObject1 = localObject2;
          if (localObject2 == null)
          {
            localObject1 = new java/util/LinkedList;
            ((LinkedList)localObject1).<init>();
            this.resources = ((List)localObject1);
          }
          j = paramVarArgs.length;
          while (i < j)
          {
            localObject2 = paramVarArgs[i];
            ObjectHelper.requireNonNull(localObject2, "d is null");
            ((List)localObject1).add(localObject2);
            i++;
          }
          return true;
        }
      }
      finally {}
    }
    int j = paramVarArgs.length;
    for (i = 0; i < j; i++) {
      paramVarArgs[i].dispose();
    }
    return false;
  }
  
  public void clear()
  {
    if (this.disposed) {
      return;
    }
    try
    {
      if (this.disposed) {
        return;
      }
      List localList = this.resources;
      this.resources = null;
      dispose(localList);
      return;
    }
    finally {}
  }
  
  public boolean delete(Disposable paramDisposable)
  {
    ObjectHelper.requireNonNull(paramDisposable, "Disposable item is null");
    if (this.disposed) {
      return false;
    }
    try
    {
      if (this.disposed) {
        return false;
      }
      List localList = this.resources;
      return (localList != null) && (localList.remove(paramDisposable));
    }
    finally {}
  }
  
  public void dispose()
  {
    if (this.disposed) {
      return;
    }
    try
    {
      if (this.disposed) {
        return;
      }
      this.disposed = true;
      List localList = this.resources;
      this.resources = null;
      dispose(localList);
      return;
    }
    finally {}
  }
  
  void dispose(List<Disposable> paramList)
  {
    if (paramList == null) {
      return;
    }
    Object localObject = null;
    Iterator localIterator = paramList.iterator();
    paramList = (List<Disposable>)localObject;
    while (localIterator.hasNext())
    {
      localObject = (Disposable)localIterator.next();
      try
      {
        ((Disposable)localObject).dispose();
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        localObject = paramList;
        if (paramList == null) {
          localObject = new ArrayList();
        }
        ((List)localObject).add(localThrowable);
        paramList = (List<Disposable>)localObject;
      }
    }
    if (paramList != null)
    {
      if (paramList.size() == 1) {
        throw ExceptionHelper.wrapOrThrow((Throwable)paramList.get(0));
      }
      throw new CompositeException(paramList);
    }
  }
  
  public boolean isDisposed()
  {
    return this.disposed;
  }
  
  public boolean remove(Disposable paramDisposable)
  {
    if (delete(paramDisposable))
    {
      paramDisposable.dispose();
      return true;
    }
    return false;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/disposables/ListCompositeDisposable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */