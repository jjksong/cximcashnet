package io.reactivex.internal.disposables;

import io.reactivex.disposables.Disposable;

public abstract interface DisposableContainer
{
  public abstract boolean add(Disposable paramDisposable);
  
  public abstract boolean delete(Disposable paramDisposable);
  
  public abstract boolean remove(Disposable paramDisposable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/disposables/DisposableContainer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */