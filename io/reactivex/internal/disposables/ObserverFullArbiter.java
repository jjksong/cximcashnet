package io.reactivex.internal.disposables;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicInteger;

public final class ObserverFullArbiter<T>
  extends FullArbiterPad1
  implements Disposable
{
  final Observer<? super T> actual;
  volatile boolean cancelled;
  final SpscLinkedArrayQueue<Object> queue;
  Disposable resource;
  volatile Disposable s;
  
  public ObserverFullArbiter(Observer<? super T> paramObserver, Disposable paramDisposable, int paramInt)
  {
    this.actual = paramObserver;
    this.resource = paramDisposable;
    this.queue = new SpscLinkedArrayQueue(paramInt);
    this.s = EmptyDisposable.INSTANCE;
  }
  
  public void dispose()
  {
    if (!this.cancelled)
    {
      this.cancelled = true;
      disposeResource();
    }
  }
  
  void disposeResource()
  {
    Disposable localDisposable = this.resource;
    this.resource = null;
    if (localDisposable != null) {
      localDisposable.dispose();
    }
  }
  
  void drain()
  {
    if (this.wip.getAndIncrement() != 0) {
      return;
    }
    SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
    Observer localObserver = this.actual;
    int i = 1;
    for (;;)
    {
      Object localObject2 = localSpscLinkedArrayQueue.poll();
      if (localObject2 == null)
      {
        int j = this.wip.addAndGet(-i);
        i = j;
        if (j != 0) {}
      }
      else
      {
        Object localObject1 = localSpscLinkedArrayQueue.poll();
        if (localObject2 == this.s) {
          if (NotificationLite.isDisposable(localObject1))
          {
            localObject1 = NotificationLite.getDisposable(localObject1);
            this.s.dispose();
            if (!this.cancelled) {
              this.s = ((Disposable)localObject1);
            } else {
              ((Disposable)localObject1).dispose();
            }
          }
          else if (NotificationLite.isError(localObject1))
          {
            localSpscLinkedArrayQueue.clear();
            disposeResource();
            localObject1 = NotificationLite.getError(localObject1);
            if (!this.cancelled)
            {
              this.cancelled = true;
              localObserver.onError((Throwable)localObject1);
            }
            else
            {
              RxJavaPlugins.onError((Throwable)localObject1);
            }
          }
          else if (NotificationLite.isComplete(localObject1))
          {
            localSpscLinkedArrayQueue.clear();
            disposeResource();
            if (!this.cancelled)
            {
              this.cancelled = true;
              localObserver.onComplete();
            }
          }
          else
          {
            localObserver.onNext(NotificationLite.getValue(localObject1));
          }
        }
      }
    }
  }
  
  public boolean isDisposed()
  {
    Disposable localDisposable = this.resource;
    boolean bool;
    if (localDisposable != null) {
      bool = localDisposable.isDisposed();
    } else {
      bool = this.cancelled;
    }
    return bool;
  }
  
  public void onComplete(Disposable paramDisposable)
  {
    this.queue.offer(paramDisposable, NotificationLite.complete());
    drain();
  }
  
  public void onError(Throwable paramThrowable, Disposable paramDisposable)
  {
    if (this.cancelled)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    this.queue.offer(paramDisposable, NotificationLite.error(paramThrowable));
    drain();
  }
  
  public boolean onNext(T paramT, Disposable paramDisposable)
  {
    if (this.cancelled) {
      return false;
    }
    this.queue.offer(paramDisposable, NotificationLite.next(paramT));
    drain();
    return true;
  }
  
  public boolean setDisposable(Disposable paramDisposable)
  {
    if (this.cancelled) {
      return false;
    }
    this.queue.offer(this.s, NotificationLite.disposable(paramDisposable));
    drain();
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/disposables/ObserverFullArbiter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */