package io.reactivex.internal.schedulers;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.processors.FlowableProcessor;
import io.reactivex.processors.UnicastProcessor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Experimental
public class SchedulerWhen
  extends Scheduler
  implements Disposable
{
  static final Disposable DISPOSED = Disposables.disposed();
  static final Disposable SUBSCRIBED = new Disposable()
  {
    public void dispose() {}
    
    public boolean isDisposed()
    {
      return false;
    }
  };
  private final Scheduler actualScheduler;
  private Disposable disposable;
  private final FlowableProcessor<Flowable<Completable>> workerProcessor;
  
  public SchedulerWhen(Function<Flowable<Flowable<Completable>>, Completable> paramFunction, Scheduler paramScheduler)
  {
    this.actualScheduler = paramScheduler;
    this.workerProcessor = UnicastProcessor.create().toSerialized();
    try
    {
      this.disposable = ((Completable)paramFunction.apply(this.workerProcessor)).subscribe();
    }
    catch (Throwable paramFunction)
    {
      Exceptions.propagate(paramFunction);
    }
  }
  
  public Scheduler.Worker createWorker()
  {
    final Object localObject = this.actualScheduler.createWorker();
    final FlowableProcessor localFlowableProcessor = UnicastProcessor.create().toSerialized();
    Flowable localFlowable = localFlowableProcessor.map(new Function()
    {
      public Completable apply(final SchedulerWhen.ScheduledAction paramAnonymousScheduledAction)
      {
        new Completable()
        {
          protected void subscribeActual(CompletableObserver paramAnonymous2CompletableObserver)
          {
            paramAnonymous2CompletableObserver.onSubscribe(paramAnonymousScheduledAction);
            paramAnonymousScheduledAction.call(SchedulerWhen.1.this.val$actualWorker, paramAnonymous2CompletableObserver);
          }
        };
      }
    });
    localObject = new Scheduler.Worker()
    {
      private final AtomicBoolean unsubscribed = new AtomicBoolean();
      
      public void dispose()
      {
        if (this.unsubscribed.compareAndSet(false, true))
        {
          localObject.dispose();
          localFlowableProcessor.onComplete();
        }
      }
      
      public boolean isDisposed()
      {
        return this.unsubscribed.get();
      }
      
      public Disposable schedule(Runnable paramAnonymousRunnable)
      {
        paramAnonymousRunnable = new SchedulerWhen.ImmediateAction(paramAnonymousRunnable);
        localFlowableProcessor.onNext(paramAnonymousRunnable);
        return paramAnonymousRunnable;
      }
      
      public Disposable schedule(Runnable paramAnonymousRunnable, long paramAnonymousLong, TimeUnit paramAnonymousTimeUnit)
      {
        paramAnonymousRunnable = new SchedulerWhen.DelayedAction(paramAnonymousRunnable, paramAnonymousLong, paramAnonymousTimeUnit);
        localFlowableProcessor.onNext(paramAnonymousRunnable);
        return paramAnonymousRunnable;
      }
    };
    this.workerProcessor.onNext(localFlowable);
    return (Scheduler.Worker)localObject;
  }
  
  public void dispose()
  {
    this.disposable.dispose();
  }
  
  public boolean isDisposed()
  {
    return this.disposable.isDisposed();
  }
  
  static class DelayedAction
    extends SchedulerWhen.ScheduledAction
  {
    private final Runnable action;
    private final long delayTime;
    private final TimeUnit unit;
    
    DelayedAction(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit)
    {
      this.action = paramRunnable;
      this.delayTime = paramLong;
      this.unit = paramTimeUnit;
    }
    
    protected Disposable callActual(Scheduler.Worker paramWorker, CompletableObserver paramCompletableObserver)
    {
      return paramWorker.schedule(new SchedulerWhen.OnCompletedAction(this.action, paramCompletableObserver), this.delayTime, this.unit);
    }
  }
  
  static class ImmediateAction
    extends SchedulerWhen.ScheduledAction
  {
    private final Runnable action;
    
    ImmediateAction(Runnable paramRunnable)
    {
      this.action = paramRunnable;
    }
    
    protected Disposable callActual(Scheduler.Worker paramWorker, CompletableObserver paramCompletableObserver)
    {
      return paramWorker.schedule(new SchedulerWhen.OnCompletedAction(this.action, paramCompletableObserver));
    }
  }
  
  static class OnCompletedAction
    implements Runnable
  {
    private Runnable action;
    private CompletableObserver actionCompletable;
    
    OnCompletedAction(Runnable paramRunnable, CompletableObserver paramCompletableObserver)
    {
      this.action = paramRunnable;
      this.actionCompletable = paramCompletableObserver;
    }
    
    public void run()
    {
      try
      {
        this.action.run();
        return;
      }
      finally
      {
        this.actionCompletable.onComplete();
      }
    }
  }
  
  static abstract class ScheduledAction
    extends AtomicReference<Disposable>
    implements Disposable
  {
    ScheduledAction()
    {
      super();
    }
    
    void call(Scheduler.Worker paramWorker, CompletableObserver paramCompletableObserver)
    {
      Disposable localDisposable = (Disposable)get();
      if (localDisposable == SchedulerWhen.DISPOSED) {
        return;
      }
      if (localDisposable != SchedulerWhen.SUBSCRIBED) {
        return;
      }
      paramWorker = callActual(paramWorker, paramCompletableObserver);
      if (!compareAndSet(SchedulerWhen.SUBSCRIBED, paramWorker)) {
        paramWorker.dispose();
      }
    }
    
    protected abstract Disposable callActual(Scheduler.Worker paramWorker, CompletableObserver paramCompletableObserver);
    
    public void dispose()
    {
      Disposable localDisposable1 = SchedulerWhen.DISPOSED;
      Disposable localDisposable2;
      do
      {
        localDisposable2 = (Disposable)get();
        if (localDisposable2 == SchedulerWhen.DISPOSED) {
          return;
        }
      } while (!compareAndSet(localDisposable2, localDisposable1));
      if (localDisposable2 != SchedulerWhen.SUBSCRIBED) {
        localDisposable2.dispose();
      }
    }
    
    public boolean isDisposed()
    {
      return ((Disposable)get()).isDisposed();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/schedulers/SchedulerWhen.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */