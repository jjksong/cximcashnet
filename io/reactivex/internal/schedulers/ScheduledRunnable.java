package io.reactivex.internal.schedulers;

import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableContainer;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReferenceArray;

public final class ScheduledRunnable
  extends AtomicReferenceArray<Object>
  implements Runnable, Callable<Object>, Disposable
{
  static final Object DISPOSED = new Object();
  static final Object DONE = new Object();
  static final int FUTURE_INDEX = 1;
  static final int PARENT_INDEX = 0;
  private static final long serialVersionUID = -6120223772001106981L;
  final Runnable actual;
  
  public ScheduledRunnable(Runnable paramRunnable, DisposableContainer paramDisposableContainer)
  {
    super(2);
    this.actual = paramRunnable;
    lazySet(0, paramDisposableContainer);
  }
  
  public Object call()
  {
    run();
    return null;
  }
  
  public void dispose()
  {
    Object localObject2;
    Object localObject1;
    do
    {
      localObject2 = get(1);
      if (localObject2 == DONE) {
        break;
      }
      localObject1 = DISPOSED;
      if (localObject2 == localObject1) {
        break;
      }
    } while (!compareAndSet(1, localObject2, localObject1));
    if (localObject2 != null) {
      ((Future)localObject2).cancel(true);
    }
    do
    {
      localObject2 = get(0);
      if (localObject2 == DONE) {
        break;
      }
      localObject1 = DISPOSED;
      if ((localObject2 == localObject1) || (localObject2 == null)) {
        break;
      }
    } while (!compareAndSet(0, localObject2, localObject1));
    ((DisposableContainer)localObject2).delete(this);
    return;
  }
  
  public boolean isDisposed()
  {
    boolean bool2 = true;
    Object localObject = get(1);
    boolean bool1 = bool2;
    if (localObject != DISPOSED) {
      if (localObject == DONE) {
        bool1 = bool2;
      } else {
        bool1 = false;
      }
    }
    return bool1;
  }
  
  /* Error */
  public void run()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 43	io/reactivex/internal/schedulers/ScheduledRunnable:actual	Ljava/lang/Runnable;
    //   4: invokeinterface 78 1 0
    //   9: goto +12 -> 21
    //   12: astore_1
    //   13: goto +74 -> 87
    //   16: astore_1
    //   17: aload_1
    //   18: invokestatic 84	io/reactivex/plugins/RxJavaPlugins:onError	(Ljava/lang/Throwable;)V
    //   21: aload_0
    //   22: iconst_0
    //   23: invokevirtual 57	io/reactivex/internal/schedulers/ScheduledRunnable:get	(I)Ljava/lang/Object;
    //   26: astore_1
    //   27: aload_1
    //   28: getstatic 34	io/reactivex/internal/schedulers/ScheduledRunnable:DISPOSED	Ljava/lang/Object;
    //   31: if_acmpeq +30 -> 61
    //   34: aload_1
    //   35: ifnull +26 -> 61
    //   38: aload_0
    //   39: iconst_0
    //   40: aload_1
    //   41: getstatic 36	io/reactivex/internal/schedulers/ScheduledRunnable:DONE	Ljava/lang/Object;
    //   44: invokevirtual 61	io/reactivex/internal/schedulers/ScheduledRunnable:compareAndSet	(ILjava/lang/Object;Ljava/lang/Object;)Z
    //   47: ifeq +14 -> 61
    //   50: aload_1
    //   51: checkcast 69	io/reactivex/internal/disposables/DisposableContainer
    //   54: aload_0
    //   55: invokeinterface 73 2 0
    //   60: pop
    //   61: aload_0
    //   62: iconst_1
    //   63: invokevirtual 57	io/reactivex/internal/schedulers/ScheduledRunnable:get	(I)Ljava/lang/Object;
    //   66: astore_1
    //   67: aload_1
    //   68: getstatic 34	io/reactivex/internal/schedulers/ScheduledRunnable:DISPOSED	Ljava/lang/Object;
    //   71: if_acmpeq +15 -> 86
    //   74: aload_0
    //   75: iconst_1
    //   76: aload_1
    //   77: getstatic 36	io/reactivex/internal/schedulers/ScheduledRunnable:DONE	Ljava/lang/Object;
    //   80: invokevirtual 61	io/reactivex/internal/schedulers/ScheduledRunnable:compareAndSet	(ILjava/lang/Object;Ljava/lang/Object;)Z
    //   83: ifeq -22 -> 61
    //   86: return
    //   87: aload_0
    //   88: iconst_0
    //   89: invokevirtual 57	io/reactivex/internal/schedulers/ScheduledRunnable:get	(I)Ljava/lang/Object;
    //   92: astore_2
    //   93: aload_2
    //   94: getstatic 34	io/reactivex/internal/schedulers/ScheduledRunnable:DISPOSED	Ljava/lang/Object;
    //   97: if_acmpeq +30 -> 127
    //   100: aload_2
    //   101: ifnull +26 -> 127
    //   104: aload_0
    //   105: iconst_0
    //   106: aload_2
    //   107: getstatic 36	io/reactivex/internal/schedulers/ScheduledRunnable:DONE	Ljava/lang/Object;
    //   110: invokevirtual 61	io/reactivex/internal/schedulers/ScheduledRunnable:compareAndSet	(ILjava/lang/Object;Ljava/lang/Object;)Z
    //   113: ifeq +14 -> 127
    //   116: aload_2
    //   117: checkcast 69	io/reactivex/internal/disposables/DisposableContainer
    //   120: aload_0
    //   121: invokeinterface 73 2 0
    //   126: pop
    //   127: aload_0
    //   128: iconst_1
    //   129: invokevirtual 57	io/reactivex/internal/schedulers/ScheduledRunnable:get	(I)Ljava/lang/Object;
    //   132: astore_2
    //   133: aload_2
    //   134: getstatic 34	io/reactivex/internal/schedulers/ScheduledRunnable:DISPOSED	Ljava/lang/Object;
    //   137: if_acmpeq +18 -> 155
    //   140: aload_0
    //   141: iconst_1
    //   142: aload_2
    //   143: getstatic 36	io/reactivex/internal/schedulers/ScheduledRunnable:DONE	Ljava/lang/Object;
    //   146: invokevirtual 61	io/reactivex/internal/schedulers/ScheduledRunnable:compareAndSet	(ILjava/lang/Object;Ljava/lang/Object;)Z
    //   149: ifne +6 -> 155
    //   152: goto -25 -> 127
    //   155: aload_1
    //   156: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	157	0	this	ScheduledRunnable
    //   12	1	1	localObject1	Object
    //   16	2	1	localThrowable	Throwable
    //   26	130	1	localObject2	Object
    //   92	51	2	localObject3	Object
    // Exception table:
    //   from	to	target	type
    //   0	9	12	finally
    //   17	21	12	finally
    //   0	9	16	java/lang/Throwable
  }
  
  public void setFuture(Future<?> paramFuture)
  {
    Object localObject;
    do
    {
      localObject = get(1);
      if (localObject == DONE) {
        return;
      }
      if (localObject == DISPOSED)
      {
        paramFuture.cancel(true);
        return;
      }
    } while (!compareAndSet(1, localObject, paramFuture));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/schedulers/ScheduledRunnable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */