package io.reactivex.internal.schedulers;

import io.reactivex.Scheduler;
import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.queue.MpscLinkedQueue;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ExecutorScheduler
  extends Scheduler
{
  static final Scheduler HELPER = ;
  final Executor executor;
  
  public ExecutorScheduler(Executor paramExecutor)
  {
    this.executor = paramExecutor;
  }
  
  public Scheduler.Worker createWorker()
  {
    return new ExecutorWorker(this.executor);
  }
  
  public Disposable scheduleDirect(Runnable paramRunnable)
  {
    Runnable localRunnable = RxJavaPlugins.onSchedule(paramRunnable);
    try
    {
      if ((this.executor instanceof ExecutorService)) {
        return Disposables.fromFuture(((ExecutorService)this.executor).submit(localRunnable));
      }
      paramRunnable = new io/reactivex/internal/schedulers/ExecutorScheduler$ExecutorWorker$BooleanRunnable;
      paramRunnable.<init>(localRunnable);
      this.executor.execute(paramRunnable);
      return paramRunnable;
    }
    catch (RejectedExecutionException paramRunnable)
    {
      RxJavaPlugins.onError(paramRunnable);
    }
    return EmptyDisposable.INSTANCE;
  }
  
  public Disposable scheduleDirect(final Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit)
  {
    paramRunnable = RxJavaPlugins.onSchedule(paramRunnable);
    Executor localExecutor = this.executor;
    if ((localExecutor instanceof ScheduledExecutorService)) {
      try
      {
        paramRunnable = Disposables.fromFuture(((ScheduledExecutorService)localExecutor).schedule(paramRunnable, paramLong, paramTimeUnit));
        return paramRunnable;
      }
      catch (RejectedExecutionException paramRunnable)
      {
        RxJavaPlugins.onError(paramRunnable);
        return EmptyDisposable.INSTANCE;
      }
    }
    paramRunnable = new DelayedRunnable(paramRunnable);
    paramTimeUnit = HELPER.scheduleDirect(new Runnable()
    {
      public void run()
      {
        paramRunnable.direct.replace(ExecutorScheduler.this.scheduleDirect(paramRunnable));
      }
    }, paramLong, paramTimeUnit);
    paramRunnable.timed.replace(paramTimeUnit);
    return paramRunnable;
  }
  
  public Disposable schedulePeriodicallyDirect(Runnable paramRunnable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
  {
    if ((this.executor instanceof ScheduledExecutorService))
    {
      paramRunnable = RxJavaPlugins.onSchedule(paramRunnable);
      try
      {
        paramRunnable = Disposables.fromFuture(((ScheduledExecutorService)this.executor).scheduleAtFixedRate(paramRunnable, paramLong1, paramLong2, paramTimeUnit));
        return paramRunnable;
      }
      catch (RejectedExecutionException paramRunnable)
      {
        RxJavaPlugins.onError(paramRunnable);
        return EmptyDisposable.INSTANCE;
      }
    }
    return super.schedulePeriodicallyDirect(paramRunnable, paramLong1, paramLong2, paramTimeUnit);
  }
  
  static final class DelayedRunnable
    extends AtomicReference<Runnable>
    implements Runnable, Disposable
  {
    private static final long serialVersionUID = -4101336210206799084L;
    final SequentialDisposable direct = new SequentialDisposable();
    final SequentialDisposable timed = new SequentialDisposable();
    
    DelayedRunnable(Runnable paramRunnable)
    {
      super();
    }
    
    public void dispose()
    {
      if (getAndSet(null) != null)
      {
        this.timed.dispose();
        this.direct.dispose();
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    /* Error */
    public void run()
    {
      // Byte code:
      //   0: aload_0
      //   1: invokevirtual 47	io/reactivex/internal/schedulers/ExecutorScheduler$DelayedRunnable:get	()Ljava/lang/Object;
      //   4: checkcast 7	java/lang/Runnable
      //   7: astore_1
      //   8: aload_1
      //   9: ifnull +65 -> 74
      //   12: aload_1
      //   13: invokeinterface 50 1 0
      //   18: aload_0
      //   19: aconst_null
      //   20: invokevirtual 53	io/reactivex/internal/schedulers/ExecutorScheduler$DelayedRunnable:lazySet	(Ljava/lang/Object;)V
      //   23: aload_0
      //   24: getfield 31	io/reactivex/internal/schedulers/ExecutorScheduler$DelayedRunnable:timed	Lio/reactivex/internal/disposables/SequentialDisposable;
      //   27: getstatic 59	io/reactivex/internal/disposables/DisposableHelper:DISPOSED	Lio/reactivex/internal/disposables/DisposableHelper;
      //   30: invokevirtual 60	io/reactivex/internal/disposables/SequentialDisposable:lazySet	(Ljava/lang/Object;)V
      //   33: aload_0
      //   34: getfield 33	io/reactivex/internal/schedulers/ExecutorScheduler$DelayedRunnable:direct	Lio/reactivex/internal/disposables/SequentialDisposable;
      //   37: getstatic 59	io/reactivex/internal/disposables/DisposableHelper:DISPOSED	Lio/reactivex/internal/disposables/DisposableHelper;
      //   40: invokevirtual 60	io/reactivex/internal/disposables/SequentialDisposable:lazySet	(Ljava/lang/Object;)V
      //   43: goto +31 -> 74
      //   46: astore_1
      //   47: aload_0
      //   48: aconst_null
      //   49: invokevirtual 53	io/reactivex/internal/schedulers/ExecutorScheduler$DelayedRunnable:lazySet	(Ljava/lang/Object;)V
      //   52: aload_0
      //   53: getfield 31	io/reactivex/internal/schedulers/ExecutorScheduler$DelayedRunnable:timed	Lio/reactivex/internal/disposables/SequentialDisposable;
      //   56: getstatic 59	io/reactivex/internal/disposables/DisposableHelper:DISPOSED	Lio/reactivex/internal/disposables/DisposableHelper;
      //   59: invokevirtual 60	io/reactivex/internal/disposables/SequentialDisposable:lazySet	(Ljava/lang/Object;)V
      //   62: aload_0
      //   63: getfield 33	io/reactivex/internal/schedulers/ExecutorScheduler$DelayedRunnable:direct	Lio/reactivex/internal/disposables/SequentialDisposable;
      //   66: getstatic 59	io/reactivex/internal/disposables/DisposableHelper:DISPOSED	Lio/reactivex/internal/disposables/DisposableHelper;
      //   69: invokevirtual 60	io/reactivex/internal/disposables/SequentialDisposable:lazySet	(Ljava/lang/Object;)V
      //   72: aload_1
      //   73: athrow
      //   74: return
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	75	0	this	DelayedRunnable
      //   7	6	1	localRunnable	Runnable
      //   46	27	1	localObject	Object
      // Exception table:
      //   from	to	target	type
      //   12	18	46	finally
    }
  }
  
  public static final class ExecutorWorker
    extends Scheduler.Worker
    implements Runnable
  {
    volatile boolean disposed;
    final Executor executor;
    final MpscLinkedQueue<Runnable> queue;
    final CompositeDisposable tasks = new CompositeDisposable();
    final AtomicInteger wip = new AtomicInteger();
    
    public ExecutorWorker(Executor paramExecutor)
    {
      this.executor = paramExecutor;
      this.queue = new MpscLinkedQueue();
    }
    
    public void dispose()
    {
      if (!this.disposed)
      {
        this.disposed = true;
        this.tasks.dispose();
        if (this.wip.getAndIncrement() == 0) {
          this.queue.clear();
        }
      }
    }
    
    public boolean isDisposed()
    {
      return this.disposed;
    }
    
    public void run()
    {
      MpscLinkedQueue localMpscLinkedQueue = this.queue;
      int i = 1;
      if (this.disposed)
      {
        localMpscLinkedQueue.clear();
        return;
      }
      do
      {
        Runnable localRunnable = (Runnable)localMpscLinkedQueue.poll();
        if (localRunnable == null)
        {
          if (this.disposed)
          {
            localMpscLinkedQueue.clear();
            return;
          }
          int j = this.wip.addAndGet(-i);
          i = j;
          if (j != 0) {
            break;
          }
          return;
        }
        localRunnable.run();
      } while (!this.disposed);
      localMpscLinkedQueue.clear();
    }
    
    public Disposable schedule(Runnable paramRunnable)
    {
      if (this.disposed) {
        return EmptyDisposable.INSTANCE;
      }
      paramRunnable = new BooleanRunnable(RxJavaPlugins.onSchedule(paramRunnable));
      this.queue.offer(paramRunnable);
      if (this.wip.getAndIncrement() == 0) {
        try
        {
          this.executor.execute(this);
        }
        catch (RejectedExecutionException paramRunnable)
        {
          this.disposed = true;
          this.queue.clear();
          RxJavaPlugins.onError(paramRunnable);
          return EmptyDisposable.INSTANCE;
        }
      }
      return paramRunnable;
    }
    
    public Disposable schedule(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit)
    {
      if (paramLong <= 0L) {
        return schedule(paramRunnable);
      }
      if (this.disposed) {
        return EmptyDisposable.INSTANCE;
      }
      SequentialDisposable localSequentialDisposable2 = new SequentialDisposable();
      final SequentialDisposable localSequentialDisposable1 = new SequentialDisposable(localSequentialDisposable2);
      paramRunnable = new ScheduledRunnable(new Runnable()
      {
        public void run()
        {
          localSequentialDisposable1.replace(ExecutorScheduler.ExecutorWorker.this.schedule(this.val$decoratedRun));
        }
      }, this.tasks);
      this.tasks.add(paramRunnable);
      Executor localExecutor = this.executor;
      if ((localExecutor instanceof ScheduledExecutorService)) {
        try
        {
          paramRunnable.setFuture(((ScheduledExecutorService)localExecutor).schedule(paramRunnable, paramLong, paramTimeUnit));
        }
        catch (RejectedExecutionException paramRunnable)
        {
          this.disposed = true;
          RxJavaPlugins.onError(paramRunnable);
          return EmptyDisposable.INSTANCE;
        }
      } else {
        paramRunnable.setFuture(new DisposeOnCancel(ExecutorScheduler.HELPER.scheduleDirect(paramRunnable, paramLong, paramTimeUnit)));
      }
      localSequentialDisposable2.replace(paramRunnable);
      return localSequentialDisposable1;
    }
    
    static final class BooleanRunnable
      extends AtomicBoolean
      implements Runnable, Disposable
    {
      private static final long serialVersionUID = -2421395018820541164L;
      final Runnable actual;
      
      BooleanRunnable(Runnable paramRunnable)
      {
        this.actual = paramRunnable;
      }
      
      public void dispose()
      {
        lazySet(true);
      }
      
      public boolean isDisposed()
      {
        return get();
      }
      
      public void run()
      {
        if (get()) {
          return;
        }
        try
        {
          this.actual.run();
          return;
        }
        finally
        {
          lazySet(true);
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/schedulers/ExecutorScheduler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */