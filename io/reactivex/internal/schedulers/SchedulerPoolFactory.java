package io.reactivex.internal.schedulers;

import io.reactivex.plugins.RxJavaPlugins;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;

public final class SchedulerPoolFactory
{
  static final Map<ScheduledThreadPoolExecutor, Object> POOLS;
  public static final boolean PURGE_ENABLED;
  static final String PURGE_ENABLED_KEY = "rx2.purge-enabled";
  public static final int PURGE_PERIOD_SECONDS;
  static final String PURGE_PERIOD_SECONDS_KEY = "rx2.purge-period-seconds";
  static final AtomicReference<ScheduledExecutorService> PURGE_THREAD = new AtomicReference();
  
  static
  {
    POOLS = new ConcurrentHashMap();
    Properties localProperties = System.getProperties();
    boolean bool1 = localProperties.containsKey("rx2.purge-enabled");
    int j = 1;
    int i;
    if (bool1)
    {
      boolean bool2 = Boolean.getBoolean("rx2.purge-enabled");
      bool1 = bool2;
      i = j;
      if (bool2)
      {
        bool1 = bool2;
        i = j;
        if (localProperties.containsKey("rx2.purge-period-seconds"))
        {
          i = Integer.getInteger("rx2.purge-period-seconds", 1).intValue();
          bool1 = bool2;
        }
      }
    }
    else
    {
      bool1 = true;
      i = j;
    }
    PURGE_ENABLED = bool1;
    PURGE_PERIOD_SECONDS = i;
    start();
  }
  
  private SchedulerPoolFactory()
  {
    throw new IllegalStateException("No instances!");
  }
  
  public static ScheduledExecutorService create(ThreadFactory paramThreadFactory)
  {
    paramThreadFactory = Executors.newScheduledThreadPool(1, paramThreadFactory);
    if ((paramThreadFactory instanceof ScheduledThreadPoolExecutor))
    {
      ScheduledThreadPoolExecutor localScheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor)paramThreadFactory;
      POOLS.put(localScheduledThreadPoolExecutor, paramThreadFactory);
    }
    return paramThreadFactory;
  }
  
  public static void shutdown()
  {
    ((ScheduledExecutorService)PURGE_THREAD.get()).shutdownNow();
    POOLS.clear();
  }
  
  public static void start()
  {
    for (;;)
    {
      Object localObject = (ScheduledExecutorService)PURGE_THREAD.get();
      if ((localObject != null) && (!((ScheduledExecutorService)localObject).isShutdown())) {
        return;
      }
      ScheduledExecutorService localScheduledExecutorService = Executors.newScheduledThreadPool(1, new RxThreadFactory("RxSchedulerPurge"));
      if (PURGE_THREAD.compareAndSet(localObject, localScheduledExecutorService))
      {
        localObject = new Runnable()
        {
          public void run()
          {
            try
            {
              Object localObject = new java/util/ArrayList;
              ((ArrayList)localObject).<init>(SchedulerPoolFactory.POOLS.keySet());
              localObject = ((ArrayList)localObject).iterator();
              while (((Iterator)localObject).hasNext())
              {
                ScheduledThreadPoolExecutor localScheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor)((Iterator)localObject).next();
                if (localScheduledThreadPoolExecutor.isShutdown()) {
                  SchedulerPoolFactory.POOLS.remove(localScheduledThreadPoolExecutor);
                } else {
                  localScheduledThreadPoolExecutor.purge();
                }
              }
              return;
            }
            catch (Throwable localThrowable)
            {
              RxJavaPlugins.onError(localThrowable);
            }
          }
        };
        int i = PURGE_PERIOD_SECONDS;
        localScheduledExecutorService.scheduleAtFixedRate((Runnable)localObject, i, i, TimeUnit.SECONDS);
        return;
      }
      localScheduledExecutorService.shutdownNow();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/schedulers/SchedulerPoolFactory.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */