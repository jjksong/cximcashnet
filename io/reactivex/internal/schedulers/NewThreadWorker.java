package io.reactivex.internal.schedulers;

import io.reactivex.Scheduler.Worker;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;
import io.reactivex.internal.disposables.DisposableContainer;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;

public class NewThreadWorker
  extends Scheduler.Worker
  implements Disposable
{
  volatile boolean disposed;
  private final ScheduledExecutorService executor;
  
  public NewThreadWorker(ThreadFactory paramThreadFactory)
  {
    this.executor = SchedulerPoolFactory.create(paramThreadFactory);
  }
  
  public void dispose()
  {
    if (!this.disposed)
    {
      this.disposed = true;
      this.executor.shutdownNow();
    }
  }
  
  public boolean isDisposed()
  {
    return this.disposed;
  }
  
  public Disposable schedule(Runnable paramRunnable)
  {
    return schedule(paramRunnable, 0L, null);
  }
  
  public Disposable schedule(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit)
  {
    if (this.disposed) {
      return EmptyDisposable.INSTANCE;
    }
    return scheduleActual(paramRunnable, paramLong, paramTimeUnit, null);
  }
  
  public ScheduledRunnable scheduleActual(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit, DisposableContainer paramDisposableContainer)
  {
    ScheduledRunnable localScheduledRunnable = new ScheduledRunnable(RxJavaPlugins.onSchedule(paramRunnable), paramDisposableContainer);
    if ((paramDisposableContainer != null) && (!paramDisposableContainer.add(localScheduledRunnable))) {
      return localScheduledRunnable;
    }
    if (paramLong <= 0L) {}
    try
    {
      paramRunnable = this.executor.submit(localScheduledRunnable);
      break label71;
      paramRunnable = this.executor.schedule(localScheduledRunnable, paramLong, paramTimeUnit);
      label71:
      localScheduledRunnable.setFuture(paramRunnable);
    }
    catch (RejectedExecutionException paramRunnable)
    {
      paramDisposableContainer.remove(localScheduledRunnable);
      RxJavaPlugins.onError(paramRunnable);
    }
    return localScheduledRunnable;
  }
  
  public Disposable scheduleDirect(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit)
  {
    paramRunnable = RxJavaPlugins.onSchedule(paramRunnable);
    if (paramLong <= 0L) {}
    try
    {
      paramRunnable = this.executor.submit(paramRunnable);
      break label39;
      paramRunnable = this.executor.schedule(paramRunnable, paramLong, paramTimeUnit);
      label39:
      paramRunnable = Disposables.fromFuture(paramRunnable);
      return paramRunnable;
    }
    catch (RejectedExecutionException paramRunnable)
    {
      RxJavaPlugins.onError(paramRunnable);
    }
    return EmptyDisposable.INSTANCE;
  }
  
  public Disposable schedulePeriodicallyDirect(Runnable paramRunnable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
  {
    paramRunnable = RxJavaPlugins.onSchedule(paramRunnable);
    try
    {
      paramRunnable = Disposables.fromFuture(this.executor.scheduleAtFixedRate(paramRunnable, paramLong1, paramLong2, paramTimeUnit));
      return paramRunnable;
    }
    catch (RejectedExecutionException paramRunnable)
    {
      RxJavaPlugins.onError(paramRunnable);
    }
    return EmptyDisposable.INSTANCE;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/internal/schedulers/NewThreadWorker.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */