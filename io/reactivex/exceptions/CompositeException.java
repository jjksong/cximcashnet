package io.reactivex.exceptions;

import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public final class CompositeException
  extends RuntimeException
{
  private static final long serialVersionUID = 3026362227162912146L;
  private Throwable cause;
  private final List<Throwable> exceptions;
  private final String message;
  
  public CompositeException(Iterable<? extends Throwable> paramIterable)
  {
    LinkedHashSet localLinkedHashSet = new LinkedHashSet();
    ArrayList localArrayList = new ArrayList();
    if (paramIterable != null)
    {
      Iterator localIterator = paramIterable.iterator();
      while (localIterator.hasNext())
      {
        paramIterable = (Throwable)localIterator.next();
        if ((paramIterable instanceof CompositeException)) {
          localLinkedHashSet.addAll(((CompositeException)paramIterable).getExceptions());
        } else if (paramIterable != null) {
          localLinkedHashSet.add(paramIterable);
        } else {
          localLinkedHashSet.add(new NullPointerException("Throwable was null!"));
        }
      }
    }
    localLinkedHashSet.add(new NullPointerException("errors was null"));
    if (!localLinkedHashSet.isEmpty())
    {
      localArrayList.addAll(localLinkedHashSet);
      this.exceptions = Collections.unmodifiableList(localArrayList);
      paramIterable = new StringBuilder();
      paramIterable.append(this.exceptions.size());
      paramIterable.append(" exceptions occurred. ");
      this.message = paramIterable.toString();
      return;
    }
    throw new IllegalArgumentException("errors is empty");
  }
  
  public CompositeException(Throwable... paramVarArgs)
  {
    this(Arrays.asList((Object[])localObject));
  }
  
  private void appendStackTrace(StringBuilder paramStringBuilder, Throwable paramThrowable, String paramString)
  {
    paramStringBuilder.append(paramString);
    paramStringBuilder.append(paramThrowable);
    paramStringBuilder.append('\n');
    for (Object localObject : paramThrowable.getStackTrace())
    {
      paramStringBuilder.append("\t\tat ");
      paramStringBuilder.append(localObject);
      paramStringBuilder.append('\n');
    }
    if (paramThrowable.getCause() != null)
    {
      paramStringBuilder.append("\tCaused by: ");
      appendStackTrace(paramStringBuilder, paramThrowable.getCause(), "");
    }
  }
  
  private List<Throwable> getListOfCauses(Throwable paramThrowable)
  {
    ArrayList localArrayList = new ArrayList();
    Throwable localThrowable2 = paramThrowable.getCause();
    if (localThrowable2 != null)
    {
      Throwable localThrowable1 = localThrowable2;
      if (localThrowable2 != paramThrowable)
      {
        for (;;)
        {
          localArrayList.add(localThrowable1);
          paramThrowable = localThrowable1.getCause();
          if ((paramThrowable == null) || (paramThrowable == localThrowable1)) {
            break;
          }
          localThrowable1 = paramThrowable;
        }
        return localArrayList;
      }
    }
    return localArrayList;
  }
  
  private Throwable getRootCause(Throwable paramThrowable)
  {
    Throwable localThrowable2 = paramThrowable.getCause();
    if (localThrowable2 != null)
    {
      Throwable localThrowable1 = localThrowable2;
      if (this.cause != localThrowable2)
      {
        for (;;)
        {
          paramThrowable = localThrowable1.getCause();
          if ((paramThrowable == null) || (paramThrowable == localThrowable1)) {
            break;
          }
          localThrowable1 = paramThrowable;
        }
        return localThrowable1;
      }
    }
    return paramThrowable;
  }
  
  private void printStackTrace(PrintStreamOrWriter paramPrintStreamOrWriter)
  {
    StringBuilder localStringBuilder = new StringBuilder(128);
    localStringBuilder.append(this);
    localStringBuilder.append('\n');
    Object localObject1;
    for (localObject1 : getStackTrace())
    {
      localStringBuilder.append("\tat ");
      localStringBuilder.append(localObject1);
      localStringBuilder.append('\n');
    }
    ??? = this.exceptions.iterator();
    for (??? = 1; ((Iterator)???).hasNext(); ???++)
    {
      localObject1 = (Throwable)((Iterator)???).next();
      localStringBuilder.append("  ComposedException ");
      localStringBuilder.append(???);
      localStringBuilder.append(" :\n");
      appendStackTrace(localStringBuilder, (Throwable)localObject1, "\t");
    }
    paramPrintStreamOrWriter.println(localStringBuilder.toString());
  }
  
  /* Error */
  public Throwable getCause()
  {
    // Byte code:
    //   0: aload_0
    //   1: monitorenter
    //   2: aload_0
    //   3: getfield 166	io/reactivex/exceptions/CompositeException:cause	Ljava/lang/Throwable;
    //   6: ifnonnull +170 -> 176
    //   9: new 6	io/reactivex/exceptions/CompositeException$CompositeExceptionCausalChain
    //   12: astore_3
    //   13: aload_3
    //   14: invokespecial 186	io/reactivex/exceptions/CompositeException$CompositeExceptionCausalChain:<init>	()V
    //   17: new 188	java/util/HashSet
    //   20: astore 5
    //   22: aload 5
    //   24: invokespecial 189	java/util/HashSet:<init>	()V
    //   27: aload_0
    //   28: getfield 93	io/reactivex/exceptions/CompositeException:exceptions	Ljava/util/List;
    //   31: invokeinterface 175 1 0
    //   36: astore 4
    //   38: aload_3
    //   39: astore_1
    //   40: aload 4
    //   42: invokeinterface 50 1 0
    //   47: ifeq +124 -> 171
    //   50: aload 4
    //   52: invokeinterface 54 1 0
    //   57: checkcast 56	java/lang/Throwable
    //   60: astore_2
    //   61: aload 5
    //   63: aload_2
    //   64: invokeinterface 192 2 0
    //   69: ifeq +6 -> 75
    //   72: goto -32 -> 40
    //   75: aload 5
    //   77: aload_2
    //   78: invokeinterface 70 2 0
    //   83: pop
    //   84: aload_0
    //   85: aload_2
    //   86: invokespecial 194	io/reactivex/exceptions/CompositeException:getListOfCauses	(Ljava/lang/Throwable;)Ljava/util/List;
    //   89: invokeinterface 175 1 0
    //   94: astore 6
    //   96: aload 6
    //   98: invokeinterface 50 1 0
    //   103: ifeq +53 -> 156
    //   106: aload 6
    //   108: invokeinterface 54 1 0
    //   113: checkcast 56	java/lang/Throwable
    //   116: astore 7
    //   118: aload 5
    //   120: aload 7
    //   122: invokeinterface 192 2 0
    //   127: ifeq +16 -> 143
    //   130: new 4	java/lang/RuntimeException
    //   133: dup
    //   134: ldc -60
    //   136: invokespecial 197	java/lang/RuntimeException:<init>	(Ljava/lang/String;)V
    //   139: astore_2
    //   140: goto -44 -> 96
    //   143: aload 5
    //   145: aload 7
    //   147: invokeinterface 70 2 0
    //   152: pop
    //   153: goto -57 -> 96
    //   156: aload_1
    //   157: aload_2
    //   158: invokevirtual 200	java/lang/Throwable:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   161: pop
    //   162: aload_0
    //   163: aload_1
    //   164: invokespecial 202	io/reactivex/exceptions/CompositeException:getRootCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   167: astore_1
    //   168: goto -128 -> 40
    //   171: aload_0
    //   172: aload_3
    //   173: putfield 166	io/reactivex/exceptions/CompositeException:cause	Ljava/lang/Throwable;
    //   176: aload_0
    //   177: getfield 166	io/reactivex/exceptions/CompositeException:cause	Ljava/lang/Throwable;
    //   180: astore_1
    //   181: aload_0
    //   182: monitorexit
    //   183: aload_1
    //   184: areturn
    //   185: astore_1
    //   186: aload_0
    //   187: monitorexit
    //   188: aload_1
    //   189: athrow
    //   190: astore_2
    //   191: goto -29 -> 162
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	194	0	this	CompositeException
    //   39	145	1	localObject1	Object
    //   185	4	1	localObject2	Object
    //   60	98	2	localObject3	Object
    //   190	1	2	localThrowable1	Throwable
    //   12	161	3	localCompositeExceptionCausalChain	CompositeExceptionCausalChain
    //   36	15	4	localIterator1	Iterator
    //   20	124	5	localHashSet	java.util.HashSet
    //   94	13	6	localIterator2	Iterator
    //   116	30	7	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   2	38	185	finally
    //   40	72	185	finally
    //   75	96	185	finally
    //   96	140	185	finally
    //   143	153	185	finally
    //   156	162	185	finally
    //   162	168	185	finally
    //   171	176	185	finally
    //   176	181	185	finally
    //   156	162	190	java/lang/Throwable
  }
  
  public List<Throwable> getExceptions()
  {
    return this.exceptions;
  }
  
  public String getMessage()
  {
    return this.message;
  }
  
  public void printStackTrace()
  {
    printStackTrace(System.err);
  }
  
  public void printStackTrace(PrintStream paramPrintStream)
  {
    printStackTrace(new WrappedPrintStream(paramPrintStream));
  }
  
  public void printStackTrace(PrintWriter paramPrintWriter)
  {
    printStackTrace(new WrappedPrintWriter(paramPrintWriter));
  }
  
  public int size()
  {
    return this.exceptions.size();
  }
  
  static final class CompositeExceptionCausalChain
    extends RuntimeException
  {
    static final String MESSAGE = "Chain of Causes for CompositeException In Order Received =>";
    private static final long serialVersionUID = 3875212506787802066L;
    
    public String getMessage()
    {
      return "Chain of Causes for CompositeException In Order Received =>";
    }
  }
  
  static abstract class PrintStreamOrWriter
  {
    abstract void println(Object paramObject);
  }
  
  static final class WrappedPrintStream
    extends CompositeException.PrintStreamOrWriter
  {
    private final PrintStream printStream;
    
    WrappedPrintStream(PrintStream paramPrintStream)
    {
      this.printStream = paramPrintStream;
    }
    
    void println(Object paramObject)
    {
      this.printStream.println(paramObject);
    }
  }
  
  static final class WrappedPrintWriter
    extends CompositeException.PrintStreamOrWriter
  {
    private final PrintWriter printWriter;
    
    WrappedPrintWriter(PrintWriter paramPrintWriter)
    {
      this.printWriter = paramPrintWriter;
    }
    
    void println(Object paramObject)
    {
      this.printWriter.println(paramObject);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/exceptions/CompositeException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */