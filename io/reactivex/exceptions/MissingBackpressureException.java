package io.reactivex.exceptions;

public final class MissingBackpressureException
  extends RuntimeException
{
  private static final long serialVersionUID = 8517344746016032542L;
  
  public MissingBackpressureException() {}
  
  public MissingBackpressureException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/exceptions/MissingBackpressureException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */