package io.reactivex.processors;

import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.internal.subscriptions.BasicIntQueueSubscription;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class UnicastProcessor<T>
  extends FlowableProcessor<T>
{
  final AtomicReference<Subscriber<? super T>> actual;
  volatile boolean cancelled;
  volatile boolean done;
  boolean enableOperatorFusion;
  Throwable error;
  final AtomicReference<Runnable> onTerminate;
  final AtomicBoolean once;
  final SpscLinkedArrayQueue<T> queue;
  final AtomicLong requested;
  final BasicIntQueueSubscription<T> wip;
  
  UnicastProcessor(int paramInt)
  {
    this.queue = new SpscLinkedArrayQueue(ObjectHelper.verifyPositive(paramInt, "capacityHint"));
    this.onTerminate = new AtomicReference();
    this.actual = new AtomicReference();
    this.once = new AtomicBoolean();
    this.wip = new UnicastQueueSubscription();
    this.requested = new AtomicLong();
  }
  
  UnicastProcessor(int paramInt, Runnable paramRunnable)
  {
    this.queue = new SpscLinkedArrayQueue(ObjectHelper.verifyPositive(paramInt, "capacityHint"));
    this.onTerminate = new AtomicReference(ObjectHelper.requireNonNull(paramRunnable, "onTerminate"));
    this.actual = new AtomicReference();
    this.once = new AtomicBoolean();
    this.wip = new UnicastQueueSubscription();
    this.requested = new AtomicLong();
  }
  
  @CheckReturnValue
  public static <T> UnicastProcessor<T> create()
  {
    return new UnicastProcessor(bufferSize());
  }
  
  @CheckReturnValue
  public static <T> UnicastProcessor<T> create(int paramInt)
  {
    return new UnicastProcessor(paramInt);
  }
  
  @CheckReturnValue
  public static <T> UnicastProcessor<T> create(int paramInt, Runnable paramRunnable)
  {
    return new UnicastProcessor(paramInt, paramRunnable);
  }
  
  boolean checkTerminated(boolean paramBoolean1, boolean paramBoolean2, Subscriber<? super T> paramSubscriber, SpscLinkedArrayQueue<T> paramSpscLinkedArrayQueue)
  {
    if (this.cancelled)
    {
      paramSpscLinkedArrayQueue.clear();
      this.actual.lazySet(null);
      return true;
    }
    if ((paramBoolean1) && (paramBoolean2))
    {
      paramSpscLinkedArrayQueue = this.error;
      this.actual.lazySet(null);
      if (paramSpscLinkedArrayQueue != null) {
        paramSubscriber.onError(paramSpscLinkedArrayQueue);
      } else {
        paramSubscriber.onComplete();
      }
      return true;
    }
    return false;
  }
  
  void doTerminate()
  {
    Runnable localRunnable = (Runnable)this.onTerminate.get();
    if ((localRunnable != null) && (this.onTerminate.compareAndSet(localRunnable, null))) {
      localRunnable.run();
    }
  }
  
  void drain()
  {
    if (this.wip.getAndIncrement() != 0) {
      return;
    }
    int i = 1;
    for (Subscriber localSubscriber = (Subscriber)this.actual.get();; localSubscriber = (Subscriber)this.actual.get())
    {
      if (localSubscriber != null)
      {
        if (this.enableOperatorFusion) {
          drainFused(localSubscriber);
        } else {
          drainRegular(localSubscriber);
        }
        return;
      }
      i = this.wip.addAndGet(-i);
      if (i == 0) {
        return;
      }
    }
  }
  
  void drainFused(Subscriber<? super T> paramSubscriber)
  {
    Object localObject = this.queue;
    int i = 1;
    int j;
    do
    {
      if (this.cancelled)
      {
        ((SpscLinkedArrayQueue)localObject).clear();
        this.actual.lazySet(null);
        return;
      }
      boolean bool = this.done;
      paramSubscriber.onNext(null);
      if (bool)
      {
        this.actual.lazySet(null);
        localObject = this.error;
        if (localObject != null) {
          paramSubscriber.onError((Throwable)localObject);
        } else {
          paramSubscriber.onComplete();
        }
        return;
      }
      j = this.wip.addAndGet(-i);
      i = j;
    } while (j != 0);
  }
  
  void drainRegular(Subscriber<? super T> paramSubscriber)
  {
    SpscLinkedArrayQueue localSpscLinkedArrayQueue = this.queue;
    int i = 1;
    int j;
    do
    {
      long l2 = this.requested.get();
      for (long l1 = 0L; l2 != l1; l1 += 1L)
      {
        boolean bool2 = this.done;
        Object localObject = localSpscLinkedArrayQueue.poll();
        boolean bool1;
        if (localObject == null) {
          bool1 = true;
        } else {
          bool1 = false;
        }
        if (checkTerminated(bool2, bool1, paramSubscriber, localSpscLinkedArrayQueue)) {
          return;
        }
        if (bool1) {
          break;
        }
        paramSubscriber.onNext(localObject);
      }
      if ((l2 == l1) && (checkTerminated(this.done, localSpscLinkedArrayQueue.isEmpty(), paramSubscriber, localSpscLinkedArrayQueue))) {
        return;
      }
      if ((l1 != 0L) && (l2 != Long.MAX_VALUE)) {
        this.requested.addAndGet(-l1);
      }
      j = this.wip.addAndGet(-i);
      i = j;
    } while (j != 0);
  }
  
  public Throwable getThrowable()
  {
    if (this.done) {
      return this.error;
    }
    return null;
  }
  
  public boolean hasComplete()
  {
    boolean bool;
    if ((this.done) && (this.error == null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasSubscribers()
  {
    boolean bool;
    if (this.actual.get() != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.done) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    if ((!this.done) && (!this.cancelled))
    {
      this.done = true;
      doTerminate();
      drain();
      return;
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if ((!this.done) && (!this.cancelled))
    {
      Object localObject = paramThrowable;
      if (paramThrowable == null) {
        localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
      }
      this.error = ((Throwable)localObject);
      this.done = true;
      doTerminate();
      drain();
      return;
    }
    RxJavaPlugins.onError(paramThrowable);
  }
  
  public void onNext(T paramT)
  {
    if ((!this.done) && (!this.cancelled))
    {
      if (paramT == null)
      {
        onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
        return;
      }
      this.queue.offer(paramT);
      drain();
      return;
    }
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if ((!this.done) && (!this.cancelled)) {
      paramSubscription.request(Long.MAX_VALUE);
    } else {
      paramSubscription.cancel();
    }
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    if ((!this.once.get()) && (this.once.compareAndSet(false, true)))
    {
      paramSubscriber.onSubscribe(this.wip);
      this.actual.set(paramSubscriber);
      if (this.cancelled) {
        this.actual.lazySet(null);
      } else {
        drain();
      }
    }
    else
    {
      EmptySubscription.error(new IllegalStateException("This processor allows only a single Subscriber"), paramSubscriber);
    }
  }
  
  final class UnicastQueueSubscription
    extends BasicIntQueueSubscription<T>
  {
    private static final long serialVersionUID = -4896760517184205454L;
    
    UnicastQueueSubscription() {}
    
    public void cancel()
    {
      if (UnicastProcessor.this.cancelled) {
        return;
      }
      UnicastProcessor localUnicastProcessor = UnicastProcessor.this;
      localUnicastProcessor.cancelled = true;
      localUnicastProcessor.doTerminate();
      if ((!UnicastProcessor.this.enableOperatorFusion) && (UnicastProcessor.this.wip.getAndIncrement() == 0))
      {
        UnicastProcessor.this.queue.clear();
        UnicastProcessor.this.actual.lazySet(null);
      }
    }
    
    public void clear()
    {
      UnicastProcessor.this.queue.clear();
    }
    
    public boolean isEmpty()
    {
      return UnicastProcessor.this.queue.isEmpty();
    }
    
    public T poll()
    {
      return (T)UnicastProcessor.this.queue.poll();
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(UnicastProcessor.this.requested, paramLong);
        UnicastProcessor.this.drain();
      }
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        UnicastProcessor.this.enableOperatorFusion = true;
        return 2;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/processors/UnicastProcessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */