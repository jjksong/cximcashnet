package io.reactivex.processors;

import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class PublishProcessor<T>
  extends FlowableProcessor<T>
{
  static final PublishSubscription[] EMPTY = new PublishSubscription[0];
  static final PublishSubscription[] TERMINATED = new PublishSubscription[0];
  Throwable error;
  final AtomicReference<PublishSubscription<T>[]> subscribers = new AtomicReference(EMPTY);
  
  @CheckReturnValue
  public static <T> PublishProcessor<T> create()
  {
    return new PublishProcessor();
  }
  
  boolean add(PublishSubscription<T> paramPublishSubscription)
  {
    PublishSubscription[] arrayOfPublishSubscription2;
    PublishSubscription[] arrayOfPublishSubscription1;
    do
    {
      arrayOfPublishSubscription2 = (PublishSubscription[])this.subscribers.get();
      if (arrayOfPublishSubscription2 == TERMINATED) {
        return false;
      }
      int i = arrayOfPublishSubscription2.length;
      arrayOfPublishSubscription1 = new PublishSubscription[i + 1];
      System.arraycopy(arrayOfPublishSubscription2, 0, arrayOfPublishSubscription1, 0, i);
      arrayOfPublishSubscription1[i] = paramPublishSubscription;
    } while (!this.subscribers.compareAndSet(arrayOfPublishSubscription2, arrayOfPublishSubscription1));
    return true;
  }
  
  public Throwable getThrowable()
  {
    if (this.subscribers.get() == TERMINATED) {
      return this.error;
    }
    return null;
  }
  
  public boolean hasComplete()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.error == null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasSubscribers()
  {
    boolean bool;
    if (((PublishSubscription[])this.subscribers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    Object localObject = this.subscribers.get();
    PublishSubscription[] arrayOfPublishSubscription = TERMINATED;
    if (localObject == arrayOfPublishSubscription) {
      return;
    }
    arrayOfPublishSubscription = (PublishSubscription[])this.subscribers.getAndSet(arrayOfPublishSubscription);
    int j = arrayOfPublishSubscription.length;
    for (int i = 0; i < j; i++) {
      arrayOfPublishSubscription[i].onComplete();
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (this.subscribers.get() == TERMINATED)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    this.error = ((Throwable)localObject);
    paramThrowable = (PublishSubscription[])this.subscribers.getAndSet(TERMINATED);
    int j = paramThrowable.length;
    for (int i = 0; i < j; i++) {
      paramThrowable[i].onError((Throwable)localObject);
    }
  }
  
  public void onNext(T paramT)
  {
    if (this.subscribers.get() == TERMINATED) {
      return;
    }
    if (paramT == null)
    {
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
      return;
    }
    PublishSubscription[] arrayOfPublishSubscription = (PublishSubscription[])this.subscribers.get();
    int j = arrayOfPublishSubscription.length;
    for (int i = 0; i < j; i++) {
      arrayOfPublishSubscription[i].onNext(paramT);
    }
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (this.subscribers.get() == TERMINATED)
    {
      paramSubscription.cancel();
      return;
    }
    paramSubscription.request(Long.MAX_VALUE);
  }
  
  void remove(PublishSubscription<T> paramPublishSubscription)
  {
    PublishSubscription[] arrayOfPublishSubscription2;
    PublishSubscription[] arrayOfPublishSubscription1;
    do
    {
      arrayOfPublishSubscription2 = (PublishSubscription[])this.subscribers.get();
      if ((arrayOfPublishSubscription2 == TERMINATED) || (arrayOfPublishSubscription2 == EMPTY)) {
        break;
      }
      int m = arrayOfPublishSubscription2.length;
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfPublishSubscription2[i] == paramPublishSubscription)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfPublishSubscription1 = EMPTY;
      }
      else
      {
        arrayOfPublishSubscription1 = new PublishSubscription[m - 1];
        System.arraycopy(arrayOfPublishSubscription2, 0, arrayOfPublishSubscription1, 0, j);
        System.arraycopy(arrayOfPublishSubscription2, j + 1, arrayOfPublishSubscription1, j, m - j - 1);
      }
    } while (!this.subscribers.compareAndSet(arrayOfPublishSubscription2, arrayOfPublishSubscription1));
    return;
  }
  
  public void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    Object localObject = new PublishSubscription(paramSubscriber, this);
    paramSubscriber.onSubscribe((Subscription)localObject);
    if (add((PublishSubscription)localObject))
    {
      if (((PublishSubscription)localObject).isCancelled()) {
        remove((PublishSubscription)localObject);
      }
    }
    else
    {
      localObject = this.error;
      if (localObject != null) {
        paramSubscriber.onError((Throwable)localObject);
      } else {
        paramSubscriber.onComplete();
      }
    }
  }
  
  static final class PublishSubscription<T>
    extends AtomicLong
    implements Subscription
  {
    private static final long serialVersionUID = 3562861878281475070L;
    final Subscriber<? super T> actual;
    final PublishProcessor<T> parent;
    
    PublishSubscription(Subscriber<? super T> paramSubscriber, PublishProcessor<T> paramPublishProcessor)
    {
      this.actual = paramSubscriber;
      this.parent = paramPublishProcessor;
    }
    
    public void cancel()
    {
      if (getAndSet(Long.MIN_VALUE) != Long.MIN_VALUE) {
        this.parent.remove(this);
      }
    }
    
    public boolean isCancelled()
    {
      boolean bool;
      if (get() == Long.MIN_VALUE) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onComplete()
    {
      if (get() != Long.MIN_VALUE) {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (get() != Long.MIN_VALUE) {
        this.actual.onError(paramThrowable);
      } else {
        RxJavaPlugins.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      long l = get();
      if (l == Long.MIN_VALUE) {
        return;
      }
      if (l != 0L)
      {
        this.actual.onNext(paramT);
        if (l != Long.MAX_VALUE) {
          decrementAndGet();
        }
      }
      else
      {
        cancel();
        this.actual.onError(new MissingBackpressureException("Could not emit value due to lack of requests"));
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        BackpressureHelper.addCancel(this, paramLong);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/processors/PublishProcessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */