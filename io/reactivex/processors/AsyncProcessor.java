package io.reactivex.processors;

import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.internal.subscriptions.DeferredScalarSubscription;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class AsyncProcessor<T>
  extends FlowableProcessor<T>
{
  static final AsyncSubscription[] EMPTY = new AsyncSubscription[0];
  static final AsyncSubscription[] TERMINATED = new AsyncSubscription[0];
  Throwable error;
  final AtomicReference<AsyncSubscription<T>[]> subscribers = new AtomicReference(EMPTY);
  T value;
  
  @CheckReturnValue
  public static <T> AsyncProcessor<T> create()
  {
    return new AsyncProcessor();
  }
  
  boolean add(AsyncSubscription<T> paramAsyncSubscription)
  {
    AsyncSubscription[] arrayOfAsyncSubscription1;
    AsyncSubscription[] arrayOfAsyncSubscription2;
    do
    {
      arrayOfAsyncSubscription1 = (AsyncSubscription[])this.subscribers.get();
      if (arrayOfAsyncSubscription1 == TERMINATED) {
        return false;
      }
      int i = arrayOfAsyncSubscription1.length;
      arrayOfAsyncSubscription2 = new AsyncSubscription[i + 1];
      System.arraycopy(arrayOfAsyncSubscription1, 0, arrayOfAsyncSubscription2, 0, i);
      arrayOfAsyncSubscription2[i] = paramAsyncSubscription;
    } while (!this.subscribers.compareAndSet(arrayOfAsyncSubscription1, arrayOfAsyncSubscription2));
    return true;
  }
  
  public Throwable getThrowable()
  {
    Throwable localThrowable;
    if (this.subscribers.get() == TERMINATED) {
      localThrowable = this.error;
    } else {
      localThrowable = null;
    }
    return localThrowable;
  }
  
  public T getValue()
  {
    Object localObject;
    if (this.subscribers.get() == TERMINATED) {
      localObject = this.value;
    } else {
      localObject = null;
    }
    return (T)localObject;
  }
  
  public Object[] getValues()
  {
    Object localObject = getValue();
    Object[] arrayOfObject;
    if (localObject != null)
    {
      arrayOfObject = new Object[1];
      arrayOfObject[0] = localObject;
    }
    else
    {
      arrayOfObject = new Object[0];
    }
    return arrayOfObject;
  }
  
  public T[] getValues(T[] paramArrayOfT)
  {
    Object localObject2 = getValue();
    if (localObject2 == null)
    {
      if (paramArrayOfT.length != 0) {
        paramArrayOfT[0] = null;
      }
      return paramArrayOfT;
    }
    Object localObject1 = paramArrayOfT;
    if (paramArrayOfT.length == 0) {
      localObject1 = Arrays.copyOf(paramArrayOfT, 1);
    }
    localObject1[0] = localObject2;
    if (localObject1.length != 1) {
      localObject1[1] = null;
    }
    return (T[])localObject1;
  }
  
  public boolean hasComplete()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.error == null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasSubscribers()
  {
    boolean bool;
    if (((AsyncSubscription[])this.subscribers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasValue()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.value != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  void nullOnNext()
  {
    this.value = null;
    NullPointerException localNullPointerException = new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
    this.error = localNullPointerException;
    AsyncSubscription[] arrayOfAsyncSubscription = (AsyncSubscription[])this.subscribers.getAndSet(TERMINATED);
    int j = arrayOfAsyncSubscription.length;
    for (int i = 0; i < j; i++) {
      arrayOfAsyncSubscription[i].onError(localNullPointerException);
    }
  }
  
  public void onComplete()
  {
    Object localObject = this.subscribers.get();
    AsyncSubscription[] arrayOfAsyncSubscription = TERMINATED;
    if (localObject == arrayOfAsyncSubscription) {
      return;
    }
    localObject = this.value;
    arrayOfAsyncSubscription = (AsyncSubscription[])this.subscribers.getAndSet(arrayOfAsyncSubscription);
    int j = 0;
    int i = 0;
    if (localObject == null)
    {
      j = arrayOfAsyncSubscription.length;
      while (i < j)
      {
        arrayOfAsyncSubscription[i].onComplete();
        i++;
      }
    }
    int k = arrayOfAsyncSubscription.length;
    for (i = j; i < k; i++) {
      arrayOfAsyncSubscription[i].complete(localObject);
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject1 = paramThrowable;
    if (paramThrowable == null) {
      localObject1 = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    Object localObject2 = this.subscribers.get();
    paramThrowable = TERMINATED;
    if (localObject2 == paramThrowable)
    {
      RxJavaPlugins.onError((Throwable)localObject1);
      return;
    }
    this.value = null;
    this.error = ((Throwable)localObject1);
    paramThrowable = (AsyncSubscription[])this.subscribers.getAndSet(paramThrowable);
    int j = paramThrowable.length;
    for (int i = 0; i < j; i++) {
      paramThrowable[i].onError((Throwable)localObject1);
    }
  }
  
  public void onNext(T paramT)
  {
    if (this.subscribers.get() == TERMINATED) {
      return;
    }
    if (paramT == null)
    {
      nullOnNext();
      return;
    }
    this.value = paramT;
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (this.subscribers.get() == TERMINATED)
    {
      paramSubscription.cancel();
      return;
    }
    paramSubscription.request(Long.MAX_VALUE);
  }
  
  void remove(AsyncSubscription<T> paramAsyncSubscription)
  {
    AsyncSubscription[] arrayOfAsyncSubscription2;
    AsyncSubscription[] arrayOfAsyncSubscription1;
    do
    {
      arrayOfAsyncSubscription2 = (AsyncSubscription[])this.subscribers.get();
      int m = arrayOfAsyncSubscription2.length;
      if (m == 0) {
        return;
      }
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfAsyncSubscription2[i] == paramAsyncSubscription)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfAsyncSubscription1 = EMPTY;
      }
      else
      {
        arrayOfAsyncSubscription1 = new AsyncSubscription[m - 1];
        System.arraycopy(arrayOfAsyncSubscription2, 0, arrayOfAsyncSubscription1, 0, j);
        System.arraycopy(arrayOfAsyncSubscription2, j + 1, arrayOfAsyncSubscription1, j, m - j - 1);
      }
    } while (!this.subscribers.compareAndSet(arrayOfAsyncSubscription2, arrayOfAsyncSubscription1));
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    AsyncSubscription localAsyncSubscription = new AsyncSubscription(paramSubscriber, this);
    paramSubscriber.onSubscribe(localAsyncSubscription);
    if (add(localAsyncSubscription))
    {
      if (localAsyncSubscription.isCancelled()) {
        remove(localAsyncSubscription);
      }
    }
    else
    {
      Throwable localThrowable = this.error;
      if (localThrowable != null)
      {
        paramSubscriber.onError(localThrowable);
      }
      else
      {
        paramSubscriber = this.value;
        if (paramSubscriber != null) {
          localAsyncSubscription.complete(paramSubscriber);
        } else {
          localAsyncSubscription.onComplete();
        }
      }
    }
  }
  
  static final class AsyncSubscription<T>
    extends DeferredScalarSubscription<T>
  {
    private static final long serialVersionUID = 5629876084736248016L;
    final AsyncProcessor<T> parent;
    
    AsyncSubscription(Subscriber<? super T> paramSubscriber, AsyncProcessor<T> paramAsyncProcessor)
    {
      super();
      this.parent = paramAsyncProcessor;
    }
    
    public void cancel()
    {
      if (super.tryCancel()) {
        this.parent.remove(this);
      }
    }
    
    void onComplete()
    {
      if (!isCancelled()) {
        this.actual.onComplete();
      }
    }
    
    void onError(Throwable paramThrowable)
    {
      if (isCancelled()) {
        RxJavaPlugins.onError(paramThrowable);
      } else {
        this.actual.onError(paramThrowable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/processors/AsyncProcessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */