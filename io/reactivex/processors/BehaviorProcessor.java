package io.reactivex.processors;

import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList.NonThrowingPredicate;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.lang.reflect.Array;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class BehaviorProcessor<T>
  extends FlowableProcessor<T>
{
  static final BehaviorSubscription[] EMPTY = new BehaviorSubscription[0];
  static final Object[] EMPTY_ARRAY = new Object[0];
  static final BehaviorSubscription[] TERMINATED = new BehaviorSubscription[0];
  boolean done;
  long index;
  final ReadWriteLock lock = new ReentrantReadWriteLock();
  final Lock readLock = this.lock.readLock();
  final AtomicReference<BehaviorSubscription<T>[]> subscribers = new AtomicReference(EMPTY);
  final AtomicReference<Object> value = new AtomicReference();
  final Lock writeLock = this.lock.writeLock();
  
  BehaviorProcessor() {}
  
  BehaviorProcessor(T paramT)
  {
    this();
    this.value.lazySet(ObjectHelper.requireNonNull(paramT, "defaultValue is null"));
  }
  
  @CheckReturnValue
  public static <T> BehaviorProcessor<T> create()
  {
    return new BehaviorProcessor();
  }
  
  @CheckReturnValue
  public static <T> BehaviorProcessor<T> createDefault(T paramT)
  {
    ObjectHelper.requireNonNull(paramT, "defaultValue is null");
    return new BehaviorProcessor(paramT);
  }
  
  boolean add(BehaviorSubscription<T> paramBehaviorSubscription)
  {
    BehaviorSubscription[] arrayOfBehaviorSubscription2;
    BehaviorSubscription[] arrayOfBehaviorSubscription1;
    do
    {
      arrayOfBehaviorSubscription2 = (BehaviorSubscription[])this.subscribers.get();
      if (arrayOfBehaviorSubscription2 == TERMINATED) {
        return false;
      }
      int i = arrayOfBehaviorSubscription2.length;
      arrayOfBehaviorSubscription1 = new BehaviorSubscription[i + 1];
      System.arraycopy(arrayOfBehaviorSubscription2, 0, arrayOfBehaviorSubscription1, 0, i);
      arrayOfBehaviorSubscription1[i] = paramBehaviorSubscription;
    } while (!this.subscribers.compareAndSet(arrayOfBehaviorSubscription2, arrayOfBehaviorSubscription1));
    return true;
  }
  
  public Throwable getThrowable()
  {
    Object localObject = this.value.get();
    if (NotificationLite.isError(localObject)) {
      return NotificationLite.getError(localObject);
    }
    return null;
  }
  
  public T getValue()
  {
    Object localObject = this.value.get();
    if ((!NotificationLite.isComplete(localObject)) && (!NotificationLite.isError(localObject))) {
      return (T)NotificationLite.getValue(localObject);
    }
    return null;
  }
  
  public Object[] getValues()
  {
    Object[] arrayOfObject = getValues((Object[])EMPTY_ARRAY);
    if (arrayOfObject == EMPTY_ARRAY) {
      return new Object[0];
    }
    return arrayOfObject;
  }
  
  public T[] getValues(T[] paramArrayOfT)
  {
    Object localObject1 = this.value.get();
    if ((localObject1 != null) && (!NotificationLite.isComplete(localObject1)) && (!NotificationLite.isError(localObject1)))
    {
      Object localObject2 = NotificationLite.getValue(localObject1);
      if (paramArrayOfT.length != 0)
      {
        paramArrayOfT[0] = localObject2;
        localObject1 = paramArrayOfT;
        if (paramArrayOfT.length != 1)
        {
          paramArrayOfT[1] = null;
          localObject1 = paramArrayOfT;
        }
      }
      else
      {
        localObject1 = (Object[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), 1);
        localObject1[0] = localObject2;
      }
      return (T[])localObject1;
    }
    if (paramArrayOfT.length != 0) {
      paramArrayOfT[0] = null;
    }
    return paramArrayOfT;
  }
  
  public boolean hasComplete()
  {
    return NotificationLite.isComplete(this.value.get());
  }
  
  public boolean hasSubscribers()
  {
    boolean bool;
    if (((BehaviorSubscription[])this.subscribers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    return NotificationLite.isError(this.value.get());
  }
  
  public boolean hasValue()
  {
    Object localObject = this.value.get();
    boolean bool;
    if ((localObject != null) && (!NotificationLite.isComplete(localObject)) && (!NotificationLite.isError(localObject))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    this.done = true;
    Object localObject = NotificationLite.complete();
    BehaviorSubscription[] arrayOfBehaviorSubscription = terminate(localObject);
    int j = arrayOfBehaviorSubscription.length;
    for (int i = 0; i < j; i++) {
      arrayOfBehaviorSubscription[i].emitNext(localObject, this.index);
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    if (this.done)
    {
      RxJavaPlugins.onError((Throwable)localObject);
      return;
    }
    this.done = true;
    localObject = NotificationLite.error((Throwable)localObject);
    paramThrowable = terminate(localObject);
    int j = paramThrowable.length;
    for (int i = 0; i < j; i++) {
      paramThrowable[i].emitNext(localObject, this.index);
    }
  }
  
  public void onNext(T paramT)
  {
    if (paramT == null)
    {
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
      return;
    }
    if (this.done) {
      return;
    }
    Object localObject = NotificationLite.next(paramT);
    setCurrent(localObject);
    paramT = (BehaviorSubscription[])this.subscribers.get();
    int j = paramT.length;
    for (int i = 0; i < j; i++) {
      paramT[i].emitNext(localObject, this.index);
    }
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (this.done)
    {
      paramSubscription.cancel();
      return;
    }
    paramSubscription.request(Long.MAX_VALUE);
  }
  
  void remove(BehaviorSubscription<T> paramBehaviorSubscription)
  {
    BehaviorSubscription[] arrayOfBehaviorSubscription2;
    BehaviorSubscription[] arrayOfBehaviorSubscription1;
    do
    {
      arrayOfBehaviorSubscription2 = (BehaviorSubscription[])this.subscribers.get();
      if ((arrayOfBehaviorSubscription2 == TERMINATED) || (arrayOfBehaviorSubscription2 == EMPTY)) {
        break;
      }
      int m = arrayOfBehaviorSubscription2.length;
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfBehaviorSubscription2[i] == paramBehaviorSubscription)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfBehaviorSubscription1 = EMPTY;
      }
      else
      {
        arrayOfBehaviorSubscription1 = new BehaviorSubscription[m - 1];
        System.arraycopy(arrayOfBehaviorSubscription2, 0, arrayOfBehaviorSubscription1, 0, j);
        System.arraycopy(arrayOfBehaviorSubscription2, j + 1, arrayOfBehaviorSubscription1, j, m - j - 1);
      }
    } while (!this.subscribers.compareAndSet(arrayOfBehaviorSubscription2, arrayOfBehaviorSubscription1));
    return;
  }
  
  void setCurrent(Object paramObject)
  {
    Lock localLock = this.writeLock;
    localLock.lock();
    this.index += 1L;
    this.value.lazySet(paramObject);
    localLock.unlock();
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    Object localObject = new BehaviorSubscription(paramSubscriber, this);
    paramSubscriber.onSubscribe((Subscription)localObject);
    if (add((BehaviorSubscription)localObject))
    {
      if (((BehaviorSubscription)localObject).cancelled) {
        remove((BehaviorSubscription)localObject);
      } else {
        ((BehaviorSubscription)localObject).emitFirst();
      }
    }
    else
    {
      localObject = this.value.get();
      if (NotificationLite.isComplete(localObject)) {
        paramSubscriber.onComplete();
      } else {
        paramSubscriber.onError(NotificationLite.getError(localObject));
      }
    }
  }
  
  int subscriberCount()
  {
    return ((BehaviorSubscription[])this.subscribers.get()).length;
  }
  
  BehaviorSubscription<T>[] terminate(Object paramObject)
  {
    BehaviorSubscription[] arrayOfBehaviorSubscription2 = (BehaviorSubscription[])this.subscribers.get();
    BehaviorSubscription[] arrayOfBehaviorSubscription3 = TERMINATED;
    BehaviorSubscription[] arrayOfBehaviorSubscription1 = arrayOfBehaviorSubscription2;
    if (arrayOfBehaviorSubscription2 != arrayOfBehaviorSubscription3)
    {
      arrayOfBehaviorSubscription2 = (BehaviorSubscription[])this.subscribers.getAndSet(arrayOfBehaviorSubscription3);
      arrayOfBehaviorSubscription1 = arrayOfBehaviorSubscription2;
      if (arrayOfBehaviorSubscription2 != TERMINATED)
      {
        setCurrent(paramObject);
        arrayOfBehaviorSubscription1 = arrayOfBehaviorSubscription2;
      }
    }
    return arrayOfBehaviorSubscription1;
  }
  
  static final class BehaviorSubscription<T>
    extends AtomicLong
    implements Subscription, AppendOnlyLinkedArrayList.NonThrowingPredicate<Object>
  {
    private static final long serialVersionUID = 3293175281126227086L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    boolean emitting;
    boolean fastPath;
    long index;
    boolean next;
    AppendOnlyLinkedArrayList<Object> queue;
    final BehaviorProcessor<T> state;
    
    BehaviorSubscription(Subscriber<? super T> paramSubscriber, BehaviorProcessor<T> paramBehaviorProcessor)
    {
      this.actual = paramSubscriber;
      this.state = paramBehaviorProcessor;
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.state.remove(this);
      }
    }
    
    void emitFirst()
    {
      if (this.cancelled) {
        return;
      }
      try
      {
        if (this.cancelled) {
          return;
        }
        if (this.next) {
          return;
        }
        Object localObject2 = this.state;
        Lock localLock = ((BehaviorProcessor)localObject2).readLock;
        localLock.lock();
        this.index = ((BehaviorProcessor)localObject2).index;
        localObject2 = ((BehaviorProcessor)localObject2).value.get();
        localLock.unlock();
        boolean bool;
        if (localObject2 != null) {
          bool = true;
        } else {
          bool = false;
        }
        this.emitting = bool;
        this.next = true;
        if (localObject2 != null)
        {
          if (test(localObject2)) {
            return;
          }
          emitLoop();
        }
        return;
      }
      finally {}
    }
    
    void emitLoop()
    {
      for (;;)
      {
        if (this.cancelled) {
          return;
        }
        try
        {
          AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList = this.queue;
          if (localAppendOnlyLinkedArrayList == null)
          {
            this.emitting = false;
            return;
          }
          this.queue = null;
          localAppendOnlyLinkedArrayList.forEachWhile(this);
        }
        finally {}
      }
    }
    
    void emitNext(Object paramObject, long paramLong)
    {
      if (this.cancelled) {
        return;
      }
      if (!this.fastPath) {
        try
        {
          if (this.cancelled) {
            return;
          }
          if (this.index == paramLong) {
            return;
          }
          if (this.emitting)
          {
            AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
            AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
            if (localAppendOnlyLinkedArrayList2 == null)
            {
              localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
              localAppendOnlyLinkedArrayList1.<init>(4);
              this.queue = localAppendOnlyLinkedArrayList1;
            }
            localAppendOnlyLinkedArrayList1.add(paramObject);
            return;
          }
          this.next = true;
          this.fastPath = true;
        }
        finally {}
      }
      test(paramObject);
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong)) {
        BackpressureHelper.add(this, paramLong);
      }
    }
    
    public boolean test(Object paramObject)
    {
      if (this.cancelled) {
        return true;
      }
      if (NotificationLite.isComplete(paramObject))
      {
        this.actual.onComplete();
        return true;
      }
      if (NotificationLite.isError(paramObject))
      {
        this.actual.onError(NotificationLite.getError(paramObject));
        return true;
      }
      long l = get();
      if (l != 0L)
      {
        this.actual.onNext(NotificationLite.getValue(paramObject));
        if (l != Long.MAX_VALUE) {
          decrementAndGet();
        }
        return false;
      }
      cancel();
      this.actual.onError(new MissingBackpressureException("Could not deliver value due to lack of requests"));
      return true;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/processors/BehaviorProcessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */