package io.reactivex.processors;

import io.reactivex.Scheduler;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.BackpressureHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class ReplayProcessor<T>
  extends FlowableProcessor<T>
{
  static final ReplaySubscription[] EMPTY = new ReplaySubscription[0];
  private static final Object[] EMPTY_ARRAY = new Object[0];
  static final ReplaySubscription[] TERMINATED = new ReplaySubscription[0];
  final ReplayBuffer<T> buffer;
  boolean done;
  final AtomicReference<ReplaySubscription<T>[]> subscribers;
  
  ReplayProcessor(ReplayBuffer<T> paramReplayBuffer)
  {
    this.buffer = paramReplayBuffer;
    this.subscribers = new AtomicReference(EMPTY);
  }
  
  @CheckReturnValue
  public static <T> ReplayProcessor<T> create()
  {
    return new ReplayProcessor(new UnboundedReplayBuffer(16));
  }
  
  @CheckReturnValue
  public static <T> ReplayProcessor<T> create(int paramInt)
  {
    return new ReplayProcessor(new UnboundedReplayBuffer(paramInt));
  }
  
  static <T> ReplayProcessor<T> createUnbounded()
  {
    return new ReplayProcessor(new SizeBoundReplayBuffer(Integer.MAX_VALUE));
  }
  
  @CheckReturnValue
  public static <T> ReplayProcessor<T> createWithSize(int paramInt)
  {
    return new ReplayProcessor(new SizeBoundReplayBuffer(paramInt));
  }
  
  @CheckReturnValue
  public static <T> ReplayProcessor<T> createWithTime(long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    return new ReplayProcessor(new SizeAndTimeBoundReplayBuffer(Integer.MAX_VALUE, paramLong, paramTimeUnit, paramScheduler));
  }
  
  @CheckReturnValue
  public static <T> ReplayProcessor<T> createWithTimeAndSize(long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, int paramInt)
  {
    return new ReplayProcessor(new SizeAndTimeBoundReplayBuffer(paramInt, paramLong, paramTimeUnit, paramScheduler));
  }
  
  boolean add(ReplaySubscription<T> paramReplaySubscription)
  {
    ReplaySubscription[] arrayOfReplaySubscription2;
    ReplaySubscription[] arrayOfReplaySubscription1;
    do
    {
      arrayOfReplaySubscription2 = (ReplaySubscription[])this.subscribers.get();
      if (arrayOfReplaySubscription2 == TERMINATED) {
        return false;
      }
      int i = arrayOfReplaySubscription2.length;
      arrayOfReplaySubscription1 = new ReplaySubscription[i + 1];
      System.arraycopy(arrayOfReplaySubscription2, 0, arrayOfReplaySubscription1, 0, i);
      arrayOfReplaySubscription1[i] = paramReplaySubscription;
    } while (!this.subscribers.compareAndSet(arrayOfReplaySubscription2, arrayOfReplaySubscription1));
    return true;
  }
  
  public Throwable getThrowable()
  {
    Object localObject = this.buffer.get();
    if (NotificationLite.isError(localObject)) {
      return NotificationLite.getError(localObject);
    }
    return null;
  }
  
  public T getValue()
  {
    return (T)this.buffer.getValue();
  }
  
  public Object[] getValues()
  {
    Object[] arrayOfObject = getValues((Object[])EMPTY_ARRAY);
    if (arrayOfObject == EMPTY_ARRAY) {
      return new Object[0];
    }
    return arrayOfObject;
  }
  
  public T[] getValues(T[] paramArrayOfT)
  {
    return this.buffer.getValues(paramArrayOfT);
  }
  
  public boolean hasComplete()
  {
    return NotificationLite.isComplete(this.buffer.get());
  }
  
  public boolean hasSubscribers()
  {
    boolean bool;
    if (((ReplaySubscription[])this.subscribers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    return NotificationLite.isError(this.buffer.get());
  }
  
  public boolean hasValue()
  {
    boolean bool;
    if (this.buffer.size() != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    this.done = true;
    Object localObject = NotificationLite.complete();
    ReplayBuffer localReplayBuffer = this.buffer;
    localReplayBuffer.addFinal(localObject);
    localObject = (ReplaySubscription[])this.subscribers.getAndSet(TERMINATED);
    int j = localObject.length;
    for (int i = 0; i < j; i++) {
      localReplayBuffer.replay(localObject[i]);
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    if (this.done)
    {
      RxJavaPlugins.onError((Throwable)localObject);
      return;
    }
    this.done = true;
    localObject = NotificationLite.error((Throwable)localObject);
    paramThrowable = this.buffer;
    paramThrowable.addFinal(localObject);
    localObject = (ReplaySubscription[])this.subscribers.getAndSet(TERMINATED);
    int j = localObject.length;
    for (int i = 0; i < j; i++) {
      paramThrowable.replay(localObject[i]);
    }
  }
  
  public void onNext(T paramT)
  {
    if (paramT == null)
    {
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
      return;
    }
    if (this.done) {
      return;
    }
    ReplayBuffer localReplayBuffer = this.buffer;
    localReplayBuffer.add(paramT);
    paramT = (ReplaySubscription[])this.subscribers.get();
    int j = paramT.length;
    for (int i = 0; i < j; i++) {
      localReplayBuffer.replay(paramT[i]);
    }
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (this.done)
    {
      paramSubscription.cancel();
      return;
    }
    paramSubscription.request(Long.MAX_VALUE);
  }
  
  void remove(ReplaySubscription<T> paramReplaySubscription)
  {
    ReplaySubscription[] arrayOfReplaySubscription2;
    ReplaySubscription[] arrayOfReplaySubscription1;
    do
    {
      arrayOfReplaySubscription2 = (ReplaySubscription[])this.subscribers.get();
      if ((arrayOfReplaySubscription2 == TERMINATED) || (arrayOfReplaySubscription2 == EMPTY)) {
        break;
      }
      int m = arrayOfReplaySubscription2.length;
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfReplaySubscription2[i] == paramReplaySubscription)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfReplaySubscription1 = EMPTY;
      }
      else
      {
        arrayOfReplaySubscription1 = new ReplaySubscription[m - 1];
        System.arraycopy(arrayOfReplaySubscription2, 0, arrayOfReplaySubscription1, 0, j);
        System.arraycopy(arrayOfReplaySubscription2, j + 1, arrayOfReplaySubscription1, j, m - j - 1);
      }
    } while (!this.subscribers.compareAndSet(arrayOfReplaySubscription2, arrayOfReplaySubscription1));
    return;
  }
  
  int size()
  {
    return this.buffer.size();
  }
  
  protected void subscribeActual(Subscriber<? super T> paramSubscriber)
  {
    ReplaySubscription localReplaySubscription = new ReplaySubscription(paramSubscriber, this);
    paramSubscriber.onSubscribe(localReplaySubscription);
    if ((add(localReplaySubscription)) && (localReplaySubscription.cancelled))
    {
      remove(localReplaySubscription);
      return;
    }
    this.buffer.replay(localReplaySubscription);
  }
  
  int subscriberCount()
  {
    return ((ReplaySubscription[])this.subscribers.get()).length;
  }
  
  static final class Node<T>
    extends AtomicReference<Node<T>>
  {
    private static final long serialVersionUID = 6404226426336033100L;
    final T value;
    
    Node(T paramT)
    {
      this.value = paramT;
    }
  }
  
  static abstract interface ReplayBuffer<T>
  {
    public abstract void add(T paramT);
    
    public abstract void addFinal(Object paramObject);
    
    public abstract Object get();
    
    public abstract T getValue();
    
    public abstract T[] getValues(T[] paramArrayOfT);
    
    public abstract void replay(ReplayProcessor.ReplaySubscription<T> paramReplaySubscription);
    
    public abstract int size();
  }
  
  static final class ReplaySubscription<T>
    extends AtomicInteger
    implements Subscription
  {
    private static final long serialVersionUID = 466549804534799122L;
    final Subscriber<? super T> actual;
    volatile boolean cancelled;
    Object index;
    final AtomicLong requested;
    final ReplayProcessor<T> state;
    
    ReplaySubscription(Subscriber<? super T> paramSubscriber, ReplayProcessor<T> paramReplayProcessor)
    {
      this.actual = paramSubscriber;
      this.state = paramReplayProcessor;
      this.requested = new AtomicLong();
    }
    
    public void cancel()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.state.remove(this);
      }
    }
    
    public void request(long paramLong)
    {
      if (SubscriptionHelper.validate(paramLong))
      {
        BackpressureHelper.add(this.requested, paramLong);
        this.state.buffer.replay(this);
      }
    }
  }
  
  static final class SizeAndTimeBoundReplayBuffer<T>
    extends AtomicReference<Object>
    implements ReplayProcessor.ReplayBuffer<T>
  {
    private static final long serialVersionUID = 1242561386470847675L;
    volatile boolean done;
    volatile ReplayProcessor.TimedNode<Object> head;
    final long maxAge;
    final int maxSize;
    final Scheduler scheduler;
    int size;
    ReplayProcessor.TimedNode<Object> tail;
    final TimeUnit unit;
    
    SizeAndTimeBoundReplayBuffer(int paramInt, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      this.maxSize = ObjectHelper.verifyPositive(paramInt, "maxSize");
      this.maxAge = ObjectHelper.verifyPositive(paramLong, "maxAge");
      this.unit = ((TimeUnit)ObjectHelper.requireNonNull(paramTimeUnit, "unit is null"));
      this.scheduler = ((Scheduler)ObjectHelper.requireNonNull(paramScheduler, "scheduler is null"));
      paramTimeUnit = new ReplayProcessor.TimedNode(null, 0L);
      this.tail = paramTimeUnit;
      this.head = paramTimeUnit;
    }
    
    public void add(T paramT)
    {
      paramT = new ReplayProcessor.TimedNode(paramT, this.scheduler.now(this.unit));
      ReplayProcessor.TimedNode localTimedNode = this.tail;
      this.tail = paramT;
      this.size += 1;
      localTimedNode.set(paramT);
      trim();
    }
    
    public void addFinal(Object paramObject)
    {
      lazySet(paramObject);
      paramObject = new ReplayProcessor.TimedNode(paramObject, Long.MAX_VALUE);
      ReplayProcessor.TimedNode localTimedNode = this.tail;
      this.tail = ((ReplayProcessor.TimedNode)paramObject);
      this.size += 1;
      localTimedNode.set(paramObject);
      trimFinal();
      this.done = true;
    }
    
    public T getValue()
    {
      Object localObject1 = this.head;
      Object localObject2 = null;
      for (;;)
      {
        ReplayProcessor.TimedNode localTimedNode = (ReplayProcessor.TimedNode)((ReplayProcessor.TimedNode)localObject1).get();
        if (localTimedNode == null)
        {
          localObject1 = ((ReplayProcessor.TimedNode)localObject1).value;
          if (localObject1 == null) {
            return null;
          }
          if ((!NotificationLite.isComplete(localObject1)) && (!NotificationLite.isError(localObject1))) {
            return (T)localObject1;
          }
          return (T)((ReplayProcessor.TimedNode)localObject2).value;
        }
        localObject2 = localObject1;
        localObject1 = localTimedNode;
      }
    }
    
    public T[] getValues(T[] paramArrayOfT)
    {
      ReplayProcessor.TimedNode localTimedNode = this.head;
      int k = size();
      int j = 0;
      Object localObject2;
      if (k == 0)
      {
        localObject2 = paramArrayOfT;
        if (paramArrayOfT.length != 0)
        {
          paramArrayOfT[0] = null;
          localObject2 = paramArrayOfT;
        }
      }
      else
      {
        localObject2 = localTimedNode;
        int i = j;
        Object localObject1 = paramArrayOfT;
        if (paramArrayOfT.length < k)
        {
          localObject1 = (Object[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), k);
          i = j;
          localObject2 = localTimedNode;
        }
        while (i != k)
        {
          localObject2 = (ReplayProcessor.TimedNode)((ReplayProcessor.TimedNode)localObject2).get();
          localObject1[i] = ((ReplayProcessor.TimedNode)localObject2).value;
          i++;
        }
        localObject2 = localObject1;
        if (localObject1.length > k)
        {
          localObject1[k] = null;
          localObject2 = localObject1;
        }
      }
      return (T[])localObject2;
    }
    
    public void replay(ReplayProcessor.ReplaySubscription<T> paramReplaySubscription)
    {
      if (paramReplaySubscription.getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = paramReplaySubscription.actual;
      Object localObject1 = (ReplayProcessor.TimedNode)paramReplaySubscription.index;
      Object localObject2;
      Object localObject3;
      int i;
      if (localObject1 == null)
      {
        localObject1 = this.head;
        if (!this.done)
        {
          l2 = this.scheduler.now(this.unit);
          l1 = this.maxAge;
          for (localObject2 = (ReplayProcessor.TimedNode)((ReplayProcessor.TimedNode)localObject1).get(); (localObject2 != null) && (((ReplayProcessor.TimedNode)localObject2).time <= l2 - l1); localObject2 = localObject3)
          {
            localObject3 = (ReplayProcessor.TimedNode)((ReplayProcessor.TimedNode)localObject2).get();
            localObject1 = localObject2;
          }
          i = 1;
        }
        else
        {
          i = 1;
        }
      }
      else
      {
        i = 1;
      }
      long l1 = paramReplaySubscription.requested.get();
      long l2 = 0L;
      for (;;)
      {
        if (paramReplaySubscription.cancelled)
        {
          paramReplaySubscription.index = null;
          return;
        }
        localObject2 = (ReplayProcessor.TimedNode)((ReplayProcessor.TimedNode)localObject1).get();
        long l3;
        if (localObject2 != null)
        {
          localObject3 = ((ReplayProcessor.TimedNode)localObject2).value;
          if ((this.done) && (((ReplayProcessor.TimedNode)localObject2).get() == null))
          {
            if (NotificationLite.isComplete(localObject3)) {
              localSubscriber.onComplete();
            } else {
              localSubscriber.onError(NotificationLite.getError(localObject3));
            }
            paramReplaySubscription.index = null;
            paramReplaySubscription.cancelled = true;
            return;
          }
          l3 = l1;
          if (l1 == 0L)
          {
            l1 = paramReplaySubscription.requested.get() + l2;
            l3 = l1;
            if (l1 != 0L) {}
          }
        }
        else
        {
          if ((l2 != 0L) && (paramReplaySubscription.requested.get() != Long.MAX_VALUE)) {
            paramReplaySubscription.requested.addAndGet(l2);
          }
          paramReplaySubscription.index = localObject1;
          int j = paramReplaySubscription.addAndGet(-i);
          i = j;
          if (j != 0) {
            break;
          }
          return;
        }
        localSubscriber.onNext(localObject3);
        l1 = l3 - 1L;
        l2 -= 1L;
        localObject1 = localObject2;
      }
    }
    
    public int size()
    {
      Object localObject = this.head;
      int i = 0;
      int j;
      for (;;)
      {
        j = i;
        if (i == Integer.MAX_VALUE) {
          break;
        }
        ReplayProcessor.TimedNode localTimedNode = (ReplayProcessor.TimedNode)((ReplayProcessor.TimedNode)localObject).get();
        if (localTimedNode == null)
        {
          localObject = ((ReplayProcessor.TimedNode)localObject).value;
          if (!NotificationLite.isComplete(localObject))
          {
            j = i;
            if (!NotificationLite.isError(localObject)) {
              break;
            }
          }
          j = i - 1;
          break;
        }
        i++;
        localObject = localTimedNode;
      }
      return j;
    }
    
    void trim()
    {
      int i = this.size;
      if (i > this.maxSize)
      {
        this.size = (i - 1);
        this.head = ((ReplayProcessor.TimedNode)this.head.get());
      }
      long l2 = this.scheduler.now(this.unit);
      long l1 = this.maxAge;
      ReplayProcessor.TimedNode localTimedNode;
      for (Object localObject = this.head;; localObject = localTimedNode)
      {
        localTimedNode = (ReplayProcessor.TimedNode)((ReplayProcessor.TimedNode)localObject).get();
        if (localTimedNode == null)
        {
          this.head = ((ReplayProcessor.TimedNode)localObject);
        }
        else
        {
          if (localTimedNode.time <= l2 - l1) {
            continue;
          }
          this.head = ((ReplayProcessor.TimedNode)localObject);
        }
        return;
      }
    }
    
    void trimFinal()
    {
      long l1 = this.scheduler.now(this.unit);
      long l2 = this.maxAge;
      ReplayProcessor.TimedNode localTimedNode;
      for (Object localObject = this.head;; localObject = localTimedNode)
      {
        localTimedNode = (ReplayProcessor.TimedNode)((ReplayProcessor.TimedNode)localObject).get();
        if (localTimedNode.get() == null)
        {
          this.head = ((ReplayProcessor.TimedNode)localObject);
        }
        else
        {
          if (localTimedNode.time <= l1 - l2) {
            continue;
          }
          this.head = ((ReplayProcessor.TimedNode)localObject);
        }
        return;
      }
    }
  }
  
  static final class SizeBoundReplayBuffer<T>
    extends AtomicReference<Object>
    implements ReplayProcessor.ReplayBuffer<T>
  {
    private static final long serialVersionUID = 3027920763113911982L;
    volatile boolean done;
    volatile ReplayProcessor.Node<Object> head;
    final int maxSize;
    int size;
    ReplayProcessor.Node<Object> tail;
    
    SizeBoundReplayBuffer(int paramInt)
    {
      this.maxSize = ObjectHelper.verifyPositive(paramInt, "maxSize");
      ReplayProcessor.Node localNode = new ReplayProcessor.Node(null);
      this.tail = localNode;
      this.head = localNode;
    }
    
    public void add(T paramT)
    {
      ReplayProcessor.Node localNode = new ReplayProcessor.Node(paramT);
      paramT = this.tail;
      this.tail = localNode;
      this.size += 1;
      paramT.set(localNode);
      trim();
    }
    
    public void addFinal(Object paramObject)
    {
      lazySet(paramObject);
      paramObject = new ReplayProcessor.Node(paramObject);
      ReplayProcessor.Node localNode = this.tail;
      this.tail = ((ReplayProcessor.Node)paramObject);
      this.size += 1;
      localNode.set(paramObject);
      this.done = true;
    }
    
    public T getValue()
    {
      Object localObject1 = this.head;
      Object localObject2 = null;
      for (;;)
      {
        ReplayProcessor.Node localNode = (ReplayProcessor.Node)((ReplayProcessor.Node)localObject1).get();
        if (localNode == null)
        {
          localObject1 = ((ReplayProcessor.Node)localObject1).value;
          if (localObject1 == null) {
            return null;
          }
          if ((!NotificationLite.isComplete(localObject1)) && (!NotificationLite.isError(localObject1))) {
            return (T)localObject1;
          }
          return (T)((ReplayProcessor.Node)localObject2).value;
        }
        localObject2 = localObject1;
        localObject1 = localNode;
      }
    }
    
    public T[] getValues(T[] paramArrayOfT)
    {
      ReplayProcessor.Node localNode = this.head;
      int k = size();
      int j = 0;
      Object localObject2;
      if (k == 0)
      {
        localObject2 = paramArrayOfT;
        if (paramArrayOfT.length != 0)
        {
          paramArrayOfT[0] = null;
          localObject2 = paramArrayOfT;
        }
      }
      else
      {
        localObject2 = localNode;
        int i = j;
        Object localObject1 = paramArrayOfT;
        if (paramArrayOfT.length < k)
        {
          localObject1 = (Object[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), k);
          i = j;
          localObject2 = localNode;
        }
        while (i != k)
        {
          localObject2 = (ReplayProcessor.Node)((ReplayProcessor.Node)localObject2).get();
          localObject1[i] = ((ReplayProcessor.Node)localObject2).value;
          i++;
        }
        localObject2 = localObject1;
        if (localObject1.length > k)
        {
          localObject1[k] = null;
          localObject2 = localObject1;
        }
      }
      return (T[])localObject2;
    }
    
    public void replay(ReplayProcessor.ReplaySubscription<T> paramReplaySubscription)
    {
      if (paramReplaySubscription.getAndIncrement() != 0) {
        return;
      }
      Subscriber localSubscriber = paramReplaySubscription.actual;
      Object localObject1 = (ReplayProcessor.Node)paramReplaySubscription.index;
      int i;
      if (localObject1 == null)
      {
        localObject1 = this.head;
        i = 1;
      }
      else
      {
        i = 1;
      }
      long l1 = paramReplaySubscription.requested.get();
      long l2 = 0L;
      for (;;)
      {
        if (paramReplaySubscription.cancelled)
        {
          paramReplaySubscription.index = null;
          return;
        }
        ReplayProcessor.Node localNode = (ReplayProcessor.Node)((ReplayProcessor.Node)localObject1).get();
        Object localObject2;
        long l3;
        if (localNode != null)
        {
          localObject2 = localNode.value;
          if ((this.done) && (localNode.get() == null))
          {
            if (NotificationLite.isComplete(localObject2)) {
              localSubscriber.onComplete();
            } else {
              localSubscriber.onError(NotificationLite.getError(localObject2));
            }
            paramReplaySubscription.index = null;
            paramReplaySubscription.cancelled = true;
            return;
          }
          l3 = l1;
          if (l1 == 0L)
          {
            l1 = paramReplaySubscription.requested.get() + l2;
            l3 = l1;
            if (l1 != 0L) {}
          }
        }
        else
        {
          if ((l2 != 0L) && (paramReplaySubscription.requested.get() != Long.MAX_VALUE)) {
            paramReplaySubscription.requested.addAndGet(l2);
          }
          paramReplaySubscription.index = localObject1;
          int j = paramReplaySubscription.addAndGet(-i);
          i = j;
          if (j != 0) {
            break;
          }
          return;
        }
        localSubscriber.onNext(localObject2);
        l1 = l3 - 1L;
        l2 -= 1L;
        localObject1 = localNode;
      }
    }
    
    public int size()
    {
      Object localObject = this.head;
      int i = 0;
      int j;
      for (;;)
      {
        j = i;
        if (i == Integer.MAX_VALUE) {
          break;
        }
        ReplayProcessor.Node localNode = (ReplayProcessor.Node)((ReplayProcessor.Node)localObject).get();
        if (localNode == null)
        {
          localObject = ((ReplayProcessor.Node)localObject).value;
          if (!NotificationLite.isComplete(localObject))
          {
            j = i;
            if (!NotificationLite.isError(localObject)) {
              break;
            }
          }
          j = i - 1;
          break;
        }
        i++;
        localObject = localNode;
      }
      return j;
    }
    
    void trim()
    {
      int i = this.size;
      if (i > this.maxSize)
      {
        this.size = (i - 1);
        this.head = ((ReplayProcessor.Node)this.head.get());
      }
    }
  }
  
  static final class TimedNode<T>
    extends AtomicReference<TimedNode<T>>
  {
    private static final long serialVersionUID = 6404226426336033100L;
    final long time;
    final T value;
    
    TimedNode(T paramT, long paramLong)
    {
      this.value = paramT;
      this.time = paramLong;
    }
  }
  
  static final class UnboundedReplayBuffer<T>
    extends AtomicReference<Object>
    implements ReplayProcessor.ReplayBuffer<T>
  {
    private static final long serialVersionUID = -4457200895834877300L;
    final List<Object> buffer;
    volatile boolean done;
    volatile int size;
    
    UnboundedReplayBuffer(int paramInt)
    {
      this.buffer = new ArrayList(ObjectHelper.verifyPositive(paramInt, "capacityHint"));
    }
    
    public void add(T paramT)
    {
      this.buffer.add(paramT);
      this.size += 1;
    }
    
    public void addFinal(Object paramObject)
    {
      lazySet(paramObject);
      this.buffer.add(paramObject);
      this.size += 1;
      this.done = true;
    }
    
    public T getValue()
    {
      int i = this.size;
      if (i != 0)
      {
        List localList = this.buffer;
        Object localObject = localList.get(i - 1);
        if ((!NotificationLite.isComplete(localObject)) && (!NotificationLite.isError(localObject))) {
          return (T)localObject;
        }
        if (i == 1) {
          return null;
        }
        return (T)localList.get(i - 2);
      }
      return null;
    }
    
    public T[] getValues(T[] paramArrayOfT)
    {
      int j = this.size;
      int k = 0;
      if (j == 0)
      {
        if (paramArrayOfT.length != 0) {
          paramArrayOfT[0] = null;
        }
        return paramArrayOfT;
      }
      List localList = this.buffer;
      Object localObject = localList.get(j - 1);
      int i;
      if (!NotificationLite.isComplete(localObject))
      {
        i = j;
        if (!NotificationLite.isError(localObject)) {}
      }
      else
      {
        j--;
        i = j;
        if (j == 0)
        {
          if (paramArrayOfT.length != 0) {
            paramArrayOfT[0] = null;
          }
          return paramArrayOfT;
        }
      }
      j = k;
      localObject = paramArrayOfT;
      if (paramArrayOfT.length < i) {
        localObject = (Object[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), i);
      }
      for (j = k; j < i; j++) {
        localObject[j] = localList.get(j);
      }
      if (localObject.length > i) {
        localObject[i] = null;
      }
      return (T[])localObject;
    }
    
    public void replay(ReplayProcessor.ReplaySubscription<T> paramReplaySubscription)
    {
      if (paramReplaySubscription.getAndIncrement() != 0) {
        return;
      }
      List localList = this.buffer;
      Subscriber localSubscriber = paramReplaySubscription.actual;
      Object localObject = (Integer)paramReplaySubscription.index;
      int j = 0;
      int i;
      if (localObject != null)
      {
        j = ((Integer)localObject).intValue();
        i = 1;
      }
      else
      {
        paramReplaySubscription.index = Integer.valueOf(0);
        i = 1;
      }
      int k;
      do
      {
        long l3;
        do
        {
          if (paramReplaySubscription.cancelled)
          {
            paramReplaySubscription.index = null;
            return;
          }
          k = this.size;
          long l1 = paramReplaySubscription.requested.get();
          long l2 = 0L;
          while (k != j)
          {
            if (paramReplaySubscription.cancelled)
            {
              paramReplaySubscription.index = null;
              return;
            }
            localObject = localList.get(j);
            if (this.done)
            {
              int m = j + 1;
              if (m == k)
              {
                k = this.size;
                if (m == k)
                {
                  if (NotificationLite.isComplete(localObject)) {
                    localSubscriber.onComplete();
                  } else {
                    localSubscriber.onError(NotificationLite.getError(localObject));
                  }
                  paramReplaySubscription.index = null;
                  paramReplaySubscription.cancelled = true;
                  return;
                }
              }
            }
            l3 = l1;
            if (l1 == 0L)
            {
              l1 = paramReplaySubscription.requested.get() + l2;
              l3 = l1;
              if (l1 == 0L) {
                break;
              }
            }
            localSubscriber.onNext(localObject);
            l1 = l3 - 1L;
            l2 -= 1L;
            j++;
          }
          l3 = l1;
          if (l2 != 0L)
          {
            l3 = l1;
            if (paramReplaySubscription.requested.get() != Long.MAX_VALUE) {
              l3 = paramReplaySubscription.requested.addAndGet(l2);
            }
          }
        } while ((j != this.size) && (l3 != 0L));
        paramReplaySubscription.index = Integer.valueOf(j);
        k = paramReplaySubscription.addAndGet(-i);
        i = k;
      } while (k != 0);
    }
    
    public int size()
    {
      int i = this.size;
      if (i != 0)
      {
        Object localObject = this.buffer;
        int j = i - 1;
        localObject = ((List)localObject).get(j);
        if ((!NotificationLite.isComplete(localObject)) && (!NotificationLite.isError(localObject))) {
          return i;
        }
        return j;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/processors/ReplayProcessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */