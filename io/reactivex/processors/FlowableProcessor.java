package io.reactivex.processors;

import io.reactivex.Flowable;
import org.reactivestreams.Processor;

public abstract class FlowableProcessor<T>
  extends Flowable<T>
  implements Processor<T, T>
{
  public abstract Throwable getThrowable();
  
  public abstract boolean hasComplete();
  
  public abstract boolean hasSubscribers();
  
  public abstract boolean hasThrowable();
  
  public final FlowableProcessor<T> toSerialized()
  {
    if ((this instanceof SerializedProcessor)) {
      return this;
    }
    return new SerializedProcessor(this);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/processors/FlowableProcessor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */