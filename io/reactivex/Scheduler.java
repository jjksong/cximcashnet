package io.reactivex;

import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Function;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.disposables.SequentialDisposable;
import io.reactivex.internal.schedulers.SchedulerWhen;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.TimeUnit;

public abstract class Scheduler
{
  static final long CLOCK_DRIFT_TOLERANCE_NANOSECONDS = TimeUnit.MINUTES.toNanos(Long.getLong("rx2.scheduler.drift-tolerance", 15L).longValue());
  
  public static long clockDriftTolerance()
  {
    return CLOCK_DRIFT_TOLERANCE_NANOSECONDS;
  }
  
  public abstract Worker createWorker();
  
  public long now(TimeUnit paramTimeUnit)
  {
    return paramTimeUnit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
  }
  
  public Disposable scheduleDirect(Runnable paramRunnable)
  {
    return scheduleDirect(paramRunnable, 0L, TimeUnit.NANOSECONDS);
  }
  
  public Disposable scheduleDirect(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit)
  {
    final Worker localWorker = createWorker();
    localWorker.schedule(new Runnable()
    {
      public void run()
      {
        try
        {
          this.val$decoratedRun.run();
          return;
        }
        finally
        {
          localWorker.dispose();
        }
      }
    }, paramLong, paramTimeUnit);
    return localWorker;
  }
  
  public Disposable schedulePeriodicallyDirect(Runnable paramRunnable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
  {
    Worker localWorker = createWorker();
    paramRunnable = new PeriodicDirectTask(RxJavaPlugins.onSchedule(paramRunnable), localWorker);
    paramTimeUnit = localWorker.schedulePeriodically(paramRunnable, paramLong1, paramLong2, paramTimeUnit);
    if (paramTimeUnit == EmptyDisposable.INSTANCE) {
      return paramTimeUnit;
    }
    return paramRunnable;
  }
  
  public void shutdown() {}
  
  public void start() {}
  
  @Experimental
  public <S extends Scheduler,  extends Disposable> S when(Function<Flowable<Flowable<Completable>>, Completable> paramFunction)
  {
    return new SchedulerWhen(paramFunction, this);
  }
  
  static class PeriodicDirectTask
    implements Runnable, Disposable
  {
    volatile boolean disposed;
    final Runnable run;
    final Scheduler.Worker worker;
    
    PeriodicDirectTask(Runnable paramRunnable, Scheduler.Worker paramWorker)
    {
      this.run = paramRunnable;
      this.worker = paramWorker;
    }
    
    public void dispose()
    {
      this.disposed = true;
      this.worker.dispose();
    }
    
    public boolean isDisposed()
    {
      return this.disposed;
    }
    
    public void run()
    {
      if (!this.disposed) {
        try
        {
          this.run.run();
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          this.worker.dispose();
          throw ExceptionHelper.wrapOrThrow(localThrowable);
        }
      }
    }
  }
  
  public static abstract class Worker
    implements Disposable
  {
    public long now(TimeUnit paramTimeUnit)
    {
      return paramTimeUnit.convert(System.currentTimeMillis(), TimeUnit.MILLISECONDS);
    }
    
    public Disposable schedule(Runnable paramRunnable)
    {
      return schedule(paramRunnable, 0L, TimeUnit.NANOSECONDS);
    }
    
    public abstract Disposable schedule(Runnable paramRunnable, long paramLong, TimeUnit paramTimeUnit);
    
    public Disposable schedulePeriodically(Runnable paramRunnable, long paramLong1, long paramLong2, TimeUnit paramTimeUnit)
    {
      SequentialDisposable localSequentialDisposable2 = new SequentialDisposable();
      SequentialDisposable localSequentialDisposable1 = new SequentialDisposable(localSequentialDisposable2);
      paramRunnable = RxJavaPlugins.onSchedule(paramRunnable);
      long l = paramTimeUnit.toNanos(paramLong2);
      paramLong2 = now(TimeUnit.NANOSECONDS);
      paramRunnable = schedule(new PeriodicTask(paramLong2 + paramTimeUnit.toNanos(paramLong1), paramRunnable, paramLong2, localSequentialDisposable1, l), paramLong1, paramTimeUnit);
      if (paramRunnable == EmptyDisposable.INSTANCE) {
        return paramRunnable;
      }
      localSequentialDisposable2.replace(paramRunnable);
      return localSequentialDisposable1;
    }
    
    final class PeriodicTask
      implements Runnable
    {
      long count;
      final Runnable decoratedRun;
      long lastNowNanoseconds;
      final long periodInNanoseconds;
      final SequentialDisposable sd;
      long startInNanoseconds;
      
      PeriodicTask(long paramLong1, Runnable paramRunnable, long paramLong2, SequentialDisposable paramSequentialDisposable, long paramLong3)
      {
        this.decoratedRun = paramRunnable;
        this.sd = paramSequentialDisposable;
        this.periodInNanoseconds = paramLong3;
        this.lastNowNanoseconds = paramLong2;
        this.startInNanoseconds = paramLong1;
      }
      
      public void run()
      {
        this.decoratedRun.run();
        if (!this.sd.isDisposed())
        {
          long l2 = Scheduler.Worker.this.now(TimeUnit.NANOSECONDS);
          long l3 = Scheduler.CLOCK_DRIFT_TOLERANCE_NANOSECONDS;
          long l1 = this.lastNowNanoseconds;
          if ((l3 + l2 >= l1) && (l2 < l1 + this.periodInNanoseconds + Scheduler.CLOCK_DRIFT_TOLERANCE_NANOSECONDS))
          {
            l3 = this.startInNanoseconds;
            l1 = this.count + 1L;
            this.count = l1;
            l1 = l3 + l1 * this.periodInNanoseconds;
          }
          else
          {
            l3 = this.periodInNanoseconds;
            l1 = l2 + l3;
            long l4 = this.count + 1L;
            this.count = l4;
            this.startInNanoseconds = (l1 - l3 * l4);
          }
          this.lastNowNanoseconds = l2;
          this.sd.replace(Scheduler.Worker.this.schedule(this, l1 - l2, TimeUnit.NANOSECONDS));
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/Scheduler.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */