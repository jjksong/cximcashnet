package io.reactivex.parallel;

import io.reactivex.Flowable;
import io.reactivex.Scheduler;
import io.reactivex.annotations.BackpressureKind;
import io.reactivex.annotations.BackpressureSupport;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.annotations.Experimental;
import io.reactivex.annotations.SchedulerSupport;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.functions.Action;
import io.reactivex.functions.BiConsumer;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.functions.LongConsumer;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.operators.parallel.ParallelCollect;
import io.reactivex.internal.operators.parallel.ParallelConcatMap;
import io.reactivex.internal.operators.parallel.ParallelFilter;
import io.reactivex.internal.operators.parallel.ParallelFlatMap;
import io.reactivex.internal.operators.parallel.ParallelFromArray;
import io.reactivex.internal.operators.parallel.ParallelFromPublisher;
import io.reactivex.internal.operators.parallel.ParallelJoin;
import io.reactivex.internal.operators.parallel.ParallelMap;
import io.reactivex.internal.operators.parallel.ParallelPeek;
import io.reactivex.internal.operators.parallel.ParallelReduce;
import io.reactivex.internal.operators.parallel.ParallelReduceFull;
import io.reactivex.internal.operators.parallel.ParallelRunOn;
import io.reactivex.internal.operators.parallel.ParallelSortedJoin;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.util.ErrorMode;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.ListAddBiConsumer;
import io.reactivex.internal.util.MergerBiFunction;
import io.reactivex.internal.util.SorterFunction;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;
import org.reactivestreams.Publisher;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

@Experimental
public abstract class ParallelFlowable<T>
{
  @CheckReturnValue
  public static <T> ParallelFlowable<T> from(Publisher<? extends T> paramPublisher)
  {
    return from(paramPublisher, Runtime.getRuntime().availableProcessors(), Flowable.bufferSize());
  }
  
  @CheckReturnValue
  public static <T> ParallelFlowable<T> from(Publisher<? extends T> paramPublisher, int paramInt)
  {
    return from(paramPublisher, paramInt, Flowable.bufferSize());
  }
  
  @CheckReturnValue
  public static <T> ParallelFlowable<T> from(Publisher<? extends T> paramPublisher, int paramInt1, int paramInt2)
  {
    ObjectHelper.requireNonNull(paramPublisher, "source");
    ObjectHelper.verifyPositive(paramInt1, "parallelism");
    ObjectHelper.verifyPositive(paramInt2, "prefetch");
    return new ParallelFromPublisher(paramPublisher, paramInt1, paramInt2);
  }
  
  @CheckReturnValue
  public static <T> ParallelFlowable<T> fromArray(Publisher<T>... paramVarArgs)
  {
    if (paramVarArgs.length != 0) {
      return new ParallelFromArray(paramVarArgs);
    }
    throw new IllegalArgumentException("Zero publishers not supported");
  }
  
  @CheckReturnValue
  public final <C> ParallelFlowable<C> collect(Callable<? extends C> paramCallable, BiConsumer<? super C, ? super T> paramBiConsumer)
  {
    return new ParallelCollect(this, paramCallable, paramBiConsumer);
  }
  
  @CheckReturnValue
  public final <U> ParallelFlowable<U> compose(Function<? super ParallelFlowable<T>, ParallelFlowable<U>> paramFunction)
  {
    return (ParallelFlowable)to(paramFunction);
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> concatMap(Function<? super T, ? extends Publisher<? extends R>> paramFunction)
  {
    return concatMap(paramFunction, 2);
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> concatMap(Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt)
  {
    return new ParallelConcatMap(this, paramFunction, paramInt, ErrorMode.IMMEDIATE);
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> concatMapDelayError(Function<? super T, ? extends Publisher<? extends R>> paramFunction, int paramInt, boolean paramBoolean)
  {
    ErrorMode localErrorMode;
    if (paramBoolean) {
      localErrorMode = ErrorMode.END;
    } else {
      localErrorMode = ErrorMode.BOUNDARY;
    }
    return new ParallelConcatMap(this, paramFunction, paramInt, localErrorMode);
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> concatMapDelayError(Function<? super T, ? extends Publisher<? extends R>> paramFunction, boolean paramBoolean)
  {
    return concatMapDelayError(paramFunction, 2, paramBoolean);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> doAfterNext(Consumer<? super T> paramConsumer)
  {
    return new ParallelPeek(this, Functions.emptyConsumer(), paramConsumer, Functions.emptyConsumer(), Functions.EMPTY_ACTION, Functions.EMPTY_ACTION, Functions.emptyConsumer(), Functions.EMPTY_LONG_CONSUMER, Functions.EMPTY_ACTION);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> doAfterTerminated(Action paramAction)
  {
    return new ParallelPeek(this, Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.EMPTY_ACTION, paramAction, Functions.emptyConsumer(), Functions.EMPTY_LONG_CONSUMER, Functions.EMPTY_ACTION);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> doOnCancel(Action paramAction)
  {
    return new ParallelPeek(this, Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.EMPTY_ACTION, Functions.EMPTY_ACTION, Functions.emptyConsumer(), Functions.EMPTY_LONG_CONSUMER, paramAction);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> doOnComplete(Action paramAction)
  {
    return new ParallelPeek(this, Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.emptyConsumer(), paramAction, Functions.EMPTY_ACTION, Functions.emptyConsumer(), Functions.EMPTY_LONG_CONSUMER, Functions.EMPTY_ACTION);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> doOnError(Consumer<Throwable> paramConsumer)
  {
    return new ParallelPeek(this, Functions.emptyConsumer(), Functions.emptyConsumer(), paramConsumer, Functions.EMPTY_ACTION, Functions.EMPTY_ACTION, Functions.emptyConsumer(), Functions.EMPTY_LONG_CONSUMER, Functions.EMPTY_ACTION);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> doOnNext(Consumer<? super T> paramConsumer)
  {
    return new ParallelPeek(this, paramConsumer, Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.EMPTY_ACTION, Functions.EMPTY_ACTION, Functions.emptyConsumer(), Functions.EMPTY_LONG_CONSUMER, Functions.EMPTY_ACTION);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> doOnRequest(LongConsumer paramLongConsumer)
  {
    return new ParallelPeek(this, Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.EMPTY_ACTION, Functions.EMPTY_ACTION, Functions.emptyConsumer(), paramLongConsumer, Functions.EMPTY_ACTION);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> doOnSubscribe(Consumer<? super Subscription> paramConsumer)
  {
    return new ParallelPeek(this, Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.emptyConsumer(), Functions.EMPTY_ACTION, Functions.EMPTY_ACTION, paramConsumer, Functions.EMPTY_LONG_CONSUMER, Functions.EMPTY_ACTION);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> filter(Predicate<? super T> paramPredicate)
  {
    ObjectHelper.requireNonNull(paramPredicate, "predicate");
    return new ParallelFilter(this, paramPredicate);
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> flatMap(Function<? super T, ? extends Publisher<? extends R>> paramFunction)
  {
    return flatMap(paramFunction, false, Integer.MAX_VALUE, Flowable.bufferSize());
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> flatMap(Function<? super T, ? extends Publisher<? extends R>> paramFunction, boolean paramBoolean)
  {
    return flatMap(paramFunction, paramBoolean, Integer.MAX_VALUE, Flowable.bufferSize());
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> flatMap(Function<? super T, ? extends Publisher<? extends R>> paramFunction, boolean paramBoolean, int paramInt)
  {
    return flatMap(paramFunction, paramBoolean, paramInt, Flowable.bufferSize());
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> flatMap(Function<? super T, ? extends Publisher<? extends R>> paramFunction, boolean paramBoolean, int paramInt1, int paramInt2)
  {
    return new ParallelFlatMap(this, paramFunction, paramBoolean, paramInt1, paramInt2);
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> map(Function<? super T, ? extends R> paramFunction)
  {
    ObjectHelper.requireNonNull(paramFunction, "mapper");
    return new ParallelMap(this, paramFunction);
  }
  
  public abstract int parallelism();
  
  @CheckReturnValue
  public final Flowable<T> reduce(BiFunction<T, T, T> paramBiFunction)
  {
    ObjectHelper.requireNonNull(paramBiFunction, "reducer");
    return RxJavaPlugins.onAssembly(new ParallelReduceFull(this, paramBiFunction));
  }
  
  @CheckReturnValue
  public final <R> ParallelFlowable<R> reduce(Callable<R> paramCallable, BiFunction<R, ? super T, R> paramBiFunction)
  {
    ObjectHelper.requireNonNull(paramCallable, "initialSupplier");
    ObjectHelper.requireNonNull(paramBiFunction, "reducer");
    return new ParallelReduce(this, paramCallable, paramBiFunction);
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> runOn(Scheduler paramScheduler)
  {
    return runOn(paramScheduler, Flowable.bufferSize());
  }
  
  @CheckReturnValue
  public final ParallelFlowable<T> runOn(Scheduler paramScheduler, int paramInt)
  {
    ObjectHelper.requireNonNull(paramScheduler, "scheduler");
    ObjectHelper.verifyPositive(paramInt, "prefetch");
    return new ParallelRunOn(this, paramScheduler, paramInt);
  }
  
  @BackpressureSupport(BackpressureKind.FULL)
  @CheckReturnValue
  @SchedulerSupport("none")
  public final Flowable<T> sequential()
  {
    return sequential(Flowable.bufferSize());
  }
  
  @BackpressureSupport(BackpressureKind.FULL)
  @CheckReturnValue
  @SchedulerSupport("none")
  public final Flowable<T> sequential(int paramInt)
  {
    ObjectHelper.verifyPositive(paramInt, "prefetch");
    return RxJavaPlugins.onAssembly(new ParallelJoin(this, paramInt));
  }
  
  @CheckReturnValue
  public final Flowable<T> sorted(Comparator<? super T> paramComparator)
  {
    return sorted(paramComparator, 16);
  }
  
  @CheckReturnValue
  public final Flowable<T> sorted(Comparator<? super T> paramComparator, int paramInt)
  {
    return RxJavaPlugins.onAssembly(new ParallelSortedJoin(reduce(Functions.createArrayList(paramInt / parallelism() + 1), ListAddBiConsumer.instance()).map(new SorterFunction(paramComparator)), paramComparator));
  }
  
  public abstract void subscribe(Subscriber<? super T>[] paramArrayOfSubscriber);
  
  @CheckReturnValue
  public final <U> U to(Function<? super ParallelFlowable<T>, U> paramFunction)
  {
    try
    {
      paramFunction = paramFunction.apply(this);
      return paramFunction;
    }
    catch (Throwable paramFunction)
    {
      Exceptions.throwIfFatal(paramFunction);
      throw ExceptionHelper.wrapOrThrow(paramFunction);
    }
  }
  
  @CheckReturnValue
  public final Flowable<List<T>> toSortedList(Comparator<? super T> paramComparator)
  {
    return toSortedList(paramComparator, 16);
  }
  
  @CheckReturnValue
  public final Flowable<List<T>> toSortedList(Comparator<? super T> paramComparator, int paramInt)
  {
    return RxJavaPlugins.onAssembly(reduce(Functions.createArrayList(paramInt / parallelism() + 1), ListAddBiConsumer.instance()).map(new SorterFunction(paramComparator)).reduce(new MergerBiFunction(paramComparator)));
  }
  
  protected final boolean validate(Subscriber<?>[] paramArrayOfSubscriber)
  {
    int i = parallelism();
    if (paramArrayOfSubscriber.length != i)
    {
      Object localObject = new StringBuilder();
      ((StringBuilder)localObject).append("parallelism = ");
      ((StringBuilder)localObject).append(i);
      ((StringBuilder)localObject).append(", subscribers = ");
      ((StringBuilder)localObject).append(paramArrayOfSubscriber.length);
      localObject = new IllegalArgumentException(((StringBuilder)localObject).toString());
      int j = paramArrayOfSubscriber.length;
      for (i = 0; i < j; i++) {
        EmptySubscription.error((Throwable)localObject, paramArrayOfSubscriber[i]);
      }
      return false;
    }
    return true;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/parallel/ParallelFlowable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */