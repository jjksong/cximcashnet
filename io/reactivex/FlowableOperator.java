package io.reactivex;

import org.reactivestreams.Subscriber;

public abstract interface FlowableOperator<Downstream, Upstream>
{
  public abstract Subscriber<? super Upstream> apply(Subscriber<? super Downstream> paramSubscriber)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/FlowableOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */