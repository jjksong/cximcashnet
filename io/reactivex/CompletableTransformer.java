package io.reactivex;

public abstract interface CompletableTransformer
{
  public abstract CompletableSource apply(Completable paramCompletable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/CompletableTransformer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */