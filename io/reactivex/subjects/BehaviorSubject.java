package io.reactivex.subjects;

import io.reactivex.Observer;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList.NonThrowingPredicate;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.lang.reflect.Array;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public final class BehaviorSubject<T>
  extends Subject<T>
{
  static final BehaviorDisposable[] EMPTY = new BehaviorDisposable[0];
  private static final Object[] EMPTY_ARRAY = new Object[0];
  static final BehaviorDisposable[] TERMINATED = new BehaviorDisposable[0];
  boolean done;
  long index;
  final ReadWriteLock lock = new ReentrantReadWriteLock();
  final Lock readLock = this.lock.readLock();
  final AtomicReference<BehaviorDisposable<T>[]> subscribers = new AtomicReference(EMPTY);
  final AtomicReference<Object> value = new AtomicReference();
  final Lock writeLock = this.lock.writeLock();
  
  BehaviorSubject() {}
  
  BehaviorSubject(T paramT)
  {
    this();
    this.value.lazySet(ObjectHelper.requireNonNull(paramT, "defaultValue is null"));
  }
  
  @CheckReturnValue
  public static <T> BehaviorSubject<T> create()
  {
    return new BehaviorSubject();
  }
  
  @CheckReturnValue
  public static <T> BehaviorSubject<T> createDefault(T paramT)
  {
    return new BehaviorSubject(paramT);
  }
  
  boolean add(BehaviorDisposable<T> paramBehaviorDisposable)
  {
    BehaviorDisposable[] arrayOfBehaviorDisposable1;
    BehaviorDisposable[] arrayOfBehaviorDisposable2;
    do
    {
      arrayOfBehaviorDisposable1 = (BehaviorDisposable[])this.subscribers.get();
      if (arrayOfBehaviorDisposable1 == TERMINATED) {
        return false;
      }
      int i = arrayOfBehaviorDisposable1.length;
      arrayOfBehaviorDisposable2 = new BehaviorDisposable[i + 1];
      System.arraycopy(arrayOfBehaviorDisposable1, 0, arrayOfBehaviorDisposable2, 0, i);
      arrayOfBehaviorDisposable2[i] = paramBehaviorDisposable;
    } while (!this.subscribers.compareAndSet(arrayOfBehaviorDisposable1, arrayOfBehaviorDisposable2));
    return true;
  }
  
  public Throwable getThrowable()
  {
    Object localObject = this.value.get();
    if (NotificationLite.isError(localObject)) {
      return NotificationLite.getError(localObject);
    }
    return null;
  }
  
  public T getValue()
  {
    Object localObject = this.value.get();
    if ((!NotificationLite.isComplete(localObject)) && (!NotificationLite.isError(localObject))) {
      return (T)NotificationLite.getValue(localObject);
    }
    return null;
  }
  
  public Object[] getValues()
  {
    Object[] arrayOfObject = getValues((Object[])EMPTY_ARRAY);
    if (arrayOfObject == EMPTY_ARRAY) {
      return new Object[0];
    }
    return arrayOfObject;
  }
  
  public T[] getValues(T[] paramArrayOfT)
  {
    Object localObject1 = this.value.get();
    if ((localObject1 != null) && (!NotificationLite.isComplete(localObject1)) && (!NotificationLite.isError(localObject1)))
    {
      Object localObject2 = NotificationLite.getValue(localObject1);
      if (paramArrayOfT.length != 0)
      {
        paramArrayOfT[0] = localObject2;
        localObject1 = paramArrayOfT;
        if (paramArrayOfT.length != 1)
        {
          paramArrayOfT[1] = null;
          localObject1 = paramArrayOfT;
        }
      }
      else
      {
        localObject1 = (Object[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), 1);
        localObject1[0] = localObject2;
      }
      return (T[])localObject1;
    }
    if (paramArrayOfT.length != 0) {
      paramArrayOfT[0] = null;
    }
    return paramArrayOfT;
  }
  
  public boolean hasComplete()
  {
    return NotificationLite.isComplete(this.value.get());
  }
  
  public boolean hasObservers()
  {
    boolean bool;
    if (((BehaviorDisposable[])this.subscribers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    return NotificationLite.isError(this.value.get());
  }
  
  public boolean hasValue()
  {
    Object localObject = this.value.get();
    boolean bool;
    if ((localObject != null) && (!NotificationLite.isComplete(localObject)) && (!NotificationLite.isError(localObject))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    this.done = true;
    Object localObject = NotificationLite.complete();
    BehaviorDisposable[] arrayOfBehaviorDisposable = terminate(localObject);
    int j = arrayOfBehaviorDisposable.length;
    for (int i = 0; i < j; i++) {
      arrayOfBehaviorDisposable[i].emitNext(localObject, this.index);
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    if (this.done)
    {
      RxJavaPlugins.onError((Throwable)localObject);
      return;
    }
    this.done = true;
    paramThrowable = NotificationLite.error((Throwable)localObject);
    localObject = terminate(paramThrowable);
    int j = localObject.length;
    for (int i = 0; i < j; i++) {
      localObject[i].emitNext(paramThrowable, this.index);
    }
  }
  
  public void onNext(T paramT)
  {
    if (paramT == null)
    {
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
      return;
    }
    if (this.done) {
      return;
    }
    Object localObject = NotificationLite.next(paramT);
    setCurrent(localObject);
    paramT = (BehaviorDisposable[])this.subscribers.get();
    int j = paramT.length;
    for (int i = 0; i < j; i++) {
      paramT[i].emitNext(localObject, this.index);
    }
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (this.done) {
      paramDisposable.dispose();
    }
  }
  
  void remove(BehaviorDisposable<T> paramBehaviorDisposable)
  {
    BehaviorDisposable[] arrayOfBehaviorDisposable2;
    BehaviorDisposable[] arrayOfBehaviorDisposable1;
    do
    {
      arrayOfBehaviorDisposable2 = (BehaviorDisposable[])this.subscribers.get();
      if ((arrayOfBehaviorDisposable2 == TERMINATED) || (arrayOfBehaviorDisposable2 == EMPTY)) {
        break;
      }
      int m = arrayOfBehaviorDisposable2.length;
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfBehaviorDisposable2[i] == paramBehaviorDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfBehaviorDisposable1 = EMPTY;
      }
      else
      {
        arrayOfBehaviorDisposable1 = new BehaviorDisposable[m - 1];
        System.arraycopy(arrayOfBehaviorDisposable2, 0, arrayOfBehaviorDisposable1, 0, j);
        System.arraycopy(arrayOfBehaviorDisposable2, j + 1, arrayOfBehaviorDisposable1, j, m - j - 1);
      }
    } while (!this.subscribers.compareAndSet(arrayOfBehaviorDisposable2, arrayOfBehaviorDisposable1));
    return;
  }
  
  void setCurrent(Object paramObject)
  {
    this.writeLock.lock();
    try
    {
      this.index += 1L;
      this.value.lazySet(paramObject);
      return;
    }
    finally
    {
      this.writeLock.unlock();
    }
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    Object localObject = new BehaviorDisposable(paramObserver, this);
    paramObserver.onSubscribe((Disposable)localObject);
    if (add((BehaviorDisposable)localObject))
    {
      if (((BehaviorDisposable)localObject).cancelled) {
        remove((BehaviorDisposable)localObject);
      } else {
        ((BehaviorDisposable)localObject).emitFirst();
      }
    }
    else
    {
      localObject = this.value.get();
      if (NotificationLite.isComplete(localObject)) {
        paramObserver.onComplete();
      } else {
        paramObserver.onError(NotificationLite.getError(localObject));
      }
    }
  }
  
  int subscriberCount()
  {
    return ((BehaviorDisposable[])this.subscribers.get()).length;
  }
  
  BehaviorDisposable<T>[] terminate(Object paramObject)
  {
    BehaviorDisposable[] arrayOfBehaviorDisposable2 = (BehaviorDisposable[])this.subscribers.get();
    BehaviorDisposable[] arrayOfBehaviorDisposable3 = TERMINATED;
    BehaviorDisposable[] arrayOfBehaviorDisposable1 = arrayOfBehaviorDisposable2;
    if (arrayOfBehaviorDisposable2 != arrayOfBehaviorDisposable3)
    {
      arrayOfBehaviorDisposable2 = (BehaviorDisposable[])this.subscribers.getAndSet(arrayOfBehaviorDisposable3);
      arrayOfBehaviorDisposable1 = arrayOfBehaviorDisposable2;
      if (arrayOfBehaviorDisposable2 != TERMINATED)
      {
        setCurrent(paramObject);
        arrayOfBehaviorDisposable1 = arrayOfBehaviorDisposable2;
      }
    }
    return arrayOfBehaviorDisposable1;
  }
  
  static final class BehaviorDisposable<T>
    implements Disposable, AppendOnlyLinkedArrayList.NonThrowingPredicate<Object>
  {
    final Observer<? super T> actual;
    volatile boolean cancelled;
    boolean emitting;
    boolean fastPath;
    long index;
    boolean next;
    AppendOnlyLinkedArrayList<Object> queue;
    final BehaviorSubject<T> state;
    
    BehaviorDisposable(Observer<? super T> paramObserver, BehaviorSubject<T> paramBehaviorSubject)
    {
      this.actual = paramObserver;
      this.state = paramBehaviorSubject;
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.state.remove(this);
      }
    }
    
    void emitFirst()
    {
      if (this.cancelled) {
        return;
      }
      try
      {
        if (this.cancelled) {
          return;
        }
        if (this.next) {
          return;
        }
        Object localObject2 = this.state;
        Lock localLock = ((BehaviorSubject)localObject2).readLock;
        localLock.lock();
        this.index = ((BehaviorSubject)localObject2).index;
        localObject2 = ((BehaviorSubject)localObject2).value.get();
        localLock.unlock();
        boolean bool;
        if (localObject2 != null) {
          bool = true;
        } else {
          bool = false;
        }
        this.emitting = bool;
        this.next = true;
        if (localObject2 != null)
        {
          if (test(localObject2)) {
            return;
          }
          emitLoop();
        }
        return;
      }
      finally {}
    }
    
    void emitLoop()
    {
      for (;;)
      {
        if (this.cancelled) {
          return;
        }
        try
        {
          AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList = this.queue;
          if (localAppendOnlyLinkedArrayList == null)
          {
            this.emitting = false;
            return;
          }
          this.queue = null;
          localAppendOnlyLinkedArrayList.forEachWhile(this);
        }
        finally {}
      }
    }
    
    void emitNext(Object paramObject, long paramLong)
    {
      if (this.cancelled) {
        return;
      }
      if (!this.fastPath) {
        try
        {
          if (this.cancelled) {
            return;
          }
          if (this.index == paramLong) {
            return;
          }
          if (this.emitting)
          {
            AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
            AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
            if (localAppendOnlyLinkedArrayList2 == null)
            {
              localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
              localAppendOnlyLinkedArrayList1.<init>(4);
              this.queue = localAppendOnlyLinkedArrayList1;
            }
            localAppendOnlyLinkedArrayList1.add(paramObject);
            return;
          }
          this.next = true;
          this.fastPath = true;
        }
        finally {}
      }
      test(paramObject);
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
    
    public boolean test(Object paramObject)
    {
      boolean bool;
      if ((!this.cancelled) && (!NotificationLite.accept(paramObject, this.actual))) {
        bool = false;
      } else {
        bool = true;
      }
      return bool;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/BehaviorSubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */