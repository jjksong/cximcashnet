package io.reactivex.subjects;

import io.reactivex.Observable;
import io.reactivex.Observer;

public abstract class Subject<T>
  extends Observable<T>
  implements Observer<T>
{
  public abstract Throwable getThrowable();
  
  public abstract boolean hasComplete();
  
  public abstract boolean hasObservers();
  
  public abstract boolean hasThrowable();
  
  public final Subject<T> toSerialized()
  {
    if ((this instanceof SerializedSubject)) {
      return this;
    }
    return new SerializedSubject(this);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/Subject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */