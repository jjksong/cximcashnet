package io.reactivex.subjects;

import io.reactivex.Observer;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.observers.DeferredScalarDisposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

public final class AsyncSubject<T>
  extends Subject<T>
{
  static final AsyncDisposable[] EMPTY = new AsyncDisposable[0];
  static final AsyncDisposable[] TERMINATED = new AsyncDisposable[0];
  Throwable error;
  final AtomicReference<AsyncDisposable<T>[]> subscribers = new AtomicReference(EMPTY);
  T value;
  
  @CheckReturnValue
  public static <T> AsyncSubject<T> create()
  {
    return new AsyncSubject();
  }
  
  boolean add(AsyncDisposable<T> paramAsyncDisposable)
  {
    AsyncDisposable[] arrayOfAsyncDisposable2;
    AsyncDisposable[] arrayOfAsyncDisposable1;
    do
    {
      arrayOfAsyncDisposable2 = (AsyncDisposable[])this.subscribers.get();
      if (arrayOfAsyncDisposable2 == TERMINATED) {
        return false;
      }
      int i = arrayOfAsyncDisposable2.length;
      arrayOfAsyncDisposable1 = new AsyncDisposable[i + 1];
      System.arraycopy(arrayOfAsyncDisposable2, 0, arrayOfAsyncDisposable1, 0, i);
      arrayOfAsyncDisposable1[i] = paramAsyncDisposable;
    } while (!this.subscribers.compareAndSet(arrayOfAsyncDisposable2, arrayOfAsyncDisposable1));
    return true;
  }
  
  public Throwable getThrowable()
  {
    Throwable localThrowable;
    if (this.subscribers.get() == TERMINATED) {
      localThrowable = this.error;
    } else {
      localThrowable = null;
    }
    return localThrowable;
  }
  
  public T getValue()
  {
    Object localObject;
    if (this.subscribers.get() == TERMINATED) {
      localObject = this.value;
    } else {
      localObject = null;
    }
    return (T)localObject;
  }
  
  public Object[] getValues()
  {
    Object localObject = getValue();
    Object[] arrayOfObject;
    if (localObject != null)
    {
      arrayOfObject = new Object[1];
      arrayOfObject[0] = localObject;
    }
    else
    {
      arrayOfObject = new Object[0];
    }
    return arrayOfObject;
  }
  
  public T[] getValues(T[] paramArrayOfT)
  {
    Object localObject2 = getValue();
    if (localObject2 == null)
    {
      if (paramArrayOfT.length != 0) {
        paramArrayOfT[0] = null;
      }
      return paramArrayOfT;
    }
    Object localObject1 = paramArrayOfT;
    if (paramArrayOfT.length == 0) {
      localObject1 = Arrays.copyOf(paramArrayOfT, 1);
    }
    localObject1[0] = localObject2;
    if (localObject1.length != 1) {
      localObject1[1] = null;
    }
    return (T[])localObject1;
  }
  
  public boolean hasComplete()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.error == null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasObservers()
  {
    boolean bool;
    if (((AsyncDisposable[])this.subscribers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasValue()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.value != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  void nullOnNext()
  {
    this.value = null;
    NullPointerException localNullPointerException = new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
    this.error = localNullPointerException;
    AsyncDisposable[] arrayOfAsyncDisposable = (AsyncDisposable[])this.subscribers.getAndSet(TERMINATED);
    int j = arrayOfAsyncDisposable.length;
    for (int i = 0; i < j; i++) {
      arrayOfAsyncDisposable[i].onError(localNullPointerException);
    }
  }
  
  public void onComplete()
  {
    Object localObject = this.subscribers.get();
    AsyncDisposable[] arrayOfAsyncDisposable = TERMINATED;
    if (localObject == arrayOfAsyncDisposable) {
      return;
    }
    localObject = this.value;
    arrayOfAsyncDisposable = (AsyncDisposable[])this.subscribers.getAndSet(arrayOfAsyncDisposable);
    int j = 0;
    int i = 0;
    if (localObject == null)
    {
      j = arrayOfAsyncDisposable.length;
      while (i < j)
      {
        arrayOfAsyncDisposable[i].onComplete();
        i++;
      }
    }
    int k = arrayOfAsyncDisposable.length;
    for (i = j; i < k; i++) {
      arrayOfAsyncDisposable[i].complete(localObject);
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    paramThrowable = this.subscribers.get();
    AsyncDisposable[] arrayOfAsyncDisposable = TERMINATED;
    if (paramThrowable == arrayOfAsyncDisposable)
    {
      RxJavaPlugins.onError((Throwable)localObject);
      return;
    }
    this.value = null;
    this.error = ((Throwable)localObject);
    paramThrowable = (AsyncDisposable[])this.subscribers.getAndSet(arrayOfAsyncDisposable);
    int j = paramThrowable.length;
    for (int i = 0; i < j; i++) {
      paramThrowable[i].onError((Throwable)localObject);
    }
  }
  
  public void onNext(T paramT)
  {
    if (this.subscribers.get() == TERMINATED) {
      return;
    }
    if (paramT == null)
    {
      nullOnNext();
      return;
    }
    this.value = paramT;
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (this.subscribers.get() == TERMINATED) {
      paramDisposable.dispose();
    }
  }
  
  void remove(AsyncDisposable<T> paramAsyncDisposable)
  {
    AsyncDisposable[] arrayOfAsyncDisposable2;
    AsyncDisposable[] arrayOfAsyncDisposable1;
    do
    {
      arrayOfAsyncDisposable2 = (AsyncDisposable[])this.subscribers.get();
      int m = arrayOfAsyncDisposable2.length;
      if (m == 0) {
        return;
      }
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfAsyncDisposable2[i] == paramAsyncDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfAsyncDisposable1 = EMPTY;
      }
      else
      {
        arrayOfAsyncDisposable1 = new AsyncDisposable[m - 1];
        System.arraycopy(arrayOfAsyncDisposable2, 0, arrayOfAsyncDisposable1, 0, j);
        System.arraycopy(arrayOfAsyncDisposable2, j + 1, arrayOfAsyncDisposable1, j, m - j - 1);
      }
    } while (!this.subscribers.compareAndSet(arrayOfAsyncDisposable2, arrayOfAsyncDisposable1));
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    AsyncDisposable localAsyncDisposable = new AsyncDisposable(paramObserver, this);
    paramObserver.onSubscribe(localAsyncDisposable);
    if (add(localAsyncDisposable))
    {
      if (localAsyncDisposable.isDisposed()) {
        remove(localAsyncDisposable);
      }
    }
    else
    {
      Throwable localThrowable = this.error;
      if (localThrowable != null)
      {
        paramObserver.onError(localThrowable);
      }
      else
      {
        paramObserver = this.value;
        if (paramObserver != null) {
          localAsyncDisposable.complete(paramObserver);
        } else {
          localAsyncDisposable.onComplete();
        }
      }
    }
  }
  
  static final class AsyncDisposable<T>
    extends DeferredScalarDisposable<T>
  {
    private static final long serialVersionUID = 5629876084736248016L;
    final AsyncSubject<T> parent;
    
    AsyncDisposable(Observer<? super T> paramObserver, AsyncSubject<T> paramAsyncSubject)
    {
      super();
      this.parent = paramAsyncSubject;
    }
    
    public void dispose()
    {
      if (super.tryDispose()) {
        this.parent.remove(this);
      }
    }
    
    void onComplete()
    {
      if (!isDisposed()) {
        this.actual.onComplete();
      }
    }
    
    void onError(Throwable paramThrowable)
    {
      if (isDisposed()) {
        RxJavaPlugins.onError(paramThrowable);
      } else {
        this.actual.onError(paramThrowable);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/AsyncSubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */