package io.reactivex.subjects;

import io.reactivex.Completable;
import io.reactivex.CompletableObserver;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Experimental
public final class CompletableSubject
  extends Completable
  implements CompletableObserver
{
  static final CompletableDisposable[] EMPTY = new CompletableDisposable[0];
  static final CompletableDisposable[] TERMINATED = new CompletableDisposable[0];
  Throwable error;
  final AtomicReference<CompletableDisposable[]> observers = new AtomicReference(EMPTY);
  final AtomicBoolean once = new AtomicBoolean();
  
  @CheckReturnValue
  public static CompletableSubject create()
  {
    return new CompletableSubject();
  }
  
  boolean add(CompletableDisposable paramCompletableDisposable)
  {
    CompletableDisposable[] arrayOfCompletableDisposable1;
    CompletableDisposable[] arrayOfCompletableDisposable2;
    do
    {
      arrayOfCompletableDisposable1 = (CompletableDisposable[])this.observers.get();
      if (arrayOfCompletableDisposable1 == TERMINATED) {
        return false;
      }
      int i = arrayOfCompletableDisposable1.length;
      arrayOfCompletableDisposable2 = new CompletableDisposable[i + 1];
      System.arraycopy(arrayOfCompletableDisposable1, 0, arrayOfCompletableDisposable2, 0, i);
      arrayOfCompletableDisposable2[i] = paramCompletableDisposable;
    } while (!this.observers.compareAndSet(arrayOfCompletableDisposable1, arrayOfCompletableDisposable2));
    return true;
  }
  
  public Throwable getThrowable()
  {
    if (this.observers.get() == TERMINATED) {
      return this.error;
    }
    return null;
  }
  
  public boolean hasComplete()
  {
    boolean bool;
    if ((this.observers.get() == TERMINATED) && (this.error == null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasObservers()
  {
    boolean bool;
    if (((CompletableDisposable[])this.observers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.observers.get() == TERMINATED) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  int observerCount()
  {
    return ((CompletableDisposable[])this.observers.get()).length;
  }
  
  public void onComplete()
  {
    Object localObject = this.once;
    int i = 0;
    if (((AtomicBoolean)localObject).compareAndSet(false, true))
    {
      localObject = (CompletableDisposable[])this.observers.getAndSet(TERMINATED);
      int j = localObject.length;
      while (i < j)
      {
        localObject[i].actual.onComplete();
        i++;
      }
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("Null errors are not allowed in 2.x");
    }
    paramThrowable = this.once;
    int i = 0;
    if (paramThrowable.compareAndSet(false, true))
    {
      this.error = ((Throwable)localObject);
      paramThrowable = (CompletableDisposable[])this.observers.getAndSet(TERMINATED);
      int j = paramThrowable.length;
      while (i < j)
      {
        paramThrowable[i].actual.onError((Throwable)localObject);
        i++;
      }
    }
    RxJavaPlugins.onError((Throwable)localObject);
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (this.observers.get() == TERMINATED) {
      paramDisposable.dispose();
    }
  }
  
  void remove(CompletableDisposable paramCompletableDisposable)
  {
    CompletableDisposable[] arrayOfCompletableDisposable2;
    CompletableDisposable[] arrayOfCompletableDisposable1;
    do
    {
      arrayOfCompletableDisposable2 = (CompletableDisposable[])this.observers.get();
      int m = arrayOfCompletableDisposable2.length;
      if (m == 0) {
        return;
      }
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfCompletableDisposable2[i] == paramCompletableDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfCompletableDisposable1 = EMPTY;
      }
      else
      {
        arrayOfCompletableDisposable1 = new CompletableDisposable[m - 1];
        System.arraycopy(arrayOfCompletableDisposable2, 0, arrayOfCompletableDisposable1, 0, j);
        System.arraycopy(arrayOfCompletableDisposable2, j + 1, arrayOfCompletableDisposable1, j, m - j - 1);
      }
    } while (!this.observers.compareAndSet(arrayOfCompletableDisposable2, arrayOfCompletableDisposable1));
  }
  
  protected void subscribeActual(CompletableObserver paramCompletableObserver)
  {
    Object localObject = new CompletableDisposable(paramCompletableObserver, this);
    paramCompletableObserver.onSubscribe((Disposable)localObject);
    if (add((CompletableDisposable)localObject))
    {
      if (((CompletableDisposable)localObject).isDisposed()) {
        remove((CompletableDisposable)localObject);
      }
    }
    else
    {
      localObject = this.error;
      if (localObject != null) {
        paramCompletableObserver.onError((Throwable)localObject);
      } else {
        paramCompletableObserver.onComplete();
      }
    }
  }
  
  static final class CompletableDisposable
    extends AtomicReference<CompletableSubject>
    implements Disposable
  {
    private static final long serialVersionUID = -7650903191002190468L;
    final CompletableObserver actual;
    
    CompletableDisposable(CompletableObserver paramCompletableObserver, CompletableSubject paramCompletableSubject)
    {
      this.actual = paramCompletableObserver;
      lazySet(paramCompletableSubject);
    }
    
    public void dispose()
    {
      CompletableSubject localCompletableSubject = (CompletableSubject)getAndSet(null);
      if (localCompletableSubject != null) {
        localCompletableSubject.remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/CompletableSubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */