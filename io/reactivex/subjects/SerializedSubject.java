package io.reactivex.subjects;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList.NonThrowingPredicate;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;

final class SerializedSubject<T>
  extends Subject<T>
  implements AppendOnlyLinkedArrayList.NonThrowingPredicate<Object>
{
  final Subject<T> actual;
  volatile boolean done;
  boolean emitting;
  AppendOnlyLinkedArrayList<Object> queue;
  
  SerializedSubject(Subject<T> paramSubject)
  {
    this.actual = paramSubject;
  }
  
  void emitLoop()
  {
    for (;;)
    {
      try
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList = this.queue;
        if (localAppendOnlyLinkedArrayList == null)
        {
          this.emitting = false;
          return;
        }
        this.queue = null;
        localAppendOnlyLinkedArrayList.forEachWhile(this);
      }
      finally {}
    }
  }
  
  public Throwable getThrowable()
  {
    return this.actual.getThrowable();
  }
  
  public boolean hasComplete()
  {
    return this.actual.hasComplete();
  }
  
  public boolean hasObservers()
  {
    return this.actual.hasObservers();
  }
  
  public boolean hasThrowable()
  {
    return this.actual.hasThrowable();
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    try
    {
      if (this.done) {
        return;
      }
      this.done = true;
      if (this.emitting)
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
        if (localAppendOnlyLinkedArrayList2 == null)
        {
          localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
          localAppendOnlyLinkedArrayList1.<init>(4);
          this.queue = localAppendOnlyLinkedArrayList1;
        }
        localAppendOnlyLinkedArrayList1.add(NotificationLite.complete());
        return;
      }
      this.emitting = true;
      this.actual.onComplete();
      return;
    }
    finally {}
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (this.done)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    try
    {
      int i;
      if (this.done)
      {
        i = 1;
      }
      else
      {
        this.done = true;
        if (this.emitting)
        {
          AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
          AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
          if (localAppendOnlyLinkedArrayList2 == null)
          {
            localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
            localAppendOnlyLinkedArrayList1.<init>(4);
            this.queue = localAppendOnlyLinkedArrayList1;
          }
          localAppendOnlyLinkedArrayList1.setFirst(NotificationLite.error(paramThrowable));
          return;
        }
        i = 0;
        this.emitting = true;
      }
      if (i != 0)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.actual.onError(paramThrowable);
      return;
    }
    finally {}
  }
  
  public void onNext(T paramT)
  {
    if (this.done) {
      return;
    }
    try
    {
      if (this.done) {
        return;
      }
      if (this.emitting)
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
        if (localAppendOnlyLinkedArrayList2 == null)
        {
          localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
          localAppendOnlyLinkedArrayList1.<init>(4);
          this.queue = localAppendOnlyLinkedArrayList1;
        }
        localAppendOnlyLinkedArrayList1.add(NotificationLite.next(paramT));
        return;
      }
      this.emitting = true;
      this.actual.onNext(paramT);
      emitLoop();
      return;
    }
    finally {}
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    boolean bool = this.done;
    int i = 1;
    int j = 1;
    if (!bool) {
      try
      {
        if (this.done)
        {
          i = j;
        }
        else
        {
          if (this.emitting)
          {
            AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
            AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
            if (localAppendOnlyLinkedArrayList2 == null)
            {
              localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
              localAppendOnlyLinkedArrayList1.<init>(4);
              this.queue = localAppendOnlyLinkedArrayList1;
            }
            localAppendOnlyLinkedArrayList1.add(NotificationLite.disposable(paramDisposable));
            return;
          }
          this.emitting = true;
          i = 0;
        }
      }
      finally {}
    }
    if (i != 0)
    {
      paramDisposable.dispose();
    }
    else
    {
      this.actual.onSubscribe(paramDisposable);
      emitLoop();
    }
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    this.actual.subscribe(paramObserver);
  }
  
  public boolean test(Object paramObject)
  {
    return NotificationLite.acceptFull(paramObject, this.actual);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/SerializedSubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */