package io.reactivex.subjects;

import io.reactivex.Observer;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.fuseable.SimpleQueue;
import io.reactivex.internal.observers.BasicIntQueueDisposable;
import io.reactivex.internal.queue.SpscLinkedArrayQueue;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class UnicastSubject<T>
  extends Subject<T>
{
  final AtomicReference<Observer<? super T>> actual;
  volatile boolean disposed;
  volatile boolean done;
  boolean enableOperatorFusion;
  Throwable error;
  final AtomicReference<Runnable> onTerminate;
  final AtomicBoolean once;
  final SpscLinkedArrayQueue<T> queue;
  final BasicIntQueueDisposable<T> wip;
  
  UnicastSubject(int paramInt)
  {
    this.queue = new SpscLinkedArrayQueue(ObjectHelper.verifyPositive(paramInt, "capacityHint"));
    this.onTerminate = new AtomicReference();
    this.actual = new AtomicReference();
    this.once = new AtomicBoolean();
    this.wip = new UnicastQueueDisposable();
  }
  
  UnicastSubject(int paramInt, Runnable paramRunnable)
  {
    this.queue = new SpscLinkedArrayQueue(ObjectHelper.verifyPositive(paramInt, "capacityHint"));
    this.onTerminate = new AtomicReference(ObjectHelper.requireNonNull(paramRunnable, "onTerminate"));
    this.actual = new AtomicReference();
    this.once = new AtomicBoolean();
    this.wip = new UnicastQueueDisposable();
  }
  
  @CheckReturnValue
  public static <T> UnicastSubject<T> create()
  {
    return new UnicastSubject(bufferSize());
  }
  
  @CheckReturnValue
  public static <T> UnicastSubject<T> create(int paramInt)
  {
    return new UnicastSubject(paramInt);
  }
  
  @CheckReturnValue
  public static <T> UnicastSubject<T> create(int paramInt, Runnable paramRunnable)
  {
    return new UnicastSubject(paramInt, paramRunnable);
  }
  
  void doTerminate()
  {
    Runnable localRunnable = (Runnable)this.onTerminate.get();
    if ((localRunnable != null) && (this.onTerminate.compareAndSet(localRunnable, null))) {
      localRunnable.run();
    }
  }
  
  void drain()
  {
    if (this.wip.getAndIncrement() != 0) {
      return;
    }
    Observer localObserver = (Observer)this.actual.get();
    int i = 1;
    for (;;)
    {
      if (localObserver != null)
      {
        if (this.enableOperatorFusion) {
          drainFused(localObserver);
        } else {
          drainNormal(localObserver);
        }
        return;
      }
      i = this.wip.addAndGet(-i);
      if (i == 0) {
        return;
      }
      localObserver = (Observer)this.actual.get();
    }
  }
  
  void drainFused(Observer<? super T> paramObserver)
  {
    Object localObject = this.queue;
    int i = 1;
    int j;
    do
    {
      if (this.disposed)
      {
        this.actual.lazySet(null);
        ((SpscLinkedArrayQueue)localObject).clear();
        return;
      }
      boolean bool = this.done;
      paramObserver.onNext(null);
      if (bool)
      {
        this.actual.lazySet(null);
        localObject = this.error;
        if (localObject != null) {
          paramObserver.onError((Throwable)localObject);
        } else {
          paramObserver.onComplete();
        }
        return;
      }
      j = this.wip.addAndGet(-i);
      i = j;
    } while (j != 0);
  }
  
  void drainNormal(Observer<? super T> paramObserver)
  {
    Object localObject1 = this.queue;
    int i = 1;
    for (;;)
    {
      if (this.disposed)
      {
        this.actual.lazySet(null);
        ((SimpleQueue)localObject1).clear();
        return;
      }
      boolean bool = this.done;
      Object localObject2 = this.queue.poll();
      int j;
      if (localObject2 == null) {
        j = 1;
      } else {
        j = 0;
      }
      if ((bool) && (j != 0))
      {
        this.actual.lazySet(null);
        localObject1 = this.error;
        if (localObject1 != null) {
          paramObserver.onError((Throwable)localObject1);
        } else {
          paramObserver.onComplete();
        }
        return;
      }
      if (j != 0)
      {
        j = this.wip.addAndGet(-i);
        i = j;
        if (j != 0) {}
      }
      else
      {
        paramObserver.onNext(localObject2);
      }
    }
  }
  
  public Throwable getThrowable()
  {
    if (this.done) {
      return this.error;
    }
    return null;
  }
  
  public boolean hasComplete()
  {
    boolean bool;
    if ((this.done) && (this.error == null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasObservers()
  {
    boolean bool;
    if (this.actual.get() != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.done) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    if ((!this.done) && (!this.disposed))
    {
      this.done = true;
      doTerminate();
      drain();
      return;
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if ((!this.done) && (!this.disposed))
    {
      Object localObject = paramThrowable;
      if (paramThrowable == null) {
        localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
      }
      this.error = ((Throwable)localObject);
      this.done = true;
      doTerminate();
      drain();
      return;
    }
    RxJavaPlugins.onError(paramThrowable);
  }
  
  public void onNext(T paramT)
  {
    if ((!this.done) && (!this.disposed))
    {
      if (paramT == null)
      {
        onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
        return;
      }
      this.queue.offer(paramT);
      drain();
      return;
    }
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if ((this.done) || (this.disposed)) {
      paramDisposable.dispose();
    }
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    if ((!this.once.get()) && (this.once.compareAndSet(false, true)))
    {
      paramObserver.onSubscribe(this.wip);
      this.actual.lazySet(paramObserver);
      if (this.disposed)
      {
        this.actual.lazySet(null);
        return;
      }
      drain();
    }
    else
    {
      EmptyDisposable.error(new IllegalStateException("Only a single observer allowed."), paramObserver);
    }
  }
  
  final class UnicastQueueDisposable
    extends BasicIntQueueDisposable<T>
  {
    private static final long serialVersionUID = 7926949470189395511L;
    
    UnicastQueueDisposable() {}
    
    public void clear()
    {
      UnicastSubject.this.queue.clear();
    }
    
    public void dispose()
    {
      if (!UnicastSubject.this.disposed)
      {
        UnicastSubject localUnicastSubject = UnicastSubject.this;
        localUnicastSubject.disposed = true;
        localUnicastSubject.doTerminate();
        UnicastSubject.this.actual.lazySet(null);
        if (UnicastSubject.this.wip.getAndIncrement() == 0)
        {
          UnicastSubject.this.actual.lazySet(null);
          UnicastSubject.this.queue.clear();
        }
      }
    }
    
    public boolean isDisposed()
    {
      return UnicastSubject.this.disposed;
    }
    
    public boolean isEmpty()
    {
      return UnicastSubject.this.queue.isEmpty();
    }
    
    public T poll()
      throws Exception
    {
      return (T)UnicastSubject.this.queue.poll();
    }
    
    public int requestFusion(int paramInt)
    {
      if ((paramInt & 0x2) != 0)
      {
        UnicastSubject.this.enableOperatorFusion = true;
        return 2;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/UnicastSubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */