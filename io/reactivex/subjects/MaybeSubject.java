package io.reactivex.subjects;

import io.reactivex.Maybe;
import io.reactivex.MaybeObserver;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Experimental
public final class MaybeSubject<T>
  extends Maybe<T>
  implements MaybeObserver<T>
{
  static final MaybeDisposable[] EMPTY = new MaybeDisposable[0];
  static final MaybeDisposable[] TERMINATED = new MaybeDisposable[0];
  Throwable error;
  final AtomicReference<MaybeDisposable<T>[]> observers = new AtomicReference(EMPTY);
  final AtomicBoolean once = new AtomicBoolean();
  T value;
  
  @CheckReturnValue
  public static <T> MaybeSubject<T> create()
  {
    return new MaybeSubject();
  }
  
  boolean add(MaybeDisposable<T> paramMaybeDisposable)
  {
    MaybeDisposable[] arrayOfMaybeDisposable2;
    MaybeDisposable[] arrayOfMaybeDisposable1;
    do
    {
      arrayOfMaybeDisposable2 = (MaybeDisposable[])this.observers.get();
      if (arrayOfMaybeDisposable2 == TERMINATED) {
        return false;
      }
      int i = arrayOfMaybeDisposable2.length;
      arrayOfMaybeDisposable1 = new MaybeDisposable[i + 1];
      System.arraycopy(arrayOfMaybeDisposable2, 0, arrayOfMaybeDisposable1, 0, i);
      arrayOfMaybeDisposable1[i] = paramMaybeDisposable;
    } while (!this.observers.compareAndSet(arrayOfMaybeDisposable2, arrayOfMaybeDisposable1));
    return true;
  }
  
  public Throwable getThrowable()
  {
    if (this.observers.get() == TERMINATED) {
      return this.error;
    }
    return null;
  }
  
  public T getValue()
  {
    if (this.observers.get() == TERMINATED) {
      return (T)this.value;
    }
    return null;
  }
  
  public boolean hasComplete()
  {
    boolean bool;
    if ((this.observers.get() == TERMINATED) && (this.value == null) && (this.error == null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasObservers()
  {
    boolean bool;
    if (((MaybeDisposable[])this.observers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.observers.get() == TERMINATED) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasValue()
  {
    boolean bool;
    if ((this.observers.get() == TERMINATED) && (this.value != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  int observerCount()
  {
    return ((MaybeDisposable[])this.observers.get()).length;
  }
  
  public void onComplete()
  {
    Object localObject = this.once;
    int i = 0;
    if (((AtomicBoolean)localObject).compareAndSet(false, true))
    {
      localObject = (MaybeDisposable[])this.observers.getAndSet(TERMINATED);
      int j = localObject.length;
      while (i < j)
      {
        localObject[i].actual.onComplete();
        i++;
      }
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("Null errors are not allowed in 2.x");
    }
    paramThrowable = this.once;
    int i = 0;
    if (paramThrowable.compareAndSet(false, true))
    {
      this.error = ((Throwable)localObject);
      paramThrowable = (MaybeDisposable[])this.observers.getAndSet(TERMINATED);
      int j = paramThrowable.length;
      while (i < j)
      {
        paramThrowable[i].actual.onError((Throwable)localObject);
        i++;
      }
    }
    RxJavaPlugins.onError((Throwable)localObject);
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (this.observers.get() == TERMINATED) {
      paramDisposable.dispose();
    }
  }
  
  public void onSuccess(T paramT)
  {
    if (paramT == null)
    {
      onError(new NullPointerException("Null values are not allowed in 2.x"));
      return;
    }
    Object localObject = this.once;
    int i = 0;
    if (((AtomicBoolean)localObject).compareAndSet(false, true))
    {
      this.value = paramT;
      localObject = (MaybeDisposable[])this.observers.getAndSet(TERMINATED);
      int j = localObject.length;
      while (i < j)
      {
        localObject[i].actual.onSuccess(paramT);
        i++;
      }
    }
  }
  
  void remove(MaybeDisposable<T> paramMaybeDisposable)
  {
    MaybeDisposable[] arrayOfMaybeDisposable2;
    MaybeDisposable[] arrayOfMaybeDisposable1;
    do
    {
      arrayOfMaybeDisposable2 = (MaybeDisposable[])this.observers.get();
      int m = arrayOfMaybeDisposable2.length;
      if (m == 0) {
        return;
      }
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfMaybeDisposable2[i] == paramMaybeDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfMaybeDisposable1 = EMPTY;
      }
      else
      {
        arrayOfMaybeDisposable1 = new MaybeDisposable[m - 1];
        System.arraycopy(arrayOfMaybeDisposable2, 0, arrayOfMaybeDisposable1, 0, j);
        System.arraycopy(arrayOfMaybeDisposable2, j + 1, arrayOfMaybeDisposable1, j, m - j - 1);
      }
    } while (!this.observers.compareAndSet(arrayOfMaybeDisposable2, arrayOfMaybeDisposable1));
  }
  
  protected void subscribeActual(MaybeObserver<? super T> paramMaybeObserver)
  {
    Object localObject = new MaybeDisposable(paramMaybeObserver, this);
    paramMaybeObserver.onSubscribe((Disposable)localObject);
    if (add((MaybeDisposable)localObject))
    {
      if (((MaybeDisposable)localObject).isDisposed()) {
        remove((MaybeDisposable)localObject);
      }
    }
    else
    {
      localObject = this.error;
      if (localObject != null)
      {
        paramMaybeObserver.onError((Throwable)localObject);
      }
      else
      {
        localObject = this.value;
        if (localObject == null) {
          paramMaybeObserver.onComplete();
        } else {
          paramMaybeObserver.onSuccess(localObject);
        }
      }
    }
  }
  
  static final class MaybeDisposable<T>
    extends AtomicReference<MaybeSubject<T>>
    implements Disposable
  {
    private static final long serialVersionUID = -7650903191002190468L;
    final MaybeObserver<? super T> actual;
    
    MaybeDisposable(MaybeObserver<? super T> paramMaybeObserver, MaybeSubject<T> paramMaybeSubject)
    {
      this.actual = paramMaybeObserver;
      lazySet(paramMaybeSubject);
    }
    
    public void dispose()
    {
      MaybeSubject localMaybeSubject = (MaybeSubject)getAndSet(null);
      if (localMaybeSubject != null) {
        localMaybeSubject.remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/MaybeSubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */