package io.reactivex.subjects;

import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

public final class ReplaySubject<T>
  extends Subject<T>
{
  static final ReplayDisposable[] EMPTY = new ReplayDisposable[0];
  private static final Object[] EMPTY_ARRAY = new Object[0];
  static final ReplayDisposable[] TERMINATED = new ReplayDisposable[0];
  final ReplayBuffer<T> buffer;
  boolean done;
  final AtomicReference<ReplayDisposable<T>[]> observers;
  
  ReplaySubject(ReplayBuffer<T> paramReplayBuffer)
  {
    this.buffer = paramReplayBuffer;
    this.observers = new AtomicReference(EMPTY);
  }
  
  @CheckReturnValue
  public static <T> ReplaySubject<T> create()
  {
    return new ReplaySubject(new UnboundedReplayBuffer(16));
  }
  
  @CheckReturnValue
  public static <T> ReplaySubject<T> create(int paramInt)
  {
    return new ReplaySubject(new UnboundedReplayBuffer(paramInt));
  }
  
  static <T> ReplaySubject<T> createUnbounded()
  {
    return new ReplaySubject(new SizeBoundReplayBuffer(Integer.MAX_VALUE));
  }
  
  @CheckReturnValue
  public static <T> ReplaySubject<T> createWithSize(int paramInt)
  {
    return new ReplaySubject(new SizeBoundReplayBuffer(paramInt));
  }
  
  @CheckReturnValue
  public static <T> ReplaySubject<T> createWithTime(long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
  {
    return new ReplaySubject(new SizeAndTimeBoundReplayBuffer(Integer.MAX_VALUE, paramLong, paramTimeUnit, paramScheduler));
  }
  
  @CheckReturnValue
  public static <T> ReplaySubject<T> createWithTimeAndSize(long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler, int paramInt)
  {
    return new ReplaySubject(new SizeAndTimeBoundReplayBuffer(paramInt, paramLong, paramTimeUnit, paramScheduler));
  }
  
  boolean add(ReplayDisposable<T> paramReplayDisposable)
  {
    ReplayDisposable[] arrayOfReplayDisposable1;
    ReplayDisposable[] arrayOfReplayDisposable2;
    do
    {
      arrayOfReplayDisposable1 = (ReplayDisposable[])this.observers.get();
      if (arrayOfReplayDisposable1 == TERMINATED) {
        return false;
      }
      int i = arrayOfReplayDisposable1.length;
      arrayOfReplayDisposable2 = new ReplayDisposable[i + 1];
      System.arraycopy(arrayOfReplayDisposable1, 0, arrayOfReplayDisposable2, 0, i);
      arrayOfReplayDisposable2[i] = paramReplayDisposable;
    } while (!this.observers.compareAndSet(arrayOfReplayDisposable1, arrayOfReplayDisposable2));
    return true;
  }
  
  public Throwable getThrowable()
  {
    Object localObject = this.buffer.get();
    if (NotificationLite.isError(localObject)) {
      return NotificationLite.getError(localObject);
    }
    return null;
  }
  
  public T getValue()
  {
    return (T)this.buffer.getValue();
  }
  
  public Object[] getValues()
  {
    Object[] arrayOfObject = getValues((Object[])EMPTY_ARRAY);
    if (arrayOfObject == EMPTY_ARRAY) {
      return new Object[0];
    }
    return arrayOfObject;
  }
  
  public T[] getValues(T[] paramArrayOfT)
  {
    return this.buffer.getValues(paramArrayOfT);
  }
  
  public boolean hasComplete()
  {
    return NotificationLite.isComplete(this.buffer.get());
  }
  
  public boolean hasObservers()
  {
    boolean bool;
    if (((ReplayDisposable[])this.observers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    return NotificationLite.isError(this.buffer.get());
  }
  
  public boolean hasValue()
  {
    boolean bool;
    if (this.buffer.size() != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  int observerCount()
  {
    return ((ReplayDisposable[])this.observers.get()).length;
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    this.done = true;
    Object localObject = NotificationLite.complete();
    ReplayBuffer localReplayBuffer = this.buffer;
    localReplayBuffer.addFinal(localObject);
    localObject = terminate(localObject);
    int j = localObject.length;
    for (int i = 0; i < j; i++) {
      localReplayBuffer.replay(localObject[i]);
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    if (this.done)
    {
      RxJavaPlugins.onError((Throwable)localObject);
      return;
    }
    this.done = true;
    localObject = NotificationLite.error((Throwable)localObject);
    paramThrowable = this.buffer;
    paramThrowable.addFinal(localObject);
    localObject = terminate(localObject);
    int j = localObject.length;
    for (int i = 0; i < j; i++) {
      paramThrowable.replay(localObject[i]);
    }
  }
  
  public void onNext(T paramT)
  {
    if (paramT == null)
    {
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
      return;
    }
    if (this.done) {
      return;
    }
    ReplayBuffer localReplayBuffer = this.buffer;
    localReplayBuffer.add(paramT);
    paramT = (ReplayDisposable[])this.observers.get();
    int j = paramT.length;
    for (int i = 0; i < j; i++) {
      localReplayBuffer.replay(paramT[i]);
    }
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (this.done) {
      paramDisposable.dispose();
    }
  }
  
  void remove(ReplayDisposable<T> paramReplayDisposable)
  {
    ReplayDisposable[] arrayOfReplayDisposable2;
    ReplayDisposable[] arrayOfReplayDisposable1;
    do
    {
      arrayOfReplayDisposable2 = (ReplayDisposable[])this.observers.get();
      if ((arrayOfReplayDisposable2 == TERMINATED) || (arrayOfReplayDisposable2 == EMPTY)) {
        break;
      }
      int m = arrayOfReplayDisposable2.length;
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfReplayDisposable2[i] == paramReplayDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfReplayDisposable1 = EMPTY;
      }
      else
      {
        arrayOfReplayDisposable1 = new ReplayDisposable[m - 1];
        System.arraycopy(arrayOfReplayDisposable2, 0, arrayOfReplayDisposable1, 0, j);
        System.arraycopy(arrayOfReplayDisposable2, j + 1, arrayOfReplayDisposable1, j, m - j - 1);
      }
    } while (!this.observers.compareAndSet(arrayOfReplayDisposable2, arrayOfReplayDisposable1));
    return;
  }
  
  int size()
  {
    return this.buffer.size();
  }
  
  protected void subscribeActual(Observer<? super T> paramObserver)
  {
    ReplayDisposable localReplayDisposable = new ReplayDisposable(paramObserver, this);
    paramObserver.onSubscribe(localReplayDisposable);
    if (!localReplayDisposable.cancelled)
    {
      if ((add(localReplayDisposable)) && (localReplayDisposable.cancelled))
      {
        remove(localReplayDisposable);
        return;
      }
      this.buffer.replay(localReplayDisposable);
    }
  }
  
  ReplayDisposable<T>[] terminate(Object paramObject)
  {
    if (this.buffer.compareAndSet(null, paramObject)) {
      return (ReplayDisposable[])this.observers.getAndSet(TERMINATED);
    }
    return TERMINATED;
  }
  
  static final class Node<T>
    extends AtomicReference<Node<T>>
  {
    private static final long serialVersionUID = 6404226426336033100L;
    final T value;
    
    Node(T paramT)
    {
      this.value = paramT;
    }
  }
  
  static abstract interface ReplayBuffer<T>
  {
    public abstract void add(T paramT);
    
    public abstract void addFinal(Object paramObject);
    
    public abstract boolean compareAndSet(Object paramObject1, Object paramObject2);
    
    public abstract Object get();
    
    public abstract T getValue();
    
    public abstract T[] getValues(T[] paramArrayOfT);
    
    public abstract void replay(ReplaySubject.ReplayDisposable<T> paramReplayDisposable);
    
    public abstract int size();
  }
  
  static final class ReplayDisposable<T>
    extends AtomicInteger
    implements Disposable
  {
    private static final long serialVersionUID = 466549804534799122L;
    final Observer<? super T> actual;
    volatile boolean cancelled;
    Object index;
    final ReplaySubject<T> state;
    
    ReplayDisposable(Observer<? super T> paramObserver, ReplaySubject<T> paramReplaySubject)
    {
      this.actual = paramObserver;
      this.state = paramReplaySubject;
    }
    
    public void dispose()
    {
      if (!this.cancelled)
      {
        this.cancelled = true;
        this.state.remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      return this.cancelled;
    }
  }
  
  static final class SizeAndTimeBoundReplayBuffer<T>
    extends AtomicReference<Object>
    implements ReplaySubject.ReplayBuffer<T>
  {
    private static final long serialVersionUID = -8056260896137901749L;
    volatile boolean done;
    volatile ReplaySubject.TimedNode<Object> head;
    final long maxAge;
    final int maxSize;
    final Scheduler scheduler;
    int size;
    ReplaySubject.TimedNode<Object> tail;
    final TimeUnit unit;
    
    SizeAndTimeBoundReplayBuffer(int paramInt, long paramLong, TimeUnit paramTimeUnit, Scheduler paramScheduler)
    {
      this.maxSize = ObjectHelper.verifyPositive(paramInt, "maxSize");
      this.maxAge = ObjectHelper.verifyPositive(paramLong, "maxAge");
      this.unit = ((TimeUnit)ObjectHelper.requireNonNull(paramTimeUnit, "unit is null"));
      this.scheduler = ((Scheduler)ObjectHelper.requireNonNull(paramScheduler, "scheduler is null"));
      paramTimeUnit = new ReplaySubject.TimedNode(null, 0L);
      this.tail = paramTimeUnit;
      this.head = paramTimeUnit;
    }
    
    public void add(T paramT)
    {
      paramT = new ReplaySubject.TimedNode(paramT, this.scheduler.now(this.unit));
      ReplaySubject.TimedNode localTimedNode = this.tail;
      this.tail = paramT;
      this.size += 1;
      localTimedNode.set(paramT);
      trim();
    }
    
    public void addFinal(Object paramObject)
    {
      paramObject = new ReplaySubject.TimedNode(paramObject, Long.MAX_VALUE);
      ReplaySubject.TimedNode localTimedNode = this.tail;
      this.tail = ((ReplaySubject.TimedNode)paramObject);
      this.size += 1;
      localTimedNode.lazySet(paramObject);
      trimFinal();
      this.done = true;
    }
    
    public T getValue()
    {
      Object localObject1 = this.head;
      Object localObject2 = null;
      for (;;)
      {
        ReplaySubject.TimedNode localTimedNode = (ReplaySubject.TimedNode)((ReplaySubject.TimedNode)localObject1).get();
        if (localTimedNode == null)
        {
          localObject1 = ((ReplaySubject.TimedNode)localObject1).value;
          if (localObject1 == null) {
            return null;
          }
          if ((!NotificationLite.isComplete(localObject1)) && (!NotificationLite.isError(localObject1))) {
            return (T)localObject1;
          }
          return (T)((ReplaySubject.TimedNode)localObject2).value;
        }
        localObject2 = localObject1;
        localObject1 = localTimedNode;
      }
    }
    
    public T[] getValues(T[] paramArrayOfT)
    {
      ReplaySubject.TimedNode localTimedNode = this.head;
      int k = size();
      int j = 0;
      Object localObject2;
      if (k == 0)
      {
        localObject2 = paramArrayOfT;
        if (paramArrayOfT.length != 0)
        {
          paramArrayOfT[0] = null;
          localObject2 = paramArrayOfT;
        }
      }
      else
      {
        localObject2 = localTimedNode;
        int i = j;
        Object localObject1 = paramArrayOfT;
        if (paramArrayOfT.length < k)
        {
          localObject1 = (Object[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), k);
          i = j;
          localObject2 = localTimedNode;
        }
        while (i != k)
        {
          localObject2 = (ReplaySubject.TimedNode)((ReplaySubject.TimedNode)localObject2).get();
          localObject1[i] = ((ReplaySubject.TimedNode)localObject2).value;
          i++;
        }
        localObject2 = localObject1;
        if (localObject1.length > k)
        {
          localObject1[k] = null;
          localObject2 = localObject1;
        }
      }
      return (T[])localObject2;
    }
    
    public void replay(ReplaySubject.ReplayDisposable<T> paramReplayDisposable)
    {
      if (paramReplayDisposable.getAndIncrement() != 0) {
        return;
      }
      Observer localObserver = paramReplayDisposable.actual;
      Object localObject1 = (ReplaySubject.TimedNode)paramReplayDisposable.index;
      Object localObject2;
      int i;
      if (localObject1 == null)
      {
        localObject1 = this.head;
        if (!this.done)
        {
          long l2 = this.scheduler.now(this.unit);
          long l1 = this.maxAge;
          ReplaySubject.TimedNode localTimedNode;
          for (localObject2 = (ReplaySubject.TimedNode)((ReplaySubject.TimedNode)localObject1).get(); (localObject2 != null) && (((ReplaySubject.TimedNode)localObject2).time <= l2 - l1); localObject2 = localTimedNode)
          {
            localTimedNode = (ReplaySubject.TimedNode)((ReplaySubject.TimedNode)localObject2).get();
            localObject1 = localObject2;
          }
          i = 1;
        }
        else
        {
          i = 1;
        }
      }
      else
      {
        i = 1;
      }
      if (paramReplayDisposable.cancelled)
      {
        paramReplayDisposable.index = null;
        return;
      }
      for (;;)
      {
        if (paramReplayDisposable.cancelled)
        {
          paramReplayDisposable.index = null;
          return;
        }
        localObject2 = (ReplaySubject.TimedNode)((ReplaySubject.TimedNode)localObject1).get();
        if (localObject2 == null)
        {
          if (((ReplaySubject.TimedNode)localObject1).get() != null) {
            break;
          }
          paramReplayDisposable.index = localObject1;
          int j = paramReplayDisposable.addAndGet(-i);
          i = j;
          if (j != 0) {
            break;
          }
          return;
        }
        localObject1 = ((ReplaySubject.TimedNode)localObject2).value;
        if ((this.done) && (((ReplaySubject.TimedNode)localObject2).get() == null))
        {
          if (NotificationLite.isComplete(localObject1)) {
            localObserver.onComplete();
          } else {
            localObserver.onError(NotificationLite.getError(localObject1));
          }
          paramReplayDisposable.index = null;
          paramReplayDisposable.cancelled = true;
          return;
        }
        localObserver.onNext(localObject1);
        localObject1 = localObject2;
      }
    }
    
    public int size()
    {
      Object localObject = this.head;
      int i = 0;
      int j;
      for (;;)
      {
        j = i;
        if (i == Integer.MAX_VALUE) {
          break;
        }
        ReplaySubject.TimedNode localTimedNode = (ReplaySubject.TimedNode)((ReplaySubject.TimedNode)localObject).get();
        if (localTimedNode == null)
        {
          localObject = ((ReplaySubject.TimedNode)localObject).value;
          if (!NotificationLite.isComplete(localObject))
          {
            j = i;
            if (!NotificationLite.isError(localObject)) {
              break;
            }
          }
          j = i - 1;
          break;
        }
        i++;
        localObject = localTimedNode;
      }
      return j;
    }
    
    void trim()
    {
      int i = this.size;
      if (i > this.maxSize)
      {
        this.size = (i - 1);
        this.head = ((ReplaySubject.TimedNode)this.head.get());
      }
      long l1 = this.scheduler.now(this.unit);
      long l2 = this.maxAge;
      ReplaySubject.TimedNode localTimedNode;
      for (Object localObject = this.head;; localObject = localTimedNode)
      {
        localTimedNode = (ReplaySubject.TimedNode)((ReplaySubject.TimedNode)localObject).get();
        if (localTimedNode == null)
        {
          this.head = ((ReplaySubject.TimedNode)localObject);
        }
        else
        {
          if (localTimedNode.time <= l1 - l2) {
            continue;
          }
          this.head = ((ReplaySubject.TimedNode)localObject);
        }
        return;
      }
    }
    
    void trimFinal()
    {
      long l1 = this.scheduler.now(this.unit);
      long l2 = this.maxAge;
      ReplaySubject.TimedNode localTimedNode;
      for (Object localObject = this.head;; localObject = localTimedNode)
      {
        localTimedNode = (ReplaySubject.TimedNode)((ReplaySubject.TimedNode)localObject).get();
        if (localTimedNode.get() == null)
        {
          this.head = ((ReplaySubject.TimedNode)localObject);
        }
        else
        {
          if (localTimedNode.time <= l1 - l2) {
            continue;
          }
          this.head = ((ReplaySubject.TimedNode)localObject);
        }
        return;
      }
    }
  }
  
  static final class SizeBoundReplayBuffer<T>
    extends AtomicReference<Object>
    implements ReplaySubject.ReplayBuffer<T>
  {
    private static final long serialVersionUID = 1107649250281456395L;
    volatile boolean done;
    volatile ReplaySubject.Node<Object> head;
    final int maxSize;
    int size;
    ReplaySubject.Node<Object> tail;
    
    SizeBoundReplayBuffer(int paramInt)
    {
      this.maxSize = ObjectHelper.verifyPositive(paramInt, "maxSize");
      ReplaySubject.Node localNode = new ReplaySubject.Node(null);
      this.tail = localNode;
      this.head = localNode;
    }
    
    public void add(T paramT)
    {
      ReplaySubject.Node localNode = new ReplaySubject.Node(paramT);
      paramT = this.tail;
      this.tail = localNode;
      this.size += 1;
      paramT.set(localNode);
      trim();
    }
    
    public void addFinal(Object paramObject)
    {
      paramObject = new ReplaySubject.Node(paramObject);
      ReplaySubject.Node localNode = this.tail;
      this.tail = ((ReplaySubject.Node)paramObject);
      this.size += 1;
      localNode.lazySet(paramObject);
      this.done = true;
    }
    
    public T getValue()
    {
      Object localObject1 = this.head;
      Object localObject2 = null;
      for (;;)
      {
        ReplaySubject.Node localNode = (ReplaySubject.Node)((ReplaySubject.Node)localObject1).get();
        if (localNode == null)
        {
          localObject1 = ((ReplaySubject.Node)localObject1).value;
          if (localObject1 == null) {
            return null;
          }
          if ((!NotificationLite.isComplete(localObject1)) && (!NotificationLite.isError(localObject1))) {
            return (T)localObject1;
          }
          return (T)((ReplaySubject.Node)localObject2).value;
        }
        localObject2 = localObject1;
        localObject1 = localNode;
      }
    }
    
    public T[] getValues(T[] paramArrayOfT)
    {
      ReplaySubject.Node localNode = this.head;
      int k = size();
      int j = 0;
      Object localObject2;
      if (k == 0)
      {
        localObject2 = paramArrayOfT;
        if (paramArrayOfT.length != 0)
        {
          paramArrayOfT[0] = null;
          localObject2 = paramArrayOfT;
        }
      }
      else
      {
        localObject2 = localNode;
        int i = j;
        Object localObject1 = paramArrayOfT;
        if (paramArrayOfT.length < k)
        {
          localObject1 = (Object[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), k);
          i = j;
          localObject2 = localNode;
        }
        while (i != k)
        {
          localObject2 = (ReplaySubject.Node)((ReplaySubject.Node)localObject2).get();
          localObject1[i] = ((ReplaySubject.Node)localObject2).value;
          i++;
        }
        localObject2 = localObject1;
        if (localObject1.length > k)
        {
          localObject1[k] = null;
          localObject2 = localObject1;
        }
      }
      return (T[])localObject2;
    }
    
    public void replay(ReplaySubject.ReplayDisposable<T> paramReplayDisposable)
    {
      if (paramReplayDisposable.getAndIncrement() != 0) {
        return;
      }
      Observer localObserver = paramReplayDisposable.actual;
      Object localObject = (ReplaySubject.Node)paramReplayDisposable.index;
      int i;
      if (localObject == null)
      {
        localObject = this.head;
        i = 1;
      }
      else
      {
        i = 1;
      }
      for (;;)
      {
        if (paramReplayDisposable.cancelled)
        {
          paramReplayDisposable.index = null;
          return;
        }
        ReplaySubject.Node localNode = (ReplaySubject.Node)((ReplaySubject.Node)localObject).get();
        if (localNode == null)
        {
          if (((ReplaySubject.Node)localObject).get() == null)
          {
            paramReplayDisposable.index = localObject;
            int j = paramReplayDisposable.addAndGet(-i);
            i = j;
            if (j != 0) {}
          }
        }
        else
        {
          localObject = localNode.value;
          if ((this.done) && (localNode.get() == null))
          {
            if (NotificationLite.isComplete(localObject)) {
              localObserver.onComplete();
            } else {
              localObserver.onError(NotificationLite.getError(localObject));
            }
            paramReplayDisposable.index = null;
            paramReplayDisposable.cancelled = true;
            return;
          }
          localObserver.onNext(localObject);
          localObject = localNode;
        }
      }
    }
    
    public int size()
    {
      Object localObject = this.head;
      int i = 0;
      int j;
      for (;;)
      {
        j = i;
        if (i == Integer.MAX_VALUE) {
          break;
        }
        ReplaySubject.Node localNode = (ReplaySubject.Node)((ReplaySubject.Node)localObject).get();
        if (localNode == null)
        {
          localObject = ((ReplaySubject.Node)localObject).value;
          if (!NotificationLite.isComplete(localObject))
          {
            j = i;
            if (!NotificationLite.isError(localObject)) {
              break;
            }
          }
          j = i - 1;
          break;
        }
        i++;
        localObject = localNode;
      }
      return j;
    }
    
    void trim()
    {
      int i = this.size;
      if (i > this.maxSize)
      {
        this.size = (i - 1);
        this.head = ((ReplaySubject.Node)this.head.get());
      }
    }
  }
  
  static final class TimedNode<T>
    extends AtomicReference<TimedNode<T>>
  {
    private static final long serialVersionUID = 6404226426336033100L;
    final long time;
    final T value;
    
    TimedNode(T paramT, long paramLong)
    {
      this.value = paramT;
      this.time = paramLong;
    }
  }
  
  static final class UnboundedReplayBuffer<T>
    extends AtomicReference<Object>
    implements ReplaySubject.ReplayBuffer<T>
  {
    private static final long serialVersionUID = -733876083048047795L;
    final List<Object> buffer;
    volatile boolean done;
    volatile int size;
    
    UnboundedReplayBuffer(int paramInt)
    {
      this.buffer = new ArrayList(ObjectHelper.verifyPositive(paramInt, "capacityHint"));
    }
    
    public void add(T paramT)
    {
      this.buffer.add(paramT);
      this.size += 1;
    }
    
    public void addFinal(Object paramObject)
    {
      this.buffer.add(paramObject);
      this.size += 1;
      this.done = true;
    }
    
    public T getValue()
    {
      int i = this.size;
      if (i != 0)
      {
        List localList = this.buffer;
        Object localObject = localList.get(i - 1);
        if ((!NotificationLite.isComplete(localObject)) && (!NotificationLite.isError(localObject))) {
          return (T)localObject;
        }
        if (i == 1) {
          return null;
        }
        return (T)localList.get(i - 2);
      }
      return null;
    }
    
    public T[] getValues(T[] paramArrayOfT)
    {
      int j = this.size;
      int k = 0;
      if (j == 0)
      {
        if (paramArrayOfT.length != 0) {
          paramArrayOfT[0] = null;
        }
        return paramArrayOfT;
      }
      List localList = this.buffer;
      Object localObject = localList.get(j - 1);
      int i;
      if (!NotificationLite.isComplete(localObject))
      {
        i = j;
        if (!NotificationLite.isError(localObject)) {}
      }
      else
      {
        j--;
        i = j;
        if (j == 0)
        {
          if (paramArrayOfT.length != 0) {
            paramArrayOfT[0] = null;
          }
          return paramArrayOfT;
        }
      }
      j = k;
      localObject = paramArrayOfT;
      if (paramArrayOfT.length < i) {
        localObject = (Object[])Array.newInstance(paramArrayOfT.getClass().getComponentType(), i);
      }
      for (j = k; j < i; j++) {
        localObject[j] = localList.get(j);
      }
      if (localObject.length > i) {
        localObject[i] = null;
      }
      return (T[])localObject;
    }
    
    public void replay(ReplaySubject.ReplayDisposable<T> paramReplayDisposable)
    {
      if (paramReplayDisposable.getAndIncrement() != 0) {
        return;
      }
      List localList = this.buffer;
      Observer localObserver = paramReplayDisposable.actual;
      Object localObject = (Integer)paramReplayDisposable.index;
      int j = 0;
      int i;
      if (localObject != null)
      {
        j = ((Integer)localObject).intValue();
        i = 1;
      }
      else
      {
        paramReplayDisposable.index = Integer.valueOf(0);
        i = 1;
      }
      int m;
      do
      {
        int k;
        for (;;)
        {
          if (paramReplayDisposable.cancelled)
          {
            paramReplayDisposable.index = null;
            return;
          }
          m = this.size;
          k = j;
          while (m != k)
          {
            if (paramReplayDisposable.cancelled)
            {
              paramReplayDisposable.index = null;
              return;
            }
            localObject = localList.get(k);
            j = m;
            if (this.done)
            {
              int n = k + 1;
              j = m;
              if (n == m)
              {
                m = this.size;
                j = m;
                if (n == m)
                {
                  if (NotificationLite.isComplete(localObject)) {
                    localObserver.onComplete();
                  } else {
                    localObserver.onError(NotificationLite.getError(localObject));
                  }
                  paramReplayDisposable.index = null;
                  paramReplayDisposable.cancelled = true;
                  return;
                }
              }
            }
            localObserver.onNext(localObject);
            k++;
            m = j;
          }
          if (k == this.size) {
            break;
          }
          j = k;
        }
        paramReplayDisposable.index = Integer.valueOf(k);
        m = paramReplayDisposable.addAndGet(-i);
        i = m;
        j = k;
      } while (m != 0);
    }
    
    public int size()
    {
      int i = this.size;
      if (i != 0)
      {
        Object localObject = this.buffer;
        int j = i - 1;
        localObject = ((List)localObject).get(j);
        if ((!NotificationLite.isComplete(localObject)) && (!NotificationLite.isError(localObject))) {
          return i;
        }
        return j;
      }
      return 0;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/ReplaySubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */