package io.reactivex.subjects;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

@Experimental
public final class SingleSubject<T>
  extends Single<T>
  implements SingleObserver<T>
{
  static final SingleDisposable[] EMPTY = new SingleDisposable[0];
  static final SingleDisposable[] TERMINATED = new SingleDisposable[0];
  Throwable error;
  final AtomicReference<SingleDisposable<T>[]> observers = new AtomicReference(EMPTY);
  final AtomicBoolean once = new AtomicBoolean();
  T value;
  
  @CheckReturnValue
  public static <T> SingleSubject<T> create()
  {
    return new SingleSubject();
  }
  
  boolean add(SingleDisposable<T> paramSingleDisposable)
  {
    SingleDisposable[] arrayOfSingleDisposable2;
    SingleDisposable[] arrayOfSingleDisposable1;
    do
    {
      arrayOfSingleDisposable2 = (SingleDisposable[])this.observers.get();
      if (arrayOfSingleDisposable2 == TERMINATED) {
        return false;
      }
      int i = arrayOfSingleDisposable2.length;
      arrayOfSingleDisposable1 = new SingleDisposable[i + 1];
      System.arraycopy(arrayOfSingleDisposable2, 0, arrayOfSingleDisposable1, 0, i);
      arrayOfSingleDisposable1[i] = paramSingleDisposable;
    } while (!this.observers.compareAndSet(arrayOfSingleDisposable2, arrayOfSingleDisposable1));
    return true;
  }
  
  public Throwable getThrowable()
  {
    if (this.observers.get() == TERMINATED) {
      return this.error;
    }
    return null;
  }
  
  public T getValue()
  {
    if (this.observers.get() == TERMINATED) {
      return (T)this.value;
    }
    return null;
  }
  
  public boolean hasObservers()
  {
    boolean bool;
    if (((SingleDisposable[])this.observers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.observers.get() == TERMINATED) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasValue()
  {
    boolean bool;
    if ((this.observers.get() == TERMINATED) && (this.value != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  int observerCount()
  {
    return ((SingleDisposable[])this.observers.get()).length;
  }
  
  public void onError(Throwable paramThrowable)
  {
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("Null errors are not allowed in 2.x");
    }
    paramThrowable = this.once;
    int i = 0;
    if (paramThrowable.compareAndSet(false, true))
    {
      this.error = ((Throwable)localObject);
      paramThrowable = (SingleDisposable[])this.observers.getAndSet(TERMINATED);
      int j = paramThrowable.length;
      while (i < j)
      {
        paramThrowable[i].actual.onError((Throwable)localObject);
        i++;
      }
    }
    RxJavaPlugins.onError((Throwable)localObject);
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (this.observers.get() == TERMINATED) {
      paramDisposable.dispose();
    }
  }
  
  public void onSuccess(T paramT)
  {
    if (paramT == null)
    {
      onError(new NullPointerException("Null values are not allowed in 2.x"));
      return;
    }
    Object localObject = this.once;
    int i = 0;
    if (((AtomicBoolean)localObject).compareAndSet(false, true))
    {
      this.value = paramT;
      localObject = (SingleDisposable[])this.observers.getAndSet(TERMINATED);
      int j = localObject.length;
      while (i < j)
      {
        localObject[i].actual.onSuccess(paramT);
        i++;
      }
    }
  }
  
  void remove(SingleDisposable<T> paramSingleDisposable)
  {
    SingleDisposable[] arrayOfSingleDisposable2;
    SingleDisposable[] arrayOfSingleDisposable1;
    do
    {
      arrayOfSingleDisposable2 = (SingleDisposable[])this.observers.get();
      int m = arrayOfSingleDisposable2.length;
      if (m == 0) {
        return;
      }
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfSingleDisposable2[i] == paramSingleDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfSingleDisposable1 = EMPTY;
      }
      else
      {
        arrayOfSingleDisposable1 = new SingleDisposable[m - 1];
        System.arraycopy(arrayOfSingleDisposable2, 0, arrayOfSingleDisposable1, 0, j);
        System.arraycopy(arrayOfSingleDisposable2, j + 1, arrayOfSingleDisposable1, j, m - j - 1);
      }
    } while (!this.observers.compareAndSet(arrayOfSingleDisposable2, arrayOfSingleDisposable1));
  }
  
  protected void subscribeActual(SingleObserver<? super T> paramSingleObserver)
  {
    Object localObject = new SingleDisposable(paramSingleObserver, this);
    paramSingleObserver.onSubscribe((Disposable)localObject);
    if (add((SingleDisposable)localObject))
    {
      if (((SingleDisposable)localObject).isDisposed()) {
        remove((SingleDisposable)localObject);
      }
    }
    else
    {
      localObject = this.error;
      if (localObject != null) {
        paramSingleObserver.onError((Throwable)localObject);
      } else {
        paramSingleObserver.onSuccess(this.value);
      }
    }
  }
  
  static final class SingleDisposable<T>
    extends AtomicReference<SingleSubject<T>>
    implements Disposable
  {
    private static final long serialVersionUID = -7650903191002190468L;
    final SingleObserver<? super T> actual;
    
    SingleDisposable(SingleObserver<? super T> paramSingleObserver, SingleSubject<T> paramSingleSubject)
    {
      this.actual = paramSingleObserver;
      lazySet(paramSingleSubject);
    }
    
    public void dispose()
    {
      SingleSubject localSingleSubject = (SingleSubject)getAndSet(null);
      if (localSingleSubject != null) {
        localSingleSubject.remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      boolean bool;
      if (get() == null) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/SingleSubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */