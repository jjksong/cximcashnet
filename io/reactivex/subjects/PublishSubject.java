package io.reactivex.subjects;

import io.reactivex.Observer;
import io.reactivex.annotations.CheckReturnValue;
import io.reactivex.disposables.Disposable;
import io.reactivex.plugins.RxJavaPlugins;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class PublishSubject<T>
  extends Subject<T>
{
  static final PublishDisposable[] EMPTY = new PublishDisposable[0];
  static final PublishDisposable[] TERMINATED = new PublishDisposable[0];
  Throwable error;
  final AtomicReference<PublishDisposable<T>[]> subscribers = new AtomicReference(EMPTY);
  
  @CheckReturnValue
  public static <T> PublishSubject<T> create()
  {
    return new PublishSubject();
  }
  
  boolean add(PublishDisposable<T> paramPublishDisposable)
  {
    PublishDisposable[] arrayOfPublishDisposable2;
    PublishDisposable[] arrayOfPublishDisposable1;
    do
    {
      arrayOfPublishDisposable2 = (PublishDisposable[])this.subscribers.get();
      if (arrayOfPublishDisposable2 == TERMINATED) {
        return false;
      }
      int i = arrayOfPublishDisposable2.length;
      arrayOfPublishDisposable1 = new PublishDisposable[i + 1];
      System.arraycopy(arrayOfPublishDisposable2, 0, arrayOfPublishDisposable1, 0, i);
      arrayOfPublishDisposable1[i] = paramPublishDisposable;
    } while (!this.subscribers.compareAndSet(arrayOfPublishDisposable2, arrayOfPublishDisposable1));
    return true;
  }
  
  public Throwable getThrowable()
  {
    if (this.subscribers.get() == TERMINATED) {
      return this.error;
    }
    return null;
  }
  
  public boolean hasComplete()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.error == null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasObservers()
  {
    boolean bool;
    if (((PublishDisposable[])this.subscribers.get()).length != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean hasThrowable()
  {
    boolean bool;
    if ((this.subscribers.get() == TERMINATED) && (this.error != null)) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void onComplete()
  {
    Object localObject = this.subscribers.get();
    PublishDisposable[] arrayOfPublishDisposable = TERMINATED;
    if (localObject == arrayOfPublishDisposable) {
      return;
    }
    localObject = (PublishDisposable[])this.subscribers.getAndSet(arrayOfPublishDisposable);
    int j = localObject.length;
    for (int i = 0; i < j; i++) {
      localObject[i].onComplete();
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (this.subscribers.get() == TERMINATED)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    this.error = ((Throwable)localObject);
    paramThrowable = (PublishDisposable[])this.subscribers.getAndSet(TERMINATED);
    int j = paramThrowable.length;
    for (int i = 0; i < j; i++) {
      paramThrowable[i].onError((Throwable)localObject);
    }
  }
  
  public void onNext(T paramT)
  {
    if (this.subscribers.get() == TERMINATED) {
      return;
    }
    if (paramT == null)
    {
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
      return;
    }
    PublishDisposable[] arrayOfPublishDisposable = (PublishDisposable[])this.subscribers.get();
    int j = arrayOfPublishDisposable.length;
    for (int i = 0; i < j; i++) {
      arrayOfPublishDisposable[i].onNext(paramT);
    }
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (this.subscribers.get() == TERMINATED) {
      paramDisposable.dispose();
    }
  }
  
  void remove(PublishDisposable<T> paramPublishDisposable)
  {
    PublishDisposable[] arrayOfPublishDisposable2;
    PublishDisposable[] arrayOfPublishDisposable1;
    do
    {
      arrayOfPublishDisposable2 = (PublishDisposable[])this.subscribers.get();
      if ((arrayOfPublishDisposable2 == TERMINATED) || (arrayOfPublishDisposable2 == EMPTY)) {
        break;
      }
      int m = arrayOfPublishDisposable2.length;
      int k = -1;
      int j;
      for (int i = 0;; i++)
      {
        j = k;
        if (i >= m) {
          break;
        }
        if (arrayOfPublishDisposable2[i] == paramPublishDisposable)
        {
          j = i;
          break;
        }
      }
      if (j < 0) {
        return;
      }
      if (m == 1)
      {
        arrayOfPublishDisposable1 = EMPTY;
      }
      else
      {
        arrayOfPublishDisposable1 = new PublishDisposable[m - 1];
        System.arraycopy(arrayOfPublishDisposable2, 0, arrayOfPublishDisposable1, 0, j);
        System.arraycopy(arrayOfPublishDisposable2, j + 1, arrayOfPublishDisposable1, j, m - j - 1);
      }
    } while (!this.subscribers.compareAndSet(arrayOfPublishDisposable2, arrayOfPublishDisposable1));
    return;
  }
  
  public void subscribeActual(Observer<? super T> paramObserver)
  {
    Object localObject = new PublishDisposable(paramObserver, this);
    paramObserver.onSubscribe((Disposable)localObject);
    if (add((PublishDisposable)localObject))
    {
      if (((PublishDisposable)localObject).isDisposed()) {
        remove((PublishDisposable)localObject);
      }
    }
    else
    {
      localObject = this.error;
      if (localObject != null) {
        paramObserver.onError((Throwable)localObject);
      } else {
        paramObserver.onComplete();
      }
    }
  }
  
  static final class PublishDisposable<T>
    extends AtomicBoolean
    implements Disposable
  {
    private static final long serialVersionUID = 3562861878281475070L;
    final Observer<? super T> actual;
    final PublishSubject<T> parent;
    
    PublishDisposable(Observer<? super T> paramObserver, PublishSubject<T> paramPublishSubject)
    {
      this.actual = paramObserver;
      this.parent = paramPublishSubject;
    }
    
    public void dispose()
    {
      if (compareAndSet(false, true)) {
        this.parent.remove(this);
      }
    }
    
    public boolean isDisposed()
    {
      return get();
    }
    
    public void onComplete()
    {
      if (!get()) {
        this.actual.onComplete();
      }
    }
    
    public void onError(Throwable paramThrowable)
    {
      if (get()) {
        RxJavaPlugins.onError(paramThrowable);
      } else {
        this.actual.onError(paramThrowable);
      }
    }
    
    public void onNext(T paramT)
    {
      if (!get()) {
        this.actual.onNext(paramT);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subjects/PublishSubject.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */