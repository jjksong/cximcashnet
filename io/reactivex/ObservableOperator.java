package io.reactivex;

public abstract interface ObservableOperator<Downstream, Upstream>
{
  public abstract Observer<? super Upstream> apply(Observer<? super Downstream> paramObserver)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/ObservableOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */