package io.reactivex;

import org.reactivestreams.Publisher;

public abstract interface FlowableTransformer<Upstream, Downstream>
{
  public abstract Publisher<Downstream> apply(Flowable<Upstream> paramFlowable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/FlowableTransformer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */