package io.reactivex;

public abstract interface CompletableOperator
{
  public abstract CompletableObserver apply(CompletableObserver paramCompletableObserver)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/CompletableOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */