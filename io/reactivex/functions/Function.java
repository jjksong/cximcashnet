package io.reactivex.functions;

public abstract interface Function<T, R>
{
  public abstract R apply(T paramT)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/functions/Function.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */