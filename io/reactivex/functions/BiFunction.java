package io.reactivex.functions;

public abstract interface BiFunction<T1, T2, R>
{
  public abstract R apply(T1 paramT1, T2 paramT2)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/functions/BiFunction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */