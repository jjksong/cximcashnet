package io.reactivex.functions;

public abstract interface LongConsumer
{
  public abstract void accept(long paramLong)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/functions/LongConsumer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */