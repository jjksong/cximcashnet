package io.reactivex.functions;

public abstract interface Function8<T1, T2, T3, T4, T5, T6, T7, T8, R>
{
  public abstract R apply(T1 paramT1, T2 paramT2, T3 paramT3, T4 paramT4, T5 paramT5, T6 paramT6, T7 paramT7, T8 paramT8)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/functions/Function8.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */