package io.reactivex.functions;

public abstract interface IntFunction<T>
{
  public abstract T apply(int paramInt)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/functions/IntFunction.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */