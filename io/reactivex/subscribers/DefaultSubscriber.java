package io.reactivex.subscribers;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public abstract class DefaultSubscriber<T>
  implements Subscriber<T>
{
  private Subscription s;
  
  protected final void cancel()
  {
    Subscription localSubscription = this.s;
    this.s = SubscriptionHelper.CANCELLED;
    localSubscription.cancel();
  }
  
  protected void onStart()
  {
    request(Long.MAX_VALUE);
  }
  
  public final void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.validate(this.s, paramSubscription))
    {
      this.s = paramSubscription;
      onStart();
    }
  }
  
  protected final void request(long paramLong)
  {
    Subscription localSubscription = this.s;
    if (localSubscription != null) {
      localSubscription.request(paramLong);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subscribers/DefaultSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */