package io.reactivex.subscribers;

import io.reactivex.disposables.Disposable;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public abstract class DisposableSubscriber<T>
  implements Subscriber<T>, Disposable
{
  final AtomicReference<Subscription> s = new AtomicReference();
  
  protected final void cancel()
  {
    dispose();
  }
  
  public final void dispose()
  {
    SubscriptionHelper.cancel(this.s);
  }
  
  public final boolean isDisposed()
  {
    boolean bool;
    if (this.s.get() == SubscriptionHelper.CANCELLED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected void onStart()
  {
    ((Subscription)this.s.get()).request(Long.MAX_VALUE);
  }
  
  public final void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.setOnce(this.s, paramSubscription)) {
      onStart();
    }
  }
  
  protected final void request(long paramLong)
  {
    ((Subscription)this.s.get()).request(paramLong);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subscribers/DisposableSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */