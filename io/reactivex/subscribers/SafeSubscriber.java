package io.reactivex.subscribers;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.subscriptions.EmptySubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class SafeSubscriber<T>
  implements Subscriber<T>, Subscription
{
  final Subscriber<? super T> actual;
  boolean done;
  Subscription s;
  
  public SafeSubscriber(Subscriber<? super T> paramSubscriber)
  {
    this.actual = paramSubscriber;
  }
  
  public void cancel()
  {
    try
    {
      this.s.cancel();
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      RxJavaPlugins.onError(localThrowable);
    }
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    this.done = true;
    if (this.s == null)
    {
      onCompleteNoSubscription();
      return;
    }
    try
    {
      this.actual.onComplete();
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      RxJavaPlugins.onError(localThrowable);
    }
  }
  
  void onCompleteNoSubscription()
  {
    NullPointerException localNullPointerException = new NullPointerException("Subscription not set!");
    try
    {
      this.actual.onSubscribe(EmptySubscription.INSTANCE);
      try
      {
        this.actual.onError(localNullPointerException);
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        RxJavaPlugins.onError(new CompositeException(new Throwable[] { localNullPointerException, localThrowable1 }));
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { localNullPointerException, localThrowable2 }));
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (this.done)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    this.done = true;
    if (this.s == null)
    {
      localObject = new NullPointerException("Subscription not set!");
      try
      {
        this.actual.onSubscribe(EmptySubscription.INSTANCE);
        try
        {
          Subscriber localSubscriber = this.actual;
          CompositeException localCompositeException = new io/reactivex/exceptions/CompositeException;
          localCompositeException.<init>(new Throwable[] { paramThrowable, localObject });
          localSubscriber.onError(localCompositeException);
        }
        catch (Throwable localThrowable1)
        {
          Exceptions.throwIfFatal(localThrowable1);
          RxJavaPlugins.onError(new CompositeException(new Throwable[] { paramThrowable, localObject, localThrowable1 }));
        }
        return;
      }
      catch (Throwable localThrowable2)
      {
        Exceptions.throwIfFatal(localThrowable2);
        RxJavaPlugins.onError(new CompositeException(new Throwable[] { paramThrowable, localObject, localThrowable2 }));
        return;
      }
    }
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    try
    {
      this.actual.onError((Throwable)localObject);
    }
    catch (Throwable paramThrowable)
    {
      Exceptions.throwIfFatal(paramThrowable);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { localObject, paramThrowable }));
    }
  }
  
  public void onNext(T paramT)
  {
    if (this.done) {
      return;
    }
    if (this.s == null)
    {
      onNextNoSubscription();
      return;
    }
    if (paramT == null)
    {
      paramT = new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
      try
      {
        this.s.cancel();
        onError(paramT);
        return;
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        onError(new CompositeException(new Throwable[] { paramT, localThrowable1 }));
        return;
      }
    }
    try
    {
      this.actual.onNext(paramT);
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
    }
    try
    {
      this.s.cancel();
      onError(localThrowable2);
      return;
    }
    catch (Throwable paramT)
    {
      Exceptions.throwIfFatal(paramT);
      onError(new CompositeException(new Throwable[] { localThrowable2, paramT }));
    }
  }
  
  void onNextNoSubscription()
  {
    this.done = true;
    NullPointerException localNullPointerException = new NullPointerException("Subscription not set!");
    try
    {
      this.actual.onSubscribe(EmptySubscription.INSTANCE);
      try
      {
        this.actual.onError(localNullPointerException);
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        RxJavaPlugins.onError(new CompositeException(new Throwable[] { localNullPointerException, localThrowable1 }));
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { localNullPointerException, localThrowable2 }));
    }
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.validate(this.s, paramSubscription))
    {
      this.s = paramSubscription;
      try
      {
        this.actual.onSubscribe(this);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.done = true;
        try
        {
          paramSubscription.cancel();
          RxJavaPlugins.onError(localThrowable);
        }
        catch (Throwable paramSubscription)
        {
          Exceptions.throwIfFatal(paramSubscription);
          RxJavaPlugins.onError(new CompositeException(new Throwable[] { localThrowable, paramSubscription }));
          return;
        }
      }
    }
  }
  
  public void request(long paramLong)
  {
    try
    {
      this.s.request(paramLong);
    }
    catch (Throwable localThrowable1)
    {
      Exceptions.throwIfFatal(localThrowable1);
    }
    try
    {
      this.s.cancel();
      RxJavaPlugins.onError(localThrowable1);
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { localThrowable1, localThrowable2 }));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subscribers/SafeSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */