package io.reactivex.subscribers;

import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public final class SerializedSubscriber<T>
  implements Subscriber<T>, Subscription
{
  static final int QUEUE_LINK_SIZE = 4;
  final Subscriber<? super T> actual;
  final boolean delayError;
  volatile boolean done;
  boolean emitting;
  AppendOnlyLinkedArrayList<Object> queue;
  Subscription subscription;
  
  public SerializedSubscriber(Subscriber<? super T> paramSubscriber)
  {
    this(paramSubscriber, false);
  }
  
  public SerializedSubscriber(Subscriber<? super T> paramSubscriber, boolean paramBoolean)
  {
    this.actual = paramSubscriber;
    this.delayError = paramBoolean;
  }
  
  public void cancel()
  {
    this.subscription.cancel();
  }
  
  void emitLoop()
  {
    for (;;)
    {
      try
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList = this.queue;
        if (localAppendOnlyLinkedArrayList == null)
        {
          this.emitting = false;
          return;
        }
        this.queue = null;
        if (!localAppendOnlyLinkedArrayList.accept(this.actual)) {
          continue;
        }
        return;
      }
      finally {}
    }
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    try
    {
      if (this.done) {
        return;
      }
      if (this.emitting)
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
        if (localAppendOnlyLinkedArrayList2 == null)
        {
          localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
          localAppendOnlyLinkedArrayList1.<init>(4);
          this.queue = localAppendOnlyLinkedArrayList1;
        }
        localAppendOnlyLinkedArrayList1.add(NotificationLite.complete());
        return;
      }
      this.done = true;
      this.emitting = true;
      this.actual.onComplete();
      return;
    }
    finally {}
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (this.done)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    try
    {
      boolean bool = this.done;
      int i = 1;
      if (!bool)
      {
        if (this.emitting)
        {
          this.done = true;
          AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
          AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
          if (localAppendOnlyLinkedArrayList2 == null)
          {
            localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
            localAppendOnlyLinkedArrayList1.<init>(4);
            this.queue = localAppendOnlyLinkedArrayList1;
          }
          paramThrowable = NotificationLite.error(paramThrowable);
          if (this.delayError) {
            localAppendOnlyLinkedArrayList1.add(paramThrowable);
          } else {
            localAppendOnlyLinkedArrayList1.setFirst(paramThrowable);
          }
          return;
        }
        this.done = true;
        this.emitting = true;
        i = 0;
      }
      if (i != 0)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.actual.onError(paramThrowable);
      return;
    }
    finally {}
  }
  
  public void onNext(T paramT)
  {
    if (this.done) {
      return;
    }
    if (paramT == null)
    {
      this.subscription.cancel();
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
      return;
    }
    try
    {
      if (this.done) {
        return;
      }
      if (this.emitting)
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
        if (localAppendOnlyLinkedArrayList2 == null)
        {
          localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
          localAppendOnlyLinkedArrayList1.<init>(4);
          this.queue = localAppendOnlyLinkedArrayList1;
        }
        localAppendOnlyLinkedArrayList1.add(NotificationLite.next(paramT));
        return;
      }
      this.emitting = true;
      this.actual.onNext(paramT);
      emitLoop();
      return;
    }
    finally {}
  }
  
  public void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.validate(this.subscription, paramSubscription))
    {
      this.subscription = paramSubscription;
      this.actual.onSubscribe(this);
    }
  }
  
  public void request(long paramLong)
  {
    this.subscription.request(paramLong);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subscribers/SerializedSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */