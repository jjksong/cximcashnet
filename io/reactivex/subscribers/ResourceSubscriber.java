package io.reactivex.subscribers;

import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.ListCompositeDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public abstract class ResourceSubscriber<T>
  implements Subscriber<T>, Disposable
{
  private final AtomicLong missedRequested = new AtomicLong();
  private final ListCompositeDisposable resources = new ListCompositeDisposable();
  private final AtomicReference<Subscription> s = new AtomicReference();
  
  public final void add(Disposable paramDisposable)
  {
    ObjectHelper.requireNonNull(paramDisposable, "resource is null");
    this.resources.add(paramDisposable);
  }
  
  public final void dispose()
  {
    if (SubscriptionHelper.cancel(this.s)) {
      this.resources.dispose();
    }
  }
  
  public final boolean isDisposed()
  {
    return SubscriptionHelper.isCancelled((Subscription)this.s.get());
  }
  
  protected void onStart()
  {
    request(Long.MAX_VALUE);
  }
  
  public final void onSubscribe(Subscription paramSubscription)
  {
    if (SubscriptionHelper.deferredSetOnce(this.s, this.missedRequested, paramSubscription)) {
      onStart();
    }
  }
  
  protected final void request(long paramLong)
  {
    SubscriptionHelper.deferredRequest(this.s, this.missedRequested, paramLong);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subscribers/ResourceSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */