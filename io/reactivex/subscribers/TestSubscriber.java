package io.reactivex.subscribers;

import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.fuseable.QueueSubscription;
import io.reactivex.internal.subscriptions.SubscriptionHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.observers.BaseTestConsumer;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

public class TestSubscriber<T>
  extends BaseTestConsumer<T, TestSubscriber<T>>
  implements Subscriber<T>, Subscription, Disposable
{
  private final Subscriber<? super T> actual;
  private volatile boolean cancelled;
  private final AtomicLong missedRequested;
  private QueueSubscription<T> qs;
  private final AtomicReference<Subscription> subscription;
  
  public TestSubscriber()
  {
    this(EmptySubscriber.INSTANCE, Long.MAX_VALUE);
  }
  
  public TestSubscriber(long paramLong)
  {
    this(EmptySubscriber.INSTANCE, paramLong);
  }
  
  public TestSubscriber(Subscriber<? super T> paramSubscriber)
  {
    this(paramSubscriber, Long.MAX_VALUE);
  }
  
  public TestSubscriber(Subscriber<? super T> paramSubscriber, long paramLong)
  {
    this.actual = paramSubscriber;
    this.subscription = new AtomicReference();
    this.missedRequested = new AtomicLong(paramLong);
  }
  
  public static <T> TestSubscriber<T> create()
  {
    return new TestSubscriber();
  }
  
  public static <T> TestSubscriber<T> create(long paramLong)
  {
    return new TestSubscriber(paramLong);
  }
  
  public static <T> TestSubscriber<T> create(Subscriber<? super T> paramSubscriber)
  {
    return new TestSubscriber(paramSubscriber);
  }
  
  static String fusionModeToString(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Unknown(");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    case 2: 
      return "ASYNC";
    case 1: 
      return "SYNC";
    }
    return "NONE";
  }
  
  final TestSubscriber<T> assertFuseable()
  {
    if (this.qs != null) {
      return this;
    }
    throw new AssertionError("Upstream is not fuseable.");
  }
  
  final TestSubscriber<T> assertFusionMode(int paramInt)
  {
    int i = this.establishedFusionMode;
    if (i != paramInt)
    {
      if (this.qs != null)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Fusion mode different. Expected: ");
        localStringBuilder.append(fusionModeToString(paramInt));
        localStringBuilder.append(", actual: ");
        localStringBuilder.append(fusionModeToString(i));
        throw new AssertionError(localStringBuilder.toString());
      }
      throw fail("Upstream is not fuseable");
    }
    return this;
  }
  
  final TestSubscriber<T> assertNotFuseable()
  {
    if (this.qs == null) {
      return this;
    }
    throw new AssertionError("Upstream is fuseable.");
  }
  
  public final TestSubscriber<T> assertNotSubscribed()
  {
    if (this.subscription.get() == null)
    {
      if (this.errors.isEmpty()) {
        return this;
      }
      throw fail("Not subscribed but errors found");
    }
    throw fail("Subscribed!");
  }
  
  public final TestSubscriber<T> assertOf(Consumer<? super TestSubscriber<T>> paramConsumer)
  {
    try
    {
      paramConsumer.accept(this);
      return this;
    }
    catch (Throwable paramConsumer)
    {
      throw ExceptionHelper.wrapOrThrow(paramConsumer);
    }
  }
  
  public final TestSubscriber<T> assertSubscribed()
  {
    if (this.subscription.get() != null) {
      return this;
    }
    throw fail("Not subscribed!");
  }
  
  public final void cancel()
  {
    if (!this.cancelled)
    {
      this.cancelled = true;
      SubscriptionHelper.cancel(this.subscription);
    }
  }
  
  public final void dispose()
  {
    cancel();
  }
  
  public final boolean hasSubscription()
  {
    boolean bool;
    if (this.subscription.get() != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final boolean isCancelled()
  {
    return this.cancelled;
  }
  
  public final boolean isDisposed()
  {
    return this.cancelled;
  }
  
  public void onComplete()
  {
    if (!this.checkSubscriptionOnce)
    {
      this.checkSubscriptionOnce = true;
      if (this.subscription.get() == null) {
        this.errors.add(new IllegalStateException("onSubscribe not called in proper order"));
      }
    }
    try
    {
      this.lastThread = Thread.currentThread();
      this.completions += 1L;
      this.actual.onComplete();
      return;
    }
    finally
    {
      this.done.countDown();
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (!this.checkSubscriptionOnce)
    {
      this.checkSubscriptionOnce = true;
      if (this.subscription.get() == null) {
        this.errors.add(new NullPointerException("onSubscribe not called in proper order"));
      }
    }
    try
    {
      this.lastThread = Thread.currentThread();
      this.errors.add(paramThrowable);
      if (paramThrowable == null)
      {
        List localList = this.errors;
        IllegalStateException localIllegalStateException = new java/lang/IllegalStateException;
        localIllegalStateException.<init>("onError received a null Throwable");
        localList.add(localIllegalStateException);
      }
      this.actual.onError(paramThrowable);
      return;
    }
    finally
    {
      this.done.countDown();
    }
  }
  
  public void onNext(T paramT)
  {
    if (!this.checkSubscriptionOnce)
    {
      this.checkSubscriptionOnce = true;
      if (this.subscription.get() == null) {
        this.errors.add(new IllegalStateException("onSubscribe not called in proper order"));
      }
    }
    this.lastThread = Thread.currentThread();
    if (this.establishedFusionMode == 2) {
      try
      {
        for (;;)
        {
          paramT = this.qs.poll();
          if (paramT == null) {
            break;
          }
          this.values.add(paramT);
        }
        return;
      }
      catch (Throwable paramT)
      {
        this.errors.add(paramT);
      }
    }
    this.values.add(paramT);
    if (paramT == null) {
      this.errors.add(new NullPointerException("onNext received a null value"));
    }
    this.actual.onNext(paramT);
  }
  
  protected void onStart() {}
  
  public void onSubscribe(Subscription paramSubscription)
  {
    this.lastThread = Thread.currentThread();
    if (paramSubscription == null)
    {
      this.errors.add(new NullPointerException("onSubscribe received a null Subscription"));
      return;
    }
    if (!this.subscription.compareAndSet(null, paramSubscription))
    {
      paramSubscription.cancel();
      if (this.subscription.get() != SubscriptionHelper.CANCELLED)
      {
        List localList = this.errors;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("onSubscribe received multiple subscriptions: ");
        localStringBuilder.append(paramSubscription);
        localList.add(new IllegalStateException(localStringBuilder.toString()));
      }
      return;
    }
    if ((this.initialFusionMode != 0) && ((paramSubscription instanceof QueueSubscription)))
    {
      this.qs = ((QueueSubscription)paramSubscription);
      int i = this.qs.requestFusion(this.initialFusionMode);
      this.establishedFusionMode = i;
      if (i == 1)
      {
        this.checkSubscriptionOnce = true;
        this.lastThread = Thread.currentThread();
        try
        {
          for (;;)
          {
            paramSubscription = this.qs.poll();
            if (paramSubscription == null) {
              break;
            }
            this.values.add(paramSubscription);
          }
          this.completions += 1L;
        }
        catch (Throwable paramSubscription)
        {
          this.errors.add(paramSubscription);
        }
        return;
      }
    }
    this.actual.onSubscribe(paramSubscription);
    long l = this.missedRequested.getAndSet(0L);
    if (l != 0L) {
      paramSubscription.request(l);
    }
    onStart();
  }
  
  public final void request(long paramLong)
  {
    SubscriptionHelper.deferredRequest(this.subscription, this.missedRequested, paramLong);
  }
  
  @Experimental
  public final TestSubscriber<T> requestMore(long paramLong)
  {
    request(paramLong);
    return this;
  }
  
  final TestSubscriber<T> setInitialFusionMode(int paramInt)
  {
    this.initialFusionMode = paramInt;
    return this;
  }
  
  static enum EmptySubscriber
    implements Subscriber<Object>
  {
    INSTANCE;
    
    private EmptySubscriber() {}
    
    public void onComplete() {}
    
    public void onError(Throwable paramThrowable) {}
    
    public void onNext(Object paramObject) {}
    
    public void onSubscribe(Subscription paramSubscription) {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/subscribers/TestSubscriber.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */