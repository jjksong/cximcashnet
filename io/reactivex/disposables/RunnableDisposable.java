package io.reactivex.disposables;

final class RunnableDisposable
  extends ReferenceDisposable<Runnable>
{
  private static final long serialVersionUID = -8219729196779211169L;
  
  RunnableDisposable(Runnable paramRunnable)
  {
    super(paramRunnable);
  }
  
  protected void onDisposed(Runnable paramRunnable)
  {
    paramRunnable.run();
  }
  
  public String toString()
  {
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("RunnableDisposable(disposed=");
    localStringBuilder.append(isDisposed());
    localStringBuilder.append(", ");
    localStringBuilder.append(get());
    localStringBuilder.append(")");
    return localStringBuilder.toString();
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/disposables/RunnableDisposable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */