package io.reactivex.disposables;

import io.reactivex.functions.Action;
import io.reactivex.internal.util.ExceptionHelper;

final class ActionDisposable
  extends ReferenceDisposable<Action>
{
  private static final long serialVersionUID = -8219729196779211169L;
  
  ActionDisposable(Action paramAction)
  {
    super(paramAction);
  }
  
  protected void onDisposed(Action paramAction)
  {
    try
    {
      paramAction.run();
      return;
    }
    catch (Throwable paramAction)
    {
      throw ExceptionHelper.wrapOrThrow(paramAction);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/disposables/ActionDisposable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */