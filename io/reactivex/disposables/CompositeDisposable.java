package io.reactivex.disposables;

import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableContainer;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.ExceptionHelper;
import io.reactivex.internal.util.OpenHashSet;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class CompositeDisposable
  implements Disposable, DisposableContainer
{
  volatile boolean disposed;
  OpenHashSet<Disposable> resources;
  
  public CompositeDisposable() {}
  
  public CompositeDisposable(Iterable<? extends Disposable> paramIterable)
  {
    ObjectHelper.requireNonNull(paramIterable, "resources is null");
    this.resources = new OpenHashSet();
    paramIterable = paramIterable.iterator();
    while (paramIterable.hasNext())
    {
      Disposable localDisposable = (Disposable)paramIterable.next();
      ObjectHelper.requireNonNull(localDisposable, "Disposable item is null");
      this.resources.add(localDisposable);
    }
  }
  
  public CompositeDisposable(Disposable... paramVarArgs)
  {
    ObjectHelper.requireNonNull(paramVarArgs, "resources is null");
    this.resources = new OpenHashSet(paramVarArgs.length + 1);
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++)
    {
      Disposable localDisposable = paramVarArgs[i];
      ObjectHelper.requireNonNull(localDisposable, "Disposable item is null");
      this.resources.add(localDisposable);
    }
  }
  
  public boolean add(Disposable paramDisposable)
  {
    ObjectHelper.requireNonNull(paramDisposable, "d is null");
    if (!this.disposed) {
      try
      {
        if (!this.disposed)
        {
          OpenHashSet localOpenHashSet2 = this.resources;
          OpenHashSet localOpenHashSet1 = localOpenHashSet2;
          if (localOpenHashSet2 == null)
          {
            localOpenHashSet1 = new io/reactivex/internal/util/OpenHashSet;
            localOpenHashSet1.<init>();
            this.resources = localOpenHashSet1;
          }
          localOpenHashSet1.add(paramDisposable);
          return true;
        }
      }
      finally {}
    }
    paramDisposable.dispose();
    return false;
  }
  
  public boolean addAll(Disposable... paramVarArgs)
  {
    ObjectHelper.requireNonNull(paramVarArgs, "ds is null");
    boolean bool = this.disposed;
    int i = 0;
    if (!bool) {
      try
      {
        if (!this.disposed)
        {
          Object localObject2 = this.resources;
          Object localObject1 = localObject2;
          if (localObject2 == null)
          {
            localObject1 = new io/reactivex/internal/util/OpenHashSet;
            ((OpenHashSet)localObject1).<init>(paramVarArgs.length + 1);
            this.resources = ((OpenHashSet)localObject1);
          }
          j = paramVarArgs.length;
          while (i < j)
          {
            localObject2 = paramVarArgs[i];
            ObjectHelper.requireNonNull(localObject2, "d is null");
            ((OpenHashSet)localObject1).add(localObject2);
            i++;
          }
          return true;
        }
      }
      finally {}
    }
    int j = paramVarArgs.length;
    for (i = 0; i < j; i++) {
      paramVarArgs[i].dispose();
    }
    return false;
  }
  
  public void clear()
  {
    if (this.disposed) {
      return;
    }
    try
    {
      if (this.disposed) {
        return;
      }
      OpenHashSet localOpenHashSet = this.resources;
      this.resources = null;
      dispose(localOpenHashSet);
      return;
    }
    finally {}
  }
  
  public boolean delete(Disposable paramDisposable)
  {
    ObjectHelper.requireNonNull(paramDisposable, "Disposable item is null");
    if (this.disposed) {
      return false;
    }
    try
    {
      if (this.disposed) {
        return false;
      }
      OpenHashSet localOpenHashSet = this.resources;
      return (localOpenHashSet != null) && (localOpenHashSet.remove(paramDisposable));
    }
    finally {}
  }
  
  public void dispose()
  {
    if (this.disposed) {
      return;
    }
    try
    {
      if (this.disposed) {
        return;
      }
      this.disposed = true;
      OpenHashSet localOpenHashSet = this.resources;
      this.resources = null;
      dispose(localOpenHashSet);
      return;
    }
    finally {}
  }
  
  void dispose(OpenHashSet<Disposable> paramOpenHashSet)
  {
    if (paramOpenHashSet == null) {
      return;
    }
    Object[] arrayOfObject = paramOpenHashSet.keys();
    int j = arrayOfObject.length;
    paramOpenHashSet = null;
    int i = 0;
    while (i < j)
    {
      Object localObject2 = arrayOfObject[i];
      Object localObject1 = paramOpenHashSet;
      if ((localObject2 instanceof Disposable)) {
        try
        {
          ((Disposable)localObject2).dispose();
          localObject1 = paramOpenHashSet;
        }
        catch (Throwable localThrowable)
        {
          Exceptions.throwIfFatal(localThrowable);
          localObject1 = paramOpenHashSet;
          if (paramOpenHashSet == null) {
            localObject1 = new ArrayList();
          }
          ((List)localObject1).add(localThrowable);
        }
      }
      i++;
      paramOpenHashSet = (OpenHashSet<Disposable>)localObject1;
    }
    if (paramOpenHashSet != null)
    {
      if (paramOpenHashSet.size() == 1) {
        throw ExceptionHelper.wrapOrThrow((Throwable)paramOpenHashSet.get(0));
      }
      throw new CompositeException(paramOpenHashSet);
    }
  }
  
  public boolean isDisposed()
  {
    return this.disposed;
  }
  
  public boolean remove(Disposable paramDisposable)
  {
    if (delete(paramDisposable))
    {
      paramDisposable.dispose();
      return true;
    }
    return false;
  }
  
  public int size()
  {
    boolean bool = this.disposed;
    int i = 0;
    if (bool) {
      return 0;
    }
    try
    {
      if (this.disposed) {
        return 0;
      }
      OpenHashSet localOpenHashSet = this.resources;
      if (localOpenHashSet != null) {
        i = localOpenHashSet.size();
      }
      return i;
    }
    finally {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/disposables/CompositeDisposable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */