package io.reactivex.disposables;

public abstract interface Disposable
{
  public abstract void dispose();
  
  public abstract boolean isDisposed();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/disposables/Disposable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */