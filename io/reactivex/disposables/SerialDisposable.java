package io.reactivex.disposables;

import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.atomic.AtomicReference;

public final class SerialDisposable
  implements Disposable
{
  final AtomicReference<Disposable> resource;
  
  public SerialDisposable()
  {
    this.resource = new AtomicReference();
  }
  
  public SerialDisposable(Disposable paramDisposable)
  {
    this.resource = new AtomicReference(paramDisposable);
  }
  
  public void dispose()
  {
    DisposableHelper.dispose(this.resource);
  }
  
  public Disposable get()
  {
    Disposable localDisposable = (Disposable)this.resource.get();
    if (localDisposable == DisposableHelper.DISPOSED) {
      return Disposables.disposed();
    }
    return localDisposable;
  }
  
  public boolean isDisposed()
  {
    return DisposableHelper.isDisposed((Disposable)this.resource.get());
  }
  
  public boolean replace(Disposable paramDisposable)
  {
    return DisposableHelper.replace(this.resource, paramDisposable);
  }
  
  public boolean set(Disposable paramDisposable)
  {
    return DisposableHelper.set(this.resource, paramDisposable);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/disposables/SerialDisposable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */