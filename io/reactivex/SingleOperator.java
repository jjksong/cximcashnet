package io.reactivex;

public abstract interface SingleOperator<Downstream, Upstream>
{
  public abstract SingleObserver<? super Upstream> apply(SingleObserver<? super Downstream> paramSingleObserver)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/SingleOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */