package io.reactivex;

public abstract interface MaybeTransformer<Upstream, Downstream>
{
  public abstract MaybeSource<Downstream> apply(Maybe<Upstream> paramMaybe);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/MaybeTransformer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */