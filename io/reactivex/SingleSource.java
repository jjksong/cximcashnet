package io.reactivex;

public abstract interface SingleSource<T>
{
  public abstract void subscribe(SingleObserver<? super T> paramSingleObserver);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/SingleSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */