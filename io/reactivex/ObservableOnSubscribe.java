package io.reactivex;

public abstract interface ObservableOnSubscribe<T>
{
  public abstract void subscribe(ObservableEmitter<T> paramObservableEmitter)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/ObservableOnSubscribe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */