package io.reactivex;

public abstract interface CompletableOnSubscribe
{
  public abstract void subscribe(CompletableEmitter paramCompletableEmitter)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/CompletableOnSubscribe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */