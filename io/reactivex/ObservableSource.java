package io.reactivex;

public abstract interface ObservableSource<T>
{
  public abstract void subscribe(Observer<? super T> paramObserver);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/ObservableSource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */