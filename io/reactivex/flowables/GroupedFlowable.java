package io.reactivex.flowables;

import io.reactivex.Flowable;

public abstract class GroupedFlowable<K, T>
  extends Flowable<T>
{
  final K key;
  
  protected GroupedFlowable(K paramK)
  {
    this.key = paramK;
  }
  
  public K getKey()
  {
    return (K)this.key;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/flowables/GroupedFlowable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */