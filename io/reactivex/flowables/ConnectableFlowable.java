package io.reactivex.flowables;

import io.reactivex.Flowable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.operators.flowable.FlowableAutoConnect;
import io.reactivex.internal.operators.flowable.FlowableRefCount;
import io.reactivex.internal.util.ConnectConsumer;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class ConnectableFlowable<T>
  extends Flowable<T>
{
  public Flowable<T> autoConnect()
  {
    return autoConnect(1);
  }
  
  public Flowable<T> autoConnect(int paramInt)
  {
    return autoConnect(paramInt, Functions.emptyConsumer());
  }
  
  public Flowable<T> autoConnect(int paramInt, Consumer<? super Disposable> paramConsumer)
  {
    if (paramInt <= 0)
    {
      connect(paramConsumer);
      return RxJavaPlugins.onAssembly(this);
    }
    return RxJavaPlugins.onAssembly(new FlowableAutoConnect(this, paramInt, paramConsumer));
  }
  
  public final Disposable connect()
  {
    ConnectConsumer localConnectConsumer = new ConnectConsumer();
    connect(localConnectConsumer);
    return localConnectConsumer.disposable;
  }
  
  public abstract void connect(Consumer<? super Disposable> paramConsumer);
  
  public Flowable<T> refCount()
  {
    return RxJavaPlugins.onAssembly(new FlowableRefCount(this));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/flowables/ConnectableFlowable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */