package io.reactivex.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.ListCompositeDisposable;
import io.reactivex.internal.functions.ObjectHelper;
import java.util.concurrent.atomic.AtomicReference;

public abstract class ResourceObserver<T>
  implements Observer<T>, Disposable
{
  private final ListCompositeDisposable resources = new ListCompositeDisposable();
  private final AtomicReference<Disposable> s = new AtomicReference();
  
  public final void add(Disposable paramDisposable)
  {
    ObjectHelper.requireNonNull(paramDisposable, "resource is null");
    this.resources.add(paramDisposable);
  }
  
  public final void dispose()
  {
    if (DisposableHelper.dispose(this.s)) {
      this.resources.dispose();
    }
  }
  
  public final boolean isDisposed()
  {
    return DisposableHelper.isDisposed((Disposable)this.s.get());
  }
  
  protected void onStart() {}
  
  public final void onSubscribe(Disposable paramDisposable)
  {
    if (DisposableHelper.setOnce(this.s, paramDisposable)) {
      onStart();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observers/ResourceObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */