package io.reactivex.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import java.util.concurrent.atomic.AtomicReference;

public abstract class DisposableObserver<T>
  implements Observer<T>, Disposable
{
  final AtomicReference<Disposable> s = new AtomicReference();
  
  public final void dispose()
  {
    DisposableHelper.dispose(this.s);
  }
  
  public final boolean isDisposed()
  {
    boolean bool;
    if (this.s.get() == DisposableHelper.DISPOSED) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  protected void onStart() {}
  
  public final void onSubscribe(Disposable paramDisposable)
  {
    if (DisposableHelper.setOnce(this.s, paramDisposable)) {
      onStart();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observers/DisposableObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */