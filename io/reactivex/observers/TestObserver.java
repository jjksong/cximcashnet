package io.reactivex.observers;

import io.reactivex.CompletableObserver;
import io.reactivex.MaybeObserver;
import io.reactivex.Observer;
import io.reactivex.SingleObserver;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.fuseable.QueueDisposable;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.atomic.AtomicReference;

public class TestObserver<T>
  extends BaseTestConsumer<T, TestObserver<T>>
  implements Observer<T>, Disposable, MaybeObserver<T>, SingleObserver<T>, CompletableObserver
{
  private final Observer<? super T> actual;
  private QueueDisposable<T> qs;
  private final AtomicReference<Disposable> subscription = new AtomicReference();
  
  public TestObserver()
  {
    this(EmptyObserver.INSTANCE);
  }
  
  public TestObserver(Observer<? super T> paramObserver)
  {
    this.actual = paramObserver;
  }
  
  public static <T> TestObserver<T> create()
  {
    return new TestObserver();
  }
  
  public static <T> TestObserver<T> create(Observer<? super T> paramObserver)
  {
    return new TestObserver(paramObserver);
  }
  
  static String fusionModeToString(int paramInt)
  {
    switch (paramInt)
    {
    default: 
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Unknown(");
      localStringBuilder.append(paramInt);
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    case 2: 
      return "ASYNC";
    case 1: 
      return "SYNC";
    }
    return "NONE";
  }
  
  final TestObserver<T> assertFuseable()
  {
    if (this.qs != null) {
      return this;
    }
    throw new AssertionError("Upstream is not fuseable.");
  }
  
  final TestObserver<T> assertFusionMode(int paramInt)
  {
    int i = this.establishedFusionMode;
    if (i != paramInt)
    {
      if (this.qs != null)
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Fusion mode different. Expected: ");
        localStringBuilder.append(fusionModeToString(paramInt));
        localStringBuilder.append(", actual: ");
        localStringBuilder.append(fusionModeToString(i));
        throw new AssertionError(localStringBuilder.toString());
      }
      throw fail("Upstream is not fuseable");
    }
    return this;
  }
  
  final TestObserver<T> assertNotFuseable()
  {
    if (this.qs == null) {
      return this;
    }
    throw new AssertionError("Upstream is fuseable.");
  }
  
  public final TestObserver<T> assertNotSubscribed()
  {
    if (this.subscription.get() == null)
    {
      if (this.errors.isEmpty()) {
        return this;
      }
      throw fail("Not subscribed but errors found");
    }
    throw fail("Subscribed!");
  }
  
  public final TestObserver<T> assertOf(Consumer<? super TestObserver<T>> paramConsumer)
  {
    try
    {
      paramConsumer.accept(this);
      return this;
    }
    catch (Throwable paramConsumer)
    {
      throw ExceptionHelper.wrapOrThrow(paramConsumer);
    }
  }
  
  public final TestObserver<T> assertSubscribed()
  {
    if (this.subscription.get() != null) {
      return this;
    }
    throw fail("Not subscribed!");
  }
  
  public final void cancel()
  {
    dispose();
  }
  
  public final void dispose()
  {
    DisposableHelper.dispose(this.subscription);
  }
  
  public final boolean hasSubscription()
  {
    boolean bool;
    if (this.subscription.get() != null) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final boolean isCancelled()
  {
    return isDisposed();
  }
  
  public final boolean isDisposed()
  {
    return DisposableHelper.isDisposed((Disposable)this.subscription.get());
  }
  
  public void onComplete()
  {
    if (!this.checkSubscriptionOnce)
    {
      this.checkSubscriptionOnce = true;
      if (this.subscription.get() == null) {
        this.errors.add(new IllegalStateException("onSubscribe not called in proper order"));
      }
    }
    try
    {
      this.lastThread = Thread.currentThread();
      this.completions += 1L;
      this.actual.onComplete();
      return;
    }
    finally
    {
      this.done.countDown();
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (!this.checkSubscriptionOnce)
    {
      this.checkSubscriptionOnce = true;
      if (this.subscription.get() == null) {
        this.errors.add(new IllegalStateException("onSubscribe not called in proper order"));
      }
    }
    try
    {
      this.lastThread = Thread.currentThread();
      if (paramThrowable == null)
      {
        List localList = this.errors;
        NullPointerException localNullPointerException = new java/lang/NullPointerException;
        localNullPointerException.<init>("onError received a null Throwable");
        localList.add(localNullPointerException);
      }
      else
      {
        this.errors.add(paramThrowable);
      }
      this.actual.onError(paramThrowable);
      return;
    }
    finally
    {
      this.done.countDown();
    }
  }
  
  public void onNext(T paramT)
  {
    if (!this.checkSubscriptionOnce)
    {
      this.checkSubscriptionOnce = true;
      if (this.subscription.get() == null) {
        this.errors.add(new IllegalStateException("onSubscribe not called in proper order"));
      }
    }
    this.lastThread = Thread.currentThread();
    if (this.establishedFusionMode == 2) {
      try
      {
        for (;;)
        {
          paramT = this.qs.poll();
          if (paramT == null) {
            break;
          }
          this.values.add(paramT);
        }
        return;
      }
      catch (Throwable paramT)
      {
        this.errors.add(paramT);
      }
    }
    this.values.add(paramT);
    if (paramT == null) {
      this.errors.add(new NullPointerException("onNext received a null value"));
    }
    this.actual.onNext(paramT);
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    this.lastThread = Thread.currentThread();
    if (paramDisposable == null)
    {
      this.errors.add(new NullPointerException("onSubscribe received a null Subscription"));
      return;
    }
    if (!this.subscription.compareAndSet(null, paramDisposable))
    {
      paramDisposable.dispose();
      if (this.subscription.get() != DisposableHelper.DISPOSED)
      {
        List localList = this.errors;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("onSubscribe received multiple subscriptions: ");
        localStringBuilder.append(paramDisposable);
        localList.add(new IllegalStateException(localStringBuilder.toString()));
      }
      return;
    }
    if ((this.initialFusionMode != 0) && ((paramDisposable instanceof QueueDisposable)))
    {
      this.qs = ((QueueDisposable)paramDisposable);
      int i = this.qs.requestFusion(this.initialFusionMode);
      this.establishedFusionMode = i;
      if (i == 1)
      {
        this.checkSubscriptionOnce = true;
        this.lastThread = Thread.currentThread();
        try
        {
          for (;;)
          {
            paramDisposable = this.qs.poll();
            if (paramDisposable == null) {
              break;
            }
            this.values.add(paramDisposable);
          }
          this.completions += 1L;
          this.subscription.lazySet(DisposableHelper.DISPOSED);
        }
        catch (Throwable paramDisposable)
        {
          this.errors.add(paramDisposable);
        }
        return;
      }
    }
    this.actual.onSubscribe(paramDisposable);
  }
  
  public void onSuccess(T paramT)
  {
    onNext(paramT);
    onComplete();
  }
  
  final TestObserver<T> setInitialFusionMode(int paramInt)
  {
    this.initialFusionMode = paramInt;
    return this;
  }
  
  static enum EmptyObserver
    implements Observer<Object>
  {
    INSTANCE;
    
    private EmptyObserver() {}
    
    public void onComplete() {}
    
    public void onError(Throwable paramThrowable) {}
    
    public void onNext(Object paramObject) {}
    
    public void onSubscribe(Disposable paramDisposable) {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observers/TestObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */