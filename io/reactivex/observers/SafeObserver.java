package io.reactivex.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.Exceptions;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.disposables.EmptyDisposable;
import io.reactivex.plugins.RxJavaPlugins;

public final class SafeObserver<T>
  implements Observer<T>, Disposable
{
  final Observer<? super T> actual;
  boolean done;
  Disposable s;
  
  public SafeObserver(Observer<? super T> paramObserver)
  {
    this.actual = paramObserver;
  }
  
  public void dispose()
  {
    this.s.dispose();
  }
  
  public boolean isDisposed()
  {
    return this.s.isDisposed();
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    this.done = true;
    if (this.s == null)
    {
      onCompleteNoSubscription();
      return;
    }
    try
    {
      this.actual.onComplete();
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      RxJavaPlugins.onError(localThrowable);
    }
  }
  
  void onCompleteNoSubscription()
  {
    NullPointerException localNullPointerException = new NullPointerException("Subscription not set!");
    try
    {
      this.actual.onSubscribe(EmptyDisposable.INSTANCE);
      try
      {
        this.actual.onError(localNullPointerException);
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        RxJavaPlugins.onError(new CompositeException(new Throwable[] { localNullPointerException, localThrowable1 }));
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { localNullPointerException, localThrowable2 }));
    }
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (this.done)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    this.done = true;
    if (this.s == null)
    {
      localObject = new NullPointerException("Subscription not set!");
      try
      {
        this.actual.onSubscribe(EmptyDisposable.INSTANCE);
        try
        {
          Observer localObserver = this.actual;
          CompositeException localCompositeException = new io/reactivex/exceptions/CompositeException;
          localCompositeException.<init>(new Throwable[] { paramThrowable, localObject });
          localObserver.onError(localCompositeException);
        }
        catch (Throwable localThrowable1)
        {
          Exceptions.throwIfFatal(localThrowable1);
          RxJavaPlugins.onError(new CompositeException(new Throwable[] { paramThrowable, localObject, localThrowable1 }));
        }
        return;
      }
      catch (Throwable localThrowable2)
      {
        Exceptions.throwIfFatal(localThrowable2);
        RxJavaPlugins.onError(new CompositeException(new Throwable[] { paramThrowable, localObject, localThrowable2 }));
        return;
      }
    }
    Object localObject = paramThrowable;
    if (paramThrowable == null) {
      localObject = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
    }
    try
    {
      this.actual.onError((Throwable)localObject);
    }
    catch (Throwable paramThrowable)
    {
      Exceptions.throwIfFatal(paramThrowable);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { localObject, paramThrowable }));
    }
  }
  
  public void onNext(T paramT)
  {
    if (this.done) {
      return;
    }
    if (this.s == null)
    {
      onNextNoSubscription();
      return;
    }
    if (paramT == null)
    {
      NullPointerException localNullPointerException = new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
      try
      {
        this.s.dispose();
        onError(localNullPointerException);
        return;
      }
      catch (Throwable paramT)
      {
        Exceptions.throwIfFatal(paramT);
        onError(new CompositeException(new Throwable[] { localNullPointerException, paramT }));
        return;
      }
    }
    try
    {
      this.actual.onNext(paramT);
    }
    catch (Throwable paramT)
    {
      Exceptions.throwIfFatal(paramT);
    }
    try
    {
      this.s.dispose();
      onError(paramT);
      return;
    }
    catch (Throwable localThrowable)
    {
      Exceptions.throwIfFatal(localThrowable);
      onError(new CompositeException(new Throwable[] { paramT, localThrowable }));
    }
  }
  
  void onNextNoSubscription()
  {
    this.done = true;
    NullPointerException localNullPointerException = new NullPointerException("Subscription not set!");
    try
    {
      this.actual.onSubscribe(EmptyDisposable.INSTANCE);
      try
      {
        this.actual.onError(localNullPointerException);
      }
      catch (Throwable localThrowable1)
      {
        Exceptions.throwIfFatal(localThrowable1);
        RxJavaPlugins.onError(new CompositeException(new Throwable[] { localNullPointerException, localThrowable1 }));
      }
      return;
    }
    catch (Throwable localThrowable2)
    {
      Exceptions.throwIfFatal(localThrowable2);
      RxJavaPlugins.onError(new CompositeException(new Throwable[] { localNullPointerException, localThrowable2 }));
    }
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (DisposableHelper.validate(this.s, paramDisposable))
    {
      this.s = paramDisposable;
      try
      {
        this.actual.onSubscribe(this);
      }
      catch (Throwable localThrowable)
      {
        Exceptions.throwIfFatal(localThrowable);
        this.done = true;
        try
        {
          paramDisposable.dispose();
          RxJavaPlugins.onError(localThrowable);
        }
        catch (Throwable paramDisposable)
        {
          Exceptions.throwIfFatal(paramDisposable);
          RxJavaPlugins.onError(new CompositeException(new Throwable[] { localThrowable, paramDisposable }));
          return;
        }
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observers/SafeObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */