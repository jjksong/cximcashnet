package io.reactivex.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;
import io.reactivex.internal.util.AppendOnlyLinkedArrayList;
import io.reactivex.internal.util.NotificationLite;
import io.reactivex.plugins.RxJavaPlugins;

public final class SerializedObserver<T>
  implements Observer<T>, Disposable
{
  static final int QUEUE_LINK_SIZE = 4;
  final Observer<? super T> actual;
  final boolean delayError;
  volatile boolean done;
  boolean emitting;
  AppendOnlyLinkedArrayList<Object> queue;
  Disposable s;
  
  public SerializedObserver(Observer<? super T> paramObserver)
  {
    this(paramObserver, false);
  }
  
  public SerializedObserver(Observer<? super T> paramObserver, boolean paramBoolean)
  {
    this.actual = paramObserver;
    this.delayError = paramBoolean;
  }
  
  public void dispose()
  {
    this.s.dispose();
  }
  
  void emitLoop()
  {
    for (;;)
    {
      try
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList = this.queue;
        if (localAppendOnlyLinkedArrayList == null)
        {
          this.emitting = false;
          return;
        }
        this.queue = null;
        if (!localAppendOnlyLinkedArrayList.accept(this.actual)) {
          continue;
        }
        return;
      }
      finally {}
    }
  }
  
  public boolean isDisposed()
  {
    return this.s.isDisposed();
  }
  
  public void onComplete()
  {
    if (this.done) {
      return;
    }
    try
    {
      if (this.done) {
        return;
      }
      if (this.emitting)
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
        if (localAppendOnlyLinkedArrayList2 == null)
        {
          localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
          localAppendOnlyLinkedArrayList1.<init>(4);
          this.queue = localAppendOnlyLinkedArrayList1;
        }
        localAppendOnlyLinkedArrayList1.add(NotificationLite.complete());
        return;
      }
      this.done = true;
      this.emitting = true;
      this.actual.onComplete();
      return;
    }
    finally {}
  }
  
  public void onError(Throwable paramThrowable)
  {
    if (this.done)
    {
      RxJavaPlugins.onError(paramThrowable);
      return;
    }
    try
    {
      boolean bool = this.done;
      int i = 1;
      if (!bool)
      {
        if (this.emitting)
        {
          this.done = true;
          AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
          AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
          if (localAppendOnlyLinkedArrayList2 == null)
          {
            localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
            localAppendOnlyLinkedArrayList1.<init>(4);
            this.queue = localAppendOnlyLinkedArrayList1;
          }
          paramThrowable = NotificationLite.error(paramThrowable);
          if (this.delayError) {
            localAppendOnlyLinkedArrayList1.add(paramThrowable);
          } else {
            localAppendOnlyLinkedArrayList1.setFirst(paramThrowable);
          }
          return;
        }
        this.done = true;
        this.emitting = true;
        i = 0;
      }
      if (i != 0)
      {
        RxJavaPlugins.onError(paramThrowable);
        return;
      }
      this.actual.onError(paramThrowable);
      return;
    }
    finally {}
  }
  
  public void onNext(T paramT)
  {
    if (this.done) {
      return;
    }
    if (paramT == null)
    {
      this.s.dispose();
      onError(new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
      return;
    }
    try
    {
      if (this.done) {
        return;
      }
      if (this.emitting)
      {
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList2 = this.queue;
        AppendOnlyLinkedArrayList localAppendOnlyLinkedArrayList1 = localAppendOnlyLinkedArrayList2;
        if (localAppendOnlyLinkedArrayList2 == null)
        {
          localAppendOnlyLinkedArrayList1 = new io/reactivex/internal/util/AppendOnlyLinkedArrayList;
          localAppendOnlyLinkedArrayList1.<init>(4);
          this.queue = localAppendOnlyLinkedArrayList1;
        }
        localAppendOnlyLinkedArrayList1.add(NotificationLite.next(paramT));
        return;
      }
      this.emitting = true;
      this.actual.onNext(paramT);
      emitLoop();
      return;
    }
    finally {}
  }
  
  public void onSubscribe(Disposable paramDisposable)
  {
    if (DisposableHelper.validate(this.s, paramDisposable))
    {
      this.s = paramDisposable;
      this.actual.onSubscribe(this);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observers/SerializedObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */