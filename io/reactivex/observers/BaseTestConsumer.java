package io.reactivex.observers;

import io.reactivex.Notification;
import io.reactivex.annotations.Experimental;
import io.reactivex.disposables.Disposable;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.functions.Predicate;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.functions.ObjectHelper;
import io.reactivex.internal.util.ExceptionHelper;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public abstract class BaseTestConsumer<T, U extends BaseTestConsumer<T, U>>
  implements Disposable
{
  protected boolean checkSubscriptionOnce;
  protected long completions;
  protected final CountDownLatch done = new CountDownLatch(1);
  protected final List<Throwable> errors = new ArrayList();
  protected int establishedFusionMode;
  protected int initialFusionMode;
  protected Thread lastThread;
  protected final List<T> values = new ArrayList();
  
  public static String valueAndClass(Object paramObject)
  {
    if (paramObject != null)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramObject);
      localStringBuilder.append(" (class: ");
      localStringBuilder.append(paramObject.getClass().getSimpleName());
      localStringBuilder.append(")");
      return localStringBuilder.toString();
    }
    return "null";
  }
  
  public final U assertComplete()
  {
    long l = this.completions;
    if (l != 0L)
    {
      if (l <= 1L) {
        return this;
      }
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Multiple completions: ");
      localStringBuilder.append(l);
      throw fail(localStringBuilder.toString());
    }
    throw fail("Not completed");
  }
  
  public final U assertEmpty()
  {
    return assertSubscribed().assertNoValues().assertNoErrors().assertNotComplete();
  }
  
  public final U assertError(Predicate<Throwable> paramPredicate)
  {
    int k = this.errors.size();
    if (k != 0)
    {
      int j = 0;
      Iterator localIterator = this.errors.iterator();
      int i;
      for (;;)
      {
        i = j;
        if (localIterator.hasNext())
        {
          Throwable localThrowable = (Throwable)localIterator.next();
          try
          {
            boolean bool = paramPredicate.test(localThrowable);
            if (bool) {
              i = 1;
            }
          }
          catch (Exception paramPredicate)
          {
            throw ExceptionHelper.wrapOrThrow(paramPredicate);
          }
        }
      }
      if (i != 0)
      {
        if (k == 1) {
          return this;
        }
        throw fail("Error present but other errors as well");
      }
      throw fail("Error not present");
    }
    throw fail("No errors");
  }
  
  public final U assertError(Class<? extends Throwable> paramClass)
  {
    return assertError(Functions.isInstanceOf(paramClass));
  }
  
  public final U assertError(Throwable paramThrowable)
  {
    return assertError(Functions.equalsWith(paramThrowable));
  }
  
  public final U assertErrorMessage(String paramString)
  {
    int i = this.errors.size();
    if (i != 0)
    {
      if (i == 1)
      {
        String str = ((Throwable)this.errors.get(0)).getMessage();
        if (ObjectHelper.equals(paramString, str)) {
          return this;
        }
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Error message differs; Expected: ");
        localStringBuilder.append(paramString);
        localStringBuilder.append(", Actual: ");
        localStringBuilder.append(str);
        throw fail(localStringBuilder.toString());
      }
      throw fail("Multiple errors");
    }
    throw fail("No errors");
  }
  
  public final U assertFailure(Predicate<Throwable> paramPredicate, T... paramVarArgs)
  {
    return assertSubscribed().assertValues(paramVarArgs).assertError(paramPredicate).assertNotComplete();
  }
  
  public final U assertFailure(Class<? extends Throwable> paramClass, T... paramVarArgs)
  {
    return assertSubscribed().assertValues(paramVarArgs).assertError(paramClass).assertNotComplete();
  }
  
  public final U assertFailureAndMessage(Class<? extends Throwable> paramClass, String paramString, T... paramVarArgs)
  {
    return assertSubscribed().assertValues(paramVarArgs).assertError(paramClass).assertErrorMessage(paramString).assertNotComplete();
  }
  
  @Experimental
  public final U assertNever(Predicate<? super T> paramPredicate)
  {
    int j = this.values.size();
    int i = 0;
    while (i < j)
    {
      Object localObject = this.values.get(i);
      try
      {
        if (!paramPredicate.test(localObject))
        {
          i++;
        }
        else
        {
          localObject = new java/lang/StringBuilder;
          ((StringBuilder)localObject).<init>();
          ((StringBuilder)localObject).append("Value at position ");
          ((StringBuilder)localObject).append(i);
          ((StringBuilder)localObject).append(" matches predicate ");
          ((StringBuilder)localObject).append(paramPredicate.toString());
          ((StringBuilder)localObject).append(", which was not expected.");
          throw fail(((StringBuilder)localObject).toString());
        }
      }
      catch (Exception paramPredicate)
      {
        throw ExceptionHelper.wrapOrThrow(paramPredicate);
      }
    }
    return this;
  }
  
  @Experimental
  public final U assertNever(T paramT)
  {
    int j = this.values.size();
    int i = 0;
    while (i < j) {
      if (!ObjectHelper.equals(this.values.get(i), paramT))
      {
        i++;
      }
      else
      {
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append("Value at position ");
        localStringBuilder.append(i);
        localStringBuilder.append(" is equal to ");
        localStringBuilder.append(valueAndClass(paramT));
        localStringBuilder.append("; Expected them to be different");
        throw fail(localStringBuilder.toString());
      }
    }
    return this;
  }
  
  public final U assertNoErrors()
  {
    if (this.errors.size() == 0) {
      return this;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Error(s) present: ");
    localStringBuilder.append(this.errors);
    throw fail(localStringBuilder.toString());
  }
  
  public final U assertNoValues()
  {
    return assertValueCount(0);
  }
  
  public final U assertNotComplete()
  {
    long l = this.completions;
    if (l != 1L)
    {
      if (l <= 1L) {
        return this;
      }
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Multiple completions: ");
      localStringBuilder.append(l);
      throw fail(localStringBuilder.toString());
    }
    throw fail("Completed!");
  }
  
  public abstract U assertNotSubscribed();
  
  public final U assertNotTerminated()
  {
    if (this.done.getCount() != 0L) {
      return this;
    }
    throw fail("Subscriber terminated!");
  }
  
  public final U assertResult(T... paramVarArgs)
  {
    return assertSubscribed().assertValues(paramVarArgs).assertNoErrors().assertComplete();
  }
  
  public abstract U assertSubscribed();
  
  public final U assertTerminated()
  {
    if (this.done.getCount() == 0L)
    {
      long l = this.completions;
      if (l <= 1L)
      {
        int i = this.errors.size();
        if (i <= 1)
        {
          if ((l != 0L) && (i != 0))
          {
            localStringBuilder = new StringBuilder();
            localStringBuilder.append("Terminated with multiple completions and errors: ");
            localStringBuilder.append(l);
            throw fail(localStringBuilder.toString());
          }
          return this;
        }
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("Terminated with multiple errors: ");
        localStringBuilder.append(i);
        throw fail(localStringBuilder.toString());
      }
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Terminated with multiple completions: ");
      localStringBuilder.append(l);
      throw fail(localStringBuilder.toString());
    }
    throw fail("Subscriber still running!");
  }
  
  public final U assertValue(Predicate<T> paramPredicate)
  {
    assertValueAt(0, paramPredicate);
    if (this.values.size() <= 1) {
      return this;
    }
    throw fail("Value present but other values as well");
  }
  
  public final U assertValue(T paramT)
  {
    if (this.values.size() == 1)
    {
      Object localObject = this.values.get(0);
      if (ObjectHelper.equals(paramT, localObject)) {
        return this;
      }
      localStringBuilder = new StringBuilder();
      localStringBuilder.append("Expected: ");
      localStringBuilder.append(valueAndClass(paramT));
      localStringBuilder.append(", Actual: ");
      localStringBuilder.append(valueAndClass(localObject));
      throw fail(localStringBuilder.toString());
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Expected: ");
    localStringBuilder.append(valueAndClass(paramT));
    localStringBuilder.append(", Actual: ");
    localStringBuilder.append(this.values);
    throw fail(localStringBuilder.toString());
  }
  
  public final U assertValueAt(int paramInt, Predicate<T> paramPredicate)
  {
    if (this.values.size() != 0)
    {
      if (paramInt < this.values.size()) {
        try
        {
          boolean bool = paramPredicate.test(this.values.get(paramInt));
          if (bool) {
            return this;
          }
          throw fail("Value not present");
        }
        catch (Exception paramPredicate)
        {
          throw ExceptionHelper.wrapOrThrow(paramPredicate);
        }
      }
      paramPredicate = new StringBuilder();
      paramPredicate.append("Invalid index: ");
      paramPredicate.append(paramInt);
      throw fail(paramPredicate.toString());
    }
    throw fail("No values");
  }
  
  public final U assertValueCount(int paramInt)
  {
    int i = this.values.size();
    if (i == paramInt) {
      return this;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Value counts differ; Expected: ");
    localStringBuilder.append(paramInt);
    localStringBuilder.append(", Actual: ");
    localStringBuilder.append(i);
    throw fail(localStringBuilder.toString());
  }
  
  public final U assertValueSequence(Iterable<? extends T> paramIterable)
  {
    Object localObject1 = this.values.iterator();
    Iterator localIterator = paramIterable.iterator();
    boolean bool2;
    boolean bool1;
    Object localObject2;
    for (int i = 0;; i++)
    {
      bool2 = localIterator.hasNext();
      bool1 = ((Iterator)localObject1).hasNext();
      if ((!bool2) || (!bool1)) {
        break label155;
      }
      localObject2 = localIterator.next();
      paramIterable = ((Iterator)localObject1).next();
      if (!ObjectHelper.equals(paramIterable, localObject2)) {
        break;
      }
    }
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("Values at position ");
    ((StringBuilder)localObject1).append(i);
    ((StringBuilder)localObject1).append(" differ; Expected: ");
    ((StringBuilder)localObject1).append(valueAndClass(paramIterable));
    ((StringBuilder)localObject1).append(", Actual: ");
    ((StringBuilder)localObject1).append(valueAndClass(localObject2));
    throw fail(((StringBuilder)localObject1).toString());
    label155:
    if (!bool2)
    {
      if (!bool1) {
        return this;
      }
      paramIterable = new StringBuilder();
      paramIterable.append("Fever values received than expected (");
      paramIterable.append(i);
      paramIterable.append(")");
      throw fail(paramIterable.toString());
    }
    paramIterable = new StringBuilder();
    paramIterable.append("More values received than expected (");
    paramIterable.append(i);
    paramIterable.append(")");
    throw fail(paramIterable.toString());
  }
  
  public final U assertValueSet(Collection<? extends T> paramCollection)
  {
    if (paramCollection.isEmpty())
    {
      assertNoValues();
      return this;
    }
    Iterator localIterator = this.values.iterator();
    while (localIterator.hasNext())
    {
      Object localObject = localIterator.next();
      if (!paramCollection.contains(localObject))
      {
        paramCollection = new StringBuilder();
        paramCollection.append("Value not in the expected collection: ");
        paramCollection.append(valueAndClass(localObject));
        throw fail(paramCollection.toString());
      }
    }
    return this;
  }
  
  public final U assertValues(T... paramVarArgs)
  {
    int j = this.values.size();
    if (j == paramVarArgs.length)
    {
      int i = 0;
      while (i < j)
      {
        Object localObject2 = this.values.get(i);
        localObject1 = paramVarArgs[i];
        if (ObjectHelper.equals(localObject1, localObject2))
        {
          i++;
        }
        else
        {
          paramVarArgs = new StringBuilder();
          paramVarArgs.append("Values at position ");
          paramVarArgs.append(i);
          paramVarArgs.append(" differ; Expected: ");
          paramVarArgs.append(valueAndClass(localObject1));
          paramVarArgs.append(", Actual: ");
          paramVarArgs.append(valueAndClass(localObject2));
          throw fail(paramVarArgs.toString());
        }
      }
      return this;
    }
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append("Value count differs; Expected: ");
    ((StringBuilder)localObject1).append(paramVarArgs.length);
    ((StringBuilder)localObject1).append(" ");
    ((StringBuilder)localObject1).append(Arrays.toString(paramVarArgs));
    ((StringBuilder)localObject1).append(", Actual: ");
    ((StringBuilder)localObject1).append(j);
    ((StringBuilder)localObject1).append(" ");
    ((StringBuilder)localObject1).append(this.values);
    throw fail(((StringBuilder)localObject1).toString());
  }
  
  public final U await()
    throws InterruptedException
  {
    if (this.done.getCount() == 0L) {
      return this;
    }
    this.done.await();
    return this;
  }
  
  public final boolean await(long paramLong, TimeUnit paramTimeUnit)
    throws InterruptedException
  {
    boolean bool;
    if ((this.done.getCount() != 0L) && (!this.done.await(paramLong, paramTimeUnit))) {
      bool = false;
    } else {
      bool = true;
    }
    return bool;
  }
  
  public final U awaitDone(long paramLong, TimeUnit paramTimeUnit)
  {
    try
    {
      if (!this.done.await(paramLong, paramTimeUnit)) {
        dispose();
      }
      return this;
    }
    catch (InterruptedException paramTimeUnit)
    {
      dispose();
      throw ExceptionHelper.wrapOrThrow(paramTimeUnit);
    }
  }
  
  public final boolean awaitTerminalEvent()
  {
    try
    {
      await();
      return true;
    }
    catch (InterruptedException localInterruptedException)
    {
      Thread.currentThread().interrupt();
    }
    return false;
  }
  
  public final boolean awaitTerminalEvent(long paramLong, TimeUnit paramTimeUnit)
  {
    try
    {
      boolean bool = await(paramLong, paramTimeUnit);
      return bool;
    }
    catch (InterruptedException paramTimeUnit)
    {
      Thread.currentThread().interrupt();
    }
    return false;
  }
  
  public final long completions()
  {
    return this.completions;
  }
  
  public final int errorCount()
  {
    return this.errors.size();
  }
  
  public final List<Throwable> errors()
  {
    return this.errors;
  }
  
  protected final AssertionError fail(String paramString)
  {
    StringBuilder localStringBuilder = new StringBuilder(paramString.length() + 64);
    localStringBuilder.append(paramString);
    localStringBuilder.append(" (");
    localStringBuilder.append("latch = ");
    localStringBuilder.append(this.done.getCount());
    localStringBuilder.append(", ");
    localStringBuilder.append("values = ");
    localStringBuilder.append(this.values.size());
    localStringBuilder.append(", ");
    localStringBuilder.append("errors = ");
    localStringBuilder.append(this.errors.size());
    localStringBuilder.append(", ");
    localStringBuilder.append("completions = ");
    localStringBuilder.append(this.completions);
    localStringBuilder.append(')');
    paramString = new AssertionError(localStringBuilder.toString());
    if (!this.errors.isEmpty()) {
      if (this.errors.size() == 1) {
        paramString.initCause((Throwable)this.errors.get(0));
      } else {
        paramString.initCause(new CompositeException(this.errors));
      }
    }
    return paramString;
  }
  
  public final List<List<Object>> getEvents()
  {
    ArrayList localArrayList1 = new ArrayList();
    localArrayList1.add(values());
    localArrayList1.add(errors());
    ArrayList localArrayList2 = new ArrayList();
    for (long l = 0L; l < this.completions; l += 1L) {
      localArrayList2.add(Notification.createOnComplete());
    }
    localArrayList1.add(localArrayList2);
    return localArrayList1;
  }
  
  public final boolean isTerminated()
  {
    boolean bool;
    if (this.done.getCount() == 0L) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public final Thread lastThread()
  {
    return this.lastThread;
  }
  
  public final int valueCount()
  {
    return this.values.size();
  }
  
  public final List<T> values()
  {
    return this.values;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observers/BaseTestConsumer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */