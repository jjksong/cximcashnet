package io.reactivex.observers;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.internal.disposables.DisposableHelper;

public abstract class DefaultObserver<T>
  implements Observer<T>
{
  private Disposable s;
  
  protected final void cancel()
  {
    Disposable localDisposable = this.s;
    this.s = DisposableHelper.DISPOSED;
    localDisposable.dispose();
  }
  
  protected void onStart() {}
  
  public final void onSubscribe(Disposable paramDisposable)
  {
    if (DisposableHelper.validate(this.s, paramDisposable))
    {
      this.s = paramDisposable;
      onStart();
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observers/DefaultObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */