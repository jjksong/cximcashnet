package io.reactivex;

import io.reactivex.disposables.Disposable;

public abstract interface MaybeObserver<T>
{
  public abstract void onComplete();
  
  public abstract void onError(Throwable paramThrowable);
  
  public abstract void onSubscribe(Disposable paramDisposable);
  
  public abstract void onSuccess(T paramT);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/MaybeObserver.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */