package io.reactivex;

public abstract interface Emitter<T>
{
  public abstract void onComplete();
  
  public abstract void onError(Throwable paramThrowable);
  
  public abstract void onNext(T paramT);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/Emitter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */