package io.reactivex.observables;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.internal.functions.Functions;
import io.reactivex.internal.operators.observable.ObservableAutoConnect;
import io.reactivex.internal.operators.observable.ObservableRefCount;
import io.reactivex.internal.util.ConnectConsumer;
import io.reactivex.plugins.RxJavaPlugins;

public abstract class ConnectableObservable<T>
  extends Observable<T>
{
  public Observable<T> autoConnect()
  {
    return autoConnect(1);
  }
  
  public Observable<T> autoConnect(int paramInt)
  {
    return autoConnect(paramInt, Functions.emptyConsumer());
  }
  
  public Observable<T> autoConnect(int paramInt, Consumer<? super Disposable> paramConsumer)
  {
    if (paramInt <= 0)
    {
      connect(paramConsumer);
      return RxJavaPlugins.onAssembly(this);
    }
    return RxJavaPlugins.onAssembly(new ObservableAutoConnect(this, paramInt, paramConsumer));
  }
  
  public final Disposable connect()
  {
    ConnectConsumer localConnectConsumer = new ConnectConsumer();
    connect(localConnectConsumer);
    return localConnectConsumer.disposable;
  }
  
  public abstract void connect(Consumer<? super Disposable> paramConsumer);
  
  public Observable<T> refCount()
  {
    return RxJavaPlugins.onAssembly(new ObservableRefCount(this));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observables/ConnectableObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */