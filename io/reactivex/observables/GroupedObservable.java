package io.reactivex.observables;

import io.reactivex.Observable;

public abstract class GroupedObservable<K, T>
  extends Observable<T>
{
  final K key;
  
  protected GroupedObservable(K paramK)
  {
    this.key = paramK;
  }
  
  public K getKey()
  {
    return (K)this.key;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/observables/GroupedObservable.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */