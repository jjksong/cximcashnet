package io.reactivex;

public abstract interface MaybeOperator<Downstream, Upstream>
{
  public abstract MaybeObserver<? super Upstream> apply(MaybeObserver<? super Downstream> paramMaybeObserver)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/MaybeOperator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */