package io.reactivex;

public abstract interface ObservableTransformer<Upstream, Downstream>
{
  public abstract ObservableSource<Downstream> apply(Observable<Upstream> paramObservable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/ObservableTransformer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */