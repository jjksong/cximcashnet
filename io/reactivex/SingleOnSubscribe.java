package io.reactivex;

public abstract interface SingleOnSubscribe<T>
{
  public abstract void subscribe(SingleEmitter<T> paramSingleEmitter)
    throws Exception;
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/SingleOnSubscribe.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */