package io.reactivex;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Cancellable;

public abstract interface ObservableEmitter<T>
  extends Emitter<T>
{
  public abstract boolean isDisposed();
  
  public abstract ObservableEmitter<T> serialize();
  
  public abstract void setCancellable(Cancellable paramCancellable);
  
  public abstract void setDisposable(Disposable paramDisposable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/ObservableEmitter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */