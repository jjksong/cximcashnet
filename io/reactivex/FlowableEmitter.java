package io.reactivex;

import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Cancellable;

public abstract interface FlowableEmitter<T>
  extends Emitter<T>
{
  public abstract boolean isCancelled();
  
  public abstract long requested();
  
  public abstract FlowableEmitter<T> serialize();
  
  public abstract void setCancellable(Cancellable paramCancellable);
  
  public abstract void setDisposable(Disposable paramDisposable);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/io/reactivex/FlowableEmitter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */