package androidx.versionedparcelable;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.RestrictTo;
import android.util.SparseIntArray;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY})
class VersionedParcelParcel
  extends VersionedParcel
{
  private static final boolean DEBUG = false;
  private static final String TAG = "VersionedParcelParcel";
  private int mCurrentField = -1;
  private final int mEnd;
  private int mNextRead = 0;
  private final int mOffset;
  private final Parcel mParcel;
  private final SparseIntArray mPositionLookup = new SparseIntArray();
  private final String mPrefix;
  
  VersionedParcelParcel(Parcel paramParcel)
  {
    this(paramParcel, paramParcel.dataPosition(), paramParcel.dataSize(), "");
  }
  
  VersionedParcelParcel(Parcel paramParcel, int paramInt1, int paramInt2, String paramString)
  {
    this.mParcel = paramParcel;
    this.mOffset = paramInt1;
    this.mEnd = paramInt2;
    this.mNextRead = this.mOffset;
    this.mPrefix = paramString;
  }
  
  private int readUntilField(int paramInt)
  {
    int i;
    do
    {
      i = this.mNextRead;
      if (i >= this.mEnd) {
        break;
      }
      this.mParcel.setDataPosition(i);
      int j = this.mParcel.readInt();
      i = this.mParcel.readInt();
      this.mNextRead += j;
    } while (i != paramInt);
    return this.mParcel.dataPosition();
    return -1;
  }
  
  public void closeField()
  {
    int i = this.mCurrentField;
    if (i >= 0)
    {
      int j = this.mPositionLookup.get(i);
      i = this.mParcel.dataPosition();
      this.mParcel.setDataPosition(j);
      this.mParcel.writeInt(i - j);
      this.mParcel.setDataPosition(i);
    }
  }
  
  protected VersionedParcel createSubParcel()
  {
    Parcel localParcel = this.mParcel;
    int k = localParcel.dataPosition();
    int j = this.mNextRead;
    int i = j;
    if (j == this.mOffset) {
      i = this.mEnd;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(this.mPrefix);
    localStringBuilder.append("  ");
    return new VersionedParcelParcel(localParcel, k, i, localStringBuilder.toString());
  }
  
  public boolean readBoolean()
  {
    boolean bool;
    if (this.mParcel.readInt() != 0) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public Bundle readBundle()
  {
    return this.mParcel.readBundle(getClass().getClassLoader());
  }
  
  public byte[] readByteArray()
  {
    int i = this.mParcel.readInt();
    if (i < 0) {
      return null;
    }
    byte[] arrayOfByte = new byte[i];
    this.mParcel.readByteArray(arrayOfByte);
    return arrayOfByte;
  }
  
  public double readDouble()
  {
    return this.mParcel.readDouble();
  }
  
  public boolean readField(int paramInt)
  {
    paramInt = readUntilField(paramInt);
    if (paramInt == -1) {
      return false;
    }
    this.mParcel.setDataPosition(paramInt);
    return true;
  }
  
  public float readFloat()
  {
    return this.mParcel.readFloat();
  }
  
  public int readInt()
  {
    return this.mParcel.readInt();
  }
  
  public long readLong()
  {
    return this.mParcel.readLong();
  }
  
  public <T extends Parcelable> T readParcelable()
  {
    return this.mParcel.readParcelable(getClass().getClassLoader());
  }
  
  public String readString()
  {
    return this.mParcel.readString();
  }
  
  public IBinder readStrongBinder()
  {
    return this.mParcel.readStrongBinder();
  }
  
  public void setOutputField(int paramInt)
  {
    closeField();
    this.mCurrentField = paramInt;
    this.mPositionLookup.put(paramInt, this.mParcel.dataPosition());
    writeInt(0);
    writeInt(paramInt);
  }
  
  public void writeBoolean(boolean paramBoolean)
  {
    throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge I and Z\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n\tat com.linchaolong.apktoolplus.core.ApkToolPlus.dex2jar(ApkToolPlus.java:293)\n\tat com.linchaolong.apktoolplus.module.main.Dex2JarMultDexSupport.<init>(Dex2JarMultDexSupport.java:60)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6$1.<init>(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6.onSelected(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainView.lambda$openFileSelector$22(MainView.java:199)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue$TaskWrapper.run(TaskQueue.java:45)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.loop(TaskQueue.java:109)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.lambda$start$0(TaskQueue.java:86)\n\tat java.lang.Thread.run(Thread.java:748)\n");
  }
  
  public void writeBundle(Bundle paramBundle)
  {
    this.mParcel.writeBundle(paramBundle);
  }
  
  public void writeByteArray(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null)
    {
      this.mParcel.writeInt(paramArrayOfByte.length);
      this.mParcel.writeByteArray(paramArrayOfByte);
    }
    else
    {
      this.mParcel.writeInt(-1);
    }
  }
  
  public void writeByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramArrayOfByte != null)
    {
      this.mParcel.writeInt(paramArrayOfByte.length);
      this.mParcel.writeByteArray(paramArrayOfByte, paramInt1, paramInt2);
    }
    else
    {
      this.mParcel.writeInt(-1);
    }
  }
  
  public void writeDouble(double paramDouble)
  {
    this.mParcel.writeDouble(paramDouble);
  }
  
  public void writeFloat(float paramFloat)
  {
    this.mParcel.writeFloat(paramFloat);
  }
  
  public void writeInt(int paramInt)
  {
    this.mParcel.writeInt(paramInt);
  }
  
  public void writeLong(long paramLong)
  {
    this.mParcel.writeLong(paramLong);
  }
  
  public void writeParcelable(Parcelable paramParcelable)
  {
    this.mParcel.writeParcelable(paramParcelable, 0);
  }
  
  public void writeString(String paramString)
  {
    this.mParcel.writeString(paramString);
  }
  
  public void writeStrongBinder(IBinder paramIBinder)
  {
    this.mParcel.writeStrongBinder(paramIBinder);
  }
  
  public void writeStrongInterface(IInterface paramIInterface)
  {
    this.mParcel.writeStrongInterface(paramIInterface);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/androidx/versionedparcelable/VersionedParcelParcel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */