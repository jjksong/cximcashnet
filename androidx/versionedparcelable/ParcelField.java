package androidx.versionedparcelable;

import android.support.annotation.RestrictTo;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.SOURCE)
@Target({java.lang.annotation.ElementType.FIELD})
@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public @interface ParcelField
{
  int value();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/androidx/versionedparcelable/ParcelField.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */