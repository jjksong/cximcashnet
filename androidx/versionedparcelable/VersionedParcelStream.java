package androidx.versionedparcelable;

import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcelable;
import android.support.annotation.RestrictTo;
import android.util.SparseArray;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.Set;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY})
class VersionedParcelStream
  extends VersionedParcel
{
  private static final int TYPE_BOOLEAN = 5;
  private static final int TYPE_BOOLEAN_ARRAY = 6;
  private static final int TYPE_DOUBLE = 7;
  private static final int TYPE_DOUBLE_ARRAY = 8;
  private static final int TYPE_FLOAT = 13;
  private static final int TYPE_FLOAT_ARRAY = 14;
  private static final int TYPE_INT = 9;
  private static final int TYPE_INT_ARRAY = 10;
  private static final int TYPE_LONG = 11;
  private static final int TYPE_LONG_ARRAY = 12;
  private static final int TYPE_NULL = 0;
  private static final int TYPE_STRING = 3;
  private static final int TYPE_STRING_ARRAY = 4;
  private static final int TYPE_SUB_BUNDLE = 1;
  private static final int TYPE_SUB_PERSISTABLE_BUNDLE = 2;
  private static final Charset UTF_16 = Charset.forName("UTF-16");
  private final SparseArray<InputBuffer> mCachedFields = new SparseArray();
  private DataInputStream mCurrentInput;
  private DataOutputStream mCurrentOutput;
  private FieldBuffer mFieldBuffer;
  private boolean mIgnoreParcelables;
  private final DataInputStream mMasterInput;
  private final DataOutputStream mMasterOutput;
  
  public VersionedParcelStream(InputStream paramInputStream, OutputStream paramOutputStream)
  {
    Object localObject = null;
    if (paramInputStream != null) {
      paramInputStream = new DataInputStream(paramInputStream);
    } else {
      paramInputStream = null;
    }
    this.mMasterInput = paramInputStream;
    paramInputStream = (InputStream)localObject;
    if (paramOutputStream != null) {
      paramInputStream = new DataOutputStream(paramOutputStream);
    }
    this.mMasterOutput = paramInputStream;
    this.mCurrentInput = this.mMasterInput;
    this.mCurrentOutput = this.mMasterOutput;
  }
  
  private void readObject(int paramInt, String paramString, Bundle paramBundle)
  {
    switch (paramInt)
    {
    default: 
      paramString = new StringBuilder();
      paramString.append("Unknown type ");
      paramString.append(paramInt);
      throw new RuntimeException(paramString.toString());
    case 14: 
      paramBundle.putFloatArray(paramString, readFloatArray());
      break;
    case 13: 
      paramBundle.putFloat(paramString, readFloat());
      break;
    case 12: 
      paramBundle.putLongArray(paramString, readLongArray());
      break;
    case 11: 
      paramBundle.putLong(paramString, readLong());
      break;
    case 10: 
      paramBundle.putIntArray(paramString, readIntArray());
      break;
    case 9: 
      paramBundle.putInt(paramString, readInt());
      break;
    case 8: 
      paramBundle.putDoubleArray(paramString, readDoubleArray());
      break;
    case 7: 
      paramBundle.putDouble(paramString, readDouble());
      break;
    case 6: 
      paramBundle.putBooleanArray(paramString, readBooleanArray());
      break;
    case 5: 
      paramBundle.putBoolean(paramString, readBoolean());
      break;
    case 4: 
      paramBundle.putStringArray(paramString, (String[])readArray(new String[0]));
      break;
    case 3: 
      paramBundle.putString(paramString, readString());
      break;
    case 2: 
      paramBundle.putBundle(paramString, readBundle());
      break;
    case 1: 
      paramBundle.putBundle(paramString, readBundle());
      break;
    case 0: 
      paramBundle.putParcelable(paramString, null);
    }
  }
  
  private void writeObject(Object paramObject)
  {
    if (paramObject == null)
    {
      writeInt(0);
    }
    else if ((paramObject instanceof Bundle))
    {
      writeInt(1);
      writeBundle((Bundle)paramObject);
    }
    else if ((paramObject instanceof String))
    {
      writeInt(3);
      writeString((String)paramObject);
    }
    else if ((paramObject instanceof String[]))
    {
      writeInt(4);
      writeArray((String[])paramObject);
    }
    else if ((paramObject instanceof Boolean))
    {
      writeInt(5);
      writeBoolean(((Boolean)paramObject).booleanValue());
    }
    else if ((paramObject instanceof boolean[]))
    {
      writeInt(6);
      writeBooleanArray((boolean[])paramObject);
    }
    else if ((paramObject instanceof Double))
    {
      writeInt(7);
      writeDouble(((Double)paramObject).doubleValue());
    }
    else if ((paramObject instanceof double[]))
    {
      writeInt(8);
      writeDoubleArray((double[])paramObject);
    }
    else if ((paramObject instanceof Integer))
    {
      writeInt(9);
      writeInt(((Integer)paramObject).intValue());
    }
    else if ((paramObject instanceof int[]))
    {
      writeInt(10);
      writeIntArray((int[])paramObject);
    }
    else if ((paramObject instanceof Long))
    {
      writeInt(11);
      writeLong(((Long)paramObject).longValue());
    }
    else if ((paramObject instanceof long[]))
    {
      writeInt(12);
      writeLongArray((long[])paramObject);
    }
    else if ((paramObject instanceof Float))
    {
      writeInt(13);
      writeFloat(((Float)paramObject).floatValue());
    }
    else
    {
      if (!(paramObject instanceof float[])) {
        break label333;
      }
      writeInt(14);
      writeFloatArray((float[])paramObject);
    }
    return;
    label333:
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("Unsupported type ");
    localStringBuilder.append(paramObject.getClass());
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public void closeField()
  {
    FieldBuffer localFieldBuffer = this.mFieldBuffer;
    if (localFieldBuffer != null) {
      try
      {
        if (localFieldBuffer.mOutput.size() != 0) {
          this.mFieldBuffer.flushField();
        }
        this.mFieldBuffer = null;
      }
      catch (IOException localIOException)
      {
        throw new VersionedParcel.ParcelException(localIOException);
      }
    }
  }
  
  protected VersionedParcel createSubParcel()
  {
    return new VersionedParcelStream(this.mCurrentInput, this.mCurrentOutput);
  }
  
  public boolean isStream()
  {
    return true;
  }
  
  public boolean readBoolean()
  {
    try
    {
      boolean bool = this.mCurrentInput.readBoolean();
      return bool;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public Bundle readBundle()
  {
    int j = readInt();
    if (j < 0) {
      return null;
    }
    Bundle localBundle = new Bundle();
    for (int i = 0; i < j; i++)
    {
      String str = readString();
      readObject(readInt(), str, localBundle);
    }
    return localBundle;
  }
  
  public byte[] readByteArray()
  {
    try
    {
      int i = this.mCurrentInput.readInt();
      if (i > 0)
      {
        byte[] arrayOfByte = new byte[i];
        this.mCurrentInput.readFully(arrayOfByte);
        return arrayOfByte;
      }
      return null;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public double readDouble()
  {
    try
    {
      double d = this.mCurrentInput.readDouble();
      return d;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public boolean readField(int paramInt)
  {
    InputBuffer localInputBuffer = (InputBuffer)this.mCachedFields.get(paramInt);
    if (localInputBuffer != null)
    {
      this.mCachedFields.remove(paramInt);
      this.mCurrentInput = localInputBuffer.mInputStream;
      return true;
    }
    try
    {
      for (;;)
      {
        int k = this.mMasterInput.readInt();
        int j = k & 0xFFFF;
        int i = j;
        if (j == 65535) {
          i = this.mMasterInput.readInt();
        }
        localInputBuffer = new androidx/versionedparcelable/VersionedParcelStream$InputBuffer;
        localInputBuffer.<init>(k >> 16 & 0xFFFF, i, this.mMasterInput);
        if (localInputBuffer.mFieldId == paramInt)
        {
          this.mCurrentInput = localInputBuffer.mInputStream;
          return true;
        }
        this.mCachedFields.put(localInputBuffer.mFieldId, localInputBuffer);
      }
      return false;
    }
    catch (IOException localIOException) {}
  }
  
  public float readFloat()
  {
    try
    {
      float f = this.mCurrentInput.readFloat();
      return f;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public int readInt()
  {
    try
    {
      int i = this.mCurrentInput.readInt();
      return i;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public long readLong()
  {
    try
    {
      long l = this.mCurrentInput.readLong();
      return l;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public <T extends Parcelable> T readParcelable()
  {
    return null;
  }
  
  public String readString()
  {
    try
    {
      int i = this.mCurrentInput.readInt();
      if (i > 0)
      {
        Object localObject = new byte[i];
        this.mCurrentInput.readFully((byte[])localObject);
        localObject = new String((byte[])localObject, UTF_16);
        return (String)localObject;
      }
      return null;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public IBinder readStrongBinder()
  {
    return null;
  }
  
  public void setOutputField(int paramInt)
  {
    closeField();
    this.mFieldBuffer = new FieldBuffer(paramInt, this.mMasterOutput);
    this.mCurrentOutput = this.mFieldBuffer.mDataStream;
  }
  
  public void setSerializationFlags(boolean paramBoolean1, boolean paramBoolean2)
  {
    if (paramBoolean1)
    {
      this.mIgnoreParcelables = paramBoolean2;
      return;
    }
    throw new RuntimeException("Serialization of this object is not allowed");
  }
  
  public void writeBoolean(boolean paramBoolean)
  {
    try
    {
      this.mCurrentOutput.writeBoolean(paramBoolean);
      return;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public void writeBundle(Bundle paramBundle)
  {
    if (paramBundle != null) {}
    try
    {
      Object localObject = paramBundle.keySet();
      this.mCurrentOutput.writeInt(((Set)localObject).size());
      Iterator localIterator = ((Set)localObject).iterator();
      while (localIterator.hasNext())
      {
        localObject = (String)localIterator.next();
        writeString((String)localObject);
        writeObject(paramBundle.get((String)localObject));
        continue;
        this.mCurrentOutput.writeInt(-1);
      }
      return;
    }
    catch (IOException paramBundle)
    {
      throw new VersionedParcel.ParcelException(paramBundle);
    }
  }
  
  public void writeByteArray(byte[] paramArrayOfByte)
  {
    if (paramArrayOfByte != null) {}
    try
    {
      this.mCurrentOutput.writeInt(paramArrayOfByte.length);
      this.mCurrentOutput.write(paramArrayOfByte);
      break label32;
      this.mCurrentOutput.writeInt(-1);
      label32:
      return;
    }
    catch (IOException paramArrayOfByte)
    {
      throw new VersionedParcel.ParcelException(paramArrayOfByte);
    }
  }
  
  public void writeByteArray(byte[] paramArrayOfByte, int paramInt1, int paramInt2)
  {
    if (paramArrayOfByte != null) {}
    try
    {
      this.mCurrentOutput.writeInt(paramInt2);
      this.mCurrentOutput.write(paramArrayOfByte, paramInt1, paramInt2);
      break label33;
      this.mCurrentOutput.writeInt(-1);
      label33:
      return;
    }
    catch (IOException paramArrayOfByte)
    {
      throw new VersionedParcel.ParcelException(paramArrayOfByte);
    }
  }
  
  public void writeDouble(double paramDouble)
  {
    try
    {
      this.mCurrentOutput.writeDouble(paramDouble);
      return;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public void writeFloat(float paramFloat)
  {
    try
    {
      this.mCurrentOutput.writeFloat(paramFloat);
      return;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public void writeInt(int paramInt)
  {
    try
    {
      this.mCurrentOutput.writeInt(paramInt);
      return;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public void writeLong(long paramLong)
  {
    try
    {
      this.mCurrentOutput.writeLong(paramLong);
      return;
    }
    catch (IOException localIOException)
    {
      throw new VersionedParcel.ParcelException(localIOException);
    }
  }
  
  public void writeParcelable(Parcelable paramParcelable)
  {
    if (this.mIgnoreParcelables) {
      return;
    }
    throw new RuntimeException("Parcelables cannot be written to an OutputStream");
  }
  
  public void writeString(String paramString)
  {
    if (paramString != null) {}
    try
    {
      paramString = paramString.getBytes(UTF_16);
      this.mCurrentOutput.writeInt(paramString.length);
      this.mCurrentOutput.write(paramString);
      break label40;
      this.mCurrentOutput.writeInt(-1);
      label40:
      return;
    }
    catch (IOException paramString)
    {
      throw new VersionedParcel.ParcelException(paramString);
    }
  }
  
  public void writeStrongBinder(IBinder paramIBinder)
  {
    if (this.mIgnoreParcelables) {
      return;
    }
    throw new RuntimeException("Binders cannot be written to an OutputStream");
  }
  
  public void writeStrongInterface(IInterface paramIInterface)
  {
    if (this.mIgnoreParcelables) {
      return;
    }
    throw new RuntimeException("Binders cannot be written to an OutputStream");
  }
  
  private static class FieldBuffer
  {
    final DataOutputStream mDataStream = new DataOutputStream(this.mOutput);
    private final int mFieldId;
    final ByteArrayOutputStream mOutput = new ByteArrayOutputStream();
    private final DataOutputStream mTarget;
    
    FieldBuffer(int paramInt, DataOutputStream paramDataOutputStream)
    {
      this.mFieldId = paramInt;
      this.mTarget = paramDataOutputStream;
    }
    
    void flushField()
      throws IOException
    {
      this.mDataStream.flush();
      int j = this.mOutput.size();
      int k = this.mFieldId;
      int i;
      if (j >= 65535) {
        i = 65535;
      } else {
        i = j;
      }
      this.mTarget.writeInt(k << 16 | i);
      if (j >= 65535) {
        this.mTarget.writeInt(j);
      }
      this.mOutput.writeTo(this.mTarget);
    }
  }
  
  private static class InputBuffer
  {
    final int mFieldId;
    final DataInputStream mInputStream;
    private final int mSize;
    
    InputBuffer(int paramInt1, int paramInt2, DataInputStream paramDataInputStream)
      throws IOException
    {
      this.mSize = paramInt2;
      this.mFieldId = paramInt1;
      byte[] arrayOfByte = new byte[this.mSize];
      paramDataInputStream.readFully(arrayOfByte);
      this.mInputStream = new DataInputStream(new ByteArrayInputStream(arrayOfByte));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/androidx/versionedparcelable/VersionedParcelStream.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */