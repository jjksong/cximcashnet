package android.support.multidex;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build.VERSION;
import android.util.Log;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipFile;

final class MultiDexExtractor
{
  private static final int BUFFER_SIZE = 16384;
  private static final String DEX_PREFIX = "classes";
  private static final String DEX_SUFFIX = ".dex";
  private static final String EXTRACTED_NAME_EXT = ".classes";
  private static final String EXTRACTED_SUFFIX = ".zip";
  private static final String KEY_CRC = "crc";
  private static final String KEY_DEX_CRC = "dex.crc.";
  private static final String KEY_DEX_NUMBER = "dex.number";
  private static final String KEY_DEX_TIME = "dex.time.";
  private static final String KEY_TIME_STAMP = "timestamp";
  private static final String LOCK_FILENAME = "MultiDex.lock";
  private static final int MAX_EXTRACT_ATTEMPTS = 3;
  private static final long NO_VALUE = -1L;
  private static final String PREFS_FILE = "multidex.version";
  private static final String TAG = "MultiDex";
  
  private static void closeQuietly(Closeable paramCloseable)
  {
    try
    {
      paramCloseable.close();
    }
    catch (IOException paramCloseable)
    {
      Log.w("MultiDex", "Failed to close resource", paramCloseable);
    }
  }
  
  /* Error */
  private static void extract(ZipFile paramZipFile, java.util.zip.ZipEntry paramZipEntry, File paramFile, String paramString)
    throws IOException, java.io.FileNotFoundException
  {
    // Byte code:
    //   0: aload_0
    //   1: aload_1
    //   2: invokevirtual 87	java/util/zip/ZipFile:getInputStream	(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;
    //   5: astore_0
    //   6: new 89	java/lang/StringBuilder
    //   9: dup
    //   10: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   13: astore 6
    //   15: aload 6
    //   17: ldc 92
    //   19: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   22: pop
    //   23: aload 6
    //   25: aload_3
    //   26: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   29: pop
    //   30: aload 6
    //   32: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   35: ldc 25
    //   37: aload_2
    //   38: invokevirtual 106	java/io/File:getParentFile	()Ljava/io/File;
    //   41: invokestatic 110	java/io/File:createTempFile	(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    //   44: astore_3
    //   45: new 89	java/lang/StringBuilder
    //   48: dup
    //   49: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   52: astore 6
    //   54: aload 6
    //   56: ldc 112
    //   58: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   61: pop
    //   62: aload 6
    //   64: aload_3
    //   65: invokevirtual 115	java/io/File:getPath	()Ljava/lang/String;
    //   68: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   71: pop
    //   72: ldc 55
    //   74: aload 6
    //   76: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   79: invokestatic 119	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   82: pop
    //   83: new 121	java/util/zip/ZipOutputStream
    //   86: astore 6
    //   88: new 123	java/io/BufferedOutputStream
    //   91: astore 7
    //   93: new 125	java/io/FileOutputStream
    //   96: astore 8
    //   98: aload 8
    //   100: aload_3
    //   101: invokespecial 128	java/io/FileOutputStream:<init>	(Ljava/io/File;)V
    //   104: aload 7
    //   106: aload 8
    //   108: invokespecial 131	java/io/BufferedOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   111: aload 6
    //   113: aload 7
    //   115: invokespecial 132	java/util/zip/ZipOutputStream:<init>	(Ljava/io/OutputStream;)V
    //   118: new 134	java/util/zip/ZipEntry
    //   121: astore 7
    //   123: aload 7
    //   125: ldc -120
    //   127: invokespecial 139	java/util/zip/ZipEntry:<init>	(Ljava/lang/String;)V
    //   130: aload 7
    //   132: aload_1
    //   133: invokevirtual 143	java/util/zip/ZipEntry:getTime	()J
    //   136: invokevirtual 147	java/util/zip/ZipEntry:setTime	(J)V
    //   139: aload 6
    //   141: aload 7
    //   143: invokevirtual 151	java/util/zip/ZipOutputStream:putNextEntry	(Ljava/util/zip/ZipEntry;)V
    //   146: sipush 16384
    //   149: newarray <illegal type>
    //   151: astore_1
    //   152: aload_0
    //   153: aload_1
    //   154: invokevirtual 157	java/io/InputStream:read	([B)I
    //   157: istore 4
    //   159: iload 4
    //   161: iconst_m1
    //   162: if_icmpeq +22 -> 184
    //   165: aload 6
    //   167: aload_1
    //   168: iconst_0
    //   169: iload 4
    //   171: invokevirtual 161	java/util/zip/ZipOutputStream:write	([BII)V
    //   174: aload_0
    //   175: aload_1
    //   176: invokevirtual 157	java/io/InputStream:read	([B)I
    //   179: istore 4
    //   181: goto -22 -> 159
    //   184: aload 6
    //   186: invokevirtual 164	java/util/zip/ZipOutputStream:closeEntry	()V
    //   189: aload 6
    //   191: invokevirtual 165	java/util/zip/ZipOutputStream:close	()V
    //   194: aload_3
    //   195: invokevirtual 169	java/io/File:setReadOnly	()Z
    //   198: ifeq +128 -> 326
    //   201: new 89	java/lang/StringBuilder
    //   204: astore_1
    //   205: aload_1
    //   206: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   209: aload_1
    //   210: ldc -85
    //   212: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   215: pop
    //   216: aload_1
    //   217: aload_2
    //   218: invokevirtual 115	java/io/File:getPath	()Ljava/lang/String;
    //   221: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   224: pop
    //   225: ldc 55
    //   227: aload_1
    //   228: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   231: invokestatic 119	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   234: pop
    //   235: aload_3
    //   236: aload_2
    //   237: invokevirtual 175	java/io/File:renameTo	(Ljava/io/File;)Z
    //   240: istore 5
    //   242: iload 5
    //   244: ifeq +13 -> 257
    //   247: aload_0
    //   248: invokestatic 177	android/support/multidex/MultiDexExtractor:closeQuietly	(Ljava/io/Closeable;)V
    //   251: aload_3
    //   252: invokevirtual 180	java/io/File:delete	()Z
    //   255: pop
    //   256: return
    //   257: new 64	java/io/IOException
    //   260: astore_1
    //   261: new 89	java/lang/StringBuilder
    //   264: astore 6
    //   266: aload 6
    //   268: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   271: aload 6
    //   273: ldc -74
    //   275: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   278: pop
    //   279: aload 6
    //   281: aload_3
    //   282: invokevirtual 185	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   285: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   288: pop
    //   289: aload 6
    //   291: ldc -69
    //   293: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   296: pop
    //   297: aload 6
    //   299: aload_2
    //   300: invokevirtual 185	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   303: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   306: pop
    //   307: aload 6
    //   309: ldc -67
    //   311: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   314: pop
    //   315: aload_1
    //   316: aload 6
    //   318: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   321: invokespecial 190	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   324: aload_1
    //   325: athrow
    //   326: new 64	java/io/IOException
    //   329: astore 6
    //   331: new 89	java/lang/StringBuilder
    //   334: astore_1
    //   335: aload_1
    //   336: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   339: aload_1
    //   340: ldc -64
    //   342: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   345: pop
    //   346: aload_1
    //   347: aload_3
    //   348: invokevirtual 185	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   351: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   354: pop
    //   355: aload_1
    //   356: ldc -62
    //   358: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   361: pop
    //   362: aload_1
    //   363: aload_2
    //   364: invokevirtual 185	java/io/File:getAbsolutePath	()Ljava/lang/String;
    //   367: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   370: pop
    //   371: aload_1
    //   372: ldc -60
    //   374: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   377: pop
    //   378: aload 6
    //   380: aload_1
    //   381: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   384: invokespecial 190	java/io/IOException:<init>	(Ljava/lang/String;)V
    //   387: aload 6
    //   389: athrow
    //   390: astore_1
    //   391: aload 6
    //   393: invokevirtual 165	java/util/zip/ZipOutputStream:close	()V
    //   396: aload_1
    //   397: athrow
    //   398: astore_1
    //   399: aload_0
    //   400: invokestatic 177	android/support/multidex/MultiDexExtractor:closeQuietly	(Ljava/io/Closeable;)V
    //   403: aload_3
    //   404: invokevirtual 180	java/io/File:delete	()Z
    //   407: pop
    //   408: aload_1
    //   409: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	410	0	paramZipFile	ZipFile
    //   0	410	1	paramZipEntry	java.util.zip.ZipEntry
    //   0	410	2	paramFile	File
    //   0	410	3	paramString	String
    //   157	23	4	i	int
    //   240	3	5	bool	boolean
    //   13	379	6	localObject1	Object
    //   91	51	7	localObject2	Object
    //   96	11	8	localFileOutputStream	java.io.FileOutputStream
    // Exception table:
    //   from	to	target	type
    //   118	159	390	finally
    //   165	181	390	finally
    //   184	189	390	finally
    //   83	118	398	finally
    //   189	242	398	finally
    //   257	326	398	finally
    //   326	390	398	finally
    //   391	398	398	finally
  }
  
  private static SharedPreferences getMultiDexPreferences(Context paramContext)
  {
    int i;
    if (Build.VERSION.SDK_INT < 11) {
      i = 0;
    } else {
      i = 4;
    }
    return paramContext.getSharedPreferences("multidex.version", i);
  }
  
  private static long getTimeStamp(File paramFile)
  {
    long l2 = paramFile.lastModified();
    long l1 = l2;
    if (l2 == -1L) {
      l1 = l2 - 1L;
    }
    return l1;
  }
  
  private static long getZipCrc(File paramFile)
    throws IOException
  {
    long l2 = ZipUtil.getZipCrc(paramFile);
    long l1 = l2;
    if (l2 == -1L) {
      l1 = l2 - 1L;
    }
    return l1;
  }
  
  private static boolean isModified(Context paramContext, File paramFile, long paramLong, String paramString)
  {
    paramContext = getMultiDexPreferences(paramContext);
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append(paramString);
    localStringBuilder.append("timestamp");
    if (paramContext.getLong(localStringBuilder.toString(), -1L) == getTimeStamp(paramFile))
    {
      paramFile = new StringBuilder();
      paramFile.append(paramString);
      paramFile.append("crc");
      if (paramContext.getLong(paramFile.toString(), -1L) == paramLong)
      {
        bool = false;
        break label104;
      }
    }
    boolean bool = true;
    label104:
    return bool;
  }
  
  /* Error */
  static List<? extends File> load(Context paramContext, File paramFile1, File paramFile2, String paramString, boolean paramBoolean)
    throws IOException
  {
    // Byte code:
    //   0: new 89	java/lang/StringBuilder
    //   3: dup
    //   4: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   7: astore 7
    //   9: aload 7
    //   11: ldc -20
    //   13: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   16: pop
    //   17: aload 7
    //   19: aload_1
    //   20: invokevirtual 115	java/io/File:getPath	()Ljava/lang/String;
    //   23: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   26: pop
    //   27: aload 7
    //   29: ldc -18
    //   31: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   34: pop
    //   35: aload 7
    //   37: iload 4
    //   39: invokevirtual 241	java/lang/StringBuilder:append	(Z)Ljava/lang/StringBuilder;
    //   42: pop
    //   43: aload 7
    //   45: ldc -18
    //   47: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   50: pop
    //   51: aload 7
    //   53: aload_3
    //   54: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   57: pop
    //   58: aload 7
    //   60: ldc -13
    //   62: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   65: pop
    //   66: ldc 55
    //   68: aload 7
    //   70: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   73: invokestatic 119	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   76: pop
    //   77: aload_1
    //   78: invokestatic 244	android/support/multidex/MultiDexExtractor:getZipCrc	(Ljava/io/File;)J
    //   81: lstore 5
    //   83: new 102	java/io/File
    //   86: dup
    //   87: aload_2
    //   88: ldc 43
    //   90: invokespecial 247	java/io/File:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   93: astore 11
    //   95: new 249	java/io/RandomAccessFile
    //   98: dup
    //   99: aload 11
    //   101: ldc -5
    //   103: invokespecial 252	java/io/RandomAccessFile:<init>	(Ljava/io/File;Ljava/lang/String;)V
    //   106: astore 10
    //   108: aload 10
    //   110: invokevirtual 256	java/io/RandomAccessFile:getChannel	()Ljava/nio/channels/FileChannel;
    //   113: astore 7
    //   115: new 89	java/lang/StringBuilder
    //   118: astore 8
    //   120: aload 8
    //   122: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   125: aload 8
    //   127: ldc_w 258
    //   130: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   133: pop
    //   134: aload 8
    //   136: aload 11
    //   138: invokevirtual 115	java/io/File:getPath	()Ljava/lang/String;
    //   141: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   144: pop
    //   145: ldc 55
    //   147: aload 8
    //   149: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   152: invokestatic 119	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   155: pop
    //   156: aload 7
    //   158: invokevirtual 264	java/nio/channels/FileChannel:lock	()Ljava/nio/channels/FileLock;
    //   161: astore 8
    //   163: new 89	java/lang/StringBuilder
    //   166: astore 9
    //   168: aload 9
    //   170: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   173: aload 9
    //   175: aload 11
    //   177: invokevirtual 115	java/io/File:getPath	()Ljava/lang/String;
    //   180: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   183: pop
    //   184: aload 9
    //   186: ldc_w 266
    //   189: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   192: pop
    //   193: ldc 55
    //   195: aload 9
    //   197: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   200: invokestatic 119	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   203: pop
    //   204: iload 4
    //   206: ifne +69 -> 275
    //   209: aload_0
    //   210: aload_1
    //   211: lload 5
    //   213: aload_3
    //   214: invokestatic 268	android/support/multidex/MultiDexExtractor:isModified	(Landroid/content/Context;Ljava/io/File;JLjava/lang/String;)Z
    //   217: istore 4
    //   219: iload 4
    //   221: ifne +54 -> 275
    //   224: aload_0
    //   225: aload_1
    //   226: aload_2
    //   227: aload_3
    //   228: invokestatic 272	android/support/multidex/MultiDexExtractor:loadExistingExtractions	(Landroid/content/Context;Ljava/io/File;Ljava/io/File;Ljava/lang/String;)Ljava/util/List;
    //   231: astore 9
    //   233: aload 9
    //   235: astore_0
    //   236: goto +68 -> 304
    //   239: astore 9
    //   241: ldc 55
    //   243: ldc_w 274
    //   246: aload 9
    //   248: invokestatic 77	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   251: pop
    //   252: aload_1
    //   253: aload_2
    //   254: invokestatic 278	android/support/multidex/MultiDexExtractor:performExtractions	(Ljava/io/File;Ljava/io/File;)Ljava/util/List;
    //   257: astore_2
    //   258: aload_0
    //   259: aload_3
    //   260: aload_1
    //   261: invokestatic 232	android/support/multidex/MultiDexExtractor:getTimeStamp	(Ljava/io/File;)J
    //   264: lload 5
    //   266: aload_2
    //   267: invokestatic 282	android/support/multidex/MultiDexExtractor:putStoredApkInfo	(Landroid/content/Context;Ljava/lang/String;JJLjava/util/List;)V
    //   270: aload_2
    //   271: astore_0
    //   272: goto -36 -> 236
    //   275: ldc 55
    //   277: ldc_w 284
    //   280: invokestatic 119	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   283: pop
    //   284: aload_1
    //   285: aload_2
    //   286: invokestatic 278	android/support/multidex/MultiDexExtractor:performExtractions	(Ljava/io/File;Ljava/io/File;)Ljava/util/List;
    //   289: astore_2
    //   290: aload_0
    //   291: aload_3
    //   292: aload_1
    //   293: invokestatic 232	android/support/multidex/MultiDexExtractor:getTimeStamp	(Ljava/io/File;)J
    //   296: lload 5
    //   298: aload_2
    //   299: invokestatic 282	android/support/multidex/MultiDexExtractor:putStoredApkInfo	(Landroid/content/Context;Ljava/lang/String;JJLjava/util/List;)V
    //   302: aload_2
    //   303: astore_0
    //   304: aload 8
    //   306: ifnull +51 -> 357
    //   309: aload 8
    //   311: invokevirtual 289	java/nio/channels/FileLock:release	()V
    //   314: goto +43 -> 357
    //   317: astore_1
    //   318: new 89	java/lang/StringBuilder
    //   321: dup
    //   322: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   325: astore_2
    //   326: aload_2
    //   327: ldc_w 291
    //   330: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   333: pop
    //   334: aload_2
    //   335: aload 11
    //   337: invokevirtual 115	java/io/File:getPath	()Ljava/lang/String;
    //   340: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   343: pop
    //   344: ldc 55
    //   346: aload_2
    //   347: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   350: invokestatic 294	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   353: pop
    //   354: goto +5 -> 359
    //   357: aconst_null
    //   358: astore_1
    //   359: aload 7
    //   361: ifnull +8 -> 369
    //   364: aload 7
    //   366: invokestatic 177	android/support/multidex/MultiDexExtractor:closeQuietly	(Ljava/io/Closeable;)V
    //   369: aload 10
    //   371: invokestatic 177	android/support/multidex/MultiDexExtractor:closeQuietly	(Ljava/io/Closeable;)V
    //   374: aload_1
    //   375: ifnonnull +50 -> 425
    //   378: new 89	java/lang/StringBuilder
    //   381: dup
    //   382: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   385: astore_1
    //   386: aload_1
    //   387: ldc_w 296
    //   390: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   393: pop
    //   394: aload_1
    //   395: aload_0
    //   396: invokeinterface 302 1 0
    //   401: invokevirtual 305	java/lang/StringBuilder:append	(I)Ljava/lang/StringBuilder;
    //   404: pop
    //   405: aload_1
    //   406: ldc_w 307
    //   409: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   412: pop
    //   413: ldc 55
    //   415: aload_1
    //   416: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   419: invokestatic 119	android/util/Log:i	(Ljava/lang/String;Ljava/lang/String;)I
    //   422: pop
    //   423: aload_0
    //   424: areturn
    //   425: aload_1
    //   426: athrow
    //   427: astore_0
    //   428: aload 7
    //   430: astore_2
    //   431: aload 8
    //   433: astore_1
    //   434: goto +17 -> 451
    //   437: astore_0
    //   438: aconst_null
    //   439: astore_1
    //   440: aload 7
    //   442: astore_2
    //   443: goto +8 -> 451
    //   446: astore_0
    //   447: aconst_null
    //   448: astore_2
    //   449: aload_2
    //   450: astore_1
    //   451: aload_1
    //   452: ifnull +47 -> 499
    //   455: aload_1
    //   456: invokevirtual 289	java/nio/channels/FileLock:release	()V
    //   459: goto +40 -> 499
    //   462: astore_1
    //   463: new 89	java/lang/StringBuilder
    //   466: dup
    //   467: invokespecial 90	java/lang/StringBuilder:<init>	()V
    //   470: astore_1
    //   471: aload_1
    //   472: ldc_w 291
    //   475: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   478: pop
    //   479: aload_1
    //   480: aload 11
    //   482: invokevirtual 115	java/io/File:getPath	()Ljava/lang/String;
    //   485: invokevirtual 96	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   488: pop
    //   489: ldc 55
    //   491: aload_1
    //   492: invokevirtual 100	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   495: invokestatic 294	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   498: pop
    //   499: aload_2
    //   500: ifnull +7 -> 507
    //   503: aload_2
    //   504: invokestatic 177	android/support/multidex/MultiDexExtractor:closeQuietly	(Ljava/io/Closeable;)V
    //   507: aload 10
    //   509: invokestatic 177	android/support/multidex/MultiDexExtractor:closeQuietly	(Ljava/io/Closeable;)V
    //   512: aload_0
    //   513: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	514	0	paramContext	Context
    //   0	514	1	paramFile1	File
    //   0	514	2	paramFile2	File
    //   0	514	3	paramString	String
    //   0	514	4	paramBoolean	boolean
    //   81	216	5	l	long
    //   7	434	7	localObject1	Object
    //   118	314	8	localObject2	Object
    //   166	68	9	localObject3	Object
    //   239	8	9	localIOException	IOException
    //   106	402	10	localRandomAccessFile	java.io.RandomAccessFile
    //   93	388	11	localFile	File
    // Exception table:
    //   from	to	target	type
    //   224	233	239	java/io/IOException
    //   309	314	317	java/io/IOException
    //   163	204	427	finally
    //   209	219	427	finally
    //   224	233	427	finally
    //   241	270	427	finally
    //   275	302	427	finally
    //   115	163	437	finally
    //   108	115	446	finally
    //   455	459	462	java/io/IOException
  }
  
  private static List<ExtractedDex> loadExistingExtractions(Context paramContext, File paramFile1, File paramFile2, String paramString)
    throws IOException
  {
    Log.i("MultiDex", "loading existing secondary dex files");
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(paramFile1.getName());
    ((StringBuilder)localObject1).append(".classes");
    paramFile1 = ((StringBuilder)localObject1).toString();
    paramContext = getMultiDexPreferences(paramContext);
    localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(paramString);
    ((StringBuilder)localObject1).append("dex.number");
    int j = paramContext.getInt(((StringBuilder)localObject1).toString(), 1);
    localObject1 = new ArrayList(j - 1);
    int i = 2;
    while (i <= j)
    {
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append(paramFile1);
      ((StringBuilder)localObject2).append(i);
      ((StringBuilder)localObject2).append(".zip");
      localObject2 = new ExtractedDex(paramFile2, ((StringBuilder)localObject2).toString());
      if (((ExtractedDex)localObject2).isFile())
      {
        ((ExtractedDex)localObject2).crc = getZipCrc((File)localObject2);
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramString);
        localStringBuilder.append("dex.crc.");
        localStringBuilder.append(i);
        long l2 = paramContext.getLong(localStringBuilder.toString(), -1L);
        localStringBuilder = new StringBuilder();
        localStringBuilder.append(paramString);
        localStringBuilder.append("dex.time.");
        localStringBuilder.append(i);
        long l3 = paramContext.getLong(localStringBuilder.toString(), -1L);
        long l1 = ((ExtractedDex)localObject2).lastModified();
        if ((l3 == l1) && (l2 == ((ExtractedDex)localObject2).crc))
        {
          ((List)localObject1).add(localObject2);
          i++;
        }
        else
        {
          paramContext = new StringBuilder();
          paramContext.append("Invalid extracted dex: ");
          paramContext.append(localObject2);
          paramContext.append(" (key \"");
          paramContext.append(paramString);
          paramContext.append("\"), expected modification time: ");
          paramContext.append(l3);
          paramContext.append(", modification time: ");
          paramContext.append(l1);
          paramContext.append(", expected crc: ");
          paramContext.append(l2);
          paramContext.append(", file crc: ");
          paramContext.append(((ExtractedDex)localObject2).crc);
          throw new IOException(paramContext.toString());
        }
      }
      else
      {
        paramContext = new StringBuilder();
        paramContext.append("Missing extracted secondary dex file '");
        paramContext.append(((ExtractedDex)localObject2).getPath());
        paramContext.append("'");
        throw new IOException(paramContext.toString());
      }
    }
    return (List<ExtractedDex>)localObject1;
  }
  
  private static List<ExtractedDex> performExtractions(File paramFile1, File paramFile2)
    throws IOException
  {
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(paramFile1.getName());
    ((StringBuilder)localObject1).append(".classes");
    String str = ((StringBuilder)localObject1).toString();
    prepareDexDir(paramFile2, str);
    ArrayList localArrayList = new ArrayList();
    ZipFile localZipFile = new ZipFile(paramFile1);
    int j = 2;
    try
    {
      paramFile1 = new java/lang/StringBuilder;
      paramFile1.<init>();
      paramFile1.append("classes");
      paramFile1.append(2);
      paramFile1.append(".dex");
      paramFile1 = localZipFile.getEntry(paramFile1.toString());
      while (paramFile1 != null)
      {
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append(str);
        ((StringBuilder)localObject1).append(j);
        ((StringBuilder)localObject1).append(".zip");
        localObject1 = ((StringBuilder)localObject1).toString();
        ExtractedDex localExtractedDex = new android/support/multidex/MultiDexExtractor$ExtractedDex;
        localExtractedDex.<init>(paramFile2, (String)localObject1);
        localArrayList.add(localExtractedDex);
        localObject1 = new java/lang/StringBuilder;
        ((StringBuilder)localObject1).<init>();
        ((StringBuilder)localObject1).append("Extraction is needed for file ");
        ((StringBuilder)localObject1).append(localExtractedDex);
        Log.i("MultiDex", ((StringBuilder)localObject1).toString());
        int m = 0;
        int k = 0;
        while ((m < 3) && (k == 0))
        {
          int n = m + 1;
          extract(localZipFile, paramFile1, localExtractedDex, str);
          int i;
          try
          {
            localExtractedDex.crc = getZipCrc(localExtractedDex);
            i = 1;
          }
          catch (IOException localIOException)
          {
            localStringBuilder = new java/lang/StringBuilder;
            localStringBuilder.<init>();
            localStringBuilder.append("Failed to read crc from ");
            localStringBuilder.append(localExtractedDex.getAbsolutePath());
            Log.w("MultiDex", localStringBuilder.toString(), localIOException);
            i = 0;
          }
          StringBuilder localStringBuilder = new java/lang/StringBuilder;
          localStringBuilder.<init>();
          localStringBuilder.append("Extraction ");
          Object localObject2;
          if (i != 0) {
            localObject2 = "succeeded";
          } else {
            localObject2 = "failed";
          }
          localStringBuilder.append((String)localObject2);
          localStringBuilder.append(" - length ");
          localStringBuilder.append(localExtractedDex.getAbsolutePath());
          localStringBuilder.append(": ");
          localStringBuilder.append(localExtractedDex.length());
          localStringBuilder.append(" - crc: ");
          localStringBuilder.append(localExtractedDex.crc);
          Log.i("MultiDex", localStringBuilder.toString());
          m = n;
          k = i;
          if (i == 0)
          {
            localExtractedDex.delete();
            m = n;
            k = i;
            if (localExtractedDex.exists())
            {
              localObject2 = new java/lang/StringBuilder;
              ((StringBuilder)localObject2).<init>();
              ((StringBuilder)localObject2).append("Failed to delete corrupted secondary dex '");
              ((StringBuilder)localObject2).append(localExtractedDex.getPath());
              ((StringBuilder)localObject2).append("'");
              Log.w("MultiDex", ((StringBuilder)localObject2).toString());
              m = n;
              k = i;
            }
          }
        }
        if (k != 0)
        {
          j++;
          paramFile1 = new java/lang/StringBuilder;
          paramFile1.<init>();
          paramFile1.append("classes");
          paramFile1.append(j);
          paramFile1.append(".dex");
          paramFile1 = localZipFile.getEntry(paramFile1.toString());
        }
        else
        {
          paramFile2 = new java/io/IOException;
          paramFile1 = new java/lang/StringBuilder;
          paramFile1.<init>();
          paramFile1.append("Could not create zip file ");
          paramFile1.append(localExtractedDex.getAbsolutePath());
          paramFile1.append(" for secondary dex (");
          paramFile1.append(j);
          paramFile1.append(")");
          paramFile2.<init>(paramFile1.toString());
          throw paramFile2;
        }
      }
      return localArrayList;
    }
    finally
    {
      try
      {
        localZipFile.close();
      }
      catch (IOException paramFile2)
      {
        Log.w("MultiDex", "Failed to close resource", paramFile2);
      }
    }
  }
  
  private static void prepareDexDir(File paramFile, String paramString)
  {
    paramString = paramFile.listFiles(new FileFilter()
    {
      public boolean accept(File paramAnonymousFile)
      {
        paramAnonymousFile = paramAnonymousFile.getName();
        boolean bool;
        if ((!paramAnonymousFile.startsWith(this.val$extractedFilePrefix)) && (!paramAnonymousFile.equals("MultiDex.lock"))) {
          bool = true;
        } else {
          bool = false;
        }
        return bool;
      }
    });
    if (paramString == null)
    {
      paramString = new StringBuilder();
      paramString.append("Failed to list secondary dex dir content (");
      paramString.append(paramFile.getPath());
      paramString.append(").");
      Log.w("MultiDex", paramString.toString());
      return;
    }
    int j = paramString.length;
    for (int i = 0; i < j; i++)
    {
      paramFile = paramString[i];
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("Trying to delete old file ");
      localStringBuilder.append(paramFile.getPath());
      localStringBuilder.append(" of size ");
      localStringBuilder.append(paramFile.length());
      Log.i("MultiDex", localStringBuilder.toString());
      if (!paramFile.delete())
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("Failed to delete old file ");
        localStringBuilder.append(paramFile.getPath());
        Log.w("MultiDex", localStringBuilder.toString());
      }
      else
      {
        localStringBuilder = new StringBuilder();
        localStringBuilder.append("Deleted old file ");
        localStringBuilder.append(paramFile.getPath());
        Log.i("MultiDex", localStringBuilder.toString());
      }
    }
  }
  
  private static void putStoredApkInfo(Context paramContext, String paramString, long paramLong1, long paramLong2, List<ExtractedDex> paramList)
  {
    paramContext = getMultiDexPreferences(paramContext).edit();
    Object localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("timestamp");
    paramContext.putLong(((StringBuilder)localObject).toString(), paramLong1);
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("crc");
    paramContext.putLong(((StringBuilder)localObject).toString(), paramLong2);
    localObject = new StringBuilder();
    ((StringBuilder)localObject).append(paramString);
    ((StringBuilder)localObject).append("dex.number");
    paramContext.putInt(((StringBuilder)localObject).toString(), paramList.size() + 1);
    paramList = paramList.iterator();
    for (int i = 2; paramList.hasNext(); i++)
    {
      localObject = (ExtractedDex)paramList.next();
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append("dex.crc.");
      localStringBuilder.append(i);
      paramContext.putLong(localStringBuilder.toString(), ((ExtractedDex)localObject).crc);
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(paramString);
      localStringBuilder.append("dex.time.");
      localStringBuilder.append(i);
      paramContext.putLong(localStringBuilder.toString(), ((ExtractedDex)localObject).lastModified());
    }
    paramContext.commit();
  }
  
  private static class ExtractedDex
    extends File
  {
    public long crc = -1L;
    
    public ExtractedDex(File paramFile, String paramString)
    {
      super(paramString);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/multidex/MultiDexExtractor.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */