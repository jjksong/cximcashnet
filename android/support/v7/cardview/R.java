package android.support.v7.cardview;

public final class R
{
  public static final class attr
  {
    public static final int cardBackgroundColor = 2130837594;
    public static final int cardCornerRadius = 2130837595;
    public static final int cardElevation = 2130837596;
    public static final int cardMaxElevation = 2130837597;
    public static final int cardPreventCornerOverlap = 2130837598;
    public static final int cardUseCompatPadding = 2130837599;
    public static final int cardViewStyle = 2130837600;
    public static final int contentPadding = 2130837642;
    public static final int contentPaddingBottom = 2130837643;
    public static final int contentPaddingLeft = 2130837644;
    public static final int contentPaddingRight = 2130837645;
    public static final int contentPaddingTop = 2130837646;
  }
  
  public static final class color
  {
    public static final int cardview_dark_background = 2130968631;
    public static final int cardview_light_background = 2130968632;
    public static final int cardview_shadow_end_color = 2130968633;
    public static final int cardview_shadow_start_color = 2130968634;
  }
  
  public static final class dimen
  {
    public static final int cardview_compat_inset_shadow = 2131034207;
    public static final int cardview_default_elevation = 2131034208;
    public static final int cardview_default_radius = 2131034209;
  }
  
  public static final class style
  {
    public static final int Base_CardView = 2131623953;
    public static final int CardView = 2131624106;
    public static final int CardView_Dark = 2131624107;
    public static final int CardView_Light = 2131624108;
  }
  
  public static final class styleable
  {
    public static final int[] CardView = { 16843071, 16843072, 2130837594, 2130837595, 2130837596, 2130837597, 2130837598, 2130837599, 2130837642, 2130837643, 2130837644, 2130837645, 2130837646 };
    public static final int CardView_android_minHeight = 1;
    public static final int CardView_android_minWidth = 0;
    public static final int CardView_cardBackgroundColor = 2;
    public static final int CardView_cardCornerRadius = 3;
    public static final int CardView_cardElevation = 4;
    public static final int CardView_cardMaxElevation = 5;
    public static final int CardView_cardPreventCornerOverlap = 6;
    public static final int CardView_cardUseCompatPadding = 7;
    public static final int CardView_contentPadding = 8;
    public static final int CardView_contentPaddingBottom = 9;
    public static final int CardView_contentPaddingLeft = 10;
    public static final int CardView_contentPaddingRight = 11;
    public static final int CardView_contentPaddingTop = 12;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/cardview/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */