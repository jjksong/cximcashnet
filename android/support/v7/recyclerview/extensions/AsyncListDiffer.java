package android.support.v7.recyclerview.extensions;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.util.AdapterListUpdateCallback;
import android.support.v7.util.DiffUtil;
import android.support.v7.util.DiffUtil.Callback;
import android.support.v7.util.DiffUtil.DiffResult;
import android.support.v7.util.DiffUtil.ItemCallback;
import android.support.v7.util.ListUpdateCallback;
import android.support.v7.widget.RecyclerView.Adapter;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executor;

public class AsyncListDiffer<T>
{
  private final AsyncDifferConfig<T> mConfig;
  @Nullable
  private List<T> mList;
  private int mMaxScheduledGeneration;
  @NonNull
  private List<T> mReadOnlyList = Collections.emptyList();
  private final ListUpdateCallback mUpdateCallback;
  
  public AsyncListDiffer(@NonNull ListUpdateCallback paramListUpdateCallback, @NonNull AsyncDifferConfig<T> paramAsyncDifferConfig)
  {
    this.mUpdateCallback = paramListUpdateCallback;
    this.mConfig = paramAsyncDifferConfig;
  }
  
  public AsyncListDiffer(@NonNull RecyclerView.Adapter paramAdapter, @NonNull DiffUtil.ItemCallback<T> paramItemCallback)
  {
    this.mUpdateCallback = new AdapterListUpdateCallback(paramAdapter);
    this.mConfig = new AsyncDifferConfig.Builder(paramItemCallback).build();
  }
  
  private void latchList(@NonNull List<T> paramList, @NonNull DiffUtil.DiffResult paramDiffResult)
  {
    this.mList = paramList;
    this.mReadOnlyList = Collections.unmodifiableList(paramList);
    paramDiffResult.dispatchUpdatesTo(this.mUpdateCallback);
  }
  
  @NonNull
  public List<T> getCurrentList()
  {
    return this.mReadOnlyList;
  }
  
  public void submitList(final List<T> paramList)
  {
    final List localList = this.mList;
    if (paramList == localList) {
      return;
    }
    final int i = this.mMaxScheduledGeneration + 1;
    this.mMaxScheduledGeneration = i;
    if (paramList == null)
    {
      i = localList.size();
      this.mList = null;
      this.mReadOnlyList = Collections.emptyList();
      this.mUpdateCallback.onRemoved(0, i);
      return;
    }
    if (localList == null)
    {
      this.mList = paramList;
      this.mReadOnlyList = Collections.unmodifiableList(paramList);
      this.mUpdateCallback.onInserted(0, paramList.size());
      return;
    }
    this.mConfig.getBackgroundThreadExecutor().execute(new Runnable()
    {
      public void run()
      {
        final DiffUtil.DiffResult localDiffResult = DiffUtil.calculateDiff(new DiffUtil.Callback()
        {
          public boolean areContentsTheSame(int paramAnonymous2Int1, int paramAnonymous2Int2)
          {
            return AsyncListDiffer.this.mConfig.getDiffCallback().areContentsTheSame(AsyncListDiffer.1.this.val$oldList.get(paramAnonymous2Int1), AsyncListDiffer.1.this.val$newList.get(paramAnonymous2Int2));
          }
          
          public boolean areItemsTheSame(int paramAnonymous2Int1, int paramAnonymous2Int2)
          {
            return AsyncListDiffer.this.mConfig.getDiffCallback().areItemsTheSame(AsyncListDiffer.1.this.val$oldList.get(paramAnonymous2Int1), AsyncListDiffer.1.this.val$newList.get(paramAnonymous2Int2));
          }
          
          @Nullable
          public Object getChangePayload(int paramAnonymous2Int1, int paramAnonymous2Int2)
          {
            return AsyncListDiffer.this.mConfig.getDiffCallback().getChangePayload(AsyncListDiffer.1.this.val$oldList.get(paramAnonymous2Int1), AsyncListDiffer.1.this.val$newList.get(paramAnonymous2Int2));
          }
          
          public int getNewListSize()
          {
            return AsyncListDiffer.1.this.val$newList.size();
          }
          
          public int getOldListSize()
          {
            return AsyncListDiffer.1.this.val$oldList.size();
          }
        });
        AsyncListDiffer.this.mConfig.getMainThreadExecutor().execute(new Runnable()
        {
          public void run()
          {
            if (AsyncListDiffer.this.mMaxScheduledGeneration == AsyncListDiffer.1.this.val$runGeneration) {
              AsyncListDiffer.this.latchList(AsyncListDiffer.1.this.val$newList, localDiffResult);
            }
          }
        });
      }
    });
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/recyclerview/extensions/AsyncListDiffer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */