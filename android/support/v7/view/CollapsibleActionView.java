package android.support.v7.view;

public abstract interface CollapsibleActionView
{
  public abstract void onActionViewCollapsed();
  
  public abstract void onActionViewExpanded();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/view/CollapsibleActionView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */