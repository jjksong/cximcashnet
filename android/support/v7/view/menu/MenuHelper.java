package android.support.v7.view.menu;

abstract interface MenuHelper
{
  public abstract void dismiss();
  
  public abstract void setPresenterCallback(MenuPresenter.Callback paramCallback);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/view/menu/MenuHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */