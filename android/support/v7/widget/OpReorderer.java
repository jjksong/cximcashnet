package android.support.v7.widget;

import java.util.List;

class OpReorderer
{
  final Callback mCallback;
  
  OpReorderer(Callback paramCallback)
  {
    this.mCallback = paramCallback;
  }
  
  private int getLastMoveOutOfOrder(List<AdapterHelper.UpdateOp> paramList)
  {
    int k = paramList.size() - 1;
    int i;
    for (int j = 0; k >= 0; j = i)
    {
      if (((AdapterHelper.UpdateOp)paramList.get(k)).cmd == 8)
      {
        i = j;
        if (j != 0) {
          return k;
        }
      }
      else
      {
        i = 1;
      }
      k--;
    }
    return -1;
  }
  
  private void swapMoveAdd(List<AdapterHelper.UpdateOp> paramList, int paramInt1, AdapterHelper.UpdateOp paramUpdateOp1, int paramInt2, AdapterHelper.UpdateOp paramUpdateOp2)
  {
    int i;
    if (paramUpdateOp1.itemCount < paramUpdateOp2.positionStart) {
      i = -1;
    } else {
      i = 0;
    }
    int j = i;
    if (paramUpdateOp1.positionStart < paramUpdateOp2.positionStart) {
      j = i + 1;
    }
    if (paramUpdateOp2.positionStart <= paramUpdateOp1.positionStart) {
      paramUpdateOp1.positionStart += paramUpdateOp2.itemCount;
    }
    if (paramUpdateOp2.positionStart <= paramUpdateOp1.itemCount) {
      paramUpdateOp1.itemCount += paramUpdateOp2.itemCount;
    }
    paramUpdateOp2.positionStart += j;
    paramList.set(paramInt1, paramUpdateOp2);
    paramList.set(paramInt2, paramUpdateOp1);
  }
  
  private void swapMoveOp(List<AdapterHelper.UpdateOp> paramList, int paramInt1, int paramInt2)
  {
    AdapterHelper.UpdateOp localUpdateOp2 = (AdapterHelper.UpdateOp)paramList.get(paramInt1);
    AdapterHelper.UpdateOp localUpdateOp1 = (AdapterHelper.UpdateOp)paramList.get(paramInt2);
    int i = localUpdateOp1.cmd;
    if (i != 4) {
      switch (i)
      {
      default: 
        break;
      case 2: 
        swapMoveRemove(paramList, paramInt1, localUpdateOp2, paramInt2, localUpdateOp1);
        break;
      case 1: 
        swapMoveAdd(paramList, paramInt1, localUpdateOp2, paramInt2, localUpdateOp1);
        break;
      }
    } else {
      swapMoveUpdate(paramList, paramInt1, localUpdateOp2, paramInt2, localUpdateOp1);
    }
  }
  
  void reorderOps(List<AdapterHelper.UpdateOp> paramList)
  {
    for (;;)
    {
      int i = getLastMoveOutOfOrder(paramList);
      if (i == -1) {
        break;
      }
      swapMoveOp(paramList, i, i + 1);
    }
  }
  
  void swapMoveRemove(List<AdapterHelper.UpdateOp> paramList, int paramInt1, AdapterHelper.UpdateOp paramUpdateOp1, int paramInt2, AdapterHelper.UpdateOp paramUpdateOp2)
  {
    int i = paramUpdateOp1.positionStart;
    int k = paramUpdateOp1.itemCount;
    int j = 0;
    if (i < k)
    {
      if ((paramUpdateOp2.positionStart == paramUpdateOp1.positionStart) && (paramUpdateOp2.itemCount == paramUpdateOp1.itemCount - paramUpdateOp1.positionStart))
      {
        i = 0;
        j = 1;
      }
      else
      {
        i = 0;
      }
    }
    else if ((paramUpdateOp2.positionStart == paramUpdateOp1.itemCount + 1) && (paramUpdateOp2.itemCount == paramUpdateOp1.positionStart - paramUpdateOp1.itemCount))
    {
      i = 1;
      j = 1;
    }
    else
    {
      i = 1;
    }
    if (paramUpdateOp1.itemCount < paramUpdateOp2.positionStart)
    {
      paramUpdateOp2.positionStart -= 1;
    }
    else if (paramUpdateOp1.itemCount < paramUpdateOp2.positionStart + paramUpdateOp2.itemCount)
    {
      paramUpdateOp2.itemCount -= 1;
      paramUpdateOp1.cmd = 2;
      paramUpdateOp1.itemCount = 1;
      if (paramUpdateOp2.itemCount == 0)
      {
        paramList.remove(paramInt2);
        this.mCallback.recycleUpdateOp(paramUpdateOp2);
      }
      return;
    }
    int m = paramUpdateOp1.positionStart;
    k = paramUpdateOp2.positionStart;
    AdapterHelper.UpdateOp localUpdateOp = null;
    if (m <= k)
    {
      paramUpdateOp2.positionStart += 1;
    }
    else if (paramUpdateOp1.positionStart < paramUpdateOp2.positionStart + paramUpdateOp2.itemCount)
    {
      m = paramUpdateOp2.positionStart;
      k = paramUpdateOp2.itemCount;
      int n = paramUpdateOp1.positionStart;
      localUpdateOp = this.mCallback.obtainUpdateOp(2, paramUpdateOp1.positionStart + 1, m + k - n, null);
      paramUpdateOp2.itemCount = (paramUpdateOp1.positionStart - paramUpdateOp2.positionStart);
    }
    if (j != 0)
    {
      paramList.set(paramInt1, paramUpdateOp2);
      paramList.remove(paramInt2);
      this.mCallback.recycleUpdateOp(paramUpdateOp1);
      return;
    }
    if (i != 0)
    {
      if (localUpdateOp != null)
      {
        if (paramUpdateOp1.positionStart > localUpdateOp.positionStart) {
          paramUpdateOp1.positionStart -= localUpdateOp.itemCount;
        }
        if (paramUpdateOp1.itemCount > localUpdateOp.positionStart) {
          paramUpdateOp1.itemCount -= localUpdateOp.itemCount;
        }
      }
      if (paramUpdateOp1.positionStart > paramUpdateOp2.positionStart) {
        paramUpdateOp1.positionStart -= paramUpdateOp2.itemCount;
      }
      if (paramUpdateOp1.itemCount > paramUpdateOp2.positionStart) {
        paramUpdateOp1.itemCount -= paramUpdateOp2.itemCount;
      }
    }
    else
    {
      if (localUpdateOp != null)
      {
        if (paramUpdateOp1.positionStart >= localUpdateOp.positionStart) {
          paramUpdateOp1.positionStart -= localUpdateOp.itemCount;
        }
        if (paramUpdateOp1.itemCount >= localUpdateOp.positionStart) {
          paramUpdateOp1.itemCount -= localUpdateOp.itemCount;
        }
      }
      if (paramUpdateOp1.positionStart >= paramUpdateOp2.positionStart) {
        paramUpdateOp1.positionStart -= paramUpdateOp2.itemCount;
      }
      if (paramUpdateOp1.itemCount >= paramUpdateOp2.positionStart) {
        paramUpdateOp1.itemCount -= paramUpdateOp2.itemCount;
      }
    }
    paramList.set(paramInt1, paramUpdateOp2);
    if (paramUpdateOp1.positionStart != paramUpdateOp1.itemCount) {
      paramList.set(paramInt2, paramUpdateOp1);
    } else {
      paramList.remove(paramInt2);
    }
    if (localUpdateOp != null) {
      paramList.add(paramInt1, localUpdateOp);
    }
  }
  
  void swapMoveUpdate(List<AdapterHelper.UpdateOp> paramList, int paramInt1, AdapterHelper.UpdateOp paramUpdateOp1, int paramInt2, AdapterHelper.UpdateOp paramUpdateOp2)
  {
    int i = paramUpdateOp1.itemCount;
    int j = paramUpdateOp2.positionStart;
    AdapterHelper.UpdateOp localUpdateOp2 = null;
    if (i < j)
    {
      paramUpdateOp2.positionStart -= 1;
    }
    else if (paramUpdateOp1.itemCount < paramUpdateOp2.positionStart + paramUpdateOp2.itemCount)
    {
      paramUpdateOp2.itemCount -= 1;
      localUpdateOp1 = this.mCallback.obtainUpdateOp(4, paramUpdateOp1.positionStart, 1, paramUpdateOp2.payload);
      break label96;
    }
    AdapterHelper.UpdateOp localUpdateOp1 = null;
    label96:
    if (paramUpdateOp1.positionStart <= paramUpdateOp2.positionStart)
    {
      paramUpdateOp2.positionStart += 1;
    }
    else if (paramUpdateOp1.positionStart < paramUpdateOp2.positionStart + paramUpdateOp2.itemCount)
    {
      i = paramUpdateOp2.positionStart + paramUpdateOp2.itemCount - paramUpdateOp1.positionStart;
      localUpdateOp2 = this.mCallback.obtainUpdateOp(4, paramUpdateOp1.positionStart + 1, i, paramUpdateOp2.payload);
      paramUpdateOp2.itemCount -= i;
    }
    paramList.set(paramInt2, paramUpdateOp1);
    if (paramUpdateOp2.itemCount > 0)
    {
      paramList.set(paramInt1, paramUpdateOp2);
    }
    else
    {
      paramList.remove(paramInt1);
      this.mCallback.recycleUpdateOp(paramUpdateOp2);
    }
    if (localUpdateOp1 != null) {
      paramList.add(paramInt1, localUpdateOp1);
    }
    if (localUpdateOp2 != null) {
      paramList.add(paramInt1, localUpdateOp2);
    }
  }
  
  static abstract interface Callback
  {
    public abstract AdapterHelper.UpdateOp obtainUpdateOp(int paramInt1, int paramInt2, int paramInt3, Object paramObject);
    
    public abstract void recycleUpdateOp(AdapterHelper.UpdateOp paramUpdateOp);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/widget/OpReorderer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */