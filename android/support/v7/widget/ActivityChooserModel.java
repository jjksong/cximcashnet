package android.support.v7.widget;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.DataSetObservable;
import android.os.AsyncTask;
import android.text.TextUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class ActivityChooserModel
  extends DataSetObservable
{
  static final String ATTRIBUTE_ACTIVITY = "activity";
  static final String ATTRIBUTE_TIME = "time";
  static final String ATTRIBUTE_WEIGHT = "weight";
  static final boolean DEBUG = false;
  private static final int DEFAULT_ACTIVITY_INFLATION = 5;
  private static final float DEFAULT_HISTORICAL_RECORD_WEIGHT = 1.0F;
  public static final String DEFAULT_HISTORY_FILE_NAME = "activity_choser_model_history.xml";
  public static final int DEFAULT_HISTORY_MAX_LENGTH = 50;
  private static final String HISTORY_FILE_EXTENSION = ".xml";
  private static final int INVALID_INDEX = -1;
  static final String LOG_TAG = "ActivityChooserModel";
  static final String TAG_HISTORICAL_RECORD = "historical-record";
  static final String TAG_HISTORICAL_RECORDS = "historical-records";
  private static final Map<String, ActivityChooserModel> sDataModelRegistry = new HashMap();
  private static final Object sRegistryLock = new Object();
  private final List<ActivityResolveInfo> mActivities = new ArrayList();
  private OnChooseActivityListener mActivityChoserModelPolicy;
  private ActivitySorter mActivitySorter = new DefaultSorter();
  boolean mCanReadHistoricalData = true;
  final Context mContext;
  private final List<HistoricalRecord> mHistoricalRecords = new ArrayList();
  private boolean mHistoricalRecordsChanged = true;
  final String mHistoryFileName;
  private int mHistoryMaxSize = 50;
  private final Object mInstanceLock = new Object();
  private Intent mIntent;
  private boolean mReadShareHistoryCalled = false;
  private boolean mReloadActivities = false;
  
  private ActivityChooserModel(Context paramContext, String paramString)
  {
    this.mContext = paramContext.getApplicationContext();
    if ((!TextUtils.isEmpty(paramString)) && (!paramString.endsWith(".xml")))
    {
      paramContext = new StringBuilder();
      paramContext.append(paramString);
      paramContext.append(".xml");
      this.mHistoryFileName = paramContext.toString();
    }
    else
    {
      this.mHistoryFileName = paramString;
    }
  }
  
  private boolean addHistoricalRecord(HistoricalRecord paramHistoricalRecord)
  {
    boolean bool = this.mHistoricalRecords.add(paramHistoricalRecord);
    if (bool)
    {
      this.mHistoricalRecordsChanged = true;
      pruneExcessiveHistoricalRecordsIfNeeded();
      persistHistoricalDataIfNeeded();
      sortActivitiesIfNeeded();
      notifyChanged();
    }
    return bool;
  }
  
  private void ensureConsistentState()
  {
    boolean bool1 = loadActivitiesIfNeeded();
    boolean bool2 = readHistoricalDataIfNeeded();
    pruneExcessiveHistoricalRecordsIfNeeded();
    if ((bool1 | bool2))
    {
      sortActivitiesIfNeeded();
      notifyChanged();
    }
  }
  
  public static ActivityChooserModel get(Context paramContext, String paramString)
  {
    synchronized (sRegistryLock)
    {
      ActivityChooserModel localActivityChooserModel2 = (ActivityChooserModel)sDataModelRegistry.get(paramString);
      ActivityChooserModel localActivityChooserModel1 = localActivityChooserModel2;
      if (localActivityChooserModel2 == null)
      {
        localActivityChooserModel1 = new android/support/v7/widget/ActivityChooserModel;
        localActivityChooserModel1.<init>(paramContext, paramString);
        sDataModelRegistry.put(paramString, localActivityChooserModel1);
      }
      return localActivityChooserModel1;
    }
  }
  
  private boolean loadActivitiesIfNeeded()
  {
    boolean bool = this.mReloadActivities;
    int i = 0;
    if ((bool) && (this.mIntent != null))
    {
      this.mReloadActivities = false;
      this.mActivities.clear();
      List localList = this.mContext.getPackageManager().queryIntentActivities(this.mIntent, 0);
      int j = localList.size();
      while (i < j)
      {
        ResolveInfo localResolveInfo = (ResolveInfo)localList.get(i);
        this.mActivities.add(new ActivityResolveInfo(localResolveInfo));
        i++;
      }
      return true;
    }
    return false;
  }
  
  private void persistHistoricalDataIfNeeded()
  {
    if (this.mReadShareHistoryCalled)
    {
      if (!this.mHistoricalRecordsChanged) {
        return;
      }
      this.mHistoricalRecordsChanged = false;
      if (!TextUtils.isEmpty(this.mHistoryFileName)) {
        new PersistHistoryAsyncTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Object[] { new ArrayList(this.mHistoricalRecords), this.mHistoryFileName });
      }
      return;
    }
    throw new IllegalStateException("No preceding call to #readHistoricalData");
  }
  
  private void pruneExcessiveHistoricalRecordsIfNeeded()
  {
    int j = this.mHistoricalRecords.size() - this.mHistoryMaxSize;
    if (j <= 0) {
      return;
    }
    this.mHistoricalRecordsChanged = true;
    for (int i = 0; i < j; i++) {
      HistoricalRecord localHistoricalRecord = (HistoricalRecord)this.mHistoricalRecords.remove(0);
    }
  }
  
  private boolean readHistoricalDataIfNeeded()
  {
    if ((this.mCanReadHistoricalData) && (this.mHistoricalRecordsChanged) && (!TextUtils.isEmpty(this.mHistoryFileName)))
    {
      this.mCanReadHistoricalData = false;
      this.mReadShareHistoryCalled = true;
      readHistoricalDataImpl();
      return true;
    }
    return false;
  }
  
  /* Error */
  private void readHistoricalDataImpl()
  {
    // Byte code:
    //   0: aload_0
    //   1: getfield 135	android/support/v7/widget/ActivityChooserModel:mContext	Landroid/content/Context;
    //   4: aload_0
    //   5: getfield 160	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   8: invokevirtual 267	android/content/Context:openFileInput	(Ljava/lang/String;)Ljava/io/FileInputStream;
    //   11: astore 5
    //   13: invokestatic 273	android/util/Xml:newPullParser	()Lorg/xmlpull/v1/XmlPullParser;
    //   16: astore 6
    //   18: aload 6
    //   20: aload 5
    //   22: ldc_w 275
    //   25: invokeinterface 281 3 0
    //   30: iconst_0
    //   31: istore_2
    //   32: iload_2
    //   33: iconst_1
    //   34: if_icmpeq +19 -> 53
    //   37: iload_2
    //   38: iconst_2
    //   39: if_icmpeq +14 -> 53
    //   42: aload 6
    //   44: invokeinterface 284 1 0
    //   49: istore_2
    //   50: goto -18 -> 32
    //   53: ldc 63
    //   55: aload 6
    //   57: invokeinterface 287 1 0
    //   62: invokevirtual 290	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   65: ifeq +153 -> 218
    //   68: aload_0
    //   69: getfield 114	android/support/v7/widget/ActivityChooserModel:mHistoricalRecords	Ljava/util/List;
    //   72: astore 7
    //   74: aload 7
    //   76: invokeinterface 206 1 0
    //   81: aload 6
    //   83: invokeinterface 284 1 0
    //   88: istore_2
    //   89: iload_2
    //   90: iconst_1
    //   91: if_icmpne +16 -> 107
    //   94: aload 5
    //   96: ifnull +257 -> 353
    //   99: aload 5
    //   101: invokevirtual 295	java/io/FileInputStream:close	()V
    //   104: goto +249 -> 353
    //   107: iload_2
    //   108: iconst_3
    //   109: if_icmpeq -28 -> 81
    //   112: iload_2
    //   113: iconst_4
    //   114: if_icmpne +6 -> 120
    //   117: goto -36 -> 81
    //   120: ldc 60
    //   122: aload 6
    //   124: invokeinterface 287 1 0
    //   129: invokevirtual 290	java/lang/String:equals	(Ljava/lang/Object;)Z
    //   132: ifeq +70 -> 202
    //   135: aload 6
    //   137: aconst_null
    //   138: ldc 29
    //   140: invokeinterface 299 3 0
    //   145: astore 8
    //   147: aload 6
    //   149: aconst_null
    //   150: ldc 32
    //   152: invokeinterface 299 3 0
    //   157: invokestatic 305	java/lang/Long:parseLong	(Ljava/lang/String;)J
    //   160: lstore_3
    //   161: aload 6
    //   163: aconst_null
    //   164: ldc 35
    //   166: invokeinterface 299 3 0
    //   171: invokestatic 311	java/lang/Float:parseFloat	(Ljava/lang/String;)F
    //   174: fstore_1
    //   175: new 18	android/support/v7/widget/ActivityChooserModel$HistoricalRecord
    //   178: astore 9
    //   180: aload 9
    //   182: aload 8
    //   184: lload_3
    //   185: fload_1
    //   186: invokespecial 314	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:<init>	(Ljava/lang/String;JF)V
    //   189: aload 7
    //   191: aload 9
    //   193: invokeinterface 168 2 0
    //   198: pop
    //   199: goto -118 -> 81
    //   202: new 261	org/xmlpull/v1/XmlPullParserException
    //   205: astore 6
    //   207: aload 6
    //   209: ldc_w 316
    //   212: invokespecial 317	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
    //   215: aload 6
    //   217: athrow
    //   218: new 261	org/xmlpull/v1/XmlPullParserException
    //   221: astore 6
    //   223: aload 6
    //   225: ldc_w 319
    //   228: invokespecial 317	org/xmlpull/v1/XmlPullParserException:<init>	(Ljava/lang/String;)V
    //   231: aload 6
    //   233: athrow
    //   234: astore 6
    //   236: goto +118 -> 354
    //   239: astore 7
    //   241: getstatic 321	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
    //   244: astore 8
    //   246: new 149	java/lang/StringBuilder
    //   249: astore 6
    //   251: aload 6
    //   253: invokespecial 150	java/lang/StringBuilder:<init>	()V
    //   256: aload 6
    //   258: ldc_w 323
    //   261: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   264: pop
    //   265: aload 6
    //   267: aload_0
    //   268: getfield 160	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   271: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   274: pop
    //   275: aload 8
    //   277: aload 6
    //   279: invokevirtual 158	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   282: aload 7
    //   284: invokestatic 329	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   287: pop
    //   288: aload 5
    //   290: ifnull +63 -> 353
    //   293: goto -194 -> 99
    //   296: astore 6
    //   298: getstatic 321	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
    //   301: astore 7
    //   303: new 149	java/lang/StringBuilder
    //   306: astore 8
    //   308: aload 8
    //   310: invokespecial 150	java/lang/StringBuilder:<init>	()V
    //   313: aload 8
    //   315: ldc_w 323
    //   318: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   321: pop
    //   322: aload 8
    //   324: aload_0
    //   325: getfield 160	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
    //   328: invokevirtual 154	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   331: pop
    //   332: aload 7
    //   334: aload 8
    //   336: invokevirtual 158	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   339: aload 6
    //   341: invokestatic 329	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   344: pop
    //   345: aload 5
    //   347: ifnull +6 -> 353
    //   350: goto -251 -> 99
    //   353: return
    //   354: aload 5
    //   356: ifnull +8 -> 364
    //   359: aload 5
    //   361: invokevirtual 295	java/io/FileInputStream:close	()V
    //   364: aload 6
    //   366: athrow
    //   367: astore 5
    //   369: return
    //   370: astore 5
    //   372: goto -19 -> 353
    //   375: astore 5
    //   377: goto -13 -> 364
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	380	0	this	ActivityChooserModel
    //   174	12	1	f	float
    //   31	84	2	i	int
    //   160	25	3	l	long
    //   11	349	5	localFileInputStream	java.io.FileInputStream
    //   367	1	5	localFileNotFoundException	java.io.FileNotFoundException
    //   370	1	5	localIOException1	java.io.IOException
    //   375	1	5	localIOException2	java.io.IOException
    //   16	216	6	localObject1	Object
    //   234	1	6	localObject2	Object
    //   249	29	6	localStringBuilder	StringBuilder
    //   296	69	6	localXmlPullParserException	org.xmlpull.v1.XmlPullParserException
    //   72	118	7	localList	List
    //   239	44	7	localIOException3	java.io.IOException
    //   301	32	7	str	String
    //   145	190	8	localObject3	Object
    //   178	14	9	localHistoricalRecord	HistoricalRecord
    // Exception table:
    //   from	to	target	type
    //   13	30	234	finally
    //   42	50	234	finally
    //   53	81	234	finally
    //   81	89	234	finally
    //   120	199	234	finally
    //   202	218	234	finally
    //   218	234	234	finally
    //   241	288	234	finally
    //   298	345	234	finally
    //   13	30	239	java/io/IOException
    //   42	50	239	java/io/IOException
    //   53	81	239	java/io/IOException
    //   81	89	239	java/io/IOException
    //   120	199	239	java/io/IOException
    //   202	218	239	java/io/IOException
    //   218	234	239	java/io/IOException
    //   13	30	296	org/xmlpull/v1/XmlPullParserException
    //   42	50	296	org/xmlpull/v1/XmlPullParserException
    //   53	81	296	org/xmlpull/v1/XmlPullParserException
    //   81	89	296	org/xmlpull/v1/XmlPullParserException
    //   120	199	296	org/xmlpull/v1/XmlPullParserException
    //   202	218	296	org/xmlpull/v1/XmlPullParserException
    //   218	234	296	org/xmlpull/v1/XmlPullParserException
    //   0	13	367	java/io/FileNotFoundException
    //   99	104	370	java/io/IOException
    //   359	364	375	java/io/IOException
  }
  
  private boolean sortActivitiesIfNeeded()
  {
    if ((this.mActivitySorter != null) && (this.mIntent != null) && (!this.mActivities.isEmpty()) && (!this.mHistoricalRecords.isEmpty()))
    {
      this.mActivitySorter.sort(this.mIntent, this.mActivities, Collections.unmodifiableList(this.mHistoricalRecords));
      return true;
    }
    return false;
  }
  
  public Intent chooseActivity(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mIntent == null) {
        return null;
      }
      ensureConsistentState();
      Object localObject3 = (ActivityResolveInfo)this.mActivities.get(paramInt);
      ComponentName localComponentName = new android/content/ComponentName;
      localComponentName.<init>(((ActivityResolveInfo)localObject3).resolveInfo.activityInfo.packageName, ((ActivityResolveInfo)localObject3).resolveInfo.activityInfo.name);
      localObject3 = new android/content/Intent;
      ((Intent)localObject3).<init>(this.mIntent);
      ((Intent)localObject3).setComponent(localComponentName);
      if (this.mActivityChoserModelPolicy != null)
      {
        localObject4 = new android/content/Intent;
        ((Intent)localObject4).<init>((Intent)localObject3);
        if (this.mActivityChoserModelPolicy.onChooseActivity(this, (Intent)localObject4)) {
          return null;
        }
      }
      Object localObject4 = new android/support/v7/widget/ActivityChooserModel$HistoricalRecord;
      ((HistoricalRecord)localObject4).<init>(localComponentName, System.currentTimeMillis(), 1.0F);
      addHistoricalRecord((HistoricalRecord)localObject4);
      return (Intent)localObject3;
    }
  }
  
  public ResolveInfo getActivity(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      ResolveInfo localResolveInfo = ((ActivityResolveInfo)this.mActivities.get(paramInt)).resolveInfo;
      return localResolveInfo;
    }
  }
  
  public int getActivityCount()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      int i = this.mActivities.size();
      return i;
    }
  }
  
  public int getActivityIndex(ResolveInfo paramResolveInfo)
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      List localList = this.mActivities;
      int j = localList.size();
      for (int i = 0; i < j; i++) {
        if (((ActivityResolveInfo)localList.get(i)).resolveInfo == paramResolveInfo) {
          return i;
        }
      }
      return -1;
    }
  }
  
  public ResolveInfo getDefaultActivity()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      if (!this.mActivities.isEmpty())
      {
        ResolveInfo localResolveInfo = ((ActivityResolveInfo)this.mActivities.get(0)).resolveInfo;
        return localResolveInfo;
      }
      return null;
    }
  }
  
  public int getHistoryMaxSize()
  {
    synchronized (this.mInstanceLock)
    {
      int i = this.mHistoryMaxSize;
      return i;
    }
  }
  
  public int getHistorySize()
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      int i = this.mHistoricalRecords.size();
      return i;
    }
  }
  
  public Intent getIntent()
  {
    synchronized (this.mInstanceLock)
    {
      Intent localIntent = this.mIntent;
      return localIntent;
    }
  }
  
  public void setActivitySorter(ActivitySorter paramActivitySorter)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mActivitySorter == paramActivitySorter) {
        return;
      }
      this.mActivitySorter = paramActivitySorter;
      if (sortActivitiesIfNeeded()) {
        notifyChanged();
      }
      return;
    }
  }
  
  public void setDefaultActivity(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      ensureConsistentState();
      Object localObject2 = (ActivityResolveInfo)this.mActivities.get(paramInt);
      Object localObject4 = (ActivityResolveInfo)this.mActivities.get(0);
      float f;
      if (localObject4 != null) {
        f = ((ActivityResolveInfo)localObject4).weight - ((ActivityResolveInfo)localObject2).weight + 5.0F;
      } else {
        f = 1.0F;
      }
      localObject4 = new android/content/ComponentName;
      ((ComponentName)localObject4).<init>(((ActivityResolveInfo)localObject2).resolveInfo.activityInfo.packageName, ((ActivityResolveInfo)localObject2).resolveInfo.activityInfo.name);
      localObject2 = new android/support/v7/widget/ActivityChooserModel$HistoricalRecord;
      ((HistoricalRecord)localObject2).<init>((ComponentName)localObject4, System.currentTimeMillis(), f);
      addHistoricalRecord((HistoricalRecord)localObject2);
      return;
    }
  }
  
  public void setHistoryMaxSize(int paramInt)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mHistoryMaxSize == paramInt) {
        return;
      }
      this.mHistoryMaxSize = paramInt;
      pruneExcessiveHistoricalRecordsIfNeeded();
      if (sortActivitiesIfNeeded()) {
        notifyChanged();
      }
      return;
    }
  }
  
  public void setIntent(Intent paramIntent)
  {
    synchronized (this.mInstanceLock)
    {
      if (this.mIntent == paramIntent) {
        return;
      }
      this.mIntent = paramIntent;
      this.mReloadActivities = true;
      ensureConsistentState();
      return;
    }
  }
  
  public void setOnChooseActivityListener(OnChooseActivityListener paramOnChooseActivityListener)
  {
    synchronized (this.mInstanceLock)
    {
      this.mActivityChoserModelPolicy = paramOnChooseActivityListener;
      return;
    }
  }
  
  public static abstract interface ActivityChooserModelClient
  {
    public abstract void setActivityChooserModel(ActivityChooserModel paramActivityChooserModel);
  }
  
  public static final class ActivityResolveInfo
    implements Comparable<ActivityResolveInfo>
  {
    public final ResolveInfo resolveInfo;
    public float weight;
    
    public ActivityResolveInfo(ResolveInfo paramResolveInfo)
    {
      this.resolveInfo = paramResolveInfo;
    }
    
    public int compareTo(ActivityResolveInfo paramActivityResolveInfo)
    {
      return Float.floatToIntBits(paramActivityResolveInfo.weight) - Float.floatToIntBits(this.weight);
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {
        return true;
      }
      if (paramObject == null) {
        return false;
      }
      if (getClass() != paramObject.getClass()) {
        return false;
      }
      paramObject = (ActivityResolveInfo)paramObject;
      return Float.floatToIntBits(this.weight) == Float.floatToIntBits(((ActivityResolveInfo)paramObject).weight);
    }
    
    public int hashCode()
    {
      return Float.floatToIntBits(this.weight) + 31;
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append("resolveInfo:");
      localStringBuilder.append(this.resolveInfo.toString());
      localStringBuilder.append("; weight:");
      localStringBuilder.append(new BigDecimal(this.weight));
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface ActivitySorter
  {
    public abstract void sort(Intent paramIntent, List<ActivityChooserModel.ActivityResolveInfo> paramList, List<ActivityChooserModel.HistoricalRecord> paramList1);
  }
  
  private static final class DefaultSorter
    implements ActivityChooserModel.ActivitySorter
  {
    private static final float WEIGHT_DECAY_COEFFICIENT = 0.95F;
    private final Map<ComponentName, ActivityChooserModel.ActivityResolveInfo> mPackageNameToActivityMap = new HashMap();
    
    public void sort(Intent paramIntent, List<ActivityChooserModel.ActivityResolveInfo> paramList, List<ActivityChooserModel.HistoricalRecord> paramList1)
    {
      paramIntent = this.mPackageNameToActivityMap;
      paramIntent.clear();
      int j = paramList.size();
      Object localObject;
      for (int i = 0; i < j; i++)
      {
        localObject = (ActivityChooserModel.ActivityResolveInfo)paramList.get(i);
        ((ActivityChooserModel.ActivityResolveInfo)localObject).weight = 0.0F;
        paramIntent.put(new ComponentName(((ActivityChooserModel.ActivityResolveInfo)localObject).resolveInfo.activityInfo.packageName, ((ActivityChooserModel.ActivityResolveInfo)localObject).resolveInfo.activityInfo.name), localObject);
      }
      i = paramList1.size() - 1;
      float f1;
      for (float f2 = 1.0F; i >= 0; f2 = f1)
      {
        localObject = (ActivityChooserModel.HistoricalRecord)paramList1.get(i);
        ActivityChooserModel.ActivityResolveInfo localActivityResolveInfo = (ActivityChooserModel.ActivityResolveInfo)paramIntent.get(((ActivityChooserModel.HistoricalRecord)localObject).activity);
        f1 = f2;
        if (localActivityResolveInfo != null)
        {
          localActivityResolveInfo.weight += ((ActivityChooserModel.HistoricalRecord)localObject).weight * f2;
          f1 = f2 * 0.95F;
        }
        i--;
      }
      Collections.sort(paramList);
    }
  }
  
  public static final class HistoricalRecord
  {
    public final ComponentName activity;
    public final long time;
    public final float weight;
    
    public HistoricalRecord(ComponentName paramComponentName, long paramLong, float paramFloat)
    {
      this.activity = paramComponentName;
      this.time = paramLong;
      this.weight = paramFloat;
    }
    
    public HistoricalRecord(String paramString, long paramLong, float paramFloat)
    {
      this(ComponentName.unflattenFromString(paramString), paramLong, paramFloat);
    }
    
    public boolean equals(Object paramObject)
    {
      if (this == paramObject) {
        return true;
      }
      if (paramObject == null) {
        return false;
      }
      if (getClass() != paramObject.getClass()) {
        return false;
      }
      paramObject = (HistoricalRecord)paramObject;
      ComponentName localComponentName = this.activity;
      if (localComponentName == null)
      {
        if (((HistoricalRecord)paramObject).activity != null) {
          return false;
        }
      }
      else if (!localComponentName.equals(((HistoricalRecord)paramObject).activity)) {
        return false;
      }
      if (this.time != ((HistoricalRecord)paramObject).time) {
        return false;
      }
      return Float.floatToIntBits(this.weight) == Float.floatToIntBits(((HistoricalRecord)paramObject).weight);
    }
    
    public int hashCode()
    {
      ComponentName localComponentName = this.activity;
      int i;
      if (localComponentName == null) {
        i = 0;
      } else {
        i = localComponentName.hashCode();
      }
      long l = this.time;
      return ((i + 31) * 31 + (int)(l ^ l >>> 32)) * 31 + Float.floatToIntBits(this.weight);
    }
    
    public String toString()
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append("[");
      localStringBuilder.append("; activity:");
      localStringBuilder.append(this.activity);
      localStringBuilder.append("; time:");
      localStringBuilder.append(this.time);
      localStringBuilder.append("; weight:");
      localStringBuilder.append(new BigDecimal(this.weight));
      localStringBuilder.append("]");
      return localStringBuilder.toString();
    }
  }
  
  public static abstract interface OnChooseActivityListener
  {
    public abstract boolean onChooseActivity(ActivityChooserModel paramActivityChooserModel, Intent paramIntent);
  }
  
  private final class PersistHistoryAsyncTask
    extends AsyncTask<Object, Void, Void>
  {
    PersistHistoryAsyncTask() {}
    
    /* Error */
    public Void doInBackground(Object... paramVarArgs)
    {
      // Byte code:
      //   0: aload_1
      //   1: iconst_0
      //   2: aaload
      //   3: checkcast 33	java/util/List
      //   6: astore 5
      //   8: aload_1
      //   9: iconst_1
      //   10: aaload
      //   11: checkcast 35	java/lang/String
      //   14: astore 4
      //   16: aload_0
      //   17: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   20: getfield 39	android/support/v7/widget/ActivityChooserModel:mContext	Landroid/content/Context;
      //   23: aload 4
      //   25: iconst_0
      //   26: invokevirtual 45	android/content/Context:openFileOutput	(Ljava/lang/String;I)Ljava/io/FileOutputStream;
      //   29: astore_1
      //   30: invokestatic 51	android/util/Xml:newSerializer	()Lorg/xmlpull/v1/XmlSerializer;
      //   33: astore 4
      //   35: aload 4
      //   37: aload_1
      //   38: aconst_null
      //   39: invokeinterface 57 3 0
      //   44: aload 4
      //   46: ldc 59
      //   48: iconst_1
      //   49: invokestatic 65	java/lang/Boolean:valueOf	(Z)Ljava/lang/Boolean;
      //   52: invokeinterface 69 3 0
      //   57: aload 4
      //   59: aconst_null
      //   60: ldc 71
      //   62: invokeinterface 75 3 0
      //   67: pop
      //   68: aload 5
      //   70: invokeinterface 79 1 0
      //   75: istore_3
      //   76: iconst_0
      //   77: istore_2
      //   78: iload_2
      //   79: iload_3
      //   80: if_icmpge +101 -> 181
      //   83: aload 5
      //   85: iconst_0
      //   86: invokeinterface 83 2 0
      //   91: checkcast 85	android/support/v7/widget/ActivityChooserModel$HistoricalRecord
      //   94: astore 6
      //   96: aload 4
      //   98: aconst_null
      //   99: ldc 87
      //   101: invokeinterface 75 3 0
      //   106: pop
      //   107: aload 4
      //   109: aconst_null
      //   110: ldc 89
      //   112: aload 6
      //   114: getfield 92	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:activity	Landroid/content/ComponentName;
      //   117: invokevirtual 98	android/content/ComponentName:flattenToString	()Ljava/lang/String;
      //   120: invokeinterface 102 4 0
      //   125: pop
      //   126: aload 4
      //   128: aconst_null
      //   129: ldc 104
      //   131: aload 6
      //   133: getfield 107	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:time	J
      //   136: invokestatic 110	java/lang/String:valueOf	(J)Ljava/lang/String;
      //   139: invokeinterface 102 4 0
      //   144: pop
      //   145: aload 4
      //   147: aconst_null
      //   148: ldc 112
      //   150: aload 6
      //   152: getfield 115	android/support/v7/widget/ActivityChooserModel$HistoricalRecord:weight	F
      //   155: invokestatic 118	java/lang/String:valueOf	(F)Ljava/lang/String;
      //   158: invokeinterface 102 4 0
      //   163: pop
      //   164: aload 4
      //   166: aconst_null
      //   167: ldc 87
      //   169: invokeinterface 121 3 0
      //   174: pop
      //   175: iinc 2 1
      //   178: goto -100 -> 78
      //   181: aload 4
      //   183: aconst_null
      //   184: ldc 71
      //   186: invokeinterface 121 3 0
      //   191: pop
      //   192: aload 4
      //   194: invokeinterface 124 1 0
      //   199: aload_0
      //   200: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   203: iconst_1
      //   204: putfield 128	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   207: aload_1
      //   208: ifnull +213 -> 421
      //   211: aload_1
      //   212: invokevirtual 133	java/io/FileOutputStream:close	()V
      //   215: goto +206 -> 421
      //   218: astore 4
      //   220: goto +203 -> 423
      //   223: astore 4
      //   225: getstatic 137	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
      //   228: astore 5
      //   230: new 139	java/lang/StringBuilder
      //   233: astore 6
      //   235: aload 6
      //   237: invokespecial 140	java/lang/StringBuilder:<init>	()V
      //   240: aload 6
      //   242: ldc -114
      //   244: invokevirtual 146	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   247: pop
      //   248: aload 6
      //   250: aload_0
      //   251: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   254: getfield 149	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
      //   257: invokevirtual 146	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   260: pop
      //   261: aload 5
      //   263: aload 6
      //   265: invokevirtual 152	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   268: aload 4
      //   270: invokestatic 158	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   273: pop
      //   274: aload_0
      //   275: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   278: iconst_1
      //   279: putfield 128	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   282: aload_1
      //   283: ifnull +138 -> 421
      //   286: goto -75 -> 211
      //   289: astore 4
      //   291: getstatic 137	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
      //   294: astore 6
      //   296: new 139	java/lang/StringBuilder
      //   299: astore 5
      //   301: aload 5
      //   303: invokespecial 140	java/lang/StringBuilder:<init>	()V
      //   306: aload 5
      //   308: ldc -114
      //   310: invokevirtual 146	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   313: pop
      //   314: aload 5
      //   316: aload_0
      //   317: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   320: getfield 149	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
      //   323: invokevirtual 146	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   326: pop
      //   327: aload 6
      //   329: aload 5
      //   331: invokevirtual 152	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   334: aload 4
      //   336: invokestatic 158	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   339: pop
      //   340: aload_0
      //   341: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   344: iconst_1
      //   345: putfield 128	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   348: aload_1
      //   349: ifnull +72 -> 421
      //   352: goto -141 -> 211
      //   355: astore 5
      //   357: getstatic 137	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
      //   360: astore 4
      //   362: new 139	java/lang/StringBuilder
      //   365: astore 6
      //   367: aload 6
      //   369: invokespecial 140	java/lang/StringBuilder:<init>	()V
      //   372: aload 6
      //   374: ldc -114
      //   376: invokevirtual 146	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   379: pop
      //   380: aload 6
      //   382: aload_0
      //   383: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   386: getfield 149	android/support/v7/widget/ActivityChooserModel:mHistoryFileName	Ljava/lang/String;
      //   389: invokevirtual 146	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   392: pop
      //   393: aload 4
      //   395: aload 6
      //   397: invokevirtual 152	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   400: aload 5
      //   402: invokestatic 158	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   405: pop
      //   406: aload_0
      //   407: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   410: iconst_1
      //   411: putfield 128	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   414: aload_1
      //   415: ifnull +6 -> 421
      //   418: goto -207 -> 211
      //   421: aconst_null
      //   422: areturn
      //   423: aload_0
      //   424: getfield 14	android/support/v7/widget/ActivityChooserModel$PersistHistoryAsyncTask:this$0	Landroid/support/v7/widget/ActivityChooserModel;
      //   427: iconst_1
      //   428: putfield 128	android/support/v7/widget/ActivityChooserModel:mCanReadHistoricalData	Z
      //   431: aload_1
      //   432: ifnull +7 -> 439
      //   435: aload_1
      //   436: invokevirtual 133	java/io/FileOutputStream:close	()V
      //   439: aload 4
      //   441: athrow
      //   442: astore 6
      //   444: getstatic 137	android/support/v7/widget/ActivityChooserModel:LOG_TAG	Ljava/lang/String;
      //   447: astore_1
      //   448: new 139	java/lang/StringBuilder
      //   451: dup
      //   452: invokespecial 140	java/lang/StringBuilder:<init>	()V
      //   455: astore 5
      //   457: aload 5
      //   459: ldc -114
      //   461: invokevirtual 146	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   464: pop
      //   465: aload 5
      //   467: aload 4
      //   469: invokevirtual 146	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
      //   472: pop
      //   473: aload_1
      //   474: aload 5
      //   476: invokevirtual 152	java/lang/StringBuilder:toString	()Ljava/lang/String;
      //   479: aload 6
      //   481: invokestatic 158	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
      //   484: pop
      //   485: aconst_null
      //   486: areturn
      //   487: astore_1
      //   488: goto -67 -> 421
      //   491: astore_1
      //   492: goto -53 -> 439
      // Local variable table:
      //   start	length	slot	name	signature
      //   0	495	0	this	PersistHistoryAsyncTask
      //   0	495	1	paramVarArgs	Object[]
      //   77	99	2	i	int
      //   75	6	3	j	int
      //   14	179	4	localObject1	Object
      //   218	1	4	localObject2	Object
      //   223	46	4	localIOException	java.io.IOException
      //   289	46	4	localIllegalStateException	IllegalStateException
      //   360	108	4	str	String
      //   6	324	5	localObject3	Object
      //   355	46	5	localIllegalArgumentException	IllegalArgumentException
      //   455	20	5	localStringBuilder	StringBuilder
      //   94	302	6	localObject4	Object
      //   442	38	6	localFileNotFoundException	java.io.FileNotFoundException
      // Exception table:
      //   from	to	target	type
      //   35	76	218	finally
      //   83	175	218	finally
      //   181	199	218	finally
      //   225	274	218	finally
      //   291	340	218	finally
      //   357	406	218	finally
      //   35	76	223	java/io/IOException
      //   83	175	223	java/io/IOException
      //   181	199	223	java/io/IOException
      //   35	76	289	java/lang/IllegalStateException
      //   83	175	289	java/lang/IllegalStateException
      //   181	199	289	java/lang/IllegalStateException
      //   35	76	355	java/lang/IllegalArgumentException
      //   83	175	355	java/lang/IllegalArgumentException
      //   181	199	355	java/lang/IllegalArgumentException
      //   16	30	442	java/io/FileNotFoundException
      //   211	215	487	java/io/IOException
      //   435	439	491	java/io/IOException
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/widget/ActivityChooserModel.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */