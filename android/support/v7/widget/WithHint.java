package android.support.v7.widget;

import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public abstract interface WithHint
{
  @Nullable
  public abstract CharSequence getHint();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/widget/WithHint.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */