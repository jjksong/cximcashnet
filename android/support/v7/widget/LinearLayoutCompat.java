package android.support.v7.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.annotation.RestrictTo;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.appcompat.R.styleable;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class LinearLayoutCompat
  extends ViewGroup
{
  public static final int HORIZONTAL = 0;
  private static final int INDEX_BOTTOM = 2;
  private static final int INDEX_CENTER_VERTICAL = 0;
  private static final int INDEX_FILL = 3;
  private static final int INDEX_TOP = 1;
  public static final int SHOW_DIVIDER_BEGINNING = 1;
  public static final int SHOW_DIVIDER_END = 4;
  public static final int SHOW_DIVIDER_MIDDLE = 2;
  public static final int SHOW_DIVIDER_NONE = 0;
  public static final int VERTICAL = 1;
  private static final int VERTICAL_GRAVITY_COUNT = 4;
  private boolean mBaselineAligned = true;
  private int mBaselineAlignedChildIndex = -1;
  private int mBaselineChildTop = 0;
  private Drawable mDivider;
  private int mDividerHeight;
  private int mDividerPadding;
  private int mDividerWidth;
  private int mGravity = 8388659;
  private int[] mMaxAscent;
  private int[] mMaxDescent;
  private int mOrientation;
  private int mShowDividers;
  private int mTotalLength;
  private boolean mUseLargestChild;
  private float mWeightSum;
  
  public LinearLayoutCompat(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public LinearLayoutCompat(Context paramContext, AttributeSet paramAttributeSet)
  {
    this(paramContext, paramAttributeSet, 0);
  }
  
  public LinearLayoutCompat(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    paramContext = TintTypedArray.obtainStyledAttributes(paramContext, paramAttributeSet, R.styleable.LinearLayoutCompat, paramInt, 0);
    paramInt = paramContext.getInt(R.styleable.LinearLayoutCompat_android_orientation, -1);
    if (paramInt >= 0) {
      setOrientation(paramInt);
    }
    paramInt = paramContext.getInt(R.styleable.LinearLayoutCompat_android_gravity, -1);
    if (paramInt >= 0) {
      setGravity(paramInt);
    }
    boolean bool = paramContext.getBoolean(R.styleable.LinearLayoutCompat_android_baselineAligned, true);
    if (!bool) {
      setBaselineAligned(bool);
    }
    this.mWeightSum = paramContext.getFloat(R.styleable.LinearLayoutCompat_android_weightSum, -1.0F);
    this.mBaselineAlignedChildIndex = paramContext.getInt(R.styleable.LinearLayoutCompat_android_baselineAlignedChildIndex, -1);
    this.mUseLargestChild = paramContext.getBoolean(R.styleable.LinearLayoutCompat_measureWithLargestChild, false);
    setDividerDrawable(paramContext.getDrawable(R.styleable.LinearLayoutCompat_divider));
    this.mShowDividers = paramContext.getInt(R.styleable.LinearLayoutCompat_showDividers, 0);
    this.mDividerPadding = paramContext.getDimensionPixelSize(R.styleable.LinearLayoutCompat_dividerPadding, 0);
    paramContext.recycle();
  }
  
  private void forceUniformHeight(int paramInt1, int paramInt2)
  {
    int j = View.MeasureSpec.makeMeasureSpec(getMeasuredHeight(), 1073741824);
    for (int i = 0; i < paramInt1; i++)
    {
      View localView = getVirtualChildAt(i);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.height == -1)
        {
          int k = localLayoutParams.width;
          localLayoutParams.width = localView.getMeasuredWidth();
          measureChildWithMargins(localView, paramInt2, 0, j, 0);
          localLayoutParams.width = k;
        }
      }
    }
  }
  
  private void forceUniformWidth(int paramInt1, int paramInt2)
  {
    int j = View.MeasureSpec.makeMeasureSpec(getMeasuredWidth(), 1073741824);
    for (int i = 0; i < paramInt1; i++)
    {
      View localView = getVirtualChildAt(i);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if (localLayoutParams.width == -1)
        {
          int k = localLayoutParams.height;
          localLayoutParams.height = localView.getMeasuredHeight();
          measureChildWithMargins(localView, j, 0, paramInt2, 0);
          localLayoutParams.height = k;
        }
      }
    }
  }
  
  private void setChildFrame(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramView.layout(paramInt1, paramInt2, paramInt3 + paramInt1, paramInt4 + paramInt2);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  void drawDividersHorizontal(Canvas paramCanvas)
  {
    int k = getVirtualChildCount();
    boolean bool = ViewUtils.isLayoutRtl(this);
    Object localObject1;
    Object localObject2;
    for (int i = 0; i < k; i++)
    {
      localObject1 = getVirtualChildAt(i);
      if ((localObject1 != null) && (((View)localObject1).getVisibility() != 8) && (hasDividerBeforeChildAt(i)))
      {
        localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
        int j;
        if (bool) {
          j = ((View)localObject1).getRight() + ((LayoutParams)localObject2).rightMargin;
        } else {
          j = ((View)localObject1).getLeft() - ((LayoutParams)localObject2).leftMargin - this.mDividerWidth;
        }
        drawVerticalDivider(paramCanvas, j);
      }
    }
    if (hasDividerBeforeChildAt(k))
    {
      localObject2 = getVirtualChildAt(k - 1);
      if (localObject2 == null)
      {
        if (bool) {
          i = getPaddingLeft();
        } else {
          i = getWidth() - getPaddingRight() - this.mDividerWidth;
        }
      }
      else
      {
        localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
        if (bool) {
          i = ((View)localObject2).getLeft() - ((LayoutParams)localObject1).leftMargin - this.mDividerWidth;
        } else {
          i = ((View)localObject2).getRight() + ((LayoutParams)localObject1).rightMargin;
        }
      }
      drawVerticalDivider(paramCanvas, i);
    }
  }
  
  void drawDividersVertical(Canvas paramCanvas)
  {
    int j = getVirtualChildCount();
    Object localObject1;
    Object localObject2;
    for (int i = 0; i < j; i++)
    {
      localObject1 = getVirtualChildAt(i);
      if ((localObject1 != null) && (((View)localObject1).getVisibility() != 8) && (hasDividerBeforeChildAt(i)))
      {
        localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
        drawHorizontalDivider(paramCanvas, ((View)localObject1).getTop() - ((LayoutParams)localObject2).topMargin - this.mDividerHeight);
      }
    }
    if (hasDividerBeforeChildAt(j))
    {
      localObject2 = getVirtualChildAt(j - 1);
      if (localObject2 == null)
      {
        i = getHeight() - getPaddingBottom() - this.mDividerHeight;
      }
      else
      {
        localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
        i = ((View)localObject2).getBottom() + ((LayoutParams)localObject1).bottomMargin;
      }
      drawHorizontalDivider(paramCanvas, i);
    }
  }
  
  void drawHorizontalDivider(Canvas paramCanvas, int paramInt)
  {
    this.mDivider.setBounds(getPaddingLeft() + this.mDividerPadding, paramInt, getWidth() - getPaddingRight() - this.mDividerPadding, this.mDividerHeight + paramInt);
    this.mDivider.draw(paramCanvas);
  }
  
  void drawVerticalDivider(Canvas paramCanvas, int paramInt)
  {
    this.mDivider.setBounds(paramInt, getPaddingTop() + this.mDividerPadding, this.mDividerWidth + paramInt, getHeight() - getPaddingBottom() - this.mDividerPadding);
    this.mDivider.draw(paramCanvas);
  }
  
  protected LayoutParams generateDefaultLayoutParams()
  {
    int i = this.mOrientation;
    if (i == 0) {
      return new LayoutParams(-2, -2);
    }
    if (i == 1) {
      return new LayoutParams(-1, -2);
    }
    return null;
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new LayoutParams(paramLayoutParams);
  }
  
  public int getBaseline()
  {
    if (this.mBaselineAlignedChildIndex < 0) {
      return super.getBaseline();
    }
    int i = getChildCount();
    int j = this.mBaselineAlignedChildIndex;
    if (i > j)
    {
      View localView = getChildAt(j);
      int k = localView.getBaseline();
      if (k == -1)
      {
        if (this.mBaselineAlignedChildIndex == 0) {
          return -1;
        }
        throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout points to a View that doesn't know how to get its baseline.");
      }
      j = this.mBaselineChildTop;
      i = j;
      if (this.mOrientation == 1)
      {
        int m = this.mGravity & 0x70;
        i = j;
        if (m != 48) {
          if (m != 16)
          {
            if (m != 80) {
              i = j;
            } else {
              i = getBottom() - getTop() - getPaddingBottom() - this.mTotalLength;
            }
          }
          else {
            i = j + (getBottom() - getTop() - getPaddingTop() - getPaddingBottom() - this.mTotalLength) / 2;
          }
        }
      }
      return i + ((LayoutParams)localView.getLayoutParams()).topMargin + k;
    }
    throw new RuntimeException("mBaselineAlignedChildIndex of LinearLayout set to an index that is out of bounds.");
  }
  
  public int getBaselineAlignedChildIndex()
  {
    return this.mBaselineAlignedChildIndex;
  }
  
  int getChildrenSkipCount(View paramView, int paramInt)
  {
    return 0;
  }
  
  public Drawable getDividerDrawable()
  {
    return this.mDivider;
  }
  
  public int getDividerPadding()
  {
    return this.mDividerPadding;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public int getDividerWidth()
  {
    return this.mDividerWidth;
  }
  
  public int getGravity()
  {
    return this.mGravity;
  }
  
  int getLocationOffset(View paramView)
  {
    return 0;
  }
  
  int getNextLocationOffset(View paramView)
  {
    return 0;
  }
  
  public int getOrientation()
  {
    return this.mOrientation;
  }
  
  public int getShowDividers()
  {
    return this.mShowDividers;
  }
  
  View getVirtualChildAt(int paramInt)
  {
    return getChildAt(paramInt);
  }
  
  int getVirtualChildCount()
  {
    return getChildCount();
  }
  
  public float getWeightSum()
  {
    return this.mWeightSum;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY})
  protected boolean hasDividerBeforeChildAt(int paramInt)
  {
    boolean bool3 = false;
    boolean bool2 = false;
    boolean bool1 = false;
    if (paramInt == 0)
    {
      if ((this.mShowDividers & 0x1) != 0) {
        bool1 = true;
      }
      return bool1;
    }
    if (paramInt == getChildCount())
    {
      bool1 = bool3;
      if ((this.mShowDividers & 0x4) != 0) {
        bool1 = true;
      }
      return bool1;
    }
    if ((this.mShowDividers & 0x2) != 0)
    {
      paramInt--;
      for (;;)
      {
        bool1 = bool2;
        if (paramInt < 0) {
          break;
        }
        if (getChildAt(paramInt).getVisibility() != 8)
        {
          bool1 = true;
          break;
        }
        paramInt--;
      }
      return bool1;
    }
    return false;
  }
  
  public boolean isBaselineAligned()
  {
    return this.mBaselineAligned;
  }
  
  public boolean isMeasureWithLargestChildEnabled()
  {
    return this.mUseLargestChild;
  }
  
  void layoutHorizontal(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    boolean bool2 = ViewUtils.isLayoutRtl(this);
    int i1 = getPaddingTop();
    int i3 = paramInt4 - paramInt2;
    int i4 = getPaddingBottom();
    int i5 = getPaddingBottom();
    int m = getVirtualChildCount();
    paramInt4 = this.mGravity;
    paramInt2 = paramInt4 & 0x70;
    boolean bool1 = this.mBaselineAligned;
    int[] arrayOfInt2 = this.mMaxAscent;
    int[] arrayOfInt1 = this.mMaxDescent;
    paramInt4 = GravityCompat.getAbsoluteGravity(0x800007 & paramInt4, ViewCompat.getLayoutDirection(this));
    if (paramInt4 != 1)
    {
      if (paramInt4 != 5) {
        paramInt1 = getPaddingLeft();
      } else {
        paramInt1 = getPaddingLeft() + paramInt3 - paramInt1 - this.mTotalLength;
      }
    }
    else {
      paramInt1 = getPaddingLeft() + (paramInt3 - paramInt1 - this.mTotalLength) / 2;
    }
    int k;
    int j;
    if (bool2)
    {
      k = m - 1;
      j = -1;
    }
    else
    {
      k = 0;
      j = 1;
    }
    paramInt4 = 0;
    paramInt3 = i1;
    int i = paramInt1;
    while (paramInt4 < m)
    {
      int i7 = k + j * paramInt4;
      View localView = getVirtualChildAt(i7);
      if (localView == null)
      {
        i += measureNullChild(i7);
      }
      else if (localView.getVisibility() != 8)
      {
        int i6 = localView.getMeasuredWidth();
        int i8 = localView.getMeasuredHeight();
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        if ((bool1) && (localLayoutParams.height != -1)) {
          paramInt1 = localView.getBaseline();
        } else {
          paramInt1 = -1;
        }
        int i2 = localLayoutParams.gravity;
        int n = i2;
        if (i2 < 0) {
          n = paramInt2;
        }
        n &= 0x70;
        if (n != 16)
        {
          if (n != 48)
          {
            if (n != 80)
            {
              paramInt1 = paramInt3;
            }
            else
            {
              n = i3 - i4 - i8 - localLayoutParams.bottomMargin;
              if (paramInt1 != -1)
              {
                i2 = localView.getMeasuredHeight();
                paramInt1 = n - (arrayOfInt1[2] - (i2 - paramInt1));
              }
              else
              {
                paramInt1 = n;
              }
            }
          }
          else
          {
            n = localLayoutParams.topMargin + paramInt3;
            if (paramInt1 != -1) {
              paramInt1 = n + (arrayOfInt2[1] - paramInt1);
            } else {
              paramInt1 = n;
            }
          }
        }
        else {
          paramInt1 = (i3 - i1 - i5 - i8) / 2 + paramInt3 + localLayoutParams.topMargin - localLayoutParams.bottomMargin;
        }
        n = i;
        if (hasDividerBeforeChildAt(i7)) {
          n = i + this.mDividerWidth;
        }
        i = localLayoutParams.leftMargin + n;
        setChildFrame(localView, i + getLocationOffset(localView), paramInt1, i6, i8);
        paramInt1 = localLayoutParams.rightMargin;
        n = getNextLocationOffset(localView);
        paramInt4 += getChildrenSkipCount(localView, i7);
        i += i6 + paramInt1 + n;
      }
      paramInt4++;
    }
  }
  
  void layoutVertical(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    int i = getPaddingLeft();
    int k = paramInt3 - paramInt1;
    int j = getPaddingRight();
    int i1 = getPaddingRight();
    int n = getVirtualChildCount();
    int m = this.mGravity;
    paramInt1 = m & 0x70;
    if (paramInt1 != 16)
    {
      if (paramInt1 != 80) {
        paramInt1 = getPaddingTop();
      } else {
        paramInt1 = getPaddingTop() + paramInt4 - paramInt2 - this.mTotalLength;
      }
    }
    else {
      paramInt1 = getPaddingTop() + (paramInt4 - paramInt2 - this.mTotalLength) / 2;
    }
    for (paramInt2 = 0; paramInt2 < n; paramInt2++)
    {
      View localView = getVirtualChildAt(paramInt2);
      if (localView == null)
      {
        paramInt1 += measureNullChild(paramInt2);
      }
      else if (localView.getVisibility() != 8)
      {
        int i3 = localView.getMeasuredWidth();
        int i2 = localView.getMeasuredHeight();
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        paramInt4 = localLayoutParams.gravity;
        paramInt3 = paramInt4;
        if (paramInt4 < 0) {
          paramInt3 = m & 0x800007;
        }
        paramInt3 = GravityCompat.getAbsoluteGravity(paramInt3, ViewCompat.getLayoutDirection(this)) & 0x7;
        if (paramInt3 != 1)
        {
          if (paramInt3 != 5) {
            paramInt3 = localLayoutParams.leftMargin + i;
          } else {
            paramInt3 = k - j - i3 - localLayoutParams.rightMargin;
          }
        }
        else {
          paramInt3 = (k - i - i1 - i3) / 2 + i + localLayoutParams.leftMargin - localLayoutParams.rightMargin;
        }
        paramInt4 = paramInt1;
        if (hasDividerBeforeChildAt(paramInt2)) {
          paramInt4 = paramInt1 + this.mDividerHeight;
        }
        paramInt1 = paramInt4 + localLayoutParams.topMargin;
        setChildFrame(localView, paramInt3, paramInt1 + getLocationOffset(localView), i3, i2);
        paramInt3 = localLayoutParams.bottomMargin;
        paramInt4 = getNextLocationOffset(localView);
        paramInt2 += getChildrenSkipCount(localView, paramInt2);
        paramInt1 += i2 + paramInt3 + paramInt4;
      }
    }
  }
  
  void measureChildBeforeLayout(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5)
  {
    measureChildWithMargins(paramView, paramInt2, paramInt3, paramInt4, paramInt5);
  }
  
  void measureHorizontal(int paramInt1, int paramInt2)
  {
    this.mTotalLength = 0;
    int i6 = getVirtualChildCount();
    int i12 = View.MeasureSpec.getMode(paramInt1);
    int i11 = View.MeasureSpec.getMode(paramInt2);
    if ((this.mMaxAscent == null) || (this.mMaxDescent == null))
    {
      this.mMaxAscent = new int[4];
      this.mMaxDescent = new int[4];
    }
    int[] arrayOfInt = this.mMaxAscent;
    Object localObject1 = this.mMaxDescent;
    arrayOfInt[3] = -1;
    arrayOfInt[2] = -1;
    arrayOfInt[1] = -1;
    arrayOfInt[0] = -1;
    localObject1[3] = -1;
    localObject1[2] = -1;
    localObject1[1] = -1;
    localObject1[0] = -1;
    boolean bool1 = this.mBaselineAligned;
    boolean bool2 = this.mUseLargestChild;
    int i5;
    if (i12 == 1073741824) {
      i5 = 1;
    } else {
      i5 = 0;
    }
    float f1 = 0.0F;
    int k = 0;
    int i1 = 0;
    int i4 = 0;
    int j = 0;
    int n = 0;
    int i2 = 0;
    int i3 = 0;
    int i = 1;
    int m = 0;
    Object localObject2;
    Object localObject3;
    int i7;
    int i8;
    while (k < i6)
    {
      localObject2 = getVirtualChildAt(k);
      if (localObject2 == null)
      {
        this.mTotalLength += measureNullChild(k);
      }
      else if (((View)localObject2).getVisibility() == 8)
      {
        k += getChildrenSkipCount((View)localObject2, k);
      }
      else
      {
        if (hasDividerBeforeChildAt(k)) {
          this.mTotalLength += this.mDividerWidth;
        }
        localObject3 = (LayoutParams)((View)localObject2).getLayoutParams();
        f1 += ((LayoutParams)localObject3).weight;
        if ((i12 == 1073741824) && (((LayoutParams)localObject3).width == 0) && (((LayoutParams)localObject3).weight > 0.0F))
        {
          if (i5 != 0)
          {
            this.mTotalLength += ((LayoutParams)localObject3).leftMargin + ((LayoutParams)localObject3).rightMargin;
          }
          else
          {
            i7 = this.mTotalLength;
            this.mTotalLength = Math.max(i7, ((LayoutParams)localObject3).leftMargin + i7 + ((LayoutParams)localObject3).rightMargin);
          }
          if (bool1)
          {
            i7 = View.MeasureSpec.makeMeasureSpec(0, 0);
            ((View)localObject2).measure(i7, i7);
          }
          else
          {
            i2 = 1;
          }
        }
        else
        {
          if ((((LayoutParams)localObject3).width == 0) && (((LayoutParams)localObject3).weight > 0.0F))
          {
            ((LayoutParams)localObject3).width = -2;
            i7 = 0;
          }
          else
          {
            i7 = Integer.MIN_VALUE;
          }
          if (f1 == 0.0F) {
            i8 = this.mTotalLength;
          } else {
            i8 = 0;
          }
          measureChildBeforeLayout((View)localObject2, k, paramInt1, i8, paramInt2, 0);
          if (i7 != Integer.MIN_VALUE) {
            ((LayoutParams)localObject3).width = i7;
          }
          i8 = ((View)localObject2).getMeasuredWidth();
          if (i5 != 0)
          {
            this.mTotalLength += ((LayoutParams)localObject3).leftMargin + i8 + ((LayoutParams)localObject3).rightMargin + getNextLocationOffset((View)localObject2);
          }
          else
          {
            i7 = this.mTotalLength;
            this.mTotalLength = Math.max(i7, i7 + i8 + ((LayoutParams)localObject3).leftMargin + ((LayoutParams)localObject3).rightMargin + getNextLocationOffset((View)localObject2));
          }
          if (bool2) {
            i1 = Math.max(i8, i1);
          }
        }
        i7 = k;
        if ((i11 != 1073741824) && (((LayoutParams)localObject3).height == -1))
        {
          k = 1;
          m = 1;
        }
        else
        {
          k = 0;
        }
        i8 = ((LayoutParams)localObject3).topMargin + ((LayoutParams)localObject3).bottomMargin;
        i9 = ((View)localObject2).getMeasuredHeight() + i8;
        int i10 = View.combineMeasuredStates(i3, ((View)localObject2).getMeasuredState());
        if (bool1)
        {
          int i13 = ((View)localObject2).getBaseline();
          if (i13 != -1)
          {
            if (((LayoutParams)localObject3).gravity < 0) {
              i3 = this.mGravity;
            } else {
              i3 = ((LayoutParams)localObject3).gravity;
            }
            i3 = ((i3 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
            arrayOfInt[i3] = Math.max(arrayOfInt[i3], i13);
            localObject1[i3] = Math.max(localObject1[i3], i9 - i13);
          }
          else {}
        }
        i4 = Math.max(i4, i9);
        if ((i != 0) && (((LayoutParams)localObject3).height == -1)) {
          i = 1;
        } else {
          i = 0;
        }
        if (((LayoutParams)localObject3).weight > 0.0F)
        {
          if (k == 0) {
            i8 = i9;
          }
          k = Math.max(n, i8);
        }
        else
        {
          if (k != 0) {
            i9 = i8;
          }
          j = Math.max(j, i9);
          k = n;
        }
        n = getChildrenSkipCount((View)localObject2, i7);
        i3 = i10;
        i7 = n + i7;
        n = k;
        k = i7;
      }
      k++;
    }
    k = i4;
    if ((this.mTotalLength > 0) && (hasDividerBeforeChildAt(i6))) {
      this.mTotalLength += this.mDividerWidth;
    }
    if ((arrayOfInt[1] == -1) && (arrayOfInt[0] == -1) && (arrayOfInt[2] == -1) && (arrayOfInt[3] == -1)) {
      break label973;
    }
    k = Math.max(k, Math.max(arrayOfInt[3], Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]))) + Math.max(localObject1[3], Math.max(localObject1[0], Math.max(localObject1[1], localObject1[2]))));
    label973:
    if ((bool2) && ((i12 == Integer.MIN_VALUE) || (i12 == 0)))
    {
      this.mTotalLength = 0;
      for (i4 = 0; i4 < i6; i4++)
      {
        localObject3 = getVirtualChildAt(i4);
        if (localObject3 == null)
        {
          this.mTotalLength += measureNullChild(i4);
        }
        else if (((View)localObject3).getVisibility() == 8)
        {
          i4 += getChildrenSkipCount((View)localObject3, i4);
        }
        else
        {
          localObject2 = (LayoutParams)((View)localObject3).getLayoutParams();
          if (i5 != 0)
          {
            this.mTotalLength += ((LayoutParams)localObject2).leftMargin + i1 + ((LayoutParams)localObject2).rightMargin + getNextLocationOffset((View)localObject3);
          }
          else
          {
            i7 = this.mTotalLength;
            this.mTotalLength = Math.max(i7, i7 + i1 + ((LayoutParams)localObject2).leftMargin + ((LayoutParams)localObject2).rightMargin + getNextLocationOffset((View)localObject3));
          }
        }
      }
    }
    this.mTotalLength += getPaddingLeft() + getPaddingRight();
    int i9 = View.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumWidth()), paramInt1, 0);
    i4 = (0xFFFFFF & i9) - this.mTotalLength;
    if ((i2 == 0) && ((i4 == 0) || (f1 <= 0.0F)))
    {
      i2 = Math.max(j, n);
      if ((bool2) && (i12 != 1073741824)) {
        for (j = 0; j < i6; j++)
        {
          localObject1 = getVirtualChildAt(j);
          if ((localObject1 != null) && (((View)localObject1).getVisibility() != 8) && (((LayoutParams)((View)localObject1).getLayoutParams()).weight > 0.0F)) {
            ((View)localObject1).measure(View.MeasureSpec.makeMeasureSpec(i1, 1073741824), View.MeasureSpec.makeMeasureSpec(((View)localObject1).getMeasuredHeight(), 1073741824));
          }
        }
      }
      n = i6;
      j = k;
      k = i2;
      i2 = i;
    }
    else
    {
      float f2 = this.mWeightSum;
      if (f2 > 0.0F) {
        f1 = f2;
      }
      arrayOfInt[3] = -1;
      arrayOfInt[2] = -1;
      arrayOfInt[1] = -1;
      arrayOfInt[0] = -1;
      localObject1[3] = -1;
      localObject1[2] = -1;
      localObject1[1] = -1;
      localObject1[0] = -1;
      this.mTotalLength = 0;
      i2 = -1;
      i7 = 0;
      n = i;
      k = i6;
      i1 = j;
      j = i3;
      i = i4;
      for (i4 = i7; i4 < k; i4++)
      {
        localObject3 = getVirtualChildAt(i4);
        if ((localObject3 != null) && (((View)localObject3).getVisibility() != 8))
        {
          localObject2 = (LayoutParams)((View)localObject3).getLayoutParams();
          f2 = ((LayoutParams)localObject2).weight;
          if (f2 > 0.0F)
          {
            i6 = (int)(i * f2 / f1);
            i8 = getChildMeasureSpec(paramInt2, getPaddingTop() + getPaddingBottom() + ((LayoutParams)localObject2).topMargin + ((LayoutParams)localObject2).bottomMargin, ((LayoutParams)localObject2).height);
            if ((((LayoutParams)localObject2).width == 0) && (i12 == 1073741824))
            {
              if (i6 > 0) {
                i3 = i6;
              } else {
                i3 = 0;
              }
              ((View)localObject3).measure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), i8);
            }
            else
            {
              i7 = ((View)localObject3).getMeasuredWidth() + i6;
              i3 = i7;
              if (i7 < 0) {
                i3 = 0;
              }
              ((View)localObject3).measure(View.MeasureSpec.makeMeasureSpec(i3, 1073741824), i8);
            }
            j = View.combineMeasuredStates(j, ((View)localObject3).getMeasuredState() & 0xFF000000);
            f1 -= f2;
            i -= i6;
          }
          if (i5 != 0)
          {
            this.mTotalLength += ((View)localObject3).getMeasuredWidth() + ((LayoutParams)localObject2).leftMargin + ((LayoutParams)localObject2).rightMargin + getNextLocationOffset((View)localObject3);
          }
          else
          {
            i3 = this.mTotalLength;
            this.mTotalLength = Math.max(i3, ((View)localObject3).getMeasuredWidth() + i3 + ((LayoutParams)localObject2).leftMargin + ((LayoutParams)localObject2).rightMargin + getNextLocationOffset((View)localObject3));
          }
          if ((i11 != 1073741824) && (((LayoutParams)localObject2).height == -1)) {
            i3 = 1;
          } else {
            i3 = 0;
          }
          i8 = ((LayoutParams)localObject2).topMargin + ((LayoutParams)localObject2).bottomMargin;
          i7 = ((View)localObject3).getMeasuredHeight() + i8;
          i6 = Math.max(i2, i7);
          if (i3 != 0) {
            i2 = i8;
          } else {
            i2 = i7;
          }
          i2 = Math.max(i1, i2);
          if ((n != 0) && (((LayoutParams)localObject2).height == -1)) {
            n = 1;
          } else {
            n = 0;
          }
          if (bool1)
          {
            i3 = ((View)localObject3).getBaseline();
            if (i3 != -1)
            {
              if (((LayoutParams)localObject2).gravity < 0) {
                i1 = this.mGravity;
              } else {
                i1 = ((LayoutParams)localObject2).gravity;
              }
              i1 = ((i1 & 0x70) >> 4 & 0xFFFFFFFE) >> 1;
              arrayOfInt[i1] = Math.max(arrayOfInt[i1], i3);
              localObject1[i1] = Math.max(localObject1[i1], i7 - i3);
            }
            else {}
          }
          i1 = i2;
          i2 = i6;
        }
      }
      this.mTotalLength += getPaddingLeft() + getPaddingRight();
      if ((arrayOfInt[1] == -1) && (arrayOfInt[0] == -1) && (arrayOfInt[2] == -1) && (arrayOfInt[3] == -1)) {
        i = i2;
      } else {
        i = Math.max(i2, Math.max(arrayOfInt[3], Math.max(arrayOfInt[0], Math.max(arrayOfInt[1], arrayOfInt[2]))) + Math.max(localObject1[3], Math.max(localObject1[0], Math.max(localObject1[1], localObject1[2]))));
      }
      i2 = n;
      n = k;
      i3 = j;
      j = i;
      k = i1;
    }
    if ((i2 == 0) && (i11 != 1073741824)) {
      j = k;
    }
    setMeasuredDimension(i9 | i3 & 0xFF000000, View.resolveSizeAndState(Math.max(j + (getPaddingTop() + getPaddingBottom()), getSuggestedMinimumHeight()), paramInt2, i3 << 16));
    if (m != 0) {
      forceUniformHeight(n, paramInt1);
    }
  }
  
  int measureNullChild(int paramInt)
  {
    return 0;
  }
  
  void measureVertical(int paramInt1, int paramInt2)
  {
    this.mTotalLength = 0;
    int i4 = getVirtualChildCount();
    int i12 = View.MeasureSpec.getMode(paramInt1);
    int k = View.MeasureSpec.getMode(paramInt2);
    int i13 = this.mBaselineAlignedChildIndex;
    boolean bool = this.mUseLargestChild;
    float f1 = 0.0F;
    int i = 0;
    int i6 = 0;
    int i1 = 0;
    int j = 0;
    int n = 0;
    int i2 = 0;
    int i5 = 0;
    int m = 1;
    int i3 = 0;
    Object localObject1;
    Object localObject2;
    int i7;
    int i9;
    int i10;
    int i11;
    while (i2 < i4)
    {
      localObject1 = getVirtualChildAt(i2);
      if (localObject1 == null)
      {
        this.mTotalLength += measureNullChild(i2);
      }
      else if (((View)localObject1).getVisibility() == 8)
      {
        i2 += getChildrenSkipCount((View)localObject1, i2);
      }
      else
      {
        if (hasDividerBeforeChildAt(i2)) {
          this.mTotalLength += this.mDividerHeight;
        }
        localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
        f1 += ((LayoutParams)localObject2).weight;
        if ((k == 1073741824) && (((LayoutParams)localObject2).height == 0) && (((LayoutParams)localObject2).weight > 0.0F))
        {
          i5 = this.mTotalLength;
          this.mTotalLength = Math.max(i5, ((LayoutParams)localObject2).topMargin + i5 + ((LayoutParams)localObject2).bottomMargin);
          i5 = 1;
        }
        else
        {
          if ((((LayoutParams)localObject2).height == 0) && (((LayoutParams)localObject2).weight > 0.0F))
          {
            ((LayoutParams)localObject2).height = -2;
            i7 = 0;
          }
          else
          {
            i7 = Integer.MIN_VALUE;
          }
          if (f1 == 0.0F) {
            i8 = this.mTotalLength;
          } else {
            i8 = 0;
          }
          measureChildBeforeLayout((View)localObject1, i2, paramInt1, 0, paramInt2, i8);
          if (i7 != Integer.MIN_VALUE) {
            ((LayoutParams)localObject2).height = i7;
          }
          i8 = ((View)localObject1).getMeasuredHeight();
          i7 = this.mTotalLength;
          this.mTotalLength = Math.max(i7, i7 + i8 + ((LayoutParams)localObject2).topMargin + ((LayoutParams)localObject2).bottomMargin + getNextLocationOffset((View)localObject1));
          if (bool) {
            i1 = Math.max(i8, i1);
          }
        }
        i9 = i2;
        if ((i13 >= 0) && (i13 == i9 + 1)) {
          this.mBaselineChildTop = this.mTotalLength;
        }
        if ((i9 < i13) && (((LayoutParams)localObject2).weight > 0.0F)) {
          throw new RuntimeException("A child of LinearLayout with index less than mBaselineAlignedChildIndex has weight > 0, which won't work.  Either remove the weight, or don't set mBaselineAlignedChildIndex.");
        }
        if ((i12 != 1073741824) && (((LayoutParams)localObject2).width == -1))
        {
          i2 = 1;
          i3 = 1;
        }
        else
        {
          i2 = 0;
        }
        i8 = ((LayoutParams)localObject2).leftMargin + ((LayoutParams)localObject2).rightMargin;
        i7 = ((View)localObject1).getMeasuredWidth() + i8;
        i10 = Math.max(i6, i7);
        i11 = View.combineMeasuredStates(i, ((View)localObject1).getMeasuredState());
        if ((m != 0) && (((LayoutParams)localObject2).width == -1)) {
          i = 1;
        } else {
          i = 0;
        }
        if (((LayoutParams)localObject2).weight > 0.0F)
        {
          if (i2 != 0) {
            i7 = i8;
          }
          m = Math.max(j, i7);
          j = n;
        }
        else
        {
          if (i2 != 0) {
            i7 = i8;
          }
          n = Math.max(n, i7);
          m = j;
          j = n;
        }
        i2 = getChildrenSkipCount((View)localObject1, i9);
        n = i;
        i6 = m;
        i = i11;
        i2 += i9;
        i7 = i10;
        m = n;
        n = j;
        j = i6;
        i6 = i7;
      }
      i2++;
    }
    if ((this.mTotalLength > 0) && (hasDividerBeforeChildAt(i4))) {
      this.mTotalLength += this.mDividerHeight;
    }
    if (bool)
    {
      i2 = k;
      if ((i2 == Integer.MIN_VALUE) || (i2 == 0))
      {
        this.mTotalLength = 0;
        for (i2 = 0; i2 < i4; i2++)
        {
          localObject2 = getVirtualChildAt(i2);
          if (localObject2 == null)
          {
            this.mTotalLength += measureNullChild(i2);
          }
          else if (((View)localObject2).getVisibility() == 8)
          {
            i2 += getChildrenSkipCount((View)localObject2, i2);
          }
          else
          {
            localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
            i7 = this.mTotalLength;
            this.mTotalLength = Math.max(i7, i7 + i1 + ((LayoutParams)localObject1).topMargin + ((LayoutParams)localObject1).bottomMargin + getNextLocationOffset((View)localObject2));
          }
        }
      }
    }
    i2 = k;
    this.mTotalLength += getPaddingTop() + getPaddingBottom();
    int i8 = View.resolveSizeAndState(Math.max(this.mTotalLength, getSuggestedMinimumHeight()), paramInt2, 0);
    k = (0xFFFFFF & i8) - this.mTotalLength;
    if ((i5 == 0) && ((k == 0) || (f1 <= 0.0F)))
    {
      k = Math.max(n, j);
      if ((bool) && (i2 != 1073741824)) {
        for (j = 0; j < i4; j++)
        {
          localObject1 = getVirtualChildAt(j);
          if ((localObject1 != null) && (((View)localObject1).getVisibility() != 8) && (((LayoutParams)((View)localObject1).getLayoutParams()).weight > 0.0F)) {
            ((View)localObject1).measure(View.MeasureSpec.makeMeasureSpec(((View)localObject1).getMeasuredWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(i1, 1073741824));
          }
        }
      }
      j = i;
      i = k;
      k = i6;
    }
    else
    {
      float f2 = this.mWeightSum;
      if (f2 > 0.0F) {
        f1 = f2;
      }
      this.mTotalLength = 0;
      i1 = 0;
      j = n;
      n = k;
      k = i6;
      while (i1 < i4)
      {
        localObject1 = getVirtualChildAt(i1);
        if (((View)localObject1).getVisibility() != 8)
        {
          localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
          f2 = ((LayoutParams)localObject2).weight;
          if (f2 > 0.0F)
          {
            i6 = (int)(n * f2 / f1);
            i7 = getPaddingLeft();
            i9 = getPaddingRight();
            i5 = n - i6;
            i10 = ((LayoutParams)localObject2).leftMargin;
            n = ((LayoutParams)localObject2).rightMargin;
            i11 = ((LayoutParams)localObject2).width;
            f1 -= f2;
            i7 = getChildMeasureSpec(paramInt1, i7 + i9 + i10 + n, i11);
            if ((((LayoutParams)localObject2).height == 0) && (i2 == 1073741824))
            {
              if (i6 > 0) {
                n = i6;
              } else {
                n = 0;
              }
              ((View)localObject1).measure(i7, View.MeasureSpec.makeMeasureSpec(n, 1073741824));
            }
            else
            {
              i6 = ((View)localObject1).getMeasuredHeight() + i6;
              n = i6;
              if (i6 < 0) {
                n = 0;
              }
              ((View)localObject1).measure(i7, View.MeasureSpec.makeMeasureSpec(n, 1073741824));
            }
            i = View.combineMeasuredStates(i, ((View)localObject1).getMeasuredState() & 0xFF00);
            n = i5;
          }
          i7 = ((LayoutParams)localObject2).leftMargin + ((LayoutParams)localObject2).rightMargin;
          i6 = ((View)localObject1).getMeasuredWidth() + i7;
          i5 = Math.max(k, i6);
          if ((i12 != 1073741824) && (((LayoutParams)localObject2).width == -1)) {
            k = 1;
          } else {
            k = 0;
          }
          if (k != 0) {
            k = i7;
          } else {
            k = i6;
          }
          k = Math.max(j, k);
          if ((m != 0) && (((LayoutParams)localObject2).width == -1)) {
            j = 1;
          } else {
            j = 0;
          }
          m = this.mTotalLength;
          this.mTotalLength = Math.max(m, ((View)localObject1).getMeasuredHeight() + m + ((LayoutParams)localObject2).topMargin + ((LayoutParams)localObject2).bottomMargin + getNextLocationOffset((View)localObject1));
          m = j;
          j = k;
          k = i5;
        }
        i1++;
      }
      this.mTotalLength += getPaddingTop() + getPaddingBottom();
      n = j;
      j = i;
      i = n;
    }
    if ((m != 0) || (i12 == 1073741824)) {
      i = k;
    }
    setMeasuredDimension(View.resolveSizeAndState(Math.max(i + (getPaddingLeft() + getPaddingRight()), getSuggestedMinimumWidth()), paramInt1, j), i8);
    if (i3 != 0) {
      forceUniformWidth(i4, paramInt2);
    }
  }
  
  protected void onDraw(Canvas paramCanvas)
  {
    if (this.mDivider == null) {
      return;
    }
    if (this.mOrientation == 1) {
      drawDividersVertical(paramCanvas);
    } else {
      drawDividersHorizontal(paramCanvas);
    }
  }
  
  public void onInitializeAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    super.onInitializeAccessibilityEvent(paramAccessibilityEvent);
    paramAccessibilityEvent.setClassName(LinearLayoutCompat.class.getName());
  }
  
  public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo paramAccessibilityNodeInfo)
  {
    super.onInitializeAccessibilityNodeInfo(paramAccessibilityNodeInfo);
    paramAccessibilityNodeInfo.setClassName(LinearLayoutCompat.class.getName());
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (this.mOrientation == 1) {
      layoutVertical(paramInt1, paramInt2, paramInt3, paramInt4);
    } else {
      layoutHorizontal(paramInt1, paramInt2, paramInt3, paramInt4);
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    if (this.mOrientation == 1) {
      measureVertical(paramInt1, paramInt2);
    } else {
      measureHorizontal(paramInt1, paramInt2);
    }
  }
  
  public void setBaselineAligned(boolean paramBoolean)
  {
    this.mBaselineAligned = paramBoolean;
  }
  
  public void setBaselineAlignedChildIndex(int paramInt)
  {
    if ((paramInt >= 0) && (paramInt < getChildCount()))
    {
      this.mBaselineAlignedChildIndex = paramInt;
      return;
    }
    StringBuilder localStringBuilder = new StringBuilder();
    localStringBuilder.append("base aligned child index out of range (0, ");
    localStringBuilder.append(getChildCount());
    localStringBuilder.append(")");
    throw new IllegalArgumentException(localStringBuilder.toString());
  }
  
  public void setDividerDrawable(Drawable paramDrawable)
  {
    if (paramDrawable == this.mDivider) {
      return;
    }
    this.mDivider = paramDrawable;
    boolean bool = false;
    if (paramDrawable != null)
    {
      this.mDividerWidth = paramDrawable.getIntrinsicWidth();
      this.mDividerHeight = paramDrawable.getIntrinsicHeight();
    }
    else
    {
      this.mDividerWidth = 0;
      this.mDividerHeight = 0;
    }
    if (paramDrawable == null) {
      bool = true;
    }
    setWillNotDraw(bool);
    requestLayout();
  }
  
  public void setDividerPadding(int paramInt)
  {
    this.mDividerPadding = paramInt;
  }
  
  public void setGravity(int paramInt)
  {
    if (this.mGravity != paramInt)
    {
      int i = paramInt;
      if ((0x800007 & paramInt) == 0) {
        i = paramInt | 0x800003;
      }
      paramInt = i;
      if ((i & 0x70) == 0) {
        paramInt = i | 0x30;
      }
      this.mGravity = paramInt;
      requestLayout();
    }
  }
  
  public void setHorizontalGravity(int paramInt)
  {
    paramInt &= 0x800007;
    int i = this.mGravity;
    if ((0x800007 & i) != paramInt)
    {
      this.mGravity = (paramInt | 0xFF7FFFF8 & i);
      requestLayout();
    }
  }
  
  public void setMeasureWithLargestChildEnabled(boolean paramBoolean)
  {
    this.mUseLargestChild = paramBoolean;
  }
  
  public void setOrientation(int paramInt)
  {
    if (this.mOrientation != paramInt)
    {
      this.mOrientation = paramInt;
      requestLayout();
    }
  }
  
  public void setShowDividers(int paramInt)
  {
    if (paramInt != this.mShowDividers) {
      requestLayout();
    }
    this.mShowDividers = paramInt;
  }
  
  public void setVerticalGravity(int paramInt)
  {
    paramInt &= 0x70;
    int i = this.mGravity;
    if ((i & 0x70) != paramInt)
    {
      this.mGravity = (paramInt | i & 0xFFFFFF8F);
      requestLayout();
    }
  }
  
  public void setWeightSum(float paramFloat)
  {
    this.mWeightSum = Math.max(0.0F, paramFloat);
  }
  
  public boolean shouldDelayChildPressedState()
  {
    return false;
  }
  
  @Retention(RetentionPolicy.SOURCE)
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public static @interface DividerMode {}
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    public int gravity = -1;
    public float weight;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.weight = 0.0F;
    }
    
    public LayoutParams(int paramInt1, int paramInt2, float paramFloat)
    {
      super(paramInt2);
      this.weight = paramFloat;
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.LinearLayoutCompat_Layout);
      this.weight = paramContext.getFloat(R.styleable.LinearLayoutCompat_Layout_android_layout_weight, 0.0F);
      this.gravity = paramContext.getInt(R.styleable.LinearLayoutCompat_Layout_android_layout_gravity, -1);
      paramContext.recycle();
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.weight = paramLayoutParams.weight;
      this.gravity = paramLayoutParams.gravity;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public LayoutParams(ViewGroup.MarginLayoutParams paramMarginLayoutParams)
    {
      super();
    }
  }
  
  @Retention(RetentionPolicy.SOURCE)
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public static @interface OrientationMode {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/widget/LinearLayoutCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */