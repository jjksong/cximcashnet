package android.support.v7.widget;

import java.util.ArrayList;

class PositionMap<E>
  implements Cloneable
{
  private static final Object DELETED = new Object();
  private boolean mGarbage = false;
  private int[] mKeys;
  private int mSize;
  private Object[] mValues;
  
  PositionMap()
  {
    this(10);
  }
  
  PositionMap(int paramInt)
  {
    if (paramInt == 0)
    {
      this.mKeys = ContainerHelpers.EMPTY_INTS;
      this.mValues = ContainerHelpers.EMPTY_OBJECTS;
    }
    else
    {
      paramInt = idealIntArraySize(paramInt);
      this.mKeys = new int[paramInt];
      this.mValues = new Object[paramInt];
    }
    this.mSize = 0;
  }
  
  private void gc()
  {
    int m = this.mSize;
    int[] arrayOfInt = this.mKeys;
    Object[] arrayOfObject = this.mValues;
    int k = 0;
    int i;
    for (int j = 0; k < m; j = i)
    {
      Object localObject = arrayOfObject[k];
      i = j;
      if (localObject != DELETED)
      {
        if (k != j)
        {
          arrayOfInt[j] = arrayOfInt[k];
          arrayOfObject[j] = localObject;
          arrayOfObject[k] = null;
        }
        i = j + 1;
      }
      k++;
    }
    this.mGarbage = false;
    this.mSize = j;
  }
  
  static int idealBooleanArraySize(int paramInt)
  {
    return idealByteArraySize(paramInt);
  }
  
  static int idealByteArraySize(int paramInt)
  {
    for (int i = 4; i < 32; i++)
    {
      int j = (1 << i) - 12;
      if (paramInt <= j) {
        return j;
      }
    }
    return paramInt;
  }
  
  static int idealCharArraySize(int paramInt)
  {
    return idealByteArraySize(paramInt * 2) / 2;
  }
  
  static int idealFloatArraySize(int paramInt)
  {
    return idealByteArraySize(paramInt * 4) / 4;
  }
  
  static int idealIntArraySize(int paramInt)
  {
    return idealByteArraySize(paramInt * 4) / 4;
  }
  
  static int idealLongArraySize(int paramInt)
  {
    return idealByteArraySize(paramInt * 8) / 8;
  }
  
  static int idealObjectArraySize(int paramInt)
  {
    return idealByteArraySize(paramInt * 4) / 4;
  }
  
  static int idealShortArraySize(int paramInt)
  {
    return idealByteArraySize(paramInt * 2) / 2;
  }
  
  public void append(int paramInt, E paramE)
  {
    int i = this.mSize;
    if ((i != 0) && (paramInt <= this.mKeys[(i - 1)]))
    {
      put(paramInt, paramE);
      return;
    }
    if ((this.mGarbage) && (this.mSize >= this.mKeys.length)) {
      gc();
    }
    int j = this.mSize;
    if (j >= this.mKeys.length)
    {
      i = idealIntArraySize(j + 1);
      int[] arrayOfInt = new int[i];
      Object[] arrayOfObject = new Object[i];
      Object localObject = this.mKeys;
      System.arraycopy(localObject, 0, arrayOfInt, 0, localObject.length);
      localObject = this.mValues;
      System.arraycopy(localObject, 0, arrayOfObject, 0, localObject.length);
      this.mKeys = arrayOfInt;
      this.mValues = arrayOfObject;
    }
    this.mKeys[j] = paramInt;
    this.mValues[j] = paramE;
    this.mSize = (j + 1);
  }
  
  public void clear()
  {
    int j = this.mSize;
    Object[] arrayOfObject = this.mValues;
    for (int i = 0; i < j; i++) {
      arrayOfObject[i] = null;
    }
    this.mSize = 0;
    this.mGarbage = false;
  }
  
  public PositionMap<E> clone()
  {
    try
    {
      PositionMap localPositionMap = (PositionMap)super.clone();
      PositionMap<E> localPositionMap1;
      return localPositionMap1;
    }
    catch (CloneNotSupportedException localCloneNotSupportedException1)
    {
      try
      {
        localPositionMap.mKeys = ((int[])this.mKeys.clone());
        localPositionMap.mValues = ((Object[])this.mValues.clone());
      }
      catch (CloneNotSupportedException localCloneNotSupportedException2)
      {
        for (;;) {}
      }
      localCloneNotSupportedException1 = localCloneNotSupportedException1;
      localPositionMap1 = null;
    }
  }
  
  public void delete(int paramInt)
  {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt >= 0)
    {
      Object[] arrayOfObject = this.mValues;
      Object localObject1 = arrayOfObject[paramInt];
      Object localObject2 = DELETED;
      if (localObject1 != localObject2)
      {
        arrayOfObject[paramInt] = localObject2;
        this.mGarbage = true;
      }
    }
  }
  
  public E get(int paramInt)
  {
    return (E)get(paramInt, null);
  }
  
  public E get(int paramInt, E paramE)
  {
    paramInt = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (paramInt >= 0)
    {
      Object[] arrayOfObject = this.mValues;
      if (arrayOfObject[paramInt] != DELETED) {
        return (E)arrayOfObject[paramInt];
      }
    }
    return paramE;
  }
  
  public int indexOfKey(int paramInt)
  {
    if (this.mGarbage) {
      gc();
    }
    return ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
  }
  
  public int indexOfValue(E paramE)
  {
    if (this.mGarbage) {
      gc();
    }
    for (int i = 0; i < this.mSize; i++) {
      if (this.mValues[i] == paramE) {
        return i;
      }
    }
    return -1;
  }
  
  public void insertKeyRange(int paramInt1, int paramInt2) {}
  
  public int keyAt(int paramInt)
  {
    if (this.mGarbage) {
      gc();
    }
    return this.mKeys[paramInt];
  }
  
  public void put(int paramInt, E paramE)
  {
    int i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt);
    if (i >= 0)
    {
      this.mValues[i] = paramE;
    }
    else
    {
      int j = i ^ 0xFFFFFFFF;
      Object localObject1;
      if (j < this.mSize)
      {
        localObject1 = this.mValues;
        if (localObject1[j] == DELETED)
        {
          this.mKeys[j] = paramInt;
          localObject1[j] = paramE;
          return;
        }
      }
      i = j;
      if (this.mGarbage)
      {
        i = j;
        if (this.mSize >= this.mKeys.length)
        {
          gc();
          i = ContainerHelpers.binarySearch(this.mKeys, this.mSize, paramInt) ^ 0xFFFFFFFF;
        }
      }
      j = this.mSize;
      if (j >= this.mKeys.length)
      {
        j = idealIntArraySize(j + 1);
        localObject1 = new int[j];
        Object[] arrayOfObject = new Object[j];
        Object localObject2 = this.mKeys;
        System.arraycopy(localObject2, 0, localObject1, 0, localObject2.length);
        localObject2 = this.mValues;
        System.arraycopy(localObject2, 0, arrayOfObject, 0, localObject2.length);
        this.mKeys = ((int[])localObject1);
        this.mValues = arrayOfObject;
      }
      int k = this.mSize;
      if (k - i != 0)
      {
        localObject1 = this.mKeys;
        j = i + 1;
        System.arraycopy(localObject1, i, localObject1, j, k - i);
        localObject1 = this.mValues;
        System.arraycopy(localObject1, i, localObject1, j, this.mSize - i);
      }
      this.mKeys[i] = paramInt;
      this.mValues[i] = paramE;
      this.mSize += 1;
    }
  }
  
  public void remove(int paramInt)
  {
    delete(paramInt);
  }
  
  public void removeAt(int paramInt)
  {
    Object[] arrayOfObject = this.mValues;
    Object localObject2 = arrayOfObject[paramInt];
    Object localObject1 = DELETED;
    if (localObject2 != localObject1)
    {
      arrayOfObject[paramInt] = localObject1;
      this.mGarbage = true;
    }
  }
  
  public void removeAtRange(int paramInt1, int paramInt2)
  {
    paramInt2 = Math.min(this.mSize, paramInt2 + paramInt1);
    while (paramInt1 < paramInt2)
    {
      removeAt(paramInt1);
      paramInt1++;
    }
  }
  
  public void removeKeyRange(ArrayList<E> paramArrayList, int paramInt1, int paramInt2) {}
  
  public void setValueAt(int paramInt, E paramE)
  {
    if (this.mGarbage) {
      gc();
    }
    this.mValues[paramInt] = paramE;
  }
  
  public int size()
  {
    if (this.mGarbage) {
      gc();
    }
    return this.mSize;
  }
  
  public String toString()
  {
    if (size() <= 0) {
      return "{}";
    }
    StringBuilder localStringBuilder = new StringBuilder(this.mSize * 28);
    localStringBuilder.append('{');
    for (int i = 0; i < this.mSize; i++)
    {
      if (i > 0) {
        localStringBuilder.append(", ");
      }
      localStringBuilder.append(keyAt(i));
      localStringBuilder.append('=');
      Object localObject = valueAt(i);
      if (localObject != this) {
        localStringBuilder.append(localObject);
      } else {
        localStringBuilder.append("(this Map)");
      }
    }
    localStringBuilder.append('}');
    return localStringBuilder.toString();
  }
  
  public E valueAt(int paramInt)
  {
    if (this.mGarbage) {
      gc();
    }
    return (E)this.mValues[paramInt];
  }
  
  static class ContainerHelpers
  {
    static final boolean[] EMPTY_BOOLEANS = new boolean[0];
    static final int[] EMPTY_INTS = new int[0];
    static final long[] EMPTY_LONGS = new long[0];
    static final Object[] EMPTY_OBJECTS = new Object[0];
    
    static int binarySearch(int[] paramArrayOfInt, int paramInt1, int paramInt2)
    {
      paramInt1--;
      int i = 0;
      while (i <= paramInt1)
      {
        int j = i + paramInt1 >>> 1;
        int k = paramArrayOfInt[j];
        if (k < paramInt2) {
          i = j + 1;
        } else if (k > paramInt2) {
          paramInt1 = j - 1;
        } else {
          return j;
        }
      }
      return i ^ 0xFFFFFFFF;
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/widget/PositionMap.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */