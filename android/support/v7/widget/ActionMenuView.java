package android.support.v7.widget;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.annotation.RestrictTo;
import android.support.annotation.StyleRes;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.view.menu.MenuBuilder;
import android.support.v7.view.menu.MenuBuilder.Callback;
import android.support.v7.view.menu.MenuBuilder.ItemInvoker;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.view.menu.MenuPresenter.Callback;
import android.support.v7.view.menu.MenuView;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewDebug.ExportedProperty;
import android.view.ViewGroup.LayoutParams;
import android.view.accessibility.AccessibilityEvent;

public class ActionMenuView
  extends LinearLayoutCompat
  implements MenuBuilder.ItemInvoker, MenuView
{
  static final int GENERATED_ITEM_PADDING = 4;
  static final int MIN_CELL_SIZE = 56;
  private static final String TAG = "ActionMenuView";
  private MenuPresenter.Callback mActionMenuPresenterCallback;
  private boolean mFormatItems;
  private int mFormatItemsWidth;
  private int mGeneratedItemPadding;
  private MenuBuilder mMenu;
  MenuBuilder.Callback mMenuBuilderCallback;
  private int mMinCellSize;
  OnMenuItemClickListener mOnMenuItemClickListener;
  private Context mPopupContext;
  private int mPopupTheme;
  private ActionMenuPresenter mPresenter;
  private boolean mReserveOverflow;
  
  public ActionMenuView(Context paramContext)
  {
    this(paramContext, null);
  }
  
  public ActionMenuView(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    setBaselineAligned(false);
    float f = paramContext.getResources().getDisplayMetrics().density;
    this.mMinCellSize = ((int)(56.0F * f));
    this.mGeneratedItemPadding = ((int)(f * 4.0F));
    this.mPopupContext = paramContext;
    this.mPopupTheme = 0;
  }
  
  static int measureChildForCells(View paramView, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    LayoutParams localLayoutParams = (LayoutParams)paramView.getLayoutParams();
    int j = View.MeasureSpec.makeMeasureSpec(View.MeasureSpec.getSize(paramInt3) - paramInt4, View.MeasureSpec.getMode(paramInt3));
    ActionMenuItemView localActionMenuItemView;
    if ((paramView instanceof ActionMenuItemView)) {
      localActionMenuItemView = (ActionMenuItemView)paramView;
    } else {
      localActionMenuItemView = null;
    }
    boolean bool = true;
    if ((localActionMenuItemView != null) && (localActionMenuItemView.hasText())) {
      paramInt3 = 1;
    } else {
      paramInt3 = 0;
    }
    paramInt4 = 2;
    if ((paramInt2 > 0) && ((paramInt3 == 0) || (paramInt2 >= 2)))
    {
      paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt2 * paramInt1, Integer.MIN_VALUE), j);
      int k = paramView.getMeasuredWidth();
      int i = k / paramInt1;
      paramInt2 = i;
      if (k % paramInt1 != 0) {
        paramInt2 = i + 1;
      }
      if ((paramInt3 != 0) && (paramInt2 < 2)) {
        paramInt2 = paramInt4;
      }
    }
    else
    {
      paramInt2 = 0;
    }
    if ((localLayoutParams.isOverflowButton) || (paramInt3 == 0)) {
      bool = false;
    }
    localLayoutParams.expandable = bool;
    localLayoutParams.cellsUsed = paramInt2;
    paramView.measure(View.MeasureSpec.makeMeasureSpec(paramInt1 * paramInt2, 1073741824), j);
    return paramInt2;
  }
  
  private void onMeasureExactFormat(int paramInt1, int paramInt2)
  {
    int i3 = View.MeasureSpec.getMode(paramInt2);
    int k = View.MeasureSpec.getSize(paramInt1);
    int i = View.MeasureSpec.getSize(paramInt2);
    paramInt1 = getPaddingLeft();
    int j = getPaddingRight();
    int i4 = getPaddingTop() + getPaddingBottom();
    int i10 = getChildMeasureSpec(paramInt2, i4, -2);
    int i6 = k - (paramInt1 + j);
    paramInt2 = this.mMinCellSize;
    paramInt1 = i6 / paramInt2;
    if (paramInt1 == 0)
    {
      setMeasuredDimension(i6, 0);
      return;
    }
    int i11 = paramInt2 + i6 % paramInt2 / paramInt1;
    int i12 = getChildCount();
    int n = 0;
    paramInt2 = 0;
    j = 0;
    int i2 = 0;
    int i1 = 0;
    k = 0;
    long l1 = 0L;
    Object localObject2;
    Object localObject1;
    while (n < i12)
    {
      localObject2 = getChildAt(n);
      if (((View)localObject2).getVisibility() != 8)
      {
        boolean bool = localObject2 instanceof ActionMenuItemView;
        i2++;
        if (bool)
        {
          m = this.mGeneratedItemPadding;
          ((View)localObject2).setPadding(m, 0, m, 0);
        }
        localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
        ((LayoutParams)localObject1).expanded = false;
        ((LayoutParams)localObject1).extraPixels = 0;
        ((LayoutParams)localObject1).cellsUsed = 0;
        ((LayoutParams)localObject1).expandable = false;
        ((LayoutParams)localObject1).leftMargin = 0;
        ((LayoutParams)localObject1).rightMargin = 0;
        if ((bool) && (((ActionMenuItemView)localObject2).hasText())) {
          bool = true;
        } else {
          bool = false;
        }
        ((LayoutParams)localObject1).preventEdgeOffset = bool;
        if (((LayoutParams)localObject1).isOverflowButton) {
          m = 1;
        } else {
          m = paramInt1;
        }
        i5 = measureChildForCells((View)localObject2, i11, m, i10, i4);
        i1 = Math.max(i1, i5);
        m = k;
        if (((LayoutParams)localObject1).expandable) {
          m = k + 1;
        }
        if (((LayoutParams)localObject1).isOverflowButton) {
          j = 1;
        }
        paramInt1 -= i5;
        paramInt2 = Math.max(paramInt2, ((View)localObject2).getMeasuredHeight());
        if (i5 == 1) {
          l1 |= 1 << n;
        }
        k = m;
      }
      n++;
    }
    if ((j != 0) && (i2 == 2)) {
      i4 = 1;
    } else {
      i4 = 0;
    }
    int m = 0;
    int i5 = paramInt1;
    paramInt1 = m;
    m = i6;
    while ((k > 0) && (i5 > 0))
    {
      int i7 = Integer.MAX_VALUE;
      int i8 = 0;
      i6 = 0;
      long l2;
      for (long l3 = 0L; i8 < i12; l3 = l2)
      {
        localObject1 = (LayoutParams)getChildAt(i8).getLayoutParams();
        int i9;
        if (!((LayoutParams)localObject1).expandable)
        {
          i9 = i7;
          n = i6;
          l2 = l3;
        }
        else if (((LayoutParams)localObject1).cellsUsed < i7)
        {
          i9 = ((LayoutParams)localObject1).cellsUsed;
          l2 = 1L << i8;
          n = 1;
        }
        else
        {
          i9 = i7;
          n = i6;
          l2 = l3;
          if (((LayoutParams)localObject1).cellsUsed == i7)
          {
            l2 = l3 | 1L << i8;
            n = i6 + 1;
            i9 = i7;
          }
        }
        i8++;
        i7 = i9;
        i6 = n;
      }
      l1 |= l3;
      if (i6 > i5) {
        break;
      }
      paramInt1 = i7 + 1;
      for (n = 0; n < i12; n++)
      {
        localObject1 = getChildAt(n);
        localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
        long l4 = 1 << n;
        if ((l3 & l4) == 0L)
        {
          l2 = l1;
          if (((LayoutParams)localObject2).cellsUsed == paramInt1) {
            l2 = l1 | l4;
          }
          l1 = l2;
        }
        else
        {
          if ((i4 != 0) && (((LayoutParams)localObject2).preventEdgeOffset) && (i5 == 1))
          {
            i6 = this.mGeneratedItemPadding;
            ((View)localObject1).setPadding(i6 + i11, 0, i6, 0);
          }
          ((LayoutParams)localObject2).cellsUsed += 1;
          ((LayoutParams)localObject2).expanded = true;
          i5--;
        }
      }
      paramInt1 = 1;
    }
    n = m;
    if ((j == 0) && (i2 == 1)) {
      j = 1;
    } else {
      j = 0;
    }
    if ((i5 > 0) && (l1 != 0L) && ((i5 < i2 - 1) || (j != 0) || (i1 > 1)))
    {
      float f2 = Long.bitCount(l1);
      if (j == 0)
      {
        float f1;
        if ((l1 & 1L) != 0L)
        {
          f1 = f2;
          if (!((LayoutParams)getChildAt(0).getLayoutParams()).preventEdgeOffset) {
            f1 = f2 - 0.5F;
          }
        }
        else
        {
          f1 = f2;
        }
        j = i12 - 1;
        f2 = f1;
        if ((l1 & 1 << j) != 0L)
        {
          f2 = f1;
          if (!((LayoutParams)getChildAt(j).getLayoutParams()).preventEdgeOffset) {
            f2 = f1 - 0.5F;
          }
        }
      }
      if (f2 > 0.0F) {
        k = (int)(i5 * i11 / f2);
      } else {
        k = 0;
      }
      m = 0;
      for (;;)
      {
        j = paramInt1;
        if (m >= i12) {
          break;
        }
        if ((l1 & 1 << m) == 0L)
        {
          j = paramInt1;
        }
        else
        {
          localObject2 = getChildAt(m);
          localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
          if ((localObject2 instanceof ActionMenuItemView))
          {
            ((LayoutParams)localObject1).extraPixels = k;
            ((LayoutParams)localObject1).expanded = true;
            if ((m == 0) && (!((LayoutParams)localObject1).preventEdgeOffset)) {
              ((LayoutParams)localObject1).leftMargin = (-k / 2);
            }
            j = 1;
          }
          else if (((LayoutParams)localObject1).isOverflowButton)
          {
            ((LayoutParams)localObject1).extraPixels = k;
            ((LayoutParams)localObject1).expanded = true;
            ((LayoutParams)localObject1).rightMargin = (-k / 2);
            j = 1;
          }
          else
          {
            if (m != 0) {
              ((LayoutParams)localObject1).leftMargin = (k / 2);
            }
            j = paramInt1;
            if (m != i12 - 1)
            {
              ((LayoutParams)localObject1).rightMargin = (k / 2);
              j = paramInt1;
            }
          }
        }
        m++;
        paramInt1 = j;
      }
    }
    j = paramInt1;
    paramInt1 = 0;
    if (j != 0) {
      while (paramInt1 < i12)
      {
        localObject1 = getChildAt(paramInt1);
        localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
        if (((LayoutParams)localObject2).expanded) {
          ((View)localObject1).measure(View.MeasureSpec.makeMeasureSpec(((LayoutParams)localObject2).cellsUsed * i11 + ((LayoutParams)localObject2).extraPixels, 1073741824), i10);
        }
        paramInt1++;
      }
    }
    if (i3 != 1073741824) {
      i = paramInt2;
    }
    setMeasuredDimension(n, i);
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    boolean bool;
    if ((paramLayoutParams != null) && ((paramLayoutParams instanceof LayoutParams))) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public void dismissPopupMenus()
  {
    ActionMenuPresenter localActionMenuPresenter = this.mPresenter;
    if (localActionMenuPresenter != null) {
      localActionMenuPresenter.dismissPopupMenus();
    }
  }
  
  public boolean dispatchPopulateAccessibilityEvent(AccessibilityEvent paramAccessibilityEvent)
  {
    return false;
  }
  
  protected LayoutParams generateDefaultLayoutParams()
  {
    LayoutParams localLayoutParams = new LayoutParams(-2, -2);
    localLayoutParams.gravity = 16;
    return localLayoutParams;
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    if (paramLayoutParams != null)
    {
      if ((paramLayoutParams instanceof LayoutParams)) {
        paramLayoutParams = new LayoutParams((LayoutParams)paramLayoutParams);
      } else {
        paramLayoutParams = new LayoutParams(paramLayoutParams);
      }
      if (paramLayoutParams.gravity <= 0) {
        paramLayoutParams.gravity = 16;
      }
      return paramLayoutParams;
    }
    return generateDefaultLayoutParams();
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public LayoutParams generateOverflowButtonLayoutParams()
  {
    LayoutParams localLayoutParams = generateDefaultLayoutParams();
    localLayoutParams.isOverflowButton = true;
    return localLayoutParams;
  }
  
  public Menu getMenu()
  {
    if (this.mMenu == null)
    {
      Object localObject = getContext();
      this.mMenu = new MenuBuilder((Context)localObject);
      this.mMenu.setCallback(new MenuBuilderCallback());
      this.mPresenter = new ActionMenuPresenter((Context)localObject);
      this.mPresenter.setReserveOverflow(true);
      ActionMenuPresenter localActionMenuPresenter = this.mPresenter;
      localObject = this.mActionMenuPresenterCallback;
      if (localObject == null) {
        localObject = new ActionMenuPresenterCallback();
      }
      localActionMenuPresenter.setCallback((MenuPresenter.Callback)localObject);
      this.mMenu.addMenuPresenter(this.mPresenter, this.mPopupContext);
      this.mPresenter.setMenuView(this);
    }
    return this.mMenu;
  }
  
  @Nullable
  public Drawable getOverflowIcon()
  {
    getMenu();
    return this.mPresenter.getOverflowIcon();
  }
  
  public int getPopupTheme()
  {
    return this.mPopupTheme;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public int getWindowAnimations()
  {
    return 0;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  protected boolean hasSupportDividerBeforeChildAt(int paramInt)
  {
    boolean bool2 = false;
    if (paramInt == 0) {
      return false;
    }
    View localView1 = getChildAt(paramInt - 1);
    View localView2 = getChildAt(paramInt);
    boolean bool1 = bool2;
    if (paramInt < getChildCount())
    {
      bool1 = bool2;
      if ((localView1 instanceof ActionMenuChildView)) {
        bool1 = false | ((ActionMenuChildView)localView1).needsDividerAfter();
      }
    }
    bool2 = bool1;
    if (paramInt > 0)
    {
      bool2 = bool1;
      if ((localView2 instanceof ActionMenuChildView)) {
        bool2 = bool1 | ((ActionMenuChildView)localView2).needsDividerBefore();
      }
    }
    return bool2;
  }
  
  public boolean hideOverflowMenu()
  {
    ActionMenuPresenter localActionMenuPresenter = this.mPresenter;
    boolean bool;
    if ((localActionMenuPresenter != null) && (localActionMenuPresenter.hideOverflowMenu())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void initialize(MenuBuilder paramMenuBuilder)
  {
    this.mMenu = paramMenuBuilder;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public boolean invokeItem(MenuItemImpl paramMenuItemImpl)
  {
    return this.mMenu.performItemAction(paramMenuItemImpl, 0);
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public boolean isOverflowMenuShowPending()
  {
    ActionMenuPresenter localActionMenuPresenter = this.mPresenter;
    boolean bool;
    if ((localActionMenuPresenter != null) && (localActionMenuPresenter.isOverflowMenuShowPending())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public boolean isOverflowMenuShowing()
  {
    ActionMenuPresenter localActionMenuPresenter = this.mPresenter;
    boolean bool;
    if ((localActionMenuPresenter != null) && (localActionMenuPresenter.isOverflowMenuShowing())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public boolean isOverflowReserved()
  {
    return this.mReserveOverflow;
  }
  
  public void onConfigurationChanged(Configuration paramConfiguration)
  {
    super.onConfigurationChanged(paramConfiguration);
    paramConfiguration = this.mPresenter;
    if (paramConfiguration != null)
    {
      paramConfiguration.updateMenuView(false);
      if (this.mPresenter.isOverflowMenuShowing())
      {
        this.mPresenter.hideOverflowMenu();
        this.mPresenter.showOverflowMenu();
      }
    }
  }
  
  public void onDetachedFromWindow()
  {
    super.onDetachedFromWindow();
    dismissPopupMenus();
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    if (!this.mFormatItems)
    {
      super.onLayout(paramBoolean, paramInt1, paramInt2, paramInt3, paramInt4);
      return;
    }
    int m = getChildCount();
    int k = (paramInt4 - paramInt2) / 2;
    int n = getDividerWidth();
    int i1 = paramInt3 - paramInt1;
    paramInt2 = getPaddingRight();
    paramInt1 = getPaddingLeft();
    paramBoolean = ViewUtils.isLayoutRtl(this);
    paramInt1 = i1 - paramInt2 - paramInt1;
    paramInt2 = 0;
    paramInt4 = 0;
    paramInt3 = 0;
    Object localObject1;
    Object localObject2;
    int i;
    int j;
    while (paramInt2 < m)
    {
      localObject1 = getChildAt(paramInt2);
      if (((View)localObject1).getVisibility() != 8)
      {
        localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
        if (((LayoutParams)localObject2).isOverflowButton)
        {
          i = ((View)localObject1).getMeasuredWidth();
          paramInt4 = i;
          if (hasSupportDividerBeforeChildAt(paramInt2)) {
            paramInt4 = i + n;
          }
          int i2 = ((View)localObject1).getMeasuredHeight();
          if (paramBoolean)
          {
            j = getPaddingLeft() + ((LayoutParams)localObject2).leftMargin;
            i = j + paramInt4;
          }
          else
          {
            i = getWidth() - getPaddingRight() - ((LayoutParams)localObject2).rightMargin;
            j = i - paramInt4;
          }
          int i3 = k - i2 / 2;
          ((View)localObject1).layout(j, i3, i, i2 + i3);
          paramInt1 -= paramInt4;
          paramInt4 = 1;
        }
        else
        {
          paramInt1 -= ((View)localObject1).getMeasuredWidth() + ((LayoutParams)localObject2).leftMargin + ((LayoutParams)localObject2).rightMargin;
          hasSupportDividerBeforeChildAt(paramInt2);
          paramInt3++;
        }
      }
      paramInt2++;
    }
    if ((m == 1) && (paramInt4 == 0))
    {
      localObject1 = getChildAt(0);
      paramInt2 = ((View)localObject1).getMeasuredWidth();
      paramInt1 = ((View)localObject1).getMeasuredHeight();
      paramInt3 = i1 / 2 - paramInt2 / 2;
      paramInt4 = k - paramInt1 / 2;
      ((View)localObject1).layout(paramInt3, paramInt4, paramInt2 + paramInt3, paramInt1 + paramInt4);
      return;
    }
    paramInt2 = paramInt3 - (paramInt4 ^ 0x1);
    if (paramInt2 > 0) {
      paramInt1 /= paramInt2;
    } else {
      paramInt1 = 0;
    }
    paramInt3 = 0;
    paramInt2 = 0;
    paramInt4 = Math.max(0, paramInt1);
    if (paramBoolean)
    {
      paramInt3 = getWidth() - getPaddingRight();
      paramInt1 = paramInt2;
      while (paramInt1 < m)
      {
        localObject2 = getChildAt(paramInt1);
        localObject1 = (LayoutParams)((View)localObject2).getLayoutParams();
        paramInt2 = paramInt3;
        if (((View)localObject2).getVisibility() != 8) {
          if (((LayoutParams)localObject1).isOverflowButton)
          {
            paramInt2 = paramInt3;
          }
          else
          {
            paramInt3 -= ((LayoutParams)localObject1).rightMargin;
            j = ((View)localObject2).getMeasuredWidth();
            paramInt2 = ((View)localObject2).getMeasuredHeight();
            i = k - paramInt2 / 2;
            ((View)localObject2).layout(paramInt3 - j, i, paramInt3, paramInt2 + i);
            paramInt2 = paramInt3 - (j + ((LayoutParams)localObject1).leftMargin + paramInt4);
          }
        }
        paramInt1++;
        paramInt3 = paramInt2;
      }
    }
    paramInt2 = getPaddingLeft();
    paramInt1 = paramInt3;
    while (paramInt1 < m)
    {
      localObject1 = getChildAt(paramInt1);
      localObject2 = (LayoutParams)((View)localObject1).getLayoutParams();
      paramInt3 = paramInt2;
      if (((View)localObject1).getVisibility() != 8) {
        if (((LayoutParams)localObject2).isOverflowButton)
        {
          paramInt3 = paramInt2;
        }
        else
        {
          paramInt3 = paramInt2 + ((LayoutParams)localObject2).leftMargin;
          j = ((View)localObject1).getMeasuredWidth();
          i = ((View)localObject1).getMeasuredHeight();
          paramInt2 = k - i / 2;
          ((View)localObject1).layout(paramInt3, paramInt2, paramInt3 + j, i + paramInt2);
          paramInt3 += j + ((LayoutParams)localObject2).rightMargin + paramInt4;
        }
      }
      paramInt1++;
      paramInt2 = paramInt3;
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    boolean bool2 = this.mFormatItems;
    boolean bool1;
    if (View.MeasureSpec.getMode(paramInt1) == 1073741824) {
      bool1 = true;
    } else {
      bool1 = false;
    }
    this.mFormatItems = bool1;
    if (bool2 != this.mFormatItems) {
      this.mFormatItemsWidth = 0;
    }
    int i = View.MeasureSpec.getSize(paramInt1);
    Object localObject;
    if (this.mFormatItems)
    {
      localObject = this.mMenu;
      if ((localObject != null) && (i != this.mFormatItemsWidth))
      {
        this.mFormatItemsWidth = i;
        ((MenuBuilder)localObject).onItemsChanged(true);
      }
    }
    int j = getChildCount();
    if ((this.mFormatItems) && (j > 0))
    {
      onMeasureExactFormat(paramInt1, paramInt2);
    }
    else
    {
      for (i = 0; i < j; i++)
      {
        localObject = (LayoutParams)getChildAt(i).getLayoutParams();
        ((LayoutParams)localObject).rightMargin = 0;
        ((LayoutParams)localObject).leftMargin = 0;
      }
      super.onMeasure(paramInt1, paramInt2);
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public MenuBuilder peekMenu()
  {
    return this.mMenu;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setExpandedActionViewsExclusive(boolean paramBoolean)
  {
    this.mPresenter.setExpandedActionViewsExclusive(paramBoolean);
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setMenuCallbacks(MenuPresenter.Callback paramCallback, MenuBuilder.Callback paramCallback1)
  {
    this.mActionMenuPresenterCallback = paramCallback;
    this.mMenuBuilderCallback = paramCallback1;
  }
  
  public void setOnMenuItemClickListener(OnMenuItemClickListener paramOnMenuItemClickListener)
  {
    this.mOnMenuItemClickListener = paramOnMenuItemClickListener;
  }
  
  public void setOverflowIcon(@Nullable Drawable paramDrawable)
  {
    getMenu();
    this.mPresenter.setOverflowIcon(paramDrawable);
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setOverflowReserved(boolean paramBoolean)
  {
    this.mReserveOverflow = paramBoolean;
  }
  
  public void setPopupTheme(@StyleRes int paramInt)
  {
    if (this.mPopupTheme != paramInt)
    {
      this.mPopupTheme = paramInt;
      if (paramInt == 0) {
        this.mPopupContext = getContext();
      } else {
        this.mPopupContext = new ContextThemeWrapper(getContext(), paramInt);
      }
    }
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public void setPresenter(ActionMenuPresenter paramActionMenuPresenter)
  {
    this.mPresenter = paramActionMenuPresenter;
    this.mPresenter.setMenuView(this);
  }
  
  public boolean showOverflowMenu()
  {
    ActionMenuPresenter localActionMenuPresenter = this.mPresenter;
    boolean bool;
    if ((localActionMenuPresenter != null) && (localActionMenuPresenter.showOverflowMenu())) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  @RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
  public static abstract interface ActionMenuChildView
  {
    public abstract boolean needsDividerAfter();
    
    public abstract boolean needsDividerBefore();
  }
  
  private static class ActionMenuPresenterCallback
    implements MenuPresenter.Callback
  {
    public void onCloseMenu(MenuBuilder paramMenuBuilder, boolean paramBoolean) {}
    
    public boolean onOpenSubMenu(MenuBuilder paramMenuBuilder)
    {
      return false;
    }
  }
  
  public static class LayoutParams
    extends LinearLayoutCompat.LayoutParams
  {
    @ViewDebug.ExportedProperty
    public int cellsUsed;
    @ViewDebug.ExportedProperty
    public boolean expandable;
    boolean expanded;
    @ViewDebug.ExportedProperty
    public int extraPixels;
    @ViewDebug.ExportedProperty
    public boolean isOverflowButton;
    @ViewDebug.ExportedProperty
    public boolean preventEdgeOffset;
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
      this.isOverflowButton = false;
    }
    
    LayoutParams(int paramInt1, int paramInt2, boolean paramBoolean)
    {
      super(paramInt2);
      this.isOverflowButton = paramBoolean;
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.isOverflowButton = paramLayoutParams.isOverflowButton;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
  }
  
  private class MenuBuilderCallback
    implements MenuBuilder.Callback
  {
    MenuBuilderCallback() {}
    
    public boolean onMenuItemSelected(MenuBuilder paramMenuBuilder, MenuItem paramMenuItem)
    {
      boolean bool;
      if ((ActionMenuView.this.mOnMenuItemClickListener != null) && (ActionMenuView.this.mOnMenuItemClickListener.onMenuItemClick(paramMenuItem))) {
        bool = true;
      } else {
        bool = false;
      }
      return bool;
    }
    
    public void onMenuModeChange(MenuBuilder paramMenuBuilder)
    {
      if (ActionMenuView.this.mMenuBuilderCallback != null) {
        ActionMenuView.this.mMenuBuilderCallback.onMenuModeChange(paramMenuBuilder);
      }
    }
  }
  
  public static abstract interface OnMenuItemClickListener
  {
    public abstract boolean onMenuItemClick(MenuItem paramMenuItem);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v7/widget/ActionMenuView.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */