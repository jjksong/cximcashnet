package android.support.v4.text;

public abstract interface TextDirectionHeuristicCompat
{
  public abstract boolean isRtl(CharSequence paramCharSequence, int paramInt1, int paramInt2);
  
  public abstract boolean isRtl(char[] paramArrayOfChar, int paramInt1, int paramInt2);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/text/TextDirectionHeuristicCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */