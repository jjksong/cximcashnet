package android.support.v4.view;

import android.view.View;

public abstract interface OnApplyWindowInsetsListener
{
  public abstract WindowInsetsCompat onApplyWindowInsets(View paramView, WindowInsetsCompat paramWindowInsetsCompat);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/view/OnApplyWindowInsetsListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */