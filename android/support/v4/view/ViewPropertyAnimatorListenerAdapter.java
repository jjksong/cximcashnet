package android.support.v4.view;

import android.view.View;

public class ViewPropertyAnimatorListenerAdapter
  implements ViewPropertyAnimatorListener
{
  public void onAnimationCancel(View paramView) {}
  
  public void onAnimationEnd(View paramView) {}
  
  public void onAnimationStart(View paramView) {}
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/view/ViewPropertyAnimatorListenerAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */