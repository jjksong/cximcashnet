package android.support.v4.view;

import android.view.View;

public abstract interface ViewPropertyAnimatorUpdateListener
{
  public abstract void onAnimationUpdate(View paramView);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/view/ViewPropertyAnimatorUpdateListener.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */