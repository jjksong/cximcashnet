package android.support.v4.print;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.os.CancellationSignal.OnCancelListener;
import android.os.ParcelFileDescriptor;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintAttributes.Builder;
import android.print.PrintAttributes.Margins;
import android.print.PrintAttributes.MediaSize;
import android.print.PrintDocumentAdapter;
import android.print.PrintDocumentAdapter.LayoutResultCallback;
import android.print.PrintDocumentAdapter.WriteResultCallback;
import android.print.PrintDocumentInfo.Builder;
import android.print.PrintManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import java.io.FileNotFoundException;

public final class PrintHelper
{
  @SuppressLint({"InlinedApi"})
  public static final int COLOR_MODE_COLOR = 2;
  @SuppressLint({"InlinedApi"})
  public static final int COLOR_MODE_MONOCHROME = 1;
  static final boolean IS_MIN_MARGINS_HANDLING_CORRECT;
  private static final String LOG_TAG = "PrintHelper";
  private static final int MAX_PRINT_SIZE = 3500;
  public static final int ORIENTATION_LANDSCAPE = 1;
  public static final int ORIENTATION_PORTRAIT = 2;
  static final boolean PRINT_ACTIVITY_RESPECTS_ORIENTATION;
  public static final int SCALE_MODE_FILL = 2;
  public static final int SCALE_MODE_FIT = 1;
  int mColorMode = 2;
  final Context mContext;
  BitmapFactory.Options mDecodeOptions = null;
  final Object mLock = new Object();
  int mOrientation = 1;
  int mScaleMode = 2;
  
  static
  {
    int i = Build.VERSION.SDK_INT;
    boolean bool2 = false;
    if ((i >= 20) && (Build.VERSION.SDK_INT <= 23)) {
      bool1 = false;
    } else {
      bool1 = true;
    }
    PRINT_ACTIVITY_RESPECTS_ORIENTATION = bool1;
    boolean bool1 = bool2;
    if (Build.VERSION.SDK_INT != 23) {
      bool1 = true;
    }
    IS_MIN_MARGINS_HANDLING_CORRECT = bool1;
  }
  
  public PrintHelper(@NonNull Context paramContext)
  {
    this.mContext = paramContext;
  }
  
  static Bitmap convertBitmapForColorMode(Bitmap paramBitmap, int paramInt)
  {
    if (paramInt != 1) {
      return paramBitmap;
    }
    Bitmap localBitmap = Bitmap.createBitmap(paramBitmap.getWidth(), paramBitmap.getHeight(), Bitmap.Config.ARGB_8888);
    Canvas localCanvas = new Canvas(localBitmap);
    Paint localPaint = new Paint();
    ColorMatrix localColorMatrix = new ColorMatrix();
    localColorMatrix.setSaturation(0.0F);
    localPaint.setColorFilter(new ColorMatrixColorFilter(localColorMatrix));
    localCanvas.drawBitmap(paramBitmap, 0.0F, 0.0F, localPaint);
    localCanvas.setBitmap(null);
    return localBitmap;
  }
  
  @RequiresApi(19)
  private static PrintAttributes.Builder copyAttributes(PrintAttributes paramPrintAttributes)
  {
    PrintAttributes.Builder localBuilder = new PrintAttributes.Builder().setMediaSize(paramPrintAttributes.getMediaSize()).setResolution(paramPrintAttributes.getResolution()).setMinMargins(paramPrintAttributes.getMinMargins());
    if (paramPrintAttributes.getColorMode() != 0) {
      localBuilder.setColorMode(paramPrintAttributes.getColorMode());
    }
    if ((Build.VERSION.SDK_INT >= 23) && (paramPrintAttributes.getDuplexMode() != 0)) {
      localBuilder.setDuplexMode(paramPrintAttributes.getDuplexMode());
    }
    return localBuilder;
  }
  
  static Matrix getMatrix(int paramInt1, int paramInt2, RectF paramRectF, int paramInt3)
  {
    Matrix localMatrix = new Matrix();
    float f1 = paramRectF.width();
    float f2 = paramInt1;
    f1 /= f2;
    if (paramInt3 == 2) {
      f1 = Math.max(f1, paramRectF.height() / paramInt2);
    } else {
      f1 = Math.min(f1, paramRectF.height() / paramInt2);
    }
    localMatrix.postScale(f1, f1);
    localMatrix.postTranslate((paramRectF.width() - f2 * f1) / 2.0F, (paramRectF.height() - paramInt2 * f1) / 2.0F);
    return localMatrix;
  }
  
  static boolean isPortrait(Bitmap paramBitmap)
  {
    boolean bool;
    if (paramBitmap.getWidth() <= paramBitmap.getHeight()) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  /* Error */
  private Bitmap loadBitmap(Uri paramUri, BitmapFactory.Options paramOptions)
    throws FileNotFoundException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +93 -> 94
    //   4: aload_0
    //   5: getfield 78	android/support/v4/print/PrintHelper:mContext	Landroid/content/Context;
    //   8: astore 4
    //   10: aload 4
    //   12: ifnull +82 -> 94
    //   15: aconst_null
    //   16: astore_3
    //   17: aload 4
    //   19: invokevirtual 222	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   22: aload_1
    //   23: invokevirtual 228	android/content/ContentResolver:openInputStream	(Landroid/net/Uri;)Ljava/io/InputStream;
    //   26: astore 4
    //   28: aload 4
    //   30: aconst_null
    //   31: aload_2
    //   32: invokestatic 234	android/graphics/BitmapFactory:decodeStream	(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   35: astore_1
    //   36: aload 4
    //   38: ifnull +21 -> 59
    //   41: aload 4
    //   43: invokevirtual 239	java/io/InputStream:close	()V
    //   46: goto +13 -> 59
    //   49: astore_2
    //   50: ldc 33
    //   52: ldc -15
    //   54: aload_2
    //   55: invokestatic 247	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   58: pop
    //   59: aload_1
    //   60: areturn
    //   61: astore_1
    //   62: aload 4
    //   64: astore_2
    //   65: goto +6 -> 71
    //   68: astore_1
    //   69: aload_3
    //   70: astore_2
    //   71: aload_2
    //   72: ifnull +20 -> 92
    //   75: aload_2
    //   76: invokevirtual 239	java/io/InputStream:close	()V
    //   79: goto +13 -> 92
    //   82: astore_2
    //   83: ldc 33
    //   85: ldc -15
    //   87: aload_2
    //   88: invokestatic 247	android/util/Log:w	(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    //   91: pop
    //   92: aload_1
    //   93: athrow
    //   94: new 249	java/lang/IllegalArgumentException
    //   97: dup
    //   98: ldc -5
    //   100: invokespecial 254	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   103: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	104	0	this	PrintHelper
    //   0	104	1	paramUri	Uri
    //   0	104	2	paramOptions	BitmapFactory.Options
    //   16	54	3	localObject1	Object
    //   8	55	4	localObject2	Object
    // Exception table:
    //   from	to	target	type
    //   41	46	49	java/io/IOException
    //   28	36	61	finally
    //   17	28	68	finally
    //   75	79	82	java/io/IOException
  }
  
  public static boolean systemSupportsPrint()
  {
    boolean bool;
    if (Build.VERSION.SDK_INT >= 19) {
      bool = true;
    } else {
      bool = false;
    }
    return bool;
  }
  
  public int getColorMode()
  {
    return this.mColorMode;
  }
  
  public int getOrientation()
  {
    if ((Build.VERSION.SDK_INT >= 19) && (this.mOrientation == 0)) {
      return 1;
    }
    return this.mOrientation;
  }
  
  public int getScaleMode()
  {
    return this.mScaleMode;
  }
  
  /* Error */
  Bitmap loadConstrainedBitmap(Uri arg1)
    throws FileNotFoundException
  {
    // Byte code:
    //   0: aload_1
    //   1: ifnull +226 -> 227
    //   4: aload_0
    //   5: getfield 78	android/support/v4/print/PrintHelper:mContext	Landroid/content/Context;
    //   8: ifnull +219 -> 227
    //   11: new 263	android/graphics/BitmapFactory$Options
    //   14: dup
    //   15: invokespecial 264	android/graphics/BitmapFactory$Options:<init>	()V
    //   18: astore 6
    //   20: aload 6
    //   22: iconst_1
    //   23: putfield 267	android/graphics/BitmapFactory$Options:inJustDecodeBounds	Z
    //   26: aload_0
    //   27: aload_1
    //   28: aload 6
    //   30: invokespecial 269	android/support/v4/print/PrintHelper:loadBitmap	(Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   33: pop
    //   34: aload 6
    //   36: getfield 272	android/graphics/BitmapFactory$Options:outWidth	I
    //   39: istore 4
    //   41: aload 6
    //   43: getfield 275	android/graphics/BitmapFactory$Options:outHeight	I
    //   46: istore 5
    //   48: iload 4
    //   50: ifle +175 -> 225
    //   53: iload 5
    //   55: ifgt +6 -> 61
    //   58: goto +167 -> 225
    //   61: iload 4
    //   63: iload 5
    //   65: invokestatic 278	java/lang/Math:max	(II)I
    //   68: istore_3
    //   69: iconst_1
    //   70: istore_2
    //   71: iload_3
    //   72: sipush 3500
    //   75: if_icmple +14 -> 89
    //   78: iload_3
    //   79: iconst_1
    //   80: iushr
    //   81: istore_3
    //   82: iload_2
    //   83: iconst_1
    //   84: ishl
    //   85: istore_2
    //   86: goto -15 -> 71
    //   89: iload_2
    //   90: ifle +133 -> 223
    //   93: iload 4
    //   95: iload 5
    //   97: invokestatic 280	java/lang/Math:min	(II)I
    //   100: iload_2
    //   101: idiv
    //   102: ifgt +6 -> 108
    //   105: goto +118 -> 223
    //   108: aload_0
    //   109: getfield 70	android/support/v4/print/PrintHelper:mLock	Ljava/lang/Object;
    //   112: astore 6
    //   114: aload 6
    //   116: monitorenter
    //   117: new 263	android/graphics/BitmapFactory$Options
    //   120: astore 7
    //   122: aload 7
    //   124: invokespecial 264	android/graphics/BitmapFactory$Options:<init>	()V
    //   127: aload_0
    //   128: aload 7
    //   130: putfield 68	android/support/v4/print/PrintHelper:mDecodeOptions	Landroid/graphics/BitmapFactory$Options;
    //   133: aload_0
    //   134: getfield 68	android/support/v4/print/PrintHelper:mDecodeOptions	Landroid/graphics/BitmapFactory$Options;
    //   137: iconst_1
    //   138: putfield 283	android/graphics/BitmapFactory$Options:inMutable	Z
    //   141: aload_0
    //   142: getfield 68	android/support/v4/print/PrintHelper:mDecodeOptions	Landroid/graphics/BitmapFactory$Options;
    //   145: iload_2
    //   146: putfield 286	android/graphics/BitmapFactory$Options:inSampleSize	I
    //   149: aload_0
    //   150: getfield 68	android/support/v4/print/PrintHelper:mDecodeOptions	Landroid/graphics/BitmapFactory$Options;
    //   153: astore 7
    //   155: aload 6
    //   157: monitorexit
    //   158: aload_0
    //   159: aload_1
    //   160: aload 7
    //   162: invokespecial 269	android/support/v4/print/PrintHelper:loadBitmap	(Landroid/net/Uri;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    //   165: astore 6
    //   167: aload_0
    //   168: getfield 70	android/support/v4/print/PrintHelper:mLock	Ljava/lang/Object;
    //   171: astore_1
    //   172: aload_1
    //   173: monitorenter
    //   174: aload_0
    //   175: aconst_null
    //   176: putfield 68	android/support/v4/print/PrintHelper:mDecodeOptions	Landroid/graphics/BitmapFactory$Options;
    //   179: aload_1
    //   180: monitorexit
    //   181: aload 6
    //   183: areturn
    //   184: astore 6
    //   186: aload_1
    //   187: monitorexit
    //   188: aload 6
    //   190: athrow
    //   191: astore 6
    //   193: aload_0
    //   194: getfield 70	android/support/v4/print/PrintHelper:mLock	Ljava/lang/Object;
    //   197: astore_1
    //   198: aload_1
    //   199: monitorenter
    //   200: aload_0
    //   201: aconst_null
    //   202: putfield 68	android/support/v4/print/PrintHelper:mDecodeOptions	Landroid/graphics/BitmapFactory$Options;
    //   205: aload_1
    //   206: monitorexit
    //   207: aload 6
    //   209: athrow
    //   210: astore 6
    //   212: aload_1
    //   213: monitorexit
    //   214: aload 6
    //   216: athrow
    //   217: astore_1
    //   218: aload 6
    //   220: monitorexit
    //   221: aload_1
    //   222: athrow
    //   223: aconst_null
    //   224: areturn
    //   225: aconst_null
    //   226: areturn
    //   227: new 249	java/lang/IllegalArgumentException
    //   230: dup
    //   231: ldc_w 288
    //   234: invokespecial 254	java/lang/IllegalArgumentException:<init>	(Ljava/lang/String;)V
    //   237: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	238	0	this	PrintHelper
    //   70	76	2	i	int
    //   68	14	3	j	int
    //   39	55	4	k	int
    //   46	50	5	m	int
    //   18	164	6	localObject1	Object
    //   184	5	6	localObject2	Object
    //   191	17	6	localObject3	Object
    //   210	9	6	localObject4	Object
    //   120	41	7	localOptions	BitmapFactory.Options
    // Exception table:
    //   from	to	target	type
    //   174	181	184	finally
    //   186	188	184	finally
    //   158	167	191	finally
    //   200	207	210	finally
    //   212	214	210	finally
    //   117	158	217	finally
    //   218	221	217	finally
  }
  
  public void printBitmap(@NonNull String paramString, @NonNull Bitmap paramBitmap)
  {
    printBitmap(paramString, paramBitmap, null);
  }
  
  public void printBitmap(@NonNull String paramString, @NonNull Bitmap paramBitmap, @Nullable OnPrintFinishCallback paramOnPrintFinishCallback)
  {
    if ((Build.VERSION.SDK_INT >= 19) && (paramBitmap != null))
    {
      PrintManager localPrintManager = (PrintManager)this.mContext.getSystemService("print");
      if (isPortrait(paramBitmap)) {
        localObject = PrintAttributes.MediaSize.UNKNOWN_PORTRAIT;
      } else {
        localObject = PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE;
      }
      Object localObject = new PrintAttributes.Builder().setMediaSize((PrintAttributes.MediaSize)localObject).setColorMode(this.mColorMode).build();
      localPrintManager.print(paramString, new PrintBitmapAdapter(paramString, this.mScaleMode, paramBitmap, paramOnPrintFinishCallback), (PrintAttributes)localObject);
      return;
    }
  }
  
  public void printBitmap(@NonNull String paramString, @NonNull Uri paramUri)
    throws FileNotFoundException
  {
    printBitmap(paramString, paramUri, null);
  }
  
  public void printBitmap(@NonNull String paramString, @NonNull Uri paramUri, @Nullable OnPrintFinishCallback paramOnPrintFinishCallback)
    throws FileNotFoundException
  {
    if (Build.VERSION.SDK_INT < 19) {
      return;
    }
    PrintUriAdapter localPrintUriAdapter = new PrintUriAdapter(paramString, paramUri, paramOnPrintFinishCallback, this.mScaleMode);
    paramOnPrintFinishCallback = (PrintManager)this.mContext.getSystemService("print");
    paramUri = new PrintAttributes.Builder();
    paramUri.setColorMode(this.mColorMode);
    int i = this.mOrientation;
    if ((i != 1) && (i != 0))
    {
      if (i == 2) {
        paramUri.setMediaSize(PrintAttributes.MediaSize.UNKNOWN_PORTRAIT);
      }
    }
    else {
      paramUri.setMediaSize(PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE);
    }
    paramOnPrintFinishCallback.print(paramString, localPrintUriAdapter, paramUri.build());
  }
  
  public void setColorMode(int paramInt)
  {
    this.mColorMode = paramInt;
  }
  
  public void setOrientation(int paramInt)
  {
    this.mOrientation = paramInt;
  }
  
  public void setScaleMode(int paramInt)
  {
    this.mScaleMode = paramInt;
  }
  
  @RequiresApi(19)
  void writeBitmap(final PrintAttributes paramPrintAttributes, final int paramInt, final Bitmap paramBitmap, final ParcelFileDescriptor paramParcelFileDescriptor, final CancellationSignal paramCancellationSignal, final PrintDocumentAdapter.WriteResultCallback paramWriteResultCallback)
  {
    final PrintAttributes localPrintAttributes;
    if (IS_MIN_MARGINS_HANDLING_CORRECT) {
      localPrintAttributes = paramPrintAttributes;
    } else {
      localPrintAttributes = copyAttributes(paramPrintAttributes).setMinMargins(new PrintAttributes.Margins(0, 0, 0, 0)).build();
    }
    new AsyncTask()
    {
      /* Error */
      protected Throwable doInBackground(Void... paramAnonymousVarArgs)
      {
        // Byte code:
        //   0: aload_0
        //   1: getfield 31	android/support/v4/print/PrintHelper$1:val$cancellationSignal	Landroid/os/CancellationSignal;
        //   4: invokevirtual 64	android/os/CancellationSignal:isCanceled	()Z
        //   7: ifeq +5 -> 12
        //   10: aconst_null
        //   11: areturn
        //   12: new 66	android/print/pdf/PrintedPdfDocument
        //   15: astore 4
        //   17: aload 4
        //   19: aload_0
        //   20: getfield 29	android/support/v4/print/PrintHelper$1:this$0	Landroid/support/v4/print/PrintHelper;
        //   23: getfield 70	android/support/v4/print/PrintHelper:mContext	Landroid/content/Context;
        //   26: aload_0
        //   27: getfield 33	android/support/v4/print/PrintHelper$1:val$pdfAttributes	Landroid/print/PrintAttributes;
        //   30: invokespecial 73	android/print/pdf/PrintedPdfDocument:<init>	(Landroid/content/Context;Landroid/print/PrintAttributes;)V
        //   33: aload_0
        //   34: getfield 35	android/support/v4/print/PrintHelper$1:val$bitmap	Landroid/graphics/Bitmap;
        //   37: aload_0
        //   38: getfield 33	android/support/v4/print/PrintHelper$1:val$pdfAttributes	Landroid/print/PrintAttributes;
        //   41: invokevirtual 79	android/print/PrintAttributes:getColorMode	()I
        //   44: invokestatic 83	android/support/v4/print/PrintHelper:convertBitmapForColorMode	(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
        //   47: astore_3
        //   48: aload_0
        //   49: getfield 31	android/support/v4/print/PrintHelper$1:val$cancellationSignal	Landroid/os/CancellationSignal;
        //   52: invokevirtual 64	android/os/CancellationSignal:isCanceled	()Z
        //   55: istore_2
        //   56: iload_2
        //   57: ifeq +5 -> 62
        //   60: aconst_null
        //   61: areturn
        //   62: aload 4
        //   64: iconst_1
        //   65: invokevirtual 87	android/print/pdf/PrintedPdfDocument:startPage	(I)Landroid/graphics/pdf/PdfDocument$Page;
        //   68: astore 5
        //   70: getstatic 91	android/support/v4/print/PrintHelper:IS_MIN_MARGINS_HANDLING_CORRECT	Z
        //   73: ifeq +22 -> 95
        //   76: new 93	android/graphics/RectF
        //   79: astore_1
        //   80: aload_1
        //   81: aload 5
        //   83: invokevirtual 99	android/graphics/pdf/PdfDocument$Page:getInfo	()Landroid/graphics/pdf/PdfDocument$PageInfo;
        //   86: invokevirtual 105	android/graphics/pdf/PdfDocument$PageInfo:getContentRect	()Landroid/graphics/Rect;
        //   89: invokespecial 108	android/graphics/RectF:<init>	(Landroid/graphics/Rect;)V
        //   92: goto +60 -> 152
        //   95: new 66	android/print/pdf/PrintedPdfDocument
        //   98: astore 7
        //   100: aload 7
        //   102: aload_0
        //   103: getfield 29	android/support/v4/print/PrintHelper$1:this$0	Landroid/support/v4/print/PrintHelper;
        //   106: getfield 70	android/support/v4/print/PrintHelper:mContext	Landroid/content/Context;
        //   109: aload_0
        //   110: getfield 37	android/support/v4/print/PrintHelper$1:val$attributes	Landroid/print/PrintAttributes;
        //   113: invokespecial 73	android/print/pdf/PrintedPdfDocument:<init>	(Landroid/content/Context;Landroid/print/PrintAttributes;)V
        //   116: aload 7
        //   118: iconst_1
        //   119: invokevirtual 87	android/print/pdf/PrintedPdfDocument:startPage	(I)Landroid/graphics/pdf/PdfDocument$Page;
        //   122: astore 6
        //   124: new 93	android/graphics/RectF
        //   127: astore_1
        //   128: aload_1
        //   129: aload 6
        //   131: invokevirtual 99	android/graphics/pdf/PdfDocument$Page:getInfo	()Landroid/graphics/pdf/PdfDocument$PageInfo;
        //   134: invokevirtual 105	android/graphics/pdf/PdfDocument$PageInfo:getContentRect	()Landroid/graphics/Rect;
        //   137: invokespecial 108	android/graphics/RectF:<init>	(Landroid/graphics/Rect;)V
        //   140: aload 7
        //   142: aload 6
        //   144: invokevirtual 112	android/print/pdf/PrintedPdfDocument:finishPage	(Landroid/graphics/pdf/PdfDocument$Page;)V
        //   147: aload 7
        //   149: invokevirtual 115	android/print/pdf/PrintedPdfDocument:close	()V
        //   152: aload_3
        //   153: invokevirtual 120	android/graphics/Bitmap:getWidth	()I
        //   156: aload_3
        //   157: invokevirtual 123	android/graphics/Bitmap:getHeight	()I
        //   160: aload_1
        //   161: aload_0
        //   162: getfield 39	android/support/v4/print/PrintHelper$1:val$fittingMode	I
        //   165: invokestatic 127	android/support/v4/print/PrintHelper:getMatrix	(IILandroid/graphics/RectF;I)Landroid/graphics/Matrix;
        //   168: astore 6
        //   170: getstatic 91	android/support/v4/print/PrintHelper:IS_MIN_MARGINS_HANDLING_CORRECT	Z
        //   173: ifeq +6 -> 179
        //   176: goto +27 -> 203
        //   179: aload 6
        //   181: aload_1
        //   182: getfield 131	android/graphics/RectF:left	F
        //   185: aload_1
        //   186: getfield 134	android/graphics/RectF:top	F
        //   189: invokevirtual 140	android/graphics/Matrix:postTranslate	(FF)Z
        //   192: pop
        //   193: aload 5
        //   195: invokevirtual 144	android/graphics/pdf/PdfDocument$Page:getCanvas	()Landroid/graphics/Canvas;
        //   198: aload_1
        //   199: invokevirtual 150	android/graphics/Canvas:clipRect	(Landroid/graphics/RectF;)Z
        //   202: pop
        //   203: aload 5
        //   205: invokevirtual 144	android/graphics/pdf/PdfDocument$Page:getCanvas	()Landroid/graphics/Canvas;
        //   208: aload_3
        //   209: aload 6
        //   211: aconst_null
        //   212: invokevirtual 154	android/graphics/Canvas:drawBitmap	(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V
        //   215: aload 4
        //   217: aload 5
        //   219: invokevirtual 112	android/print/pdf/PrintedPdfDocument:finishPage	(Landroid/graphics/pdf/PdfDocument$Page;)V
        //   222: aload_0
        //   223: getfield 31	android/support/v4/print/PrintHelper$1:val$cancellationSignal	Landroid/os/CancellationSignal;
        //   226: invokevirtual 64	android/os/CancellationSignal:isCanceled	()Z
        //   229: istore_2
        //   230: iload_2
        //   231: ifeq +38 -> 269
        //   234: aload 4
        //   236: invokevirtual 115	android/print/pdf/PrintedPdfDocument:close	()V
        //   239: aload_0
        //   240: getfield 41	android/support/v4/print/PrintHelper$1:val$fileDescriptor	Landroid/os/ParcelFileDescriptor;
        //   243: astore_1
        //   244: aload_1
        //   245: ifnull +10 -> 255
        //   248: aload_0
        //   249: getfield 41	android/support/v4/print/PrintHelper$1:val$fileDescriptor	Landroid/os/ParcelFileDescriptor;
        //   252: invokevirtual 157	android/os/ParcelFileDescriptor:close	()V
        //   255: aload_3
        //   256: aload_0
        //   257: getfield 35	android/support/v4/print/PrintHelper$1:val$bitmap	Landroid/graphics/Bitmap;
        //   260: if_acmpeq +7 -> 267
        //   263: aload_3
        //   264: invokevirtual 160	android/graphics/Bitmap:recycle	()V
        //   267: aconst_null
        //   268: areturn
        //   269: new 162	java/io/FileOutputStream
        //   272: astore_1
        //   273: aload_1
        //   274: aload_0
        //   275: getfield 41	android/support/v4/print/PrintHelper$1:val$fileDescriptor	Landroid/os/ParcelFileDescriptor;
        //   278: invokevirtual 166	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
        //   281: invokespecial 169	java/io/FileOutputStream:<init>	(Ljava/io/FileDescriptor;)V
        //   284: aload 4
        //   286: aload_1
        //   287: invokevirtual 173	android/print/pdf/PrintedPdfDocument:writeTo	(Ljava/io/OutputStream;)V
        //   290: aload 4
        //   292: invokevirtual 115	android/print/pdf/PrintedPdfDocument:close	()V
        //   295: aload_0
        //   296: getfield 41	android/support/v4/print/PrintHelper$1:val$fileDescriptor	Landroid/os/ParcelFileDescriptor;
        //   299: astore_1
        //   300: aload_1
        //   301: ifnull +10 -> 311
        //   304: aload_0
        //   305: getfield 41	android/support/v4/print/PrintHelper$1:val$fileDescriptor	Landroid/os/ParcelFileDescriptor;
        //   308: invokevirtual 157	android/os/ParcelFileDescriptor:close	()V
        //   311: aload_3
        //   312: aload_0
        //   313: getfield 35	android/support/v4/print/PrintHelper$1:val$bitmap	Landroid/graphics/Bitmap;
        //   316: if_acmpeq +7 -> 323
        //   319: aload_3
        //   320: invokevirtual 160	android/graphics/Bitmap:recycle	()V
        //   323: aconst_null
        //   324: areturn
        //   325: astore_1
        //   326: aload 4
        //   328: invokevirtual 115	android/print/pdf/PrintedPdfDocument:close	()V
        //   331: aload_0
        //   332: getfield 41	android/support/v4/print/PrintHelper$1:val$fileDescriptor	Landroid/os/ParcelFileDescriptor;
        //   335: astore 4
        //   337: aload 4
        //   339: ifnull +10 -> 349
        //   342: aload_0
        //   343: getfield 41	android/support/v4/print/PrintHelper$1:val$fileDescriptor	Landroid/os/ParcelFileDescriptor;
        //   346: invokevirtual 157	android/os/ParcelFileDescriptor:close	()V
        //   349: aload_3
        //   350: aload_0
        //   351: getfield 35	android/support/v4/print/PrintHelper$1:val$bitmap	Landroid/graphics/Bitmap;
        //   354: if_acmpeq +7 -> 361
        //   357: aload_3
        //   358: invokevirtual 160	android/graphics/Bitmap:recycle	()V
        //   361: aload_1
        //   362: athrow
        //   363: astore_1
        //   364: aload_1
        //   365: areturn
        //   366: astore_1
        //   367: goto -112 -> 255
        //   370: astore_1
        //   371: goto -60 -> 311
        //   374: astore 4
        //   376: goto -27 -> 349
        // Local variable table:
        //   start	length	slot	name	signature
        //   0	379	0	this	1
        //   0	379	1	paramAnonymousVarArgs	Void[]
        //   55	176	2	bool	boolean
        //   47	311	3	localBitmap	Bitmap
        //   15	323	4	localObject1	Object
        //   374	1	4	localIOException	java.io.IOException
        //   68	150	5	localPage	android.graphics.pdf.PdfDocument.Page
        //   122	88	6	localObject2	Object
        //   98	50	7	localPrintedPdfDocument	android.print.pdf.PrintedPdfDocument
        // Exception table:
        //   from	to	target	type
        //   62	92	325	finally
        //   95	152	325	finally
        //   152	176	325	finally
        //   179	203	325	finally
        //   203	230	325	finally
        //   269	290	325	finally
        //   0	10	363	java/lang/Throwable
        //   12	56	363	java/lang/Throwable
        //   234	244	363	java/lang/Throwable
        //   248	255	363	java/lang/Throwable
        //   255	267	363	java/lang/Throwable
        //   290	300	363	java/lang/Throwable
        //   304	311	363	java/lang/Throwable
        //   311	323	363	java/lang/Throwable
        //   326	337	363	java/lang/Throwable
        //   342	349	363	java/lang/Throwable
        //   349	361	363	java/lang/Throwable
        //   361	363	363	java/lang/Throwable
        //   248	255	366	java/io/IOException
        //   304	311	370	java/io/IOException
        //   342	349	374	java/io/IOException
      }
      
      protected void onPostExecute(Throwable paramAnonymousThrowable)
      {
        if (paramCancellationSignal.isCanceled())
        {
          paramWriteResultCallback.onWriteCancelled();
        }
        else if (paramAnonymousThrowable == null)
        {
          paramWriteResultCallback.onWriteFinished(new PageRange[] { PageRange.ALL_PAGES });
        }
        else
        {
          Log.e("PrintHelper", "Error writing printed content", paramAnonymousThrowable);
          paramWriteResultCallback.onWriteFailed(null);
        }
      }
    }.execute(new Void[0]);
  }
  
  public static abstract interface OnPrintFinishCallback
  {
    public abstract void onFinish();
  }
  
  @RequiresApi(19)
  private class PrintBitmapAdapter
    extends PrintDocumentAdapter
  {
    private PrintAttributes mAttributes;
    private final Bitmap mBitmap;
    private final PrintHelper.OnPrintFinishCallback mCallback;
    private final int mFittingMode;
    private final String mJobName;
    
    PrintBitmapAdapter(String paramString, int paramInt, Bitmap paramBitmap, PrintHelper.OnPrintFinishCallback paramOnPrintFinishCallback)
    {
      this.mJobName = paramString;
      this.mFittingMode = paramInt;
      this.mBitmap = paramBitmap;
      this.mCallback = paramOnPrintFinishCallback;
    }
    
    public void onFinish()
    {
      PrintHelper.OnPrintFinishCallback localOnPrintFinishCallback = this.mCallback;
      if (localOnPrintFinishCallback != null) {
        localOnPrintFinishCallback.onFinish();
      }
    }
    
    public void onLayout(PrintAttributes paramPrintAttributes1, PrintAttributes paramPrintAttributes2, CancellationSignal paramCancellationSignal, PrintDocumentAdapter.LayoutResultCallback paramLayoutResultCallback, Bundle paramBundle)
    {
      this.mAttributes = paramPrintAttributes2;
      paramLayoutResultCallback.onLayoutFinished(new PrintDocumentInfo.Builder(this.mJobName).setContentType(1).setPageCount(1).build(), paramPrintAttributes2.equals(paramPrintAttributes1) ^ true);
    }
    
    public void onWrite(PageRange[] paramArrayOfPageRange, ParcelFileDescriptor paramParcelFileDescriptor, CancellationSignal paramCancellationSignal, PrintDocumentAdapter.WriteResultCallback paramWriteResultCallback)
    {
      PrintHelper.this.writeBitmap(this.mAttributes, this.mFittingMode, this.mBitmap, paramParcelFileDescriptor, paramCancellationSignal, paramWriteResultCallback);
    }
  }
  
  @RequiresApi(19)
  private class PrintUriAdapter
    extends PrintDocumentAdapter
  {
    PrintAttributes mAttributes;
    Bitmap mBitmap;
    final PrintHelper.OnPrintFinishCallback mCallback;
    final int mFittingMode;
    final Uri mImageFile;
    final String mJobName;
    AsyncTask<Uri, Boolean, Bitmap> mLoadBitmap;
    
    PrintUriAdapter(String paramString, Uri paramUri, PrintHelper.OnPrintFinishCallback paramOnPrintFinishCallback, int paramInt)
    {
      this.mJobName = paramString;
      this.mImageFile = paramUri;
      this.mCallback = paramOnPrintFinishCallback;
      this.mFittingMode = paramInt;
      this.mBitmap = null;
    }
    
    void cancelLoad()
    {
      synchronized (PrintHelper.this.mLock)
      {
        if (PrintHelper.this.mDecodeOptions != null)
        {
          if (Build.VERSION.SDK_INT < 24) {
            PrintHelper.this.mDecodeOptions.requestCancelDecode();
          }
          PrintHelper.this.mDecodeOptions = null;
        }
        return;
      }
    }
    
    public void onFinish()
    {
      super.onFinish();
      cancelLoad();
      Object localObject = this.mLoadBitmap;
      if (localObject != null) {
        ((AsyncTask)localObject).cancel(true);
      }
      localObject = this.mCallback;
      if (localObject != null) {
        ((PrintHelper.OnPrintFinishCallback)localObject).onFinish();
      }
      localObject = this.mBitmap;
      if (localObject != null)
      {
        ((Bitmap)localObject).recycle();
        this.mBitmap = null;
      }
    }
    
    public void onLayout(final PrintAttributes paramPrintAttributes1, final PrintAttributes paramPrintAttributes2, final CancellationSignal paramCancellationSignal, final PrintDocumentAdapter.LayoutResultCallback paramLayoutResultCallback, Bundle paramBundle)
    {
      try
      {
        this.mAttributes = paramPrintAttributes2;
        if (paramCancellationSignal.isCanceled())
        {
          paramLayoutResultCallback.onLayoutCancelled();
          return;
        }
        if (this.mBitmap != null)
        {
          paramLayoutResultCallback.onLayoutFinished(new PrintDocumentInfo.Builder(this.mJobName).setContentType(1).setPageCount(1).build(), paramPrintAttributes2.equals(paramPrintAttributes1) ^ true);
          return;
        }
        this.mLoadBitmap = new AsyncTask()
        {
          protected Bitmap doInBackground(Uri... paramAnonymousVarArgs)
          {
            try
            {
              paramAnonymousVarArgs = PrintHelper.this.loadConstrainedBitmap(PrintHelper.PrintUriAdapter.this.mImageFile);
              return paramAnonymousVarArgs;
            }
            catch (FileNotFoundException paramAnonymousVarArgs) {}
            return null;
          }
          
          protected void onCancelled(Bitmap paramAnonymousBitmap)
          {
            paramLayoutResultCallback.onLayoutCancelled();
            PrintHelper.PrintUriAdapter.this.mLoadBitmap = null;
          }
          
          protected void onPostExecute(Bitmap paramAnonymousBitmap)
          {
            super.onPostExecute(paramAnonymousBitmap);
            Object localObject = paramAnonymousBitmap;
            if (paramAnonymousBitmap != null) {
              if (PrintHelper.PRINT_ACTIVITY_RESPECTS_ORIENTATION)
              {
                localObject = paramAnonymousBitmap;
                if (PrintHelper.this.mOrientation != 0) {}
              }
              else
              {
                try
                {
                  PrintAttributes.MediaSize localMediaSize = PrintHelper.PrintUriAdapter.this.mAttributes.getMediaSize();
                  localObject = paramAnonymousBitmap;
                  if (localMediaSize != null)
                  {
                    localObject = paramAnonymousBitmap;
                    if (localMediaSize.isPortrait() != PrintHelper.isPortrait(paramAnonymousBitmap))
                    {
                      localObject = new Matrix();
                      ((Matrix)localObject).postRotate(90.0F);
                      localObject = Bitmap.createBitmap(paramAnonymousBitmap, 0, 0, paramAnonymousBitmap.getWidth(), paramAnonymousBitmap.getHeight(), (Matrix)localObject, true);
                    }
                  }
                }
                finally {}
              }
            }
            paramAnonymousBitmap = PrintHelper.PrintUriAdapter.this;
            paramAnonymousBitmap.mBitmap = ((Bitmap)localObject);
            if (localObject != null)
            {
              paramAnonymousBitmap = new PrintDocumentInfo.Builder(paramAnonymousBitmap.mJobName).setContentType(1).setPageCount(1).build();
              boolean bool = paramPrintAttributes2.equals(paramPrintAttributes1);
              paramLayoutResultCallback.onLayoutFinished(paramAnonymousBitmap, true ^ bool);
            }
            else
            {
              paramLayoutResultCallback.onLayoutFailed(null);
            }
            PrintHelper.PrintUriAdapter.this.mLoadBitmap = null;
          }
          
          protected void onPreExecute()
          {
            paramCancellationSignal.setOnCancelListener(new CancellationSignal.OnCancelListener()
            {
              public void onCancel()
              {
                PrintHelper.PrintUriAdapter.this.cancelLoad();
                PrintHelper.PrintUriAdapter.1.this.cancel(false);
              }
            });
          }
        }.execute(new Uri[0]);
        return;
      }
      finally {}
    }
    
    public void onWrite(PageRange[] paramArrayOfPageRange, ParcelFileDescriptor paramParcelFileDescriptor, CancellationSignal paramCancellationSignal, PrintDocumentAdapter.WriteResultCallback paramWriteResultCallback)
    {
      PrintHelper.this.writeBitmap(this.mAttributes, this.mFittingMode, this.mBitmap, paramParcelFileDescriptor, paramCancellationSignal, paramWriteResultCallback);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/print/PrintHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */