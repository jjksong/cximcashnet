package android.support.v4.app;

import android.util.AndroidRuntimeException;

final class SuperNotCalledException
  extends AndroidRuntimeException
{
  public SuperNotCalledException(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/app/SuperNotCalledException.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */