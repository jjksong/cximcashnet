package android.support.v4.util;

public abstract interface Consumer<T>
{
  public abstract void accept(T paramT);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/util/Consumer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */