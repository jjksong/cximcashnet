package android.support.v4.graphics;

import android.graphics.Path;
import android.graphics.PointF;
import android.support.annotation.FloatRange;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public final class PathUtils
{
  @NonNull
  @RequiresApi(26)
  public static Collection<PathSegment> flatten(@NonNull Path paramPath)
  {
    return flatten(paramPath, 0.5F);
  }
  
  @NonNull
  @RequiresApi(26)
  public static Collection<PathSegment> flatten(@NonNull Path paramPath, @FloatRange(from=0.0D) float paramFloat)
  {
    float[] arrayOfFloat = paramPath.approximate(paramFloat);
    int j = arrayOfFloat.length / 3;
    paramPath = new ArrayList(j);
    for (int i = 1; i < j; i++)
    {
      int m = i * 3;
      int k = (i - 1) * 3;
      paramFloat = arrayOfFloat[m];
      float f5 = arrayOfFloat[(m + 1)];
      float f4 = arrayOfFloat[(m + 2)];
      float f3 = arrayOfFloat[k];
      float f1 = arrayOfFloat[(k + 1)];
      float f2 = arrayOfFloat[(k + 2)];
      if ((paramFloat != f3) && ((f5 != f1) || (f4 != f2))) {
        paramPath.add(new PathSegment(new PointF(f1, f2), f3, new PointF(f5, f4), paramFloat));
      }
    }
    return paramPath;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/graphics/PathUtils.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */