package android.support.v4.graphics;

import android.content.Context;
import android.content.res.Resources;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.RestrictTo;
import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class TypefaceCompatUtil
{
  private static final String CACHE_FILE_PREFIX = ".font";
  private static final String TAG = "TypefaceCompatUtil";
  
  public static void closeQuietly(Closeable paramCloseable)
  {
    if (paramCloseable != null) {}
    try
    {
      paramCloseable.close();
      return;
    }
    catch (IOException paramCloseable)
    {
      for (;;) {}
    }
  }
  
  @Nullable
  @RequiresApi(19)
  public static ByteBuffer copyToDirectBuffer(Context paramContext, Resources paramResources, int paramInt)
  {
    paramContext = getTempFile(paramContext);
    if (paramContext == null) {
      return null;
    }
    try
    {
      boolean bool = copyToFile(paramContext, paramResources, paramInt);
      if (!bool) {
        return null;
      }
      paramResources = mmap(paramContext);
      return paramResources;
    }
    finally
    {
      paramContext.delete();
    }
  }
  
  /* Error */
  public static boolean copyToFile(File paramFile, Resources paramResources, int paramInt)
  {
    // Byte code:
    //   0: aload_1
    //   1: iload_2
    //   2: invokevirtual 59	android/content/res/Resources:openRawResource	(I)Ljava/io/InputStream;
    //   5: astore_1
    //   6: aload_0
    //   7: aload_1
    //   8: invokestatic 62	android/support/v4/graphics/TypefaceCompatUtil:copyToFile	(Ljava/io/File;Ljava/io/InputStream;)Z
    //   11: istore_3
    //   12: aload_1
    //   13: invokestatic 64	android/support/v4/graphics/TypefaceCompatUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   16: iload_3
    //   17: ireturn
    //   18: astore_0
    //   19: goto +6 -> 25
    //   22: astore_0
    //   23: aconst_null
    //   24: astore_1
    //   25: aload_1
    //   26: invokestatic 64	android/support/v4/graphics/TypefaceCompatUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   29: aload_0
    //   30: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	31	0	paramFile	File
    //   0	31	1	paramResources	Resources
    //   0	31	2	paramInt	int
    //   11	6	3	bool	boolean
    // Exception table:
    //   from	to	target	type
    //   6	12	18	finally
    //   0	6	22	finally
  }
  
  /* Error */
  public static boolean copyToFile(File paramFile, java.io.InputStream paramInputStream)
  {
    // Byte code:
    //   0: invokestatic 70	android/os/StrictMode:allowThreadDiskWrites	()Landroid/os/StrictMode$ThreadPolicy;
    //   3: astore 7
    //   5: aconst_null
    //   6: astore 5
    //   8: aconst_null
    //   9: astore 6
    //   11: aload 6
    //   13: astore_3
    //   14: new 72	java/io/FileOutputStream
    //   17: astore 4
    //   19: aload 6
    //   21: astore_3
    //   22: aload 4
    //   24: aload_0
    //   25: iconst_0
    //   26: invokespecial 75	java/io/FileOutputStream:<init>	(Ljava/io/File;Z)V
    //   29: sipush 1024
    //   32: newarray <illegal type>
    //   34: astore_0
    //   35: aload_1
    //   36: aload_0
    //   37: invokevirtual 81	java/io/InputStream:read	([B)I
    //   40: istore_2
    //   41: iload_2
    //   42: iconst_m1
    //   43: if_icmpeq +14 -> 57
    //   46: aload 4
    //   48: aload_0
    //   49: iconst_0
    //   50: iload_2
    //   51: invokevirtual 85	java/io/FileOutputStream:write	([BII)V
    //   54: goto -19 -> 35
    //   57: aload 4
    //   59: invokestatic 64	android/support/v4/graphics/TypefaceCompatUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   62: aload 7
    //   64: invokestatic 89	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
    //   67: iconst_1
    //   68: ireturn
    //   69: astore_0
    //   70: aload 4
    //   72: astore_3
    //   73: goto +78 -> 151
    //   76: astore_1
    //   77: aload 4
    //   79: astore_0
    //   80: goto +11 -> 91
    //   83: astore_0
    //   84: goto +67 -> 151
    //   87: astore_1
    //   88: aload 5
    //   90: astore_0
    //   91: aload_0
    //   92: astore_3
    //   93: new 91	java/lang/StringBuilder
    //   96: astore 4
    //   98: aload_0
    //   99: astore_3
    //   100: aload 4
    //   102: invokespecial 92	java/lang/StringBuilder:<init>	()V
    //   105: aload_0
    //   106: astore_3
    //   107: aload 4
    //   109: ldc 94
    //   111: invokevirtual 98	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   114: pop
    //   115: aload_0
    //   116: astore_3
    //   117: aload 4
    //   119: aload_1
    //   120: invokevirtual 102	java/io/IOException:getMessage	()Ljava/lang/String;
    //   123: invokevirtual 98	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   126: pop
    //   127: aload_0
    //   128: astore_3
    //   129: ldc 15
    //   131: aload 4
    //   133: invokevirtual 105	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   136: invokestatic 111	android/util/Log:e	(Ljava/lang/String;Ljava/lang/String;)I
    //   139: pop
    //   140: aload_0
    //   141: invokestatic 64	android/support/v4/graphics/TypefaceCompatUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   144: aload 7
    //   146: invokestatic 89	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
    //   149: iconst_0
    //   150: ireturn
    //   151: aload_3
    //   152: invokestatic 64	android/support/v4/graphics/TypefaceCompatUtil:closeQuietly	(Ljava/io/Closeable;)V
    //   155: aload 7
    //   157: invokestatic 89	android/os/StrictMode:setThreadPolicy	(Landroid/os/StrictMode$ThreadPolicy;)V
    //   160: aload_0
    //   161: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	162	0	paramFile	File
    //   0	162	1	paramInputStream	java.io.InputStream
    //   40	11	2	i	int
    //   13	139	3	localObject1	Object
    //   17	115	4	localObject2	Object
    //   6	83	5	localObject3	Object
    //   9	11	6	localObject4	Object
    //   3	153	7	localThreadPolicy	android.os.StrictMode.ThreadPolicy
    // Exception table:
    //   from	to	target	type
    //   29	35	69	finally
    //   35	41	69	finally
    //   46	54	69	finally
    //   29	35	76	java/io/IOException
    //   35	41	76	java/io/IOException
    //   46	54	76	java/io/IOException
    //   14	19	83	finally
    //   22	29	83	finally
    //   93	98	83	finally
    //   100	105	83	finally
    //   107	115	83	finally
    //   117	127	83	finally
    //   129	140	83	finally
    //   14	19	87	java/io/IOException
    //   22	29	87	java/io/IOException
  }
  
  @Nullable
  public static File getTempFile(Context paramContext)
  {
    Object localObject1 = new StringBuilder();
    ((StringBuilder)localObject1).append(".font");
    ((StringBuilder)localObject1).append(Process.myPid());
    ((StringBuilder)localObject1).append("-");
    ((StringBuilder)localObject1).append(Process.myTid());
    ((StringBuilder)localObject1).append("-");
    localObject1 = ((StringBuilder)localObject1).toString();
    for (int i = 0; i < 100; i++)
    {
      File localFile = paramContext.getCacheDir();
      Object localObject2 = new StringBuilder();
      ((StringBuilder)localObject2).append((String)localObject1);
      ((StringBuilder)localObject2).append(i);
      localObject2 = new File(localFile, ((StringBuilder)localObject2).toString());
      try
      {
        boolean bool = ((File)localObject2).createNewFile();
        if (bool) {
          return (File)localObject2;
        }
      }
      catch (IOException localIOException)
      {
        for (;;) {}
      }
    }
    return null;
  }
  
  /* Error */
  @Nullable
  @RequiresApi(19)
  public static ByteBuffer mmap(Context paramContext, android.os.CancellationSignal paramCancellationSignal, android.net.Uri paramUri)
  {
    // Byte code:
    //   0: aload_0
    //   1: invokevirtual 144	android/content/Context:getContentResolver	()Landroid/content/ContentResolver;
    //   4: astore_0
    //   5: aload_0
    //   6: aload_2
    //   7: ldc -110
    //   9: aload_1
    //   10: invokevirtual 152	android/content/ContentResolver:openFileDescriptor	(Landroid/net/Uri;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/os/ParcelFileDescriptor;
    //   13: astore_2
    //   14: aload_2
    //   15: ifnonnull +13 -> 28
    //   18: aload_2
    //   19: ifnull +7 -> 26
    //   22: aload_2
    //   23: invokevirtual 155	android/os/ParcelFileDescriptor:close	()V
    //   26: aconst_null
    //   27: areturn
    //   28: new 157	java/io/FileInputStream
    //   31: astore 5
    //   33: aload 5
    //   35: aload_2
    //   36: invokevirtual 161	android/os/ParcelFileDescriptor:getFileDescriptor	()Ljava/io/FileDescriptor;
    //   39: invokespecial 164	java/io/FileInputStream:<init>	(Ljava/io/FileDescriptor;)V
    //   42: aload 5
    //   44: invokevirtual 168	java/io/FileInputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   47: astore_0
    //   48: aload_0
    //   49: invokevirtual 174	java/nio/channels/FileChannel:size	()J
    //   52: lstore_3
    //   53: aload_0
    //   54: getstatic 180	java/nio/channels/FileChannel$MapMode:READ_ONLY	Ljava/nio/channels/FileChannel$MapMode;
    //   57: lconst_0
    //   58: lload_3
    //   59: invokevirtual 184	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   62: astore_0
    //   63: aload 5
    //   65: invokevirtual 185	java/io/FileInputStream:close	()V
    //   68: aload_2
    //   69: ifnull +7 -> 76
    //   72: aload_2
    //   73: invokevirtual 155	android/os/ParcelFileDescriptor:close	()V
    //   76: aload_0
    //   77: areturn
    //   78: astore_1
    //   79: aconst_null
    //   80: astore_0
    //   81: goto +7 -> 88
    //   84: astore_0
    //   85: aload_0
    //   86: athrow
    //   87: astore_1
    //   88: aload_0
    //   89: ifnull +22 -> 111
    //   92: aload 5
    //   94: invokevirtual 185	java/io/FileInputStream:close	()V
    //   97: goto +19 -> 116
    //   100: astore 5
    //   102: aload_0
    //   103: aload 5
    //   105: invokevirtual 189	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   108: goto +8 -> 116
    //   111: aload 5
    //   113: invokevirtual 185	java/io/FileInputStream:close	()V
    //   116: aload_1
    //   117: athrow
    //   118: astore_1
    //   119: aconst_null
    //   120: astore_0
    //   121: goto +7 -> 128
    //   124: astore_0
    //   125: aload_0
    //   126: athrow
    //   127: astore_1
    //   128: aload_2
    //   129: ifnull +27 -> 156
    //   132: aload_0
    //   133: ifnull +19 -> 152
    //   136: aload_2
    //   137: invokevirtual 155	android/os/ParcelFileDescriptor:close	()V
    //   140: goto +16 -> 156
    //   143: astore_2
    //   144: aload_0
    //   145: aload_2
    //   146: invokevirtual 189	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   149: goto +7 -> 156
    //   152: aload_2
    //   153: invokevirtual 155	android/os/ParcelFileDescriptor:close	()V
    //   156: aload_1
    //   157: athrow
    //   158: astore_0
    //   159: aconst_null
    //   160: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	161	0	paramContext	Context
    //   0	161	1	paramCancellationSignal	android.os.CancellationSignal
    //   0	161	2	paramUri	android.net.Uri
    //   52	7	3	l	long
    //   31	62	5	localFileInputStream	java.io.FileInputStream
    //   100	12	5	localThrowable	Throwable
    // Exception table:
    //   from	to	target	type
    //   42	63	78	finally
    //   42	63	84	java/lang/Throwable
    //   85	87	87	finally
    //   92	97	100	java/lang/Throwable
    //   28	42	118	finally
    //   63	68	118	finally
    //   92	97	118	finally
    //   102	108	118	finally
    //   111	116	118	finally
    //   116	118	118	finally
    //   28	42	124	java/lang/Throwable
    //   63	68	124	java/lang/Throwable
    //   102	108	124	java/lang/Throwable
    //   111	116	124	java/lang/Throwable
    //   116	118	124	java/lang/Throwable
    //   125	127	127	finally
    //   136	140	143	java/lang/Throwable
    //   5	14	158	java/io/IOException
    //   22	26	158	java/io/IOException
    //   72	76	158	java/io/IOException
    //   136	140	158	java/io/IOException
    //   144	149	158	java/io/IOException
    //   152	156	158	java/io/IOException
    //   156	158	158	java/io/IOException
  }
  
  /* Error */
  @Nullable
  @RequiresApi(19)
  private static ByteBuffer mmap(File paramFile)
  {
    // Byte code:
    //   0: new 157	java/io/FileInputStream
    //   3: astore 4
    //   5: aload 4
    //   7: aload_0
    //   8: invokespecial 192	java/io/FileInputStream:<init>	(Ljava/io/File;)V
    //   11: aload 4
    //   13: invokevirtual 168	java/io/FileInputStream:getChannel	()Ljava/nio/channels/FileChannel;
    //   16: astore_0
    //   17: aload_0
    //   18: invokevirtual 174	java/nio/channels/FileChannel:size	()J
    //   21: lstore_1
    //   22: aload_0
    //   23: getstatic 180	java/nio/channels/FileChannel$MapMode:READ_ONLY	Ljava/nio/channels/FileChannel$MapMode;
    //   26: lconst_0
    //   27: lload_1
    //   28: invokevirtual 184	java/nio/channels/FileChannel:map	(Ljava/nio/channels/FileChannel$MapMode;JJ)Ljava/nio/MappedByteBuffer;
    //   31: astore_0
    //   32: aload 4
    //   34: invokevirtual 185	java/io/FileInputStream:close	()V
    //   37: aload_0
    //   38: areturn
    //   39: astore_0
    //   40: aconst_null
    //   41: astore_3
    //   42: goto +7 -> 49
    //   45: astore_3
    //   46: aload_3
    //   47: athrow
    //   48: astore_0
    //   49: aload_3
    //   50: ifnull +22 -> 72
    //   53: aload 4
    //   55: invokevirtual 185	java/io/FileInputStream:close	()V
    //   58: goto +19 -> 77
    //   61: astore 4
    //   63: aload_3
    //   64: aload 4
    //   66: invokevirtual 189	java/lang/Throwable:addSuppressed	(Ljava/lang/Throwable;)V
    //   69: goto +8 -> 77
    //   72: aload 4
    //   74: invokevirtual 185	java/io/FileInputStream:close	()V
    //   77: aload_0
    //   78: athrow
    //   79: astore_0
    //   80: aconst_null
    //   81: areturn
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	82	0	paramFile	File
    //   21	7	1	l	long
    //   41	1	3	localObject	Object
    //   45	19	3	localThrowable1	Throwable
    //   3	51	4	localFileInputStream	java.io.FileInputStream
    //   61	12	4	localThrowable2	Throwable
    // Exception table:
    //   from	to	target	type
    //   11	32	39	finally
    //   11	32	45	java/lang/Throwable
    //   46	48	48	finally
    //   53	58	61	java/lang/Throwable
    //   0	11	79	java/io/IOException
    //   32	37	79	java/io/IOException
    //   53	58	79	java/io/IOException
    //   63	69	79	java/io/IOException
    //   72	77	79	java/io/IOException
    //   77	79	79	java/io/IOException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/v4/graphics/TypefaceCompatUtil.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */