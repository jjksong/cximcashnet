package android.support.graphics.drawable;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.Resources.Theme;
import android.support.annotation.RestrictTo;
import android.util.Xml;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AnticipateInterpolator;
import android.view.animation.AnticipateOvershootInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.CycleInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public class AnimationUtilsCompat
{
  private static Interpolator createInterpolatorFromXml(Context paramContext, Resources paramResources, Resources.Theme paramTheme, XmlPullParser paramXmlPullParser)
    throws XmlPullParserException, IOException
  {
    int i = paramXmlPullParser.getDepth();
    paramResources = null;
    for (;;)
    {
      int j = paramXmlPullParser.next();
      if (((j == 3) && (paramXmlPullParser.getDepth() <= i)) || (j == 1)) {
        return paramResources;
      }
      if (j == 2)
      {
        paramTheme = Xml.asAttributeSet(paramXmlPullParser);
        paramResources = paramXmlPullParser.getName();
        if (paramResources.equals("linearInterpolator"))
        {
          paramResources = new LinearInterpolator();
        }
        else if (paramResources.equals("accelerateInterpolator"))
        {
          paramResources = new AccelerateInterpolator(paramContext, paramTheme);
        }
        else if (paramResources.equals("decelerateInterpolator"))
        {
          paramResources = new DecelerateInterpolator(paramContext, paramTheme);
        }
        else if (paramResources.equals("accelerateDecelerateInterpolator"))
        {
          paramResources = new AccelerateDecelerateInterpolator();
        }
        else if (paramResources.equals("cycleInterpolator"))
        {
          paramResources = new CycleInterpolator(paramContext, paramTheme);
        }
        else if (paramResources.equals("anticipateInterpolator"))
        {
          paramResources = new AnticipateInterpolator(paramContext, paramTheme);
        }
        else if (paramResources.equals("overshootInterpolator"))
        {
          paramResources = new OvershootInterpolator(paramContext, paramTheme);
        }
        else if (paramResources.equals("anticipateOvershootInterpolator"))
        {
          paramResources = new AnticipateOvershootInterpolator(paramContext, paramTheme);
        }
        else if (paramResources.equals("bounceInterpolator"))
        {
          paramResources = new BounceInterpolator();
        }
        else
        {
          if (!paramResources.equals("pathInterpolator")) {
            break;
          }
          paramResources = new PathInterpolatorCompat(paramContext, paramTheme, paramXmlPullParser);
        }
      }
    }
    paramContext = new StringBuilder();
    paramContext.append("Unknown interpolator name: ");
    paramContext.append(paramXmlPullParser.getName());
    throw new RuntimeException(paramContext.toString());
    return paramResources;
  }
  
  /* Error */
  public static Interpolator loadInterpolator(Context paramContext, int paramInt)
    throws android.content.res.Resources.NotFoundException
  {
    // Byte code:
    //   0: getstatic 126	android/os/Build$VERSION:SDK_INT	I
    //   3: bipush 21
    //   5: if_icmplt +9 -> 14
    //   8: aload_0
    //   9: iload_1
    //   10: invokestatic 130	android/view/animation/AnimationUtils:loadInterpolator	(Landroid/content/Context;I)Landroid/view/animation/Interpolator;
    //   13: areturn
    //   14: aconst_null
    //   15: astore_3
    //   16: aconst_null
    //   17: astore 4
    //   19: aconst_null
    //   20: astore_2
    //   21: iload_1
    //   22: ldc -125
    //   24: if_icmpne +25 -> 49
    //   27: new 133	android/support/v4/view/animation/FastOutLinearInInterpolator
    //   30: dup
    //   31: invokespecial 134	android/support/v4/view/animation/FastOutLinearInInterpolator:<init>	()V
    //   34: areturn
    //   35: astore_0
    //   36: goto +225 -> 261
    //   39: astore 5
    //   41: goto +85 -> 126
    //   44: astore 5
    //   46: goto +146 -> 192
    //   49: iload_1
    //   50: ldc -121
    //   52: if_icmpne +11 -> 63
    //   55: new 137	android/support/v4/view/animation/FastOutSlowInInterpolator
    //   58: dup
    //   59: invokespecial 138	android/support/v4/view/animation/FastOutSlowInInterpolator:<init>	()V
    //   62: areturn
    //   63: iload_1
    //   64: ldc -117
    //   66: if_icmpne +11 -> 77
    //   69: new 141	android/support/v4/view/animation/LinearOutSlowInInterpolator
    //   72: dup
    //   73: invokespecial 142	android/support/v4/view/animation/LinearOutSlowInInterpolator:<init>	()V
    //   76: areturn
    //   77: aload_0
    //   78: invokevirtual 148	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   81: iload_1
    //   82: invokevirtual 154	android/content/res/Resources:getAnimation	(I)Landroid/content/res/XmlResourceParser;
    //   85: astore 5
    //   87: aload 5
    //   89: astore_2
    //   90: aload 5
    //   92: astore_3
    //   93: aload 5
    //   95: astore 4
    //   97: aload_0
    //   98: aload_0
    //   99: invokevirtual 148	android/content/Context:getResources	()Landroid/content/res/Resources;
    //   102: aload_0
    //   103: invokevirtual 158	android/content/Context:getTheme	()Landroid/content/res/Resources$Theme;
    //   106: aload 5
    //   108: invokestatic 160	android/support/graphics/drawable/AnimationUtilsCompat:createInterpolatorFromXml	(Landroid/content/Context;Landroid/content/res/Resources;Landroid/content/res/Resources$Theme;Lorg/xmlpull/v1/XmlPullParser;)Landroid/view/animation/Interpolator;
    //   111: astore_0
    //   112: aload 5
    //   114: ifnull +10 -> 124
    //   117: aload 5
    //   119: invokeinterface 165 1 0
    //   124: aload_0
    //   125: areturn
    //   126: aload_3
    //   127: astore_2
    //   128: new 120	android/content/res/Resources$NotFoundException
    //   131: astore_0
    //   132: aload_3
    //   133: astore_2
    //   134: new 100	java/lang/StringBuilder
    //   137: astore 4
    //   139: aload_3
    //   140: astore_2
    //   141: aload 4
    //   143: invokespecial 101	java/lang/StringBuilder:<init>	()V
    //   146: aload_3
    //   147: astore_2
    //   148: aload 4
    //   150: ldc -89
    //   152: invokevirtual 107	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   155: pop
    //   156: aload_3
    //   157: astore_2
    //   158: aload 4
    //   160: iload_1
    //   161: invokestatic 173	java/lang/Integer:toHexString	(I)Ljava/lang/String;
    //   164: invokevirtual 107	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   167: pop
    //   168: aload_3
    //   169: astore_2
    //   170: aload_0
    //   171: aload 4
    //   173: invokevirtual 112	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   176: invokespecial 174	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
    //   179: aload_3
    //   180: astore_2
    //   181: aload_0
    //   182: aload 5
    //   184: invokevirtual 178	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   187: pop
    //   188: aload_3
    //   189: astore_2
    //   190: aload_0
    //   191: athrow
    //   192: aload 4
    //   194: astore_2
    //   195: new 120	android/content/res/Resources$NotFoundException
    //   198: astore_3
    //   199: aload 4
    //   201: astore_2
    //   202: new 100	java/lang/StringBuilder
    //   205: astore_0
    //   206: aload 4
    //   208: astore_2
    //   209: aload_0
    //   210: invokespecial 101	java/lang/StringBuilder:<init>	()V
    //   213: aload 4
    //   215: astore_2
    //   216: aload_0
    //   217: ldc -89
    //   219: invokevirtual 107	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   222: pop
    //   223: aload 4
    //   225: astore_2
    //   226: aload_0
    //   227: iload_1
    //   228: invokestatic 173	java/lang/Integer:toHexString	(I)Ljava/lang/String;
    //   231: invokevirtual 107	java/lang/StringBuilder:append	(Ljava/lang/String;)Ljava/lang/StringBuilder;
    //   234: pop
    //   235: aload 4
    //   237: astore_2
    //   238: aload_3
    //   239: aload_0
    //   240: invokevirtual 112	java/lang/StringBuilder:toString	()Ljava/lang/String;
    //   243: invokespecial 174	android/content/res/Resources$NotFoundException:<init>	(Ljava/lang/String;)V
    //   246: aload 4
    //   248: astore_2
    //   249: aload_3
    //   250: aload 5
    //   252: invokevirtual 178	android/content/res/Resources$NotFoundException:initCause	(Ljava/lang/Throwable;)Ljava/lang/Throwable;
    //   255: pop
    //   256: aload 4
    //   258: astore_2
    //   259: aload_3
    //   260: athrow
    //   261: aload_2
    //   262: ifnull +9 -> 271
    //   265: aload_2
    //   266: invokeinterface 165 1 0
    //   271: aload_0
    //   272: athrow
    // Local variable table:
    //   start	length	slot	name	signature
    //   0	273	0	paramContext	Context
    //   0	273	1	paramInt	int
    //   20	246	2	localObject1	Object
    //   15	245	3	localObject2	Object
    //   17	240	4	localObject3	Object
    //   39	1	5	localIOException	IOException
    //   44	1	5	localXmlPullParserException	XmlPullParserException
    //   85	166	5	localXmlResourceParser	android.content.res.XmlResourceParser
    // Exception table:
    //   from	to	target	type
    //   27	35	35	finally
    //   55	63	35	finally
    //   69	77	35	finally
    //   77	87	35	finally
    //   97	112	35	finally
    //   128	132	35	finally
    //   134	139	35	finally
    //   141	146	35	finally
    //   148	156	35	finally
    //   158	168	35	finally
    //   170	179	35	finally
    //   181	188	35	finally
    //   190	192	35	finally
    //   195	199	35	finally
    //   202	206	35	finally
    //   209	213	35	finally
    //   216	223	35	finally
    //   226	235	35	finally
    //   238	246	35	finally
    //   249	256	35	finally
    //   259	261	35	finally
    //   27	35	39	java/io/IOException
    //   55	63	39	java/io/IOException
    //   69	77	39	java/io/IOException
    //   77	87	39	java/io/IOException
    //   97	112	39	java/io/IOException
    //   27	35	44	org/xmlpull/v1/XmlPullParserException
    //   55	63	44	org/xmlpull/v1/XmlPullParserException
    //   69	77	44	org/xmlpull/v1/XmlPullParserException
    //   77	87	44	org/xmlpull/v1/XmlPullParserException
    //   97	112	44	org/xmlpull/v1/XmlPullParserException
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/graphics/drawable/AnimationUtilsCompat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */