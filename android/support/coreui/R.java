package android.support.coreui;

public final class R
{
  public static final class attr
  {
    public static final int alpha = 2130837550;
    public static final int coordinatorLayoutStyle = 2130837655;
    public static final int font = 2130837688;
    public static final int fontProviderAuthority = 2130837690;
    public static final int fontProviderCerts = 2130837691;
    public static final int fontProviderFetchStrategy = 2130837692;
    public static final int fontProviderFetchTimeout = 2130837693;
    public static final int fontProviderPackage = 2130837694;
    public static final int fontProviderQuery = 2130837695;
    public static final int fontStyle = 2130837696;
    public static final int fontVariationSettings = 2130837697;
    public static final int fontWeight = 2130837698;
    public static final int keylines = 2130837734;
    public static final int layout_anchor = 2130837738;
    public static final int layout_anchorGravity = 2130837739;
    public static final int layout_behavior = 2130837740;
    public static final int layout_dodgeInsetEdges = 2130837782;
    public static final int layout_insetEdge = 2130837791;
    public static final int layout_keyline = 2130837792;
    public static final int statusBarBackground = 2130837909;
    public static final int ttcIndex = 2130837965;
  }
  
  public static final class color
  {
    public static final int notification_action_color_filter = 2130968724;
    public static final int notification_icon_bg_color = 2130968725;
    public static final int ripple_material_light = 2130968742;
    public static final int secondary_text_default_material_light = 2130968744;
  }
  
  public static final class dimen
  {
    public static final int compat_button_inset_horizontal_material = 2131034212;
    public static final int compat_button_inset_vertical_material = 2131034213;
    public static final int compat_button_padding_horizontal_material = 2131034214;
    public static final int compat_button_padding_vertical_material = 2131034215;
    public static final int compat_control_corner_material = 2131034216;
    public static final int compat_notification_large_icon_max_height = 2131034217;
    public static final int compat_notification_large_icon_max_width = 2131034218;
    public static final int notification_action_icon_size = 2131034263;
    public static final int notification_action_text_size = 2131034264;
    public static final int notification_big_circle_margin = 2131034265;
    public static final int notification_content_margin_start = 2131034266;
    public static final int notification_large_icon_height = 2131034267;
    public static final int notification_large_icon_width = 2131034268;
    public static final int notification_main_column_padding_top = 2131034269;
    public static final int notification_media_narrow_margin = 2131034270;
    public static final int notification_right_icon_size = 2131034271;
    public static final int notification_right_side_padding_top = 2131034272;
    public static final int notification_small_icon_background_padding = 2131034273;
    public static final int notification_small_icon_size_as_large = 2131034274;
    public static final int notification_subtext_size = 2131034275;
    public static final int notification_top_pad = 2131034276;
    public static final int notification_top_pad_large_text = 2131034277;
  }
  
  public static final class drawable
  {
    public static final int notification_action_background = 2131099887;
    public static final int notification_bg = 2131099888;
    public static final int notification_bg_low = 2131099889;
    public static final int notification_bg_low_normal = 2131099890;
    public static final int notification_bg_low_pressed = 2131099891;
    public static final int notification_bg_normal = 2131099892;
    public static final int notification_bg_normal_pressed = 2131099893;
    public static final int notification_icon_background = 2131099894;
    public static final int notification_template_icon_bg = 2131099895;
    public static final int notification_template_icon_low_bg = 2131099896;
    public static final int notification_tile_bg = 2131099897;
    public static final int notify_panel_notification_icon_bg = 2131099898;
  }
  
  public static final class id
  {
    public static final int action_container = 2131165208;
    public static final int action_divider = 2131165210;
    public static final int action_image = 2131165211;
    public static final int action_text = 2131165217;
    public static final int actions = 2131165219;
    public static final int async = 2131165244;
    public static final int blocking = 2131165282;
    public static final int bottom = 2131165284;
    public static final int chronometer = 2131165337;
    public static final int end = 2131165421;
    public static final int forever = 2131165438;
    public static final int icon = 2131165473;
    public static final int icon_group = 2131165474;
    public static final int info = 2131165497;
    public static final int italic = 2131165510;
    public static final int left = 2131165528;
    public static final int line1 = 2131165532;
    public static final int line3 = 2131165533;
    public static final int none = 2131165608;
    public static final int normal = 2131165609;
    public static final int notification_background = 2131165612;
    public static final int notification_main_column = 2131165613;
    public static final int notification_main_column_container = 2131165614;
    public static final int right = 2131165713;
    public static final int right_icon = 2131165714;
    public static final int right_side = 2131165716;
    public static final int start = 2131165791;
    public static final int tag_transition_group = 2131165804;
    public static final int tag_unhandled_key_event_manager = 2131165805;
    public static final int tag_unhandled_key_listeners = 2131165806;
    public static final int text = 2131165812;
    public static final int text2 = 2131165814;
    public static final int time = 2131165817;
    public static final int title = 2131165822;
    public static final int top = 2131165837;
  }
  
  public static final class integer
  {
    public static final int status_bar_notification_info_maxnum = 2131230724;
  }
  
  public static final class layout
  {
    public static final int notification_action = 2131296457;
    public static final int notification_action_tombstone = 2131296458;
    public static final int notification_template_custom_big = 2131296465;
    public static final int notification_template_icon_group = 2131296466;
    public static final int notification_template_part_chronometer = 2131296470;
    public static final int notification_template_part_time = 2131296471;
  }
  
  public static final class string
  {
    public static final int status_bar_notification_info_overflow = 2131558555;
  }
  
  public static final class style
  {
    public static final int TextAppearance_Compat_Notification = 2131624192;
    public static final int TextAppearance_Compat_Notification_Info = 2131624193;
    public static final int TextAppearance_Compat_Notification_Line2 = 2131624195;
    public static final int TextAppearance_Compat_Notification_Time = 2131624198;
    public static final int TextAppearance_Compat_Notification_Title = 2131624200;
    public static final int Widget_Compat_NotificationActionContainer = 2131624308;
    public static final int Widget_Compat_NotificationActionText = 2131624309;
    public static final int Widget_Support_CoordinatorLayout = 2131624310;
  }
  
  public static final class styleable
  {
    public static final int[] ColorStateListItem = { 16843173, 16843551, 2130837550 };
    public static final int ColorStateListItem_alpha = 2;
    public static final int ColorStateListItem_android_alpha = 1;
    public static final int ColorStateListItem_android_color = 0;
    public static final int[] CoordinatorLayout = { 2130837734, 2130837909 };
    public static final int[] CoordinatorLayout_Layout = { 16842931, 2130837738, 2130837739, 2130837740, 2130837782, 2130837791, 2130837792 };
    public static final int CoordinatorLayout_Layout_android_layout_gravity = 0;
    public static final int CoordinatorLayout_Layout_layout_anchor = 1;
    public static final int CoordinatorLayout_Layout_layout_anchorGravity = 2;
    public static final int CoordinatorLayout_Layout_layout_behavior = 3;
    public static final int CoordinatorLayout_Layout_layout_dodgeInsetEdges = 4;
    public static final int CoordinatorLayout_Layout_layout_insetEdge = 5;
    public static final int CoordinatorLayout_Layout_layout_keyline = 6;
    public static final int CoordinatorLayout_keylines = 0;
    public static final int CoordinatorLayout_statusBarBackground = 1;
    public static final int[] FontFamily = { 2130837690, 2130837691, 2130837692, 2130837693, 2130837694, 2130837695 };
    public static final int[] FontFamilyFont = { 16844082, 16844083, 16844095, 16844143, 16844144, 2130837688, 2130837696, 2130837697, 2130837698, 2130837965 };
    public static final int FontFamilyFont_android_font = 0;
    public static final int FontFamilyFont_android_fontStyle = 2;
    public static final int FontFamilyFont_android_fontVariationSettings = 4;
    public static final int FontFamilyFont_android_fontWeight = 1;
    public static final int FontFamilyFont_android_ttcIndex = 3;
    public static final int FontFamilyFont_font = 5;
    public static final int FontFamilyFont_fontStyle = 6;
    public static final int FontFamilyFont_fontVariationSettings = 7;
    public static final int FontFamilyFont_fontWeight = 8;
    public static final int FontFamilyFont_ttcIndex = 9;
    public static final int FontFamily_fontProviderAuthority = 0;
    public static final int FontFamily_fontProviderCerts = 1;
    public static final int FontFamily_fontProviderFetchStrategy = 2;
    public static final int FontFamily_fontProviderFetchTimeout = 3;
    public static final int FontFamily_fontProviderPackage = 4;
    public static final int FontFamily_fontProviderQuery = 5;
    public static final int[] GradientColor = { 16843165, 16843166, 16843169, 16843170, 16843171, 16843172, 16843265, 16843275, 16844048, 16844049, 16844050, 16844051 };
    public static final int[] GradientColorItem = { 16843173, 16844052 };
    public static final int GradientColorItem_android_color = 0;
    public static final int GradientColorItem_android_offset = 1;
    public static final int GradientColor_android_centerColor = 7;
    public static final int GradientColor_android_centerX = 3;
    public static final int GradientColor_android_centerY = 4;
    public static final int GradientColor_android_endColor = 1;
    public static final int GradientColor_android_endX = 10;
    public static final int GradientColor_android_endY = 11;
    public static final int GradientColor_android_gradientRadius = 5;
    public static final int GradientColor_android_startColor = 0;
    public static final int GradientColor_android_startX = 8;
    public static final int GradientColor_android_startY = 9;
    public static final int GradientColor_android_tileMode = 6;
    public static final int GradientColor_android_type = 2;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/coreui/R.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */