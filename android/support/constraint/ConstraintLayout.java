package android.support.constraint;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build.VERSION;
import android.support.constraint.solver.Metrics;
import android.support.constraint.solver.widgets.Analyzer;
import android.support.constraint.solver.widgets.ConstraintAnchor;
import android.support.constraint.solver.widgets.ConstraintAnchor.Type;
import android.support.constraint.solver.widgets.ConstraintWidget;
import android.support.constraint.solver.widgets.ConstraintWidget.DimensionBehaviour;
import android.support.constraint.solver.widgets.ConstraintWidgetContainer;
import android.support.constraint.solver.widgets.ResolutionAnchor;
import android.support.constraint.solver.widgets.ResolutionDimension;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseIntArray;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import java.util.ArrayList;
import java.util.HashMap;

public class ConstraintLayout
  extends ViewGroup
{
  static final boolean ALLOWS_EMBEDDED = false;
  private static final boolean CACHE_MEASURED_DIMENSION = false;
  private static final boolean DEBUG = false;
  public static final int DESIGN_INFO_ID = 0;
  private static final String TAG = "ConstraintLayout";
  private static final boolean USE_CONSTRAINTS_HELPER = true;
  public static final String VERSION = "ConstraintLayout-1.1.3";
  SparseArray<View> mChildrenByIds = new SparseArray();
  private ArrayList<ConstraintHelper> mConstraintHelpers = new ArrayList(4);
  private ConstraintSet mConstraintSet = null;
  private int mConstraintSetId = -1;
  private HashMap<String, Integer> mDesignIds = new HashMap();
  private boolean mDirtyHierarchy = true;
  private int mLastMeasureHeight = -1;
  int mLastMeasureHeightMode = 0;
  int mLastMeasureHeightSize = -1;
  private int mLastMeasureWidth = -1;
  int mLastMeasureWidthMode = 0;
  int mLastMeasureWidthSize = -1;
  ConstraintWidgetContainer mLayoutWidget = new ConstraintWidgetContainer();
  private int mMaxHeight = Integer.MAX_VALUE;
  private int mMaxWidth = Integer.MAX_VALUE;
  private Metrics mMetrics;
  private int mMinHeight = 0;
  private int mMinWidth = 0;
  private int mOptimizationLevel = 7;
  private final ArrayList<ConstraintWidget> mVariableDimensionsWidgets = new ArrayList(100);
  
  public ConstraintLayout(Context paramContext)
  {
    super(paramContext);
    init(null);
  }
  
  public ConstraintLayout(Context paramContext, AttributeSet paramAttributeSet)
  {
    super(paramContext, paramAttributeSet);
    init(paramAttributeSet);
  }
  
  public ConstraintLayout(Context paramContext, AttributeSet paramAttributeSet, int paramInt)
  {
    super(paramContext, paramAttributeSet, paramInt);
    init(paramAttributeSet);
  }
  
  private final ConstraintWidget getTargetWidget(int paramInt)
  {
    if (paramInt == 0) {
      return this.mLayoutWidget;
    }
    View localView = (View)this.mChildrenByIds.get(paramInt);
    Object localObject = localView;
    if (localView == null)
    {
      localView = findViewById(paramInt);
      localObject = localView;
      if (localView != null)
      {
        localObject = localView;
        if (localView != this)
        {
          localObject = localView;
          if (localView.getParent() == this)
          {
            onViewAdded(localView);
            localObject = localView;
          }
        }
      }
    }
    if (localObject == this) {
      return this.mLayoutWidget;
    }
    if (localObject == null) {
      localObject = null;
    } else {
      localObject = ((LayoutParams)((View)localObject).getLayoutParams()).widget;
    }
    return (ConstraintWidget)localObject;
  }
  
  private void init(AttributeSet paramAttributeSet)
  {
    this.mLayoutWidget.setCompanionWidget(this);
    this.mChildrenByIds.put(getId(), this);
    this.mConstraintSet = null;
    if (paramAttributeSet != null)
    {
      paramAttributeSet = getContext().obtainStyledAttributes(paramAttributeSet, R.styleable.ConstraintLayout_Layout);
      int j = paramAttributeSet.getIndexCount();
      for (int i = 0; i < j; i++)
      {
        int k = paramAttributeSet.getIndex(i);
        if (k == R.styleable.ConstraintLayout_Layout_android_minWidth)
        {
          this.mMinWidth = paramAttributeSet.getDimensionPixelOffset(k, this.mMinWidth);
        }
        else if (k == R.styleable.ConstraintLayout_Layout_android_minHeight)
        {
          this.mMinHeight = paramAttributeSet.getDimensionPixelOffset(k, this.mMinHeight);
        }
        else if (k == R.styleable.ConstraintLayout_Layout_android_maxWidth)
        {
          this.mMaxWidth = paramAttributeSet.getDimensionPixelOffset(k, this.mMaxWidth);
        }
        else if (k == R.styleable.ConstraintLayout_Layout_android_maxHeight)
        {
          this.mMaxHeight = paramAttributeSet.getDimensionPixelOffset(k, this.mMaxHeight);
        }
        else if (k == R.styleable.ConstraintLayout_Layout_layout_optimizationLevel)
        {
          this.mOptimizationLevel = paramAttributeSet.getInt(k, this.mOptimizationLevel);
        }
        else if (k == R.styleable.ConstraintLayout_Layout_constraintSet)
        {
          k = paramAttributeSet.getResourceId(k, 0);
          try
          {
            ConstraintSet localConstraintSet = new android/support/constraint/ConstraintSet;
            localConstraintSet.<init>();
            this.mConstraintSet = localConstraintSet;
            this.mConstraintSet.load(getContext(), k);
          }
          catch (Resources.NotFoundException localNotFoundException)
          {
            this.mConstraintSet = null;
          }
          this.mConstraintSetId = k;
        }
      }
      paramAttributeSet.recycle();
    }
    this.mLayoutWidget.setOptimizationLevel(this.mOptimizationLevel);
  }
  
  private void internalMeasureChildren(int paramInt1, int paramInt2)
  {
    int i5 = getPaddingTop() + getPaddingBottom();
    int i3 = getPaddingLeft() + getPaddingRight();
    int i4 = getChildCount();
    for (int n = 0; n < i4; n++)
    {
      View localView = getChildAt(n);
      if (localView.getVisibility() != 8)
      {
        LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
        ConstraintWidget localConstraintWidget = localLayoutParams.widget;
        if ((!localLayoutParams.isGuideline) && (!localLayoutParams.isHelper))
        {
          localConstraintWidget.setVisibility(localView.getVisibility());
          int i1 = localLayoutParams.width;
          int i2 = localLayoutParams.height;
          int i;
          if ((!localLayoutParams.horizontalDimensionFixed) && (!localLayoutParams.verticalDimensionFixed) && ((localLayoutParams.horizontalDimensionFixed) || (localLayoutParams.matchConstraintDefaultWidth != 1)) && (localLayoutParams.width != -1) && ((localLayoutParams.verticalDimensionFixed) || ((localLayoutParams.matchConstraintDefaultHeight != 1) && (localLayoutParams.height != -1)))) {
            i = 0;
          } else {
            i = 1;
          }
          int k;
          int m;
          int j;
          if (i != 0)
          {
            if (i1 == 0)
            {
              k = getChildMeasureSpec(paramInt1, i3, -2);
              i = 1;
            }
            else if (i1 == -1)
            {
              k = getChildMeasureSpec(paramInt1, i3, -1);
              i = 0;
            }
            else
            {
              if (i1 == -2) {
                i = 1;
              } else {
                i = 0;
              }
              k = getChildMeasureSpec(paramInt1, i3, i1);
            }
            if (i2 == 0)
            {
              m = getChildMeasureSpec(paramInt2, i5, -2);
              j = 1;
            }
            else if (i2 == -1)
            {
              m = getChildMeasureSpec(paramInt2, i5, -1);
              j = 0;
            }
            else
            {
              if (i2 == -2) {
                j = 1;
              } else {
                j = 0;
              }
              m = getChildMeasureSpec(paramInt2, i5, i2);
            }
            localView.measure(k, m);
            Metrics localMetrics = this.mMetrics;
            if (localMetrics != null) {
              localMetrics.measures += 1L;
            }
            boolean bool;
            if (i1 == -2) {
              bool = true;
            } else {
              bool = false;
            }
            localConstraintWidget.setWidthWrapContent(bool);
            if (i2 == -2) {
              bool = true;
            } else {
              bool = false;
            }
            localConstraintWidget.setHeightWrapContent(bool);
            m = localView.getMeasuredWidth();
            k = localView.getMeasuredHeight();
          }
          else
          {
            i = 0;
            j = 0;
            k = i2;
            m = i1;
          }
          localConstraintWidget.setWidth(m);
          localConstraintWidget.setHeight(k);
          if (i != 0) {
            localConstraintWidget.setWrapWidth(m);
          }
          if (j != 0) {
            localConstraintWidget.setWrapHeight(k);
          }
          if (localLayoutParams.needsBaseline)
          {
            i = localView.getBaseline();
            if (i != -1) {
              localConstraintWidget.setBaselineDistance(i);
            }
          }
        }
      }
    }
  }
  
  private void internalMeasureDimensions(int paramInt1, int paramInt2)
  {
    Object localObject1 = this;
    int i1 = getPaddingTop() + getPaddingBottom();
    int i8 = getPaddingLeft() + getPaddingRight();
    int i2 = getChildCount();
    long l1;
    Object localObject5;
    Object localObject3;
    Object localObject4;
    int m;
    int n;
    int j;
    int k;
    Object localObject2;
    boolean bool;
    for (int i = 0;; i++)
    {
      l1 = 1L;
      if (i >= i2) {
        break;
      }
      localObject5 = ((ConstraintLayout)localObject1).getChildAt(i);
      if (((View)localObject5).getVisibility() != 8)
      {
        localObject3 = (LayoutParams)((View)localObject5).getLayoutParams();
        localObject4 = ((LayoutParams)localObject3).widget;
        if ((!((LayoutParams)localObject3).isGuideline) && (!((LayoutParams)localObject3).isHelper))
        {
          ((ConstraintWidget)localObject4).setVisibility(((View)localObject5).getVisibility());
          m = ((LayoutParams)localObject3).width;
          n = ((LayoutParams)localObject3).height;
          if ((m != 0) && (n != 0))
          {
            if (m == -2) {
              j = 1;
            } else {
              j = 0;
            }
            i3 = getChildMeasureSpec(paramInt1, i8, m);
            if (n == -2) {
              k = 1;
            } else {
              k = 0;
            }
            ((View)localObject5).measure(i3, getChildMeasureSpec(paramInt2, i1, n));
            localObject2 = ((ConstraintLayout)localObject1).mMetrics;
            if (localObject2 != null) {
              ((Metrics)localObject2).measures += 1L;
            }
            if (m == -2) {
              bool = true;
            } else {
              bool = false;
            }
            ((ConstraintWidget)localObject4).setWidthWrapContent(bool);
            if (n == -2) {
              bool = true;
            } else {
              bool = false;
            }
            ((ConstraintWidget)localObject4).setHeightWrapContent(bool);
            m = ((View)localObject5).getMeasuredWidth();
            n = ((View)localObject5).getMeasuredHeight();
            ((ConstraintWidget)localObject4).setWidth(m);
            ((ConstraintWidget)localObject4).setHeight(n);
            if (j != 0) {
              ((ConstraintWidget)localObject4).setWrapWidth(m);
            }
            if (k != 0) {
              ((ConstraintWidget)localObject4).setWrapHeight(n);
            }
            if (((LayoutParams)localObject3).needsBaseline)
            {
              j = ((View)localObject5).getBaseline();
              if (j != -1) {
                ((ConstraintWidget)localObject4).setBaselineDistance(j);
              }
            }
            if ((((LayoutParams)localObject3).horizontalDimensionFixed) && (((LayoutParams)localObject3).verticalDimensionFixed))
            {
              ((ConstraintWidget)localObject4).getResolutionWidth().resolve(m);
              ((ConstraintWidget)localObject4).getResolutionHeight().resolve(n);
            }
          }
          else
          {
            ((ConstraintWidget)localObject4).getResolutionWidth().invalidate();
            ((ConstraintWidget)localObject4).getResolutionHeight().invalidate();
          }
        }
      }
    }
    ((ConstraintLayout)localObject1).mLayoutWidget.solveGraph();
    int i3 = 0;
    while (i3 < i2)
    {
      localObject2 = ((ConstraintLayout)localObject1).getChildAt(i3);
      if (((View)localObject2).getVisibility() == 8)
      {
        localObject2 = localObject1;
      }
      else
      {
        localObject4 = (LayoutParams)((View)localObject2).getLayoutParams();
        localObject3 = ((LayoutParams)localObject4).widget;
        if (!((LayoutParams)localObject4).isGuideline)
        {
          if (((LayoutParams)localObject4).isHelper)
          {
            localObject2 = localObject1;
          }
          else
          {
            ((ConstraintWidget)localObject3).setVisibility(((View)localObject2).getVisibility());
            int i5 = ((LayoutParams)localObject4).width;
            int i7 = ((LayoutParams)localObject4).height;
            if ((i5 != 0) && (i7 != 0))
            {
              localObject2 = localObject1;
            }
            else
            {
              ResolutionAnchor localResolutionAnchor2 = ((ConstraintWidget)localObject3).getAnchor(ConstraintAnchor.Type.LEFT).getResolutionNode();
              ResolutionAnchor localResolutionAnchor1 = ((ConstraintWidget)localObject3).getAnchor(ConstraintAnchor.Type.RIGHT).getResolutionNode();
              if ((((ConstraintWidget)localObject3).getAnchor(ConstraintAnchor.Type.LEFT).getTarget() != null) && (((ConstraintWidget)localObject3).getAnchor(ConstraintAnchor.Type.RIGHT).getTarget() != null)) {
                j = 1;
              } else {
                j = 0;
              }
              ResolutionAnchor localResolutionAnchor3 = ((ConstraintWidget)localObject3).getAnchor(ConstraintAnchor.Type.TOP).getResolutionNode();
              localObject5 = ((ConstraintWidget)localObject3).getAnchor(ConstraintAnchor.Type.BOTTOM).getResolutionNode();
              if ((((ConstraintWidget)localObject3).getAnchor(ConstraintAnchor.Type.TOP).getTarget() != null) && (((ConstraintWidget)localObject3).getAnchor(ConstraintAnchor.Type.BOTTOM).getTarget() != null)) {
                n = 1;
              } else {
                n = 0;
              }
              if ((i5 == 0) && (i7 == 0) && (j != 0) && (n != 0))
              {
                l1 = 1L;
                localObject2 = localObject1;
              }
              else
              {
                if (((ConstraintLayout)localObject1).mLayoutWidget.getHorizontalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                  m = 1;
                } else {
                  m = 0;
                }
                if (((ConstraintLayout)localObject1).mLayoutWidget.getVerticalDimensionBehaviour() != ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
                  i = 1;
                } else {
                  i = 0;
                }
                if (m == 0) {
                  ((ConstraintWidget)localObject3).getResolutionWidth().invalidate();
                }
                if (i == 0) {
                  ((ConstraintWidget)localObject3).getResolutionHeight().invalidate();
                }
                int i4;
                if (i5 == 0)
                {
                  if ((m != 0) && (((ConstraintWidget)localObject3).isSpreadWidth()) && (j != 0) && (localResolutionAnchor2.isResolved()) && (localResolutionAnchor1.isResolved()))
                  {
                    i5 = (int)(localResolutionAnchor1.getResolvedValue() - localResolutionAnchor2.getResolvedValue());
                    ((ConstraintWidget)localObject3).getResolutionWidth().resolve(i5);
                    k = getChildMeasureSpec(paramInt1, i8, i5);
                    j = 0;
                    i4 = m;
                  }
                  else
                  {
                    k = getChildMeasureSpec(paramInt1, i8, -2);
                    j = 1;
                    i4 = 0;
                  }
                }
                else if (i5 == -1)
                {
                  k = getChildMeasureSpec(paramInt1, i8, -1);
                  j = 0;
                  i4 = m;
                }
                else
                {
                  if (i5 == -2) {
                    j = 1;
                  } else {
                    j = 0;
                  }
                  k = getChildMeasureSpec(paramInt1, i8, i5);
                  i4 = m;
                }
                if (i7 == 0)
                {
                  if ((i != 0) && (((ConstraintWidget)localObject3).isSpreadHeight()) && (n != 0) && (localResolutionAnchor3.isResolved()) && (((ResolutionAnchor)localObject5).isResolved()))
                  {
                    i7 = (int)(((ResolutionAnchor)localObject5).getResolvedValue() - localResolutionAnchor3.getResolvedValue());
                    ((ConstraintWidget)localObject3).getResolutionHeight().resolve(i7);
                    m = getChildMeasureSpec(paramInt2, i1, i7);
                    n = i;
                    i = 0;
                  }
                  else
                  {
                    m = getChildMeasureSpec(paramInt2, i1, -2);
                    i = 1;
                    n = 0;
                  }
                }
                else if (i7 == -1)
                {
                  m = getChildMeasureSpec(paramInt2, i1, -1);
                  n = i;
                  i = 0;
                }
                else
                {
                  if (i7 == -2) {
                    m = 1;
                  } else {
                    m = 0;
                  }
                  int i6 = getChildMeasureSpec(paramInt2, i1, i7);
                  n = i;
                  i = m;
                  m = i6;
                }
                ((View)localObject2).measure(k, m);
                localObject1 = this;
                localObject5 = ((ConstraintLayout)localObject1).mMetrics;
                if (localObject5 != null) {
                  ((Metrics)localObject5).measures += 1L;
                }
                long l2 = 1L;
                if (i5 == -2) {
                  bool = true;
                } else {
                  bool = false;
                }
                ((ConstraintWidget)localObject3).setWidthWrapContent(bool);
                if (i7 == -2) {
                  bool = true;
                } else {
                  bool = false;
                }
                ((ConstraintWidget)localObject3).setHeightWrapContent(bool);
                m = ((View)localObject2).getMeasuredWidth();
                k = ((View)localObject2).getMeasuredHeight();
                ((ConstraintWidget)localObject3).setWidth(m);
                ((ConstraintWidget)localObject3).setHeight(k);
                if (j != 0) {
                  ((ConstraintWidget)localObject3).setWrapWidth(m);
                }
                if (i != 0) {
                  ((ConstraintWidget)localObject3).setWrapHeight(k);
                }
                if (i4 != 0) {
                  ((ConstraintWidget)localObject3).getResolutionWidth().resolve(m);
                } else {
                  ((ConstraintWidget)localObject3).getResolutionWidth().remove();
                }
                if (n != 0) {
                  ((ConstraintWidget)localObject3).getResolutionHeight().resolve(k);
                } else {
                  ((ConstraintWidget)localObject3).getResolutionHeight().remove();
                }
                if (((LayoutParams)localObject4).needsBaseline)
                {
                  i = ((View)localObject2).getBaseline();
                  localObject2 = localObject1;
                  l1 = l2;
                  if (i != -1)
                  {
                    ((ConstraintWidget)localObject3).setBaselineDistance(i);
                    localObject2 = localObject1;
                    l1 = l2;
                  }
                }
                else
                {
                  localObject2 = localObject1;
                  l1 = l2;
                }
              }
            }
          }
        }
        else {
          localObject2 = localObject1;
        }
      }
      i3++;
      localObject1 = localObject2;
    }
  }
  
  private void setChildrenConstraints()
  {
    throw new RuntimeException("d2j fail translate: java.lang.RuntimeException: can not merge Z and I\n\tat com.googlecode.dex2jar.ir.TypeClass.merge(TypeClass.java:100)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeRef.updateTypeClass(TypeTransformer.java:174)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.copyTypes(TypeTransformer.java:311)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.fixTypes(TypeTransformer.java:226)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer$TypeAnalyze.analyze(TypeTransformer.java:207)\n\tat com.googlecode.dex2jar.ir.ts.TypeTransformer.transform(TypeTransformer.java:44)\n\tat com.googlecode.d2j.dex.Dex2jar$2.optimize(Dex2jar.java:161)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertCode(Dex2Asm.java:433)\n\tat com.googlecode.d2j.dex.ExDex2Asm.convertCode(ExDex2Asm.java:42)\n\tat com.googlecode.d2j.dex.Dex2jar$2.convertCode(Dex2jar.java:129)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertMethod(Dex2Asm.java:528)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertClass(Dex2Asm.java:425)\n\tat com.googlecode.d2j.dex.Dex2Asm.convertDex(Dex2Asm.java:441)\n\tat com.googlecode.d2j.dex.Dex2jar.doTranslate(Dex2jar.java:171)\n\tat com.googlecode.d2j.dex.Dex2jar.to(Dex2jar.java:271)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.doCommandLine(Dex2jarCmd.java:109)\n\tat com.googlecode.dex2jar.tools.BaseCmd.doMain(BaseCmd.java:290)\n\tat com.googlecode.dex2jar.tools.Dex2jarCmd.main(Dex2jarCmd.java:33)\n\tat com.linchaolong.apktoolplus.core.ApkToolPlus.dex2jar(ApkToolPlus.java:293)\n\tat com.linchaolong.apktoolplus.module.main.Dex2JarMultDexSupport.<init>(Dex2JarMultDexSupport.java:60)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6$1.<init>(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainActivity$6.onSelected(MainActivity.java:385)\n\tat com.linchaolong.apktoolplus.module.main.MainView.lambda$openFileSelector$22(MainView.java:199)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue$TaskWrapper.run(TaskQueue.java:45)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.loop(TaskQueue.java:109)\n\tat com.linchaolong.apktoolplus.utils.TaskQueue.lambda$start$0(TaskQueue.java:86)\n\tat java.lang.Thread.run(Thread.java:748)\n");
  }
  
  private void setSelfDimensionBehaviour(int paramInt1, int paramInt2)
  {
    int m = View.MeasureSpec.getMode(paramInt1);
    paramInt1 = View.MeasureSpec.getSize(paramInt1);
    int i = View.MeasureSpec.getMode(paramInt2);
    paramInt2 = View.MeasureSpec.getSize(paramInt2);
    int j = getPaddingTop();
    int k = getPaddingBottom();
    int n = getPaddingLeft();
    int i1 = getPaddingRight();
    ConstraintWidget.DimensionBehaviour localDimensionBehaviour1 = ConstraintWidget.DimensionBehaviour.FIXED;
    ConstraintWidget.DimensionBehaviour localDimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.FIXED;
    getLayoutParams();
    if (m != Integer.MIN_VALUE)
    {
      if (m != 0)
      {
        if (m != 1073741824) {
          paramInt1 = 0;
        } else {
          paramInt1 = Math.min(this.mMaxWidth, paramInt1) - (n + i1);
        }
      }
      else
      {
        localDimensionBehaviour1 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        paramInt1 = 0;
      }
    }
    else {
      localDimensionBehaviour1 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
    }
    if (i != Integer.MIN_VALUE)
    {
      if (i != 0)
      {
        if (i != 1073741824) {
          paramInt2 = 0;
        } else {
          paramInt2 = Math.min(this.mMaxHeight, paramInt2) - (j + k);
        }
      }
      else
      {
        localDimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
        paramInt2 = 0;
      }
    }
    else {
      localDimensionBehaviour2 = ConstraintWidget.DimensionBehaviour.WRAP_CONTENT;
    }
    this.mLayoutWidget.setMinWidth(0);
    this.mLayoutWidget.setMinHeight(0);
    this.mLayoutWidget.setHorizontalDimensionBehaviour(localDimensionBehaviour1);
    this.mLayoutWidget.setWidth(paramInt1);
    this.mLayoutWidget.setVerticalDimensionBehaviour(localDimensionBehaviour2);
    this.mLayoutWidget.setHeight(paramInt2);
    this.mLayoutWidget.setMinWidth(this.mMinWidth - getPaddingLeft() - getPaddingRight());
    this.mLayoutWidget.setMinHeight(this.mMinHeight - getPaddingTop() - getPaddingBottom());
  }
  
  private void updateHierarchy()
  {
    int m = getChildCount();
    int k = 0;
    int j;
    for (int i = 0;; i++)
    {
      j = k;
      if (i >= m) {
        break;
      }
      if (getChildAt(i).isLayoutRequested())
      {
        j = 1;
        break;
      }
    }
    if (j != 0)
    {
      this.mVariableDimensionsWidgets.clear();
      setChildrenConstraints();
    }
  }
  
  private void updatePostMeasures()
  {
    int k = getChildCount();
    int j = 0;
    for (int i = 0; i < k; i++)
    {
      View localView = getChildAt(i);
      if ((localView instanceof Placeholder)) {
        ((Placeholder)localView).updatePostMeasure(this);
      }
    }
    k = this.mConstraintHelpers.size();
    if (k > 0) {
      for (i = j; i < k; i++) {
        ((ConstraintHelper)this.mConstraintHelpers.get(i)).updatePostMeasure(this);
      }
    }
  }
  
  public void addView(View paramView, int paramInt, ViewGroup.LayoutParams paramLayoutParams)
  {
    super.addView(paramView, paramInt, paramLayoutParams);
    if (Build.VERSION.SDK_INT < 14) {
      onViewAdded(paramView);
    }
  }
  
  protected boolean checkLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return paramLayoutParams instanceof LayoutParams;
  }
  
  public void dispatchDraw(Canvas paramCanvas)
  {
    super.dispatchDraw(paramCanvas);
    if (isInEditMode())
    {
      int j = getChildCount();
      float f2 = getWidth();
      float f6 = getHeight();
      for (int i = 0; i < j; i++)
      {
        Object localObject = getChildAt(i);
        if (((View)localObject).getVisibility() != 8)
        {
          localObject = ((View)localObject).getTag();
          if ((localObject != null) && ((localObject instanceof String)))
          {
            localObject = ((String)localObject).split(",");
            if (localObject.length == 4)
            {
              int k = Integer.parseInt(localObject[0]);
              int i1 = Integer.parseInt(localObject[1]);
              int n = Integer.parseInt(localObject[2]);
              int m = Integer.parseInt(localObject[3]);
              k = (int)(k / 1080.0F * f2);
              i1 = (int)(i1 / 1920.0F * f6);
              n = (int)(n / 1080.0F * f2);
              m = (int)(m / 1920.0F * f6);
              localObject = new Paint();
              ((Paint)localObject).setColor(-65536);
              float f4 = k;
              float f5 = i1;
              float f1 = k + n;
              paramCanvas.drawLine(f4, f5, f1, f5, (Paint)localObject);
              float f3 = i1 + m;
              paramCanvas.drawLine(f1, f5, f1, f3, (Paint)localObject);
              paramCanvas.drawLine(f1, f3, f4, f3, (Paint)localObject);
              paramCanvas.drawLine(f4, f3, f4, f5, (Paint)localObject);
              ((Paint)localObject).setColor(-16711936);
              paramCanvas.drawLine(f4, f5, f1, f3, (Paint)localObject);
              paramCanvas.drawLine(f4, f3, f1, f5, (Paint)localObject);
            }
          }
        }
      }
    }
  }
  
  public void fillMetrics(Metrics paramMetrics)
  {
    this.mMetrics = paramMetrics;
    this.mLayoutWidget.fillMetrics(paramMetrics);
  }
  
  protected LayoutParams generateDefaultLayoutParams()
  {
    return new LayoutParams(-2, -2);
  }
  
  public LayoutParams generateLayoutParams(AttributeSet paramAttributeSet)
  {
    return new LayoutParams(getContext(), paramAttributeSet);
  }
  
  protected ViewGroup.LayoutParams generateLayoutParams(ViewGroup.LayoutParams paramLayoutParams)
  {
    return new LayoutParams(paramLayoutParams);
  }
  
  public Object getDesignInformation(int paramInt, Object paramObject)
  {
    if ((paramInt == 0) && ((paramObject instanceof String)))
    {
      paramObject = (String)paramObject;
      HashMap localHashMap = this.mDesignIds;
      if ((localHashMap != null) && (localHashMap.containsKey(paramObject))) {
        return this.mDesignIds.get(paramObject);
      }
    }
    return null;
  }
  
  public int getMaxHeight()
  {
    return this.mMaxHeight;
  }
  
  public int getMaxWidth()
  {
    return this.mMaxWidth;
  }
  
  public int getMinHeight()
  {
    return this.mMinHeight;
  }
  
  public int getMinWidth()
  {
    return this.mMinWidth;
  }
  
  public int getOptimizationLevel()
  {
    return this.mLayoutWidget.getOptimizationLevel();
  }
  
  public View getViewById(int paramInt)
  {
    return (View)this.mChildrenByIds.get(paramInt);
  }
  
  public final ConstraintWidget getViewWidget(View paramView)
  {
    if (paramView == this) {
      return this.mLayoutWidget;
    }
    if (paramView == null) {
      paramView = null;
    } else {
      paramView = ((LayoutParams)paramView.getLayoutParams()).widget;
    }
    return paramView;
  }
  
  protected void onLayout(boolean paramBoolean, int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    paramInt3 = getChildCount();
    paramBoolean = isInEditMode();
    paramInt2 = 0;
    for (paramInt1 = 0; paramInt1 < paramInt3; paramInt1++)
    {
      View localView = getChildAt(paramInt1);
      LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
      Object localObject = localLayoutParams.widget;
      if (((localView.getVisibility() != 8) || (localLayoutParams.isGuideline) || (localLayoutParams.isHelper) || (paramBoolean)) && (!localLayoutParams.isInPlaceholder))
      {
        int k = ((ConstraintWidget)localObject).getDrawX();
        paramInt4 = ((ConstraintWidget)localObject).getDrawY();
        int i = ((ConstraintWidget)localObject).getWidth() + k;
        int j = ((ConstraintWidget)localObject).getHeight() + paramInt4;
        localView.layout(k, paramInt4, i, j);
        if ((localView instanceof Placeholder))
        {
          localObject = ((Placeholder)localView).getContent();
          if (localObject != null)
          {
            ((View)localObject).setVisibility(0);
            ((View)localObject).layout(k, paramInt4, i, j);
          }
        }
      }
    }
    paramInt3 = this.mConstraintHelpers.size();
    if (paramInt3 > 0) {
      for (paramInt1 = paramInt2; paramInt1 < paramInt3; paramInt1++) {
        ((ConstraintHelper)this.mConstraintHelpers.get(paramInt1)).updatePostLayout(this);
      }
    }
  }
  
  protected void onMeasure(int paramInt1, int paramInt2)
  {
    System.currentTimeMillis();
    int i3 = View.MeasureSpec.getMode(paramInt1);
    int i2 = View.MeasureSpec.getSize(paramInt1);
    int m = View.MeasureSpec.getMode(paramInt2);
    int n = View.MeasureSpec.getSize(paramInt2);
    int j = getPaddingLeft();
    int k = getPaddingTop();
    this.mLayoutWidget.setX(j);
    this.mLayoutWidget.setY(k);
    this.mLayoutWidget.setMaxWidth(this.mMaxWidth);
    this.mLayoutWidget.setMaxHeight(this.mMaxHeight);
    Object localObject;
    if (Build.VERSION.SDK_INT >= 17)
    {
      localObject = this.mLayoutWidget;
      boolean bool;
      if (getLayoutDirection() == 1) {
        bool = true;
      } else {
        bool = false;
      }
      ((ConstraintWidgetContainer)localObject).setRtl(bool);
    }
    setSelfDimensionBehaviour(paramInt1, paramInt2);
    int i6 = this.mLayoutWidget.getWidth();
    int i5 = this.mLayoutWidget.getHeight();
    if (this.mDirtyHierarchy)
    {
      this.mDirtyHierarchy = false;
      updateHierarchy();
      i = 1;
    }
    else
    {
      i = 0;
    }
    int i1;
    if ((this.mOptimizationLevel & 0x8) == 8) {
      i1 = 1;
    } else {
      i1 = 0;
    }
    if (i1 != 0)
    {
      this.mLayoutWidget.preOptimize();
      this.mLayoutWidget.optimizeForDimensions(i6, i5);
      internalMeasureDimensions(paramInt1, paramInt2);
    }
    else
    {
      internalMeasureChildren(paramInt1, paramInt2);
    }
    updatePostMeasures();
    if ((getChildCount() > 0) && (i != 0)) {
      Analyzer.determineGroups(this.mLayoutWidget);
    }
    if (this.mLayoutWidget.mGroupsWrapOptimized)
    {
      if ((this.mLayoutWidget.mHorizontalWrapOptimized) && (i3 == Integer.MIN_VALUE))
      {
        if (this.mLayoutWidget.mWrapFixedWidth < i2)
        {
          localObject = this.mLayoutWidget;
          ((ConstraintWidgetContainer)localObject).setWidth(((ConstraintWidgetContainer)localObject).mWrapFixedWidth);
        }
        this.mLayoutWidget.setHorizontalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
      }
      if ((this.mLayoutWidget.mVerticalWrapOptimized) && (m == Integer.MIN_VALUE))
      {
        if (this.mLayoutWidget.mWrapFixedHeight < n)
        {
          localObject = this.mLayoutWidget;
          ((ConstraintWidgetContainer)localObject).setHeight(((ConstraintWidgetContainer)localObject).mWrapFixedHeight);
        }
        this.mLayoutWidget.setVerticalDimensionBehaviour(ConstraintWidget.DimensionBehaviour.FIXED);
      }
    }
    if ((this.mOptimizationLevel & 0x20) == 32)
    {
      i4 = this.mLayoutWidget.getWidth();
      i = this.mLayoutWidget.getHeight();
      if ((this.mLastMeasureWidth != i4) && (i3 == 1073741824)) {
        Analyzer.setPosition(this.mLayoutWidget.mWidgetGroups, 0, i4);
      }
      if ((this.mLastMeasureHeight != i) && (m == 1073741824)) {
        Analyzer.setPosition(this.mLayoutWidget.mWidgetGroups, 1, i);
      }
      if ((this.mLayoutWidget.mHorizontalWrapOptimized) && (this.mLayoutWidget.mWrapFixedWidth > i2)) {
        Analyzer.setPosition(this.mLayoutWidget.mWidgetGroups, 0, i2);
      }
      if ((this.mLayoutWidget.mVerticalWrapOptimized) && (this.mLayoutWidget.mWrapFixedHeight > n)) {
        Analyzer.setPosition(this.mLayoutWidget.mWidgetGroups, 1, n);
      } else {}
    }
    if (getChildCount() > 0) {
      solveLinearSystem("First pass");
    }
    int i4 = this.mVariableDimensionsWidgets.size();
    int i9 = k + getPaddingBottom();
    int i10 = j + getPaddingRight();
    if (i4 > 0)
    {
      if (this.mLayoutWidget.getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
        i2 = 1;
      } else {
        i2 = 0;
      }
      if (this.mLayoutWidget.getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
        i3 = 1;
      } else {
        i3 = 0;
      }
      k = Math.max(this.mLayoutWidget.getWidth(), this.mMinWidth);
      j = Math.max(this.mLayoutWidget.getHeight(), this.mMinHeight);
      int i7 = 0;
      m = 0;
      i = 0;
      View localView;
      while (i7 < i4)
      {
        ConstraintWidget localConstraintWidget = (ConstraintWidget)this.mVariableDimensionsWidgets.get(i7);
        localView = (View)localConstraintWidget.getCompanionWidget();
        if (localView != null)
        {
          LayoutParams localLayoutParams = (LayoutParams)localView.getLayoutParams();
          if ((!localLayoutParams.isHelper) && (!localLayoutParams.isGuideline))
          {
            n = localView.getVisibility();
            int i8 = m;
            if ((n != 8) && ((i1 == 0) || (!localConstraintWidget.getResolutionWidth().isResolved()) || (!localConstraintWidget.getResolutionHeight().isResolved())))
            {
              if ((localLayoutParams.width == -2) && (localLayoutParams.horizontalDimensionFixed)) {
                m = getChildMeasureSpec(paramInt1, i10, localLayoutParams.width);
              } else {
                m = View.MeasureSpec.makeMeasureSpec(localConstraintWidget.getWidth(), 1073741824);
              }
              if ((localLayoutParams.height == -2) && (localLayoutParams.verticalDimensionFixed)) {
                n = getChildMeasureSpec(paramInt2, i9, localLayoutParams.height);
              } else {
                n = View.MeasureSpec.makeMeasureSpec(localConstraintWidget.getHeight(), 1073741824);
              }
              localView.measure(m, n);
              localObject = this.mMetrics;
              if (localObject != null) {
                ((Metrics)localObject).additionalMeasures += 1L;
              }
              int i12 = localView.getMeasuredWidth();
              int i11 = localView.getMeasuredHeight();
              n = k;
              m = i8;
              if (i12 != localConstraintWidget.getWidth())
              {
                localConstraintWidget.setWidth(i12);
                if (i1 != 0) {
                  localConstraintWidget.getResolutionWidth().resolve(i12);
                }
                n = k;
                if (i2 != 0)
                {
                  n = k;
                  if (localConstraintWidget.getRight() > k) {
                    n = Math.max(k, localConstraintWidget.getRight() + localConstraintWidget.getAnchor(ConstraintAnchor.Type.RIGHT).getMargin());
                  }
                }
                m = 1;
              }
              k = j;
              if (i11 != localConstraintWidget.getHeight())
              {
                localConstraintWidget.setHeight(i11);
                if (i1 != 0) {
                  localConstraintWidget.getResolutionHeight().resolve(i11);
                }
                k = j;
                if (i3 != 0)
                {
                  k = j;
                  if (localConstraintWidget.getBottom() > j) {
                    k = Math.max(j, localConstraintWidget.getBottom() + localConstraintWidget.getAnchor(ConstraintAnchor.Type.BOTTOM).getMargin());
                  }
                }
                m = 1;
              }
              j = m;
              if (localLayoutParams.needsBaseline)
              {
                i8 = localView.getBaseline();
                j = m;
                if (i8 != -1)
                {
                  j = m;
                  if (i8 != localConstraintWidget.getBaselineDistance())
                  {
                    localConstraintWidget.setBaselineDistance(i8);
                    j = 1;
                  }
                }
              }
              if (Build.VERSION.SDK_INT >= 11)
              {
                i = combineMeasuredStates(i, localView.getMeasuredState());
                m = j;
                j = k;
                k = n;
              }
              else
              {
                m = j;
                j = k;
                k = n;
              }
            }
          }
        }
        i7++;
      }
      n = i;
      if (m != 0)
      {
        this.mLayoutWidget.setWidth(i6);
        this.mLayoutWidget.setHeight(i5);
        if (i1 != 0) {
          this.mLayoutWidget.solveGraph();
        }
        solveLinearSystem("2nd pass");
        if (this.mLayoutWidget.getWidth() < k)
        {
          this.mLayoutWidget.setWidth(k);
          i = 1;
        }
        else
        {
          i = 0;
        }
        if (this.mLayoutWidget.getHeight() < j)
        {
          this.mLayoutWidget.setHeight(j);
          i = 1;
        }
        if (i != 0) {
          solveLinearSystem("3rd pass");
        }
      }
      for (j = 0;; j++)
      {
        i = n;
        if (j >= i4) {
          break;
        }
        localObject = (ConstraintWidget)this.mVariableDimensionsWidgets.get(j);
        localView = (View)((ConstraintWidget)localObject).getCompanionWidget();
        if (localView != null)
        {
          if ((localView.getMeasuredWidth() == ((ConstraintWidget)localObject).getWidth()) && (localView.getMeasuredHeight() == ((ConstraintWidget)localObject).getHeight())) {}
          if (((ConstraintWidget)localObject).getVisibility() != 8)
          {
            localView.measure(View.MeasureSpec.makeMeasureSpec(((ConstraintWidget)localObject).getWidth(), 1073741824), View.MeasureSpec.makeMeasureSpec(((ConstraintWidget)localObject).getHeight(), 1073741824));
            localObject = this.mMetrics;
            if (localObject != null) {
              ((Metrics)localObject).additionalMeasures += 1L;
            }
          }
        }
      }
    }
    int i = 0;
    j = this.mLayoutWidget.getWidth() + i10;
    k = this.mLayoutWidget.getHeight() + i9;
    if (Build.VERSION.SDK_INT >= 11)
    {
      paramInt1 = resolveSizeAndState(j, paramInt1, i);
      i = resolveSizeAndState(k, paramInt2, i << 16);
      paramInt2 = Math.min(this.mMaxWidth, paramInt1 & 0xFFFFFF);
      i = Math.min(this.mMaxHeight, i & 0xFFFFFF);
      paramInt1 = paramInt2;
      if (this.mLayoutWidget.isWidthMeasuredTooSmall()) {
        paramInt1 = paramInt2 | 0x1000000;
      }
      paramInt2 = i;
      if (this.mLayoutWidget.isHeightMeasuredTooSmall()) {
        paramInt2 = i | 0x1000000;
      }
      setMeasuredDimension(paramInt1, paramInt2);
      this.mLastMeasureWidth = paramInt1;
      this.mLastMeasureHeight = paramInt2;
    }
    else
    {
      setMeasuredDimension(j, k);
      this.mLastMeasureWidth = j;
      this.mLastMeasureHeight = k;
    }
  }
  
  public void onViewAdded(View paramView)
  {
    if (Build.VERSION.SDK_INT >= 14) {
      super.onViewAdded(paramView);
    }
    Object localObject = getViewWidget(paramView);
    if (((paramView instanceof Guideline)) && (!(localObject instanceof android.support.constraint.solver.widgets.Guideline)))
    {
      localObject = (LayoutParams)paramView.getLayoutParams();
      ((LayoutParams)localObject).widget = new android.support.constraint.solver.widgets.Guideline();
      ((LayoutParams)localObject).isGuideline = true;
      ((android.support.constraint.solver.widgets.Guideline)((LayoutParams)localObject).widget).setOrientation(((LayoutParams)localObject).orientation);
    }
    if ((paramView instanceof ConstraintHelper))
    {
      localObject = (ConstraintHelper)paramView;
      ((ConstraintHelper)localObject).validateParams();
      ((LayoutParams)paramView.getLayoutParams()).isHelper = true;
      if (!this.mConstraintHelpers.contains(localObject)) {
        this.mConstraintHelpers.add(localObject);
      }
    }
    this.mChildrenByIds.put(paramView.getId(), paramView);
    this.mDirtyHierarchy = true;
  }
  
  public void onViewRemoved(View paramView)
  {
    if (Build.VERSION.SDK_INT >= 14) {
      super.onViewRemoved(paramView);
    }
    this.mChildrenByIds.remove(paramView.getId());
    ConstraintWidget localConstraintWidget = getViewWidget(paramView);
    this.mLayoutWidget.remove(localConstraintWidget);
    this.mConstraintHelpers.remove(paramView);
    this.mVariableDimensionsWidgets.remove(localConstraintWidget);
    this.mDirtyHierarchy = true;
  }
  
  public void removeView(View paramView)
  {
    super.removeView(paramView);
    if (Build.VERSION.SDK_INT < 14) {
      onViewRemoved(paramView);
    }
  }
  
  public void requestLayout()
  {
    super.requestLayout();
    this.mDirtyHierarchy = true;
    this.mLastMeasureWidth = -1;
    this.mLastMeasureHeight = -1;
    this.mLastMeasureWidthSize = -1;
    this.mLastMeasureHeightSize = -1;
    this.mLastMeasureWidthMode = 0;
    this.mLastMeasureHeightMode = 0;
  }
  
  public void setConstraintSet(ConstraintSet paramConstraintSet)
  {
    this.mConstraintSet = paramConstraintSet;
  }
  
  public void setDesignInformation(int paramInt, Object paramObject1, Object paramObject2)
  {
    if ((paramInt == 0) && ((paramObject1 instanceof String)) && ((paramObject2 instanceof Integer)))
    {
      if (this.mDesignIds == null) {
        this.mDesignIds = new HashMap();
      }
      String str = (String)paramObject1;
      paramInt = str.indexOf("/");
      paramObject1 = str;
      if (paramInt != -1) {
        paramObject1 = str.substring(paramInt + 1);
      }
      paramInt = ((Integer)paramObject2).intValue();
      this.mDesignIds.put(paramObject1, Integer.valueOf(paramInt));
    }
  }
  
  public void setId(int paramInt)
  {
    this.mChildrenByIds.remove(getId());
    super.setId(paramInt);
    this.mChildrenByIds.put(getId(), this);
  }
  
  public void setMaxHeight(int paramInt)
  {
    if (paramInt == this.mMaxHeight) {
      return;
    }
    this.mMaxHeight = paramInt;
    requestLayout();
  }
  
  public void setMaxWidth(int paramInt)
  {
    if (paramInt == this.mMaxWidth) {
      return;
    }
    this.mMaxWidth = paramInt;
    requestLayout();
  }
  
  public void setMinHeight(int paramInt)
  {
    if (paramInt == this.mMinHeight) {
      return;
    }
    this.mMinHeight = paramInt;
    requestLayout();
  }
  
  public void setMinWidth(int paramInt)
  {
    if (paramInt == this.mMinWidth) {
      return;
    }
    this.mMinWidth = paramInt;
    requestLayout();
  }
  
  public void setOptimizationLevel(int paramInt)
  {
    this.mLayoutWidget.setOptimizationLevel(paramInt);
  }
  
  public boolean shouldDelayChildPressedState()
  {
    return false;
  }
  
  protected void solveLinearSystem(String paramString)
  {
    this.mLayoutWidget.layout();
    paramString = this.mMetrics;
    if (paramString != null) {
      paramString.resolutions += 1L;
    }
  }
  
  public static class LayoutParams
    extends ViewGroup.MarginLayoutParams
  {
    public static final int BASELINE = 5;
    public static final int BOTTOM = 4;
    public static final int CHAIN_PACKED = 2;
    public static final int CHAIN_SPREAD = 0;
    public static final int CHAIN_SPREAD_INSIDE = 1;
    public static final int END = 7;
    public static final int HORIZONTAL = 0;
    public static final int LEFT = 1;
    public static final int MATCH_CONSTRAINT = 0;
    public static final int MATCH_CONSTRAINT_PERCENT = 2;
    public static final int MATCH_CONSTRAINT_SPREAD = 0;
    public static final int MATCH_CONSTRAINT_WRAP = 1;
    public static final int PARENT_ID = 0;
    public static final int RIGHT = 2;
    public static final int START = 6;
    public static final int TOP = 3;
    public static final int UNSET = -1;
    public static final int VERTICAL = 1;
    public int baselineToBaseline = -1;
    public int bottomToBottom = -1;
    public int bottomToTop = -1;
    public float circleAngle = 0.0F;
    public int circleConstraint = -1;
    public int circleRadius = 0;
    public boolean constrainedHeight = false;
    public boolean constrainedWidth = false;
    public String dimensionRatio = null;
    int dimensionRatioSide = 1;
    float dimensionRatioValue = 0.0F;
    public int editorAbsoluteX = -1;
    public int editorAbsoluteY = -1;
    public int endToEnd = -1;
    public int endToStart = -1;
    public int goneBottomMargin = -1;
    public int goneEndMargin = -1;
    public int goneLeftMargin = -1;
    public int goneRightMargin = -1;
    public int goneStartMargin = -1;
    public int goneTopMargin = -1;
    public int guideBegin = -1;
    public int guideEnd = -1;
    public float guidePercent = -1.0F;
    public boolean helped = false;
    public float horizontalBias = 0.5F;
    public int horizontalChainStyle = 0;
    boolean horizontalDimensionFixed = true;
    public float horizontalWeight = -1.0F;
    boolean isGuideline = false;
    boolean isHelper = false;
    boolean isInPlaceholder = false;
    public int leftToLeft = -1;
    public int leftToRight = -1;
    public int matchConstraintDefaultHeight = 0;
    public int matchConstraintDefaultWidth = 0;
    public int matchConstraintMaxHeight = 0;
    public int matchConstraintMaxWidth = 0;
    public int matchConstraintMinHeight = 0;
    public int matchConstraintMinWidth = 0;
    public float matchConstraintPercentHeight = 1.0F;
    public float matchConstraintPercentWidth = 1.0F;
    boolean needsBaseline = false;
    public int orientation = -1;
    int resolveGoneLeftMargin = -1;
    int resolveGoneRightMargin = -1;
    int resolvedGuideBegin;
    int resolvedGuideEnd;
    float resolvedGuidePercent;
    float resolvedHorizontalBias = 0.5F;
    int resolvedLeftToLeft = -1;
    int resolvedLeftToRight = -1;
    int resolvedRightToLeft = -1;
    int resolvedRightToRight = -1;
    public int rightToLeft = -1;
    public int rightToRight = -1;
    public int startToEnd = -1;
    public int startToStart = -1;
    public int topToBottom = -1;
    public int topToTop = -1;
    public float verticalBias = 0.5F;
    public int verticalChainStyle = 0;
    boolean verticalDimensionFixed = true;
    public float verticalWeight = -1.0F;
    ConstraintWidget widget = new ConstraintWidget();
    
    public LayoutParams(int paramInt1, int paramInt2)
    {
      super(paramInt2);
    }
    
    public LayoutParams(Context paramContext, AttributeSet paramAttributeSet)
    {
      super(paramAttributeSet);
      paramContext = paramContext.obtainStyledAttributes(paramAttributeSet, R.styleable.ConstraintLayout_Layout);
      int k = paramContext.getIndexCount();
      for (int i = 0; i < k; i++)
      {
        int j = paramContext.getIndex(i);
        String str;
        switch (Table.map.get(j))
        {
        case 43: 
        default: 
          break;
        case 50: 
          this.editorAbsoluteY = paramContext.getDimensionPixelOffset(j, this.editorAbsoluteY);
          break;
        case 49: 
          this.editorAbsoluteX = paramContext.getDimensionPixelOffset(j, this.editorAbsoluteX);
          break;
        case 48: 
          this.verticalChainStyle = paramContext.getInt(j, 0);
          break;
        case 47: 
          this.horizontalChainStyle = paramContext.getInt(j, 0);
          break;
        case 46: 
          this.verticalWeight = paramContext.getFloat(j, this.verticalWeight);
          break;
        case 45: 
          this.horizontalWeight = paramContext.getFloat(j, this.horizontalWeight);
          break;
        case 44: 
          this.dimensionRatio = paramContext.getString(j);
          this.dimensionRatioValue = NaN.0F;
          this.dimensionRatioSide = -1;
          paramAttributeSet = this.dimensionRatio;
          if (paramAttributeSet != null)
          {
            int m = paramAttributeSet.length();
            j = this.dimensionRatio.indexOf(',');
            if ((j > 0) && (j < m - 1))
            {
              paramAttributeSet = this.dimensionRatio.substring(0, j);
              if (paramAttributeSet.equalsIgnoreCase("W")) {
                this.dimensionRatioSide = 0;
              } else if (paramAttributeSet.equalsIgnoreCase("H")) {
                this.dimensionRatioSide = 1;
              }
              j++;
            }
            else
            {
              j = 0;
            }
            int n = this.dimensionRatio.indexOf(':');
            if ((n >= 0) && (n < m - 1))
            {
              paramAttributeSet = this.dimensionRatio.substring(j, n);
              str = this.dimensionRatio.substring(n + 1);
              if ((paramAttributeSet.length() <= 0) || (str.length() <= 0)) {
                continue;
              }
            }
          }
          break;
        }
        try
        {
          float f2 = Float.parseFloat(paramAttributeSet);
          f1 = Float.parseFloat(str);
          if ((f2 <= 0.0F) || (f1 <= 0.0F)) {
            continue;
          }
          if (this.dimensionRatioSide == 1) {
            this.dimensionRatioValue = Math.abs(f1 / f2);
          } else {
            this.dimensionRatioValue = Math.abs(f2 / f1);
          }
        }
        catch (NumberFormatException paramAttributeSet)
        {
          float f1;
          continue;
        }
        paramAttributeSet = this.dimensionRatio.substring(j);
        if (paramAttributeSet.length() > 0)
        {
          this.dimensionRatioValue = Float.parseFloat(paramAttributeSet);
          continue;
          this.matchConstraintPercentHeight = Math.max(0.0F, paramContext.getFloat(j, this.matchConstraintPercentHeight));
          continue;
          try
          {
            this.matchConstraintMaxHeight = paramContext.getDimensionPixelSize(j, this.matchConstraintMaxHeight);
          }
          catch (Exception paramAttributeSet)
          {
            if (paramContext.getInt(j, this.matchConstraintMaxHeight) != -2) {
              continue;
            }
          }
          this.matchConstraintMaxHeight = -2;
          continue;
          try
          {
            this.matchConstraintMinHeight = paramContext.getDimensionPixelSize(j, this.matchConstraintMinHeight);
          }
          catch (Exception paramAttributeSet)
          {
            if (paramContext.getInt(j, this.matchConstraintMinHeight) != -2) {
              continue;
            }
          }
          this.matchConstraintMinHeight = -2;
          continue;
          this.matchConstraintPercentWidth = Math.max(0.0F, paramContext.getFloat(j, this.matchConstraintPercentWidth));
          continue;
          try
          {
            this.matchConstraintMaxWidth = paramContext.getDimensionPixelSize(j, this.matchConstraintMaxWidth);
          }
          catch (Exception paramAttributeSet)
          {
            if (paramContext.getInt(j, this.matchConstraintMaxWidth) != -2) {
              continue;
            }
          }
          this.matchConstraintMaxWidth = -2;
          continue;
          try
          {
            this.matchConstraintMinWidth = paramContext.getDimensionPixelSize(j, this.matchConstraintMinWidth);
          }
          catch (Exception paramAttributeSet)
          {
            if (paramContext.getInt(j, this.matchConstraintMinWidth) != -2) {
              continue;
            }
          }
          this.matchConstraintMinWidth = -2;
          continue;
          this.matchConstraintDefaultHeight = paramContext.getInt(j, 0);
          if (this.matchConstraintDefaultHeight == 1)
          {
            Log.e("ConstraintLayout", "layout_constraintHeight_default=\"wrap\" is deprecated.\nUse layout_height=\"WRAP_CONTENT\" and layout_constrainedHeight=\"true\" instead.");
            continue;
            this.matchConstraintDefaultWidth = paramContext.getInt(j, 0);
            if (this.matchConstraintDefaultWidth == 1)
            {
              Log.e("ConstraintLayout", "layout_constraintWidth_default=\"wrap\" is deprecated.\nUse layout_width=\"WRAP_CONTENT\" and layout_constrainedWidth=\"true\" instead.");
              continue;
              this.verticalBias = paramContext.getFloat(j, this.verticalBias);
              continue;
              this.horizontalBias = paramContext.getFloat(j, this.horizontalBias);
              continue;
              this.constrainedHeight = paramContext.getBoolean(j, this.constrainedHeight);
              continue;
              this.constrainedWidth = paramContext.getBoolean(j, this.constrainedWidth);
              continue;
              this.goneEndMargin = paramContext.getDimensionPixelSize(j, this.goneEndMargin);
              continue;
              this.goneStartMargin = paramContext.getDimensionPixelSize(j, this.goneStartMargin);
              continue;
              this.goneBottomMargin = paramContext.getDimensionPixelSize(j, this.goneBottomMargin);
              continue;
              this.goneRightMargin = paramContext.getDimensionPixelSize(j, this.goneRightMargin);
              continue;
              this.goneTopMargin = paramContext.getDimensionPixelSize(j, this.goneTopMargin);
              continue;
              this.goneLeftMargin = paramContext.getDimensionPixelSize(j, this.goneLeftMargin);
              continue;
              this.endToEnd = paramContext.getResourceId(j, this.endToEnd);
              if (this.endToEnd == -1)
              {
                this.endToEnd = paramContext.getInt(j, -1);
                continue;
                this.endToStart = paramContext.getResourceId(j, this.endToStart);
                if (this.endToStart == -1)
                {
                  this.endToStart = paramContext.getInt(j, -1);
                  continue;
                  this.startToStart = paramContext.getResourceId(j, this.startToStart);
                  if (this.startToStart == -1)
                  {
                    this.startToStart = paramContext.getInt(j, -1);
                    continue;
                    this.startToEnd = paramContext.getResourceId(j, this.startToEnd);
                    if (this.startToEnd == -1)
                    {
                      this.startToEnd = paramContext.getInt(j, -1);
                      continue;
                      this.baselineToBaseline = paramContext.getResourceId(j, this.baselineToBaseline);
                      if (this.baselineToBaseline == -1)
                      {
                        this.baselineToBaseline = paramContext.getInt(j, -1);
                        continue;
                        this.bottomToBottom = paramContext.getResourceId(j, this.bottomToBottom);
                        if (this.bottomToBottom == -1)
                        {
                          this.bottomToBottom = paramContext.getInt(j, -1);
                          continue;
                          this.bottomToTop = paramContext.getResourceId(j, this.bottomToTop);
                          if (this.bottomToTop == -1)
                          {
                            this.bottomToTop = paramContext.getInt(j, -1);
                            continue;
                            this.topToBottom = paramContext.getResourceId(j, this.topToBottom);
                            if (this.topToBottom == -1)
                            {
                              this.topToBottom = paramContext.getInt(j, -1);
                              continue;
                              this.topToTop = paramContext.getResourceId(j, this.topToTop);
                              if (this.topToTop == -1)
                              {
                                this.topToTop = paramContext.getInt(j, -1);
                                continue;
                                this.rightToRight = paramContext.getResourceId(j, this.rightToRight);
                                if (this.rightToRight == -1)
                                {
                                  this.rightToRight = paramContext.getInt(j, -1);
                                  continue;
                                  this.rightToLeft = paramContext.getResourceId(j, this.rightToLeft);
                                  if (this.rightToLeft == -1)
                                  {
                                    this.rightToLeft = paramContext.getInt(j, -1);
                                    continue;
                                    this.leftToRight = paramContext.getResourceId(j, this.leftToRight);
                                    if (this.leftToRight == -1)
                                    {
                                      this.leftToRight = paramContext.getInt(j, -1);
                                      continue;
                                      this.leftToLeft = paramContext.getResourceId(j, this.leftToLeft);
                                      if (this.leftToLeft == -1)
                                      {
                                        this.leftToLeft = paramContext.getInt(j, -1);
                                        continue;
                                        this.guidePercent = paramContext.getFloat(j, this.guidePercent);
                                        continue;
                                        this.guideEnd = paramContext.getDimensionPixelOffset(j, this.guideEnd);
                                        continue;
                                        this.guideBegin = paramContext.getDimensionPixelOffset(j, this.guideBegin);
                                        continue;
                                        this.circleAngle = (paramContext.getFloat(j, this.circleAngle) % 360.0F);
                                        f1 = this.circleAngle;
                                        if (f1 < 0.0F)
                                        {
                                          this.circleAngle = ((360.0F - f1) % 360.0F);
                                          continue;
                                          this.circleRadius = paramContext.getDimensionPixelSize(j, this.circleRadius);
                                          continue;
                                          this.circleConstraint = paramContext.getResourceId(j, this.circleConstraint);
                                          if (this.circleConstraint == -1)
                                          {
                                            this.circleConstraint = paramContext.getInt(j, -1);
                                            continue;
                                            this.orientation = paramContext.getInt(j, this.orientation);
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
      paramContext.recycle();
      validate();
    }
    
    public LayoutParams(LayoutParams paramLayoutParams)
    {
      super();
      this.guideBegin = paramLayoutParams.guideBegin;
      this.guideEnd = paramLayoutParams.guideEnd;
      this.guidePercent = paramLayoutParams.guidePercent;
      this.leftToLeft = paramLayoutParams.leftToLeft;
      this.leftToRight = paramLayoutParams.leftToRight;
      this.rightToLeft = paramLayoutParams.rightToLeft;
      this.rightToRight = paramLayoutParams.rightToRight;
      this.topToTop = paramLayoutParams.topToTop;
      this.topToBottom = paramLayoutParams.topToBottom;
      this.bottomToTop = paramLayoutParams.bottomToTop;
      this.bottomToBottom = paramLayoutParams.bottomToBottom;
      this.baselineToBaseline = paramLayoutParams.baselineToBaseline;
      this.circleConstraint = paramLayoutParams.circleConstraint;
      this.circleRadius = paramLayoutParams.circleRadius;
      this.circleAngle = paramLayoutParams.circleAngle;
      this.startToEnd = paramLayoutParams.startToEnd;
      this.startToStart = paramLayoutParams.startToStart;
      this.endToStart = paramLayoutParams.endToStart;
      this.endToEnd = paramLayoutParams.endToEnd;
      this.goneLeftMargin = paramLayoutParams.goneLeftMargin;
      this.goneTopMargin = paramLayoutParams.goneTopMargin;
      this.goneRightMargin = paramLayoutParams.goneRightMargin;
      this.goneBottomMargin = paramLayoutParams.goneBottomMargin;
      this.goneStartMargin = paramLayoutParams.goneStartMargin;
      this.goneEndMargin = paramLayoutParams.goneEndMargin;
      this.horizontalBias = paramLayoutParams.horizontalBias;
      this.verticalBias = paramLayoutParams.verticalBias;
      this.dimensionRatio = paramLayoutParams.dimensionRatio;
      this.dimensionRatioValue = paramLayoutParams.dimensionRatioValue;
      this.dimensionRatioSide = paramLayoutParams.dimensionRatioSide;
      this.horizontalWeight = paramLayoutParams.horizontalWeight;
      this.verticalWeight = paramLayoutParams.verticalWeight;
      this.horizontalChainStyle = paramLayoutParams.horizontalChainStyle;
      this.verticalChainStyle = paramLayoutParams.verticalChainStyle;
      this.constrainedWidth = paramLayoutParams.constrainedWidth;
      this.constrainedHeight = paramLayoutParams.constrainedHeight;
      this.matchConstraintDefaultWidth = paramLayoutParams.matchConstraintDefaultWidth;
      this.matchConstraintDefaultHeight = paramLayoutParams.matchConstraintDefaultHeight;
      this.matchConstraintMinWidth = paramLayoutParams.matchConstraintMinWidth;
      this.matchConstraintMaxWidth = paramLayoutParams.matchConstraintMaxWidth;
      this.matchConstraintMinHeight = paramLayoutParams.matchConstraintMinHeight;
      this.matchConstraintMaxHeight = paramLayoutParams.matchConstraintMaxHeight;
      this.matchConstraintPercentWidth = paramLayoutParams.matchConstraintPercentWidth;
      this.matchConstraintPercentHeight = paramLayoutParams.matchConstraintPercentHeight;
      this.editorAbsoluteX = paramLayoutParams.editorAbsoluteX;
      this.editorAbsoluteY = paramLayoutParams.editorAbsoluteY;
      this.orientation = paramLayoutParams.orientation;
      this.horizontalDimensionFixed = paramLayoutParams.horizontalDimensionFixed;
      this.verticalDimensionFixed = paramLayoutParams.verticalDimensionFixed;
      this.needsBaseline = paramLayoutParams.needsBaseline;
      this.isGuideline = paramLayoutParams.isGuideline;
      this.resolvedLeftToLeft = paramLayoutParams.resolvedLeftToLeft;
      this.resolvedLeftToRight = paramLayoutParams.resolvedLeftToRight;
      this.resolvedRightToLeft = paramLayoutParams.resolvedRightToLeft;
      this.resolvedRightToRight = paramLayoutParams.resolvedRightToRight;
      this.resolveGoneLeftMargin = paramLayoutParams.resolveGoneLeftMargin;
      this.resolveGoneRightMargin = paramLayoutParams.resolveGoneRightMargin;
      this.resolvedHorizontalBias = paramLayoutParams.resolvedHorizontalBias;
      this.widget = paramLayoutParams.widget;
    }
    
    public LayoutParams(ViewGroup.LayoutParams paramLayoutParams)
    {
      super();
    }
    
    public void reset()
    {
      ConstraintWidget localConstraintWidget = this.widget;
      if (localConstraintWidget != null) {
        localConstraintWidget.reset();
      }
    }
    
    @TargetApi(17)
    public void resolveLayoutDirection(int paramInt)
    {
      int j = this.leftMargin;
      int k = this.rightMargin;
      super.resolveLayoutDirection(paramInt);
      this.resolvedRightToLeft = -1;
      this.resolvedRightToRight = -1;
      this.resolvedLeftToLeft = -1;
      this.resolvedLeftToRight = -1;
      this.resolveGoneLeftMargin = -1;
      this.resolveGoneRightMargin = -1;
      this.resolveGoneLeftMargin = this.goneLeftMargin;
      this.resolveGoneRightMargin = this.goneRightMargin;
      this.resolvedHorizontalBias = this.horizontalBias;
      this.resolvedGuideBegin = this.guideBegin;
      this.resolvedGuideEnd = this.guideEnd;
      this.resolvedGuidePercent = this.guidePercent;
      paramInt = getLayoutDirection();
      int i = 0;
      if (1 == paramInt) {
        paramInt = 1;
      } else {
        paramInt = 0;
      }
      if (paramInt != 0)
      {
        paramInt = this.startToEnd;
        if (paramInt != -1)
        {
          this.resolvedRightToLeft = paramInt;
          paramInt = 1;
        }
        else
        {
          int m = this.startToStart;
          paramInt = i;
          if (m != -1)
          {
            this.resolvedRightToRight = m;
            paramInt = 1;
          }
        }
        i = this.endToStart;
        if (i != -1)
        {
          this.resolvedLeftToRight = i;
          paramInt = 1;
        }
        i = this.endToEnd;
        if (i != -1)
        {
          this.resolvedLeftToLeft = i;
          paramInt = 1;
        }
        i = this.goneStartMargin;
        if (i != -1) {
          this.resolveGoneRightMargin = i;
        }
        i = this.goneEndMargin;
        if (i != -1) {
          this.resolveGoneLeftMargin = i;
        }
        if (paramInt != 0) {
          this.resolvedHorizontalBias = (1.0F - this.horizontalBias);
        }
        if ((this.isGuideline) && (this.orientation == 1))
        {
          float f = this.guidePercent;
          if (f != -1.0F)
          {
            this.resolvedGuidePercent = (1.0F - f);
            this.resolvedGuideBegin = -1;
            this.resolvedGuideEnd = -1;
          }
          else
          {
            paramInt = this.guideBegin;
            if (paramInt != -1)
            {
              this.resolvedGuideEnd = paramInt;
              this.resolvedGuideBegin = -1;
              this.resolvedGuidePercent = -1.0F;
            }
            else
            {
              paramInt = this.guideEnd;
              if (paramInt != -1)
              {
                this.resolvedGuideBegin = paramInt;
                this.resolvedGuideEnd = -1;
                this.resolvedGuidePercent = -1.0F;
              }
            }
          }
        }
      }
      else
      {
        paramInt = this.startToEnd;
        if (paramInt != -1) {
          this.resolvedLeftToRight = paramInt;
        }
        paramInt = this.startToStart;
        if (paramInt != -1) {
          this.resolvedLeftToLeft = paramInt;
        }
        paramInt = this.endToStart;
        if (paramInt != -1) {
          this.resolvedRightToLeft = paramInt;
        }
        paramInt = this.endToEnd;
        if (paramInt != -1) {
          this.resolvedRightToRight = paramInt;
        }
        paramInt = this.goneStartMargin;
        if (paramInt != -1) {
          this.resolveGoneLeftMargin = paramInt;
        }
        paramInt = this.goneEndMargin;
        if (paramInt != -1) {
          this.resolveGoneRightMargin = paramInt;
        }
      }
      if ((this.endToStart == -1) && (this.endToEnd == -1) && (this.startToStart == -1) && (this.startToEnd == -1))
      {
        paramInt = this.rightToLeft;
        if (paramInt != -1)
        {
          this.resolvedRightToLeft = paramInt;
          if ((this.rightMargin <= 0) && (k > 0)) {
            this.rightMargin = k;
          }
        }
        else
        {
          paramInt = this.rightToRight;
          if (paramInt != -1)
          {
            this.resolvedRightToRight = paramInt;
            if ((this.rightMargin <= 0) && (k > 0)) {
              this.rightMargin = k;
            }
          }
        }
        paramInt = this.leftToLeft;
        if (paramInt != -1)
        {
          this.resolvedLeftToLeft = paramInt;
          if ((this.leftMargin <= 0) && (j > 0)) {
            this.leftMargin = j;
          }
        }
        else
        {
          paramInt = this.leftToRight;
          if (paramInt != -1)
          {
            this.resolvedLeftToRight = paramInt;
            if ((this.leftMargin <= 0) && (j > 0)) {
              this.leftMargin = j;
            }
          }
        }
      }
    }
    
    public void validate()
    {
      this.isGuideline = false;
      this.horizontalDimensionFixed = true;
      this.verticalDimensionFixed = true;
      if ((this.width == -2) && (this.constrainedWidth))
      {
        this.horizontalDimensionFixed = false;
        this.matchConstraintDefaultWidth = 1;
      }
      if ((this.height == -2) && (this.constrainedHeight))
      {
        this.verticalDimensionFixed = false;
        this.matchConstraintDefaultHeight = 1;
      }
      if ((this.width == 0) || (this.width == -1))
      {
        this.horizontalDimensionFixed = false;
        if ((this.width == 0) && (this.matchConstraintDefaultWidth == 1))
        {
          this.width = -2;
          this.constrainedWidth = true;
        }
      }
      if ((this.height == 0) || (this.height == -1))
      {
        this.verticalDimensionFixed = false;
        if ((this.height == 0) && (this.matchConstraintDefaultHeight == 1))
        {
          this.height = -2;
          this.constrainedHeight = true;
        }
      }
      if ((this.guidePercent != -1.0F) || (this.guideBegin != -1) || (this.guideEnd != -1))
      {
        this.isGuideline = true;
        this.horizontalDimensionFixed = true;
        this.verticalDimensionFixed = true;
        if (!(this.widget instanceof android.support.constraint.solver.widgets.Guideline)) {
          this.widget = new android.support.constraint.solver.widgets.Guideline();
        }
        ((android.support.constraint.solver.widgets.Guideline)this.widget).setOrientation(this.orientation);
      }
    }
    
    private static class Table
    {
      public static final int ANDROID_ORIENTATION = 1;
      public static final int LAYOUT_CONSTRAINED_HEIGHT = 28;
      public static final int LAYOUT_CONSTRAINED_WIDTH = 27;
      public static final int LAYOUT_CONSTRAINT_BASELINE_CREATOR = 43;
      public static final int LAYOUT_CONSTRAINT_BASELINE_TO_BASELINE_OF = 16;
      public static final int LAYOUT_CONSTRAINT_BOTTOM_CREATOR = 42;
      public static final int LAYOUT_CONSTRAINT_BOTTOM_TO_BOTTOM_OF = 15;
      public static final int LAYOUT_CONSTRAINT_BOTTOM_TO_TOP_OF = 14;
      public static final int LAYOUT_CONSTRAINT_CIRCLE = 2;
      public static final int LAYOUT_CONSTRAINT_CIRCLE_ANGLE = 4;
      public static final int LAYOUT_CONSTRAINT_CIRCLE_RADIUS = 3;
      public static final int LAYOUT_CONSTRAINT_DIMENSION_RATIO = 44;
      public static final int LAYOUT_CONSTRAINT_END_TO_END_OF = 20;
      public static final int LAYOUT_CONSTRAINT_END_TO_START_OF = 19;
      public static final int LAYOUT_CONSTRAINT_GUIDE_BEGIN = 5;
      public static final int LAYOUT_CONSTRAINT_GUIDE_END = 6;
      public static final int LAYOUT_CONSTRAINT_GUIDE_PERCENT = 7;
      public static final int LAYOUT_CONSTRAINT_HEIGHT_DEFAULT = 32;
      public static final int LAYOUT_CONSTRAINT_HEIGHT_MAX = 37;
      public static final int LAYOUT_CONSTRAINT_HEIGHT_MIN = 36;
      public static final int LAYOUT_CONSTRAINT_HEIGHT_PERCENT = 38;
      public static final int LAYOUT_CONSTRAINT_HORIZONTAL_BIAS = 29;
      public static final int LAYOUT_CONSTRAINT_HORIZONTAL_CHAINSTYLE = 47;
      public static final int LAYOUT_CONSTRAINT_HORIZONTAL_WEIGHT = 45;
      public static final int LAYOUT_CONSTRAINT_LEFT_CREATOR = 39;
      public static final int LAYOUT_CONSTRAINT_LEFT_TO_LEFT_OF = 8;
      public static final int LAYOUT_CONSTRAINT_LEFT_TO_RIGHT_OF = 9;
      public static final int LAYOUT_CONSTRAINT_RIGHT_CREATOR = 41;
      public static final int LAYOUT_CONSTRAINT_RIGHT_TO_LEFT_OF = 10;
      public static final int LAYOUT_CONSTRAINT_RIGHT_TO_RIGHT_OF = 11;
      public static final int LAYOUT_CONSTRAINT_START_TO_END_OF = 17;
      public static final int LAYOUT_CONSTRAINT_START_TO_START_OF = 18;
      public static final int LAYOUT_CONSTRAINT_TOP_CREATOR = 40;
      public static final int LAYOUT_CONSTRAINT_TOP_TO_BOTTOM_OF = 13;
      public static final int LAYOUT_CONSTRAINT_TOP_TO_TOP_OF = 12;
      public static final int LAYOUT_CONSTRAINT_VERTICAL_BIAS = 30;
      public static final int LAYOUT_CONSTRAINT_VERTICAL_CHAINSTYLE = 48;
      public static final int LAYOUT_CONSTRAINT_VERTICAL_WEIGHT = 46;
      public static final int LAYOUT_CONSTRAINT_WIDTH_DEFAULT = 31;
      public static final int LAYOUT_CONSTRAINT_WIDTH_MAX = 34;
      public static final int LAYOUT_CONSTRAINT_WIDTH_MIN = 33;
      public static final int LAYOUT_CONSTRAINT_WIDTH_PERCENT = 35;
      public static final int LAYOUT_EDITOR_ABSOLUTEX = 49;
      public static final int LAYOUT_EDITOR_ABSOLUTEY = 50;
      public static final int LAYOUT_GONE_MARGIN_BOTTOM = 24;
      public static final int LAYOUT_GONE_MARGIN_END = 26;
      public static final int LAYOUT_GONE_MARGIN_LEFT = 21;
      public static final int LAYOUT_GONE_MARGIN_RIGHT = 23;
      public static final int LAYOUT_GONE_MARGIN_START = 25;
      public static final int LAYOUT_GONE_MARGIN_TOP = 22;
      public static final int UNUSED = 0;
      public static final SparseIntArray map = new SparseIntArray();
      
      static
      {
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_toLeftOf, 8);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_toRightOf, 9);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_toLeftOf, 10);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_toRightOf, 11);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_toTopOf, 12);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_toBottomOf, 13);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_toTopOf, 14);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_toBottomOf, 15);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBaseline_toBaselineOf, 16);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircle, 2);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircleRadius, 3);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintCircleAngle, 4);
        map.append(R.styleable.ConstraintLayout_Layout_layout_editor_absoluteX, 49);
        map.append(R.styleable.ConstraintLayout_Layout_layout_editor_absoluteY, 50);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_begin, 5);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_end, 6);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintGuide_percent, 7);
        map.append(R.styleable.ConstraintLayout_Layout_android_orientation, 1);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintStart_toEndOf, 17);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintStart_toStartOf, 18);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintEnd_toStartOf, 19);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintEnd_toEndOf, 20);
        map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginLeft, 21);
        map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginTop, 22);
        map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginRight, 23);
        map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginBottom, 24);
        map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginStart, 25);
        map.append(R.styleable.ConstraintLayout_Layout_layout_goneMarginEnd, 26);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_bias, 29);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_bias, 30);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintDimensionRatio, 44);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_weight, 45);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_weight, 46);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHorizontal_chainStyle, 47);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintVertical_chainStyle, 48);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constrainedWidth, 27);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constrainedHeight, 28);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_default, 31);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_default, 32);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_min, 33);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_max, 34);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintWidth_percent, 35);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_min, 36);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_max, 37);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintHeight_percent, 38);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintLeft_creator, 39);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintTop_creator, 40);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintRight_creator, 41);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBottom_creator, 42);
        map.append(R.styleable.ConstraintLayout_Layout_layout_constraintBaseline_creator, 43);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/constraint/ConstraintLayout.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */