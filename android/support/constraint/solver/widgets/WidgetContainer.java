package android.support.constraint.solver.widgets;

import android.support.constraint.solver.Cache;
import java.util.ArrayList;

public class WidgetContainer
  extends ConstraintWidget
{
  protected ArrayList<ConstraintWidget> mChildren = new ArrayList();
  
  public WidgetContainer() {}
  
  public WidgetContainer(int paramInt1, int paramInt2)
  {
    super(paramInt1, paramInt2);
  }
  
  public WidgetContainer(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    super(paramInt1, paramInt2, paramInt3, paramInt4);
  }
  
  public static Rectangle getBounds(ArrayList<ConstraintWidget> paramArrayList)
  {
    Rectangle localRectangle = new Rectangle();
    if (paramArrayList.size() == 0) {
      return localRectangle;
    }
    int i5 = paramArrayList.size();
    int i4 = Integer.MAX_VALUE;
    int j = 0;
    int n = Integer.MAX_VALUE;
    int k = 0;
    int i3;
    for (int i = 0; j < i5; i = i3)
    {
      ConstraintWidget localConstraintWidget = (ConstraintWidget)paramArrayList.get(j);
      int m = i4;
      if (localConstraintWidget.getX() < i4) {
        m = localConstraintWidget.getX();
      }
      int i1 = n;
      if (localConstraintWidget.getY() < n) {
        i1 = localConstraintWidget.getY();
      }
      int i2 = k;
      if (localConstraintWidget.getRight() > k) {
        i2 = localConstraintWidget.getRight();
      }
      i3 = i;
      if (localConstraintWidget.getBottom() > i) {
        i3 = localConstraintWidget.getBottom();
      }
      j++;
      i4 = m;
      n = i1;
      k = i2;
    }
    localRectangle.setBounds(i4, n, k - i4, i - n);
    return localRectangle;
  }
  
  public void add(ConstraintWidget paramConstraintWidget)
  {
    this.mChildren.add(paramConstraintWidget);
    if (paramConstraintWidget.getParent() != null) {
      ((WidgetContainer)paramConstraintWidget.getParent()).remove(paramConstraintWidget);
    }
    paramConstraintWidget.setParent(this);
  }
  
  public void add(ConstraintWidget... paramVarArgs)
  {
    int j = paramVarArgs.length;
    for (int i = 0; i < j; i++) {
      add(paramVarArgs[i]);
    }
  }
  
  public ConstraintWidget findWidget(float paramFloat1, float paramFloat2)
  {
    int i = getDrawX();
    int k = getDrawY();
    int m = getWidth();
    int j = getHeight();
    Object localObject1;
    if ((paramFloat1 >= i) && (paramFloat1 <= m + i) && (paramFloat2 >= k) && (paramFloat2 <= j + k)) {
      localObject1 = this;
    } else {
      localObject1 = null;
    }
    i = 0;
    j = this.mChildren.size();
    for (Object localObject2 = localObject1; i < j; localObject2 = localObject1)
    {
      ConstraintWidget localConstraintWidget = (ConstraintWidget)this.mChildren.get(i);
      if ((localConstraintWidget instanceof WidgetContainer))
      {
        localConstraintWidget = ((WidgetContainer)localConstraintWidget).findWidget(paramFloat1, paramFloat2);
        localObject1 = localObject2;
        if (localConstraintWidget != null) {
          localObject1 = localConstraintWidget;
        }
      }
      else
      {
        int n = localConstraintWidget.getDrawX();
        m = localConstraintWidget.getDrawY();
        k = localConstraintWidget.getWidth();
        int i1 = localConstraintWidget.getHeight();
        localObject1 = localObject2;
        if (paramFloat1 >= n)
        {
          localObject1 = localObject2;
          if (paramFloat1 <= k + n)
          {
            localObject1 = localObject2;
            if (paramFloat2 >= m)
            {
              localObject1 = localObject2;
              if (paramFloat2 <= i1 + m) {
                localObject1 = localConstraintWidget;
              }
            }
          }
        }
      }
      i++;
    }
    return (ConstraintWidget)localObject2;
  }
  
  public ArrayList<ConstraintWidget> findWidgets(int paramInt1, int paramInt2, int paramInt3, int paramInt4)
  {
    ArrayList localArrayList = new ArrayList();
    Rectangle localRectangle2 = new Rectangle();
    localRectangle2.setBounds(paramInt1, paramInt2, paramInt3, paramInt4);
    paramInt2 = this.mChildren.size();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++)
    {
      ConstraintWidget localConstraintWidget = (ConstraintWidget)this.mChildren.get(paramInt1);
      Rectangle localRectangle1 = new Rectangle();
      localRectangle1.setBounds(localConstraintWidget.getDrawX(), localConstraintWidget.getDrawY(), localConstraintWidget.getWidth(), localConstraintWidget.getHeight());
      if (localRectangle2.intersects(localRectangle1)) {
        localArrayList.add(localConstraintWidget);
      }
    }
    return localArrayList;
  }
  
  public ArrayList<ConstraintWidget> getChildren()
  {
    return this.mChildren;
  }
  
  public ConstraintWidgetContainer getRootConstraintContainer()
  {
    Object localObject = getParent();
    ConstraintWidgetContainer localConstraintWidgetContainer;
    if ((this instanceof ConstraintWidgetContainer)) {
      localConstraintWidgetContainer = (ConstraintWidgetContainer)this;
    } else {
      localConstraintWidgetContainer = null;
    }
    while (localObject != null)
    {
      ConstraintWidget localConstraintWidget = ((ConstraintWidget)localObject).getParent();
      if ((localObject instanceof ConstraintWidgetContainer))
      {
        localConstraintWidgetContainer = (ConstraintWidgetContainer)localObject;
        localObject = localConstraintWidget;
      }
      else
      {
        localObject = localConstraintWidget;
      }
    }
    return localConstraintWidgetContainer;
  }
  
  public void layout()
  {
    updateDrawPosition();
    Object localObject = this.mChildren;
    if (localObject == null) {
      return;
    }
    int j = ((ArrayList)localObject).size();
    for (int i = 0; i < j; i++)
    {
      localObject = (ConstraintWidget)this.mChildren.get(i);
      if ((localObject instanceof WidgetContainer)) {
        ((WidgetContainer)localObject).layout();
      }
    }
  }
  
  public void remove(ConstraintWidget paramConstraintWidget)
  {
    this.mChildren.remove(paramConstraintWidget);
    paramConstraintWidget.setParent(null);
  }
  
  public void removeAllChildren()
  {
    this.mChildren.clear();
  }
  
  public void reset()
  {
    this.mChildren.clear();
    super.reset();
  }
  
  public void resetSolverVariables(Cache paramCache)
  {
    super.resetSolverVariables(paramCache);
    int j = this.mChildren.size();
    for (int i = 0; i < j; i++) {
      ((ConstraintWidget)this.mChildren.get(i)).resetSolverVariables(paramCache);
    }
  }
  
  public void setOffset(int paramInt1, int paramInt2)
  {
    super.setOffset(paramInt1, paramInt2);
    paramInt2 = this.mChildren.size();
    for (paramInt1 = 0; paramInt1 < paramInt2; paramInt1++) {
      ((ConstraintWidget)this.mChildren.get(paramInt1)).setOffset(getRootX(), getRootY());
    }
  }
  
  public void updateDrawPosition()
  {
    super.updateDrawPosition();
    Object localObject = this.mChildren;
    if (localObject == null) {
      return;
    }
    int j = ((ArrayList)localObject).size();
    for (int i = 0; i < j; i++)
    {
      localObject = (ConstraintWidget)this.mChildren.get(i);
      ((ConstraintWidget)localObject).setOffset(getDrawX(), getDrawY());
      if (!(localObject instanceof ConstraintWidgetContainer)) {
        ((ConstraintWidget)localObject).updateDrawPosition();
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/constraint/solver/widgets/WidgetContainer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */