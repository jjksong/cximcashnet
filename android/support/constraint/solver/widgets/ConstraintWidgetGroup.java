package android.support.constraint.solver.widgets;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ConstraintWidgetGroup
{
  public List<ConstraintWidget> mConstrainedGroup;
  public final int[] mGroupDimensions = { this.mGroupWidth, this.mGroupHeight };
  int mGroupHeight = -1;
  int mGroupWidth = -1;
  public boolean mSkipSolver = false;
  List<ConstraintWidget> mStartHorizontalWidgets = new ArrayList();
  List<ConstraintWidget> mStartVerticalWidgets = new ArrayList();
  List<ConstraintWidget> mUnresolvedWidgets = new ArrayList();
  HashSet<ConstraintWidget> mWidgetsToSetHorizontal = new HashSet();
  HashSet<ConstraintWidget> mWidgetsToSetVertical = new HashSet();
  List<ConstraintWidget> mWidgetsToSolve = new ArrayList();
  
  ConstraintWidgetGroup(List<ConstraintWidget> paramList)
  {
    this.mConstrainedGroup = paramList;
  }
  
  ConstraintWidgetGroup(List<ConstraintWidget> paramList, boolean paramBoolean)
  {
    this.mConstrainedGroup = paramList;
    this.mSkipSolver = paramBoolean;
  }
  
  private void getWidgetsToSolveTraversal(ArrayList<ConstraintWidget> paramArrayList, ConstraintWidget paramConstraintWidget)
  {
    if (paramConstraintWidget.mGroupsToSolver) {
      return;
    }
    paramArrayList.add(paramConstraintWidget);
    paramConstraintWidget.mGroupsToSolver = true;
    if (paramConstraintWidget.isFullyResolved()) {
      return;
    }
    boolean bool = paramConstraintWidget instanceof Helper;
    int j = 0;
    Object localObject;
    if (bool)
    {
      localObject = (Helper)paramConstraintWidget;
      k = ((Helper)localObject).mWidgetsCount;
      for (i = 0; i < k; i++) {
        getWidgetsToSolveTraversal(paramArrayList, localObject.mWidgets[i]);
      }
    }
    int k = paramConstraintWidget.mListAnchors.length;
    for (int i = j; i < k; i++)
    {
      ConstraintAnchor localConstraintAnchor = paramConstraintWidget.mListAnchors[i].mTarget;
      if (localConstraintAnchor != null)
      {
        localObject = localConstraintAnchor.mOwner;
        if ((localConstraintAnchor != null) && (localObject != paramConstraintWidget.getParent())) {
          getWidgetsToSolveTraversal(paramArrayList, (ConstraintWidget)localObject);
        }
      }
    }
  }
  
  private void updateResolvedDimension(ConstraintWidget paramConstraintWidget)
  {
    if (paramConstraintWidget.mOptimizerMeasurable)
    {
      if (paramConstraintWidget.isFullyResolved()) {
        return;
      }
      ConstraintAnchor localConstraintAnchor = paramConstraintWidget.mRight.mTarget;
      int k = 0;
      if (localConstraintAnchor != null) {
        j = 1;
      } else {
        j = 0;
      }
      if (j != 0) {
        localConstraintAnchor = paramConstraintWidget.mRight.mTarget;
      } else {
        localConstraintAnchor = paramConstraintWidget.mLeft.mTarget;
      }
      if (localConstraintAnchor != null)
      {
        if (!localConstraintAnchor.mOwner.mOptimizerMeasured) {
          updateResolvedDimension(localConstraintAnchor.mOwner);
        }
        if (localConstraintAnchor.mType == ConstraintAnchor.Type.RIGHT)
        {
          i = localConstraintAnchor.mOwner.mX;
          i = localConstraintAnchor.mOwner.getWidth() + i;
          break label148;
        }
        if (localConstraintAnchor.mType == ConstraintAnchor.Type.LEFT)
        {
          i = localConstraintAnchor.mOwner.mX;
          break label148;
        }
      }
      int i = 0;
      label148:
      if (j != 0) {
        i -= paramConstraintWidget.mRight.getMargin();
      } else {
        i += paramConstraintWidget.mLeft.getMargin() + paramConstraintWidget.getWidth();
      }
      paramConstraintWidget.setHorizontalDimension(i - paramConstraintWidget.getWidth(), i);
      if (paramConstraintWidget.mBaseline.mTarget != null)
      {
        localConstraintAnchor = paramConstraintWidget.mBaseline.mTarget;
        if (!localConstraintAnchor.mOwner.mOptimizerMeasured) {
          updateResolvedDimension(localConstraintAnchor.mOwner);
        }
        i = localConstraintAnchor.mOwner.mY + localConstraintAnchor.mOwner.mBaselineDistance - paramConstraintWidget.mBaselineDistance;
        paramConstraintWidget.setVerticalDimension(i, paramConstraintWidget.mHeight + i);
        paramConstraintWidget.mOptimizerMeasured = true;
        return;
      }
      if (paramConstraintWidget.mBottom.mTarget != null) {
        k = 1;
      }
      if (k != 0) {
        localConstraintAnchor = paramConstraintWidget.mBottom.mTarget;
      } else {
        localConstraintAnchor = paramConstraintWidget.mTop.mTarget;
      }
      int j = i;
      if (localConstraintAnchor != null)
      {
        if (!localConstraintAnchor.mOwner.mOptimizerMeasured) {
          updateResolvedDimension(localConstraintAnchor.mOwner);
        }
        if (localConstraintAnchor.mType == ConstraintAnchor.Type.BOTTOM)
        {
          j = localConstraintAnchor.mOwner.mY + localConstraintAnchor.mOwner.getHeight();
        }
        else
        {
          j = i;
          if (localConstraintAnchor.mType == ConstraintAnchor.Type.TOP) {
            j = localConstraintAnchor.mOwner.mY;
          }
        }
      }
      if (k != 0) {
        i = j - paramConstraintWidget.mBottom.getMargin();
      } else {
        i = j + (paramConstraintWidget.mTop.getMargin() + paramConstraintWidget.getHeight());
      }
      paramConstraintWidget.setVerticalDimension(i - paramConstraintWidget.getHeight(), i);
      paramConstraintWidget.mOptimizerMeasured = true;
    }
  }
  
  void addWidgetsToSet(ConstraintWidget paramConstraintWidget, int paramInt)
  {
    if (paramInt == 0) {
      this.mWidgetsToSetHorizontal.add(paramConstraintWidget);
    } else if (paramInt == 1) {
      this.mWidgetsToSetVertical.add(paramConstraintWidget);
    }
  }
  
  public List<ConstraintWidget> getStartWidgets(int paramInt)
  {
    if (paramInt == 0) {
      return this.mStartHorizontalWidgets;
    }
    if (paramInt == 1) {
      return this.mStartVerticalWidgets;
    }
    return null;
  }
  
  Set<ConstraintWidget> getWidgetsToSet(int paramInt)
  {
    if (paramInt == 0) {
      return this.mWidgetsToSetHorizontal;
    }
    if (paramInt == 1) {
      return this.mWidgetsToSetVertical;
    }
    return null;
  }
  
  List<ConstraintWidget> getWidgetsToSolve()
  {
    if (!this.mWidgetsToSolve.isEmpty()) {
      return this.mWidgetsToSolve;
    }
    int j = this.mConstrainedGroup.size();
    for (int i = 0; i < j; i++)
    {
      ConstraintWidget localConstraintWidget = (ConstraintWidget)this.mConstrainedGroup.get(i);
      if (!localConstraintWidget.mOptimizerMeasurable) {
        getWidgetsToSolveTraversal((ArrayList)this.mWidgetsToSolve, localConstraintWidget);
      }
    }
    this.mUnresolvedWidgets.clear();
    this.mUnresolvedWidgets.addAll(this.mConstrainedGroup);
    this.mUnresolvedWidgets.removeAll(this.mWidgetsToSolve);
    return this.mWidgetsToSolve;
  }
  
  void updateUnresolvedWidgets()
  {
    int j = this.mUnresolvedWidgets.size();
    for (int i = 0; i < j; i++) {
      updateResolvedDimension((ConstraintWidget)this.mUnresolvedWidgets.get(i));
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/constraint/solver/widgets/ConstraintWidgetGroup.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */