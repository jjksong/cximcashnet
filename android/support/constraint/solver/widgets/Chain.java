package android.support.constraint.solver.widgets;

import android.support.constraint.solver.ArrayRow;
import android.support.constraint.solver.LinearSystem;
import android.support.constraint.solver.SolverVariable;
import java.util.ArrayList;

class Chain
{
  private static final boolean DEBUG = false;
  
  static void applyChainConstraints(ConstraintWidgetContainer paramConstraintWidgetContainer, LinearSystem paramLinearSystem, int paramInt)
  {
    int k = 0;
    int j;
    ChainHead[] arrayOfChainHead;
    int i;
    if (paramInt == 0)
    {
      j = paramConstraintWidgetContainer.mHorizontalChainsSize;
      arrayOfChainHead = paramConstraintWidgetContainer.mHorizontalChainsArray;
      i = 0;
    }
    else
    {
      i = 2;
      j = paramConstraintWidgetContainer.mVerticalChainsSize;
      arrayOfChainHead = paramConstraintWidgetContainer.mVerticalChainsArray;
    }
    while (k < j)
    {
      ChainHead localChainHead = arrayOfChainHead[k];
      localChainHead.define();
      if (paramConstraintWidgetContainer.optimizeFor(4))
      {
        if (!Optimizer.applyChainOptimized(paramConstraintWidgetContainer, paramLinearSystem, paramInt, i, localChainHead)) {
          applyChainConstraints(paramConstraintWidgetContainer, paramLinearSystem, paramInt, i, localChainHead);
        }
      }
      else {
        applyChainConstraints(paramConstraintWidgetContainer, paramLinearSystem, paramInt, i, localChainHead);
      }
      k++;
    }
  }
  
  static void applyChainConstraints(ConstraintWidgetContainer paramConstraintWidgetContainer, LinearSystem paramLinearSystem, int paramInt1, int paramInt2, ChainHead paramChainHead)
  {
    ConstraintWidget localConstraintWidget2 = paramChainHead.mFirst;
    ConstraintWidget localConstraintWidget3 = paramChainHead.mLast;
    ConstraintWidget localConstraintWidget1 = paramChainHead.mFirstVisibleWidget;
    ConstraintWidget localConstraintWidget4 = paramChainHead.mLastVisibleWidget;
    Object localObject3 = paramChainHead.mHead;
    float f1 = paramChainHead.mTotalWeight;
    Object localObject1 = paramChainHead.mFirstMatchConstraintWidget;
    localObject1 = paramChainHead.mLastMatchConstraintWidget;
    int n;
    if (paramConstraintWidgetContainer.mListDimensionBehaviors[paramInt1] == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
      n = 1;
    } else {
      n = 0;
    }
    int i;
    int j;
    int k;
    int i2;
    int m;
    int i1;
    if (paramInt1 == 0)
    {
      if (((ConstraintWidget)localObject3).mHorizontalChainStyle == 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (((ConstraintWidget)localObject3).mHorizontalChainStyle == 1) {
        j = 1;
      } else {
        j = 0;
      }
      if (((ConstraintWidget)localObject3).mHorizontalChainStyle == 2) {
        k = 1;
      } else {
        k = 0;
      }
      i2 = i;
      localObject1 = localConstraintWidget2;
      m = j;
      i = 0;
      i1 = k;
      j = i2;
    }
    else
    {
      if (((ConstraintWidget)localObject3).mVerticalChainStyle == 0) {
        i = 1;
      } else {
        i = 0;
      }
      if (((ConstraintWidget)localObject3).mVerticalChainStyle == 1) {
        j = 1;
      } else {
        j = 0;
      }
      if (((ConstraintWidget)localObject3).mVerticalChainStyle == 2) {
        k = 1;
      } else {
        k = 0;
      }
      localObject1 = localConstraintWidget2;
      i2 = 0;
      m = j;
      j = i;
      i1 = k;
      i = i2;
    }
    Object localObject4;
    Object localObject2;
    Object localObject5;
    for (;;)
    {
      localObject4 = null;
      if (i != 0) {
        break;
      }
      localObject2 = localObject1.mListAnchors[paramInt2];
      if ((n == 0) && (i1 == 0)) {
        k = 4;
      } else {
        k = 1;
      }
      i2 = ((ConstraintAnchor)localObject2).getMargin();
      if ((((ConstraintAnchor)localObject2).mTarget != null) && (localObject1 != localConstraintWidget2)) {
        i2 += ((ConstraintAnchor)localObject2).mTarget.getMargin();
      }
      if ((i1 != 0) && (localObject1 != localConstraintWidget2) && (localObject1 != localConstraintWidget1)) {
        k = 6;
      } else if ((j != 0) && (n != 0)) {
        k = 4;
      }
      if (((ConstraintAnchor)localObject2).mTarget != null)
      {
        if (localObject1 == localConstraintWidget1) {
          paramLinearSystem.addGreaterThan(((ConstraintAnchor)localObject2).mSolverVariable, ((ConstraintAnchor)localObject2).mTarget.mSolverVariable, i2, 5);
        } else {
          paramLinearSystem.addGreaterThan(((ConstraintAnchor)localObject2).mSolverVariable, ((ConstraintAnchor)localObject2).mTarget.mSolverVariable, i2, 6);
        }
        paramLinearSystem.addEquality(((ConstraintAnchor)localObject2).mSolverVariable, ((ConstraintAnchor)localObject2).mTarget.mSolverVariable, i2, k);
      }
      if (n != 0)
      {
        if ((((ConstraintWidget)localObject1).getVisibility() != 8) && (localObject1.mListDimensionBehaviors[paramInt1] == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT)) {
          paramLinearSystem.addGreaterThan(localObject1.mListAnchors[(paramInt2 + 1)].mSolverVariable, localObject1.mListAnchors[paramInt2].mSolverVariable, 0, 5);
        }
        paramLinearSystem.addGreaterThan(localObject1.mListAnchors[paramInt2].mSolverVariable, paramConstraintWidgetContainer.mListAnchors[paramInt2].mSolverVariable, 0, 6);
      }
      localObject5 = localObject1.mListAnchors[(paramInt2 + 1)].mTarget;
      localObject2 = localObject4;
      if (localObject5 != null)
      {
        localObject5 = ((ConstraintAnchor)localObject5).mOwner;
        localObject2 = localObject4;
        if (localObject5.mListAnchors[paramInt2].mTarget != null) {
          if (localObject5.mListAnchors[paramInt2].mTarget.mOwner != localObject1) {
            localObject2 = localObject4;
          } else {
            localObject2 = localObject5;
          }
        }
      }
      if (localObject2 != null) {
        localObject1 = localObject2;
      } else {
        i = 1;
      }
    }
    if (localConstraintWidget4 != null)
    {
      localObject1 = localConstraintWidget3.mListAnchors;
      i = paramInt2 + 1;
      if (localObject1[i].mTarget != null)
      {
        localObject1 = localConstraintWidget4.mListAnchors[i];
        paramLinearSystem.addLowerThan(((ConstraintAnchor)localObject1).mSolverVariable, localConstraintWidget3.mListAnchors[i].mTarget.mSolverVariable, -((ConstraintAnchor)localObject1).getMargin(), 5);
      }
    }
    if (n != 0)
    {
      paramConstraintWidgetContainer = paramConstraintWidgetContainer.mListAnchors;
      i = paramInt2 + 1;
      paramLinearSystem.addGreaterThan(paramConstraintWidgetContainer[i].mSolverVariable, localConstraintWidget3.mListAnchors[i].mSolverVariable, localConstraintWidget3.mListAnchors[i].getMargin(), 6);
    }
    paramConstraintWidgetContainer = paramChainHead.mWeightedMatchConstraintsWidgets;
    label918:
    SolverVariable localSolverVariable1;
    if (paramConstraintWidgetContainer != null)
    {
      k = paramConstraintWidgetContainer.size();
      if (k > 1)
      {
        float f2;
        if ((paramChainHead.mHasUndefinedWeights) && (!paramChainHead.mHasComplexMatchWeights)) {
          f2 = paramChainHead.mWidgetsMatchCount;
        } else {
          f2 = f1;
        }
        localObject1 = null;
        i = 0;
        float f3 = 0.0F;
        while (i < k)
        {
          localObject2 = (ConstraintWidget)paramConstraintWidgetContainer.get(i);
          f1 = localObject2.mWeight[paramInt1];
          if (f1 < 0.0F)
          {
            if (paramChainHead.mHasComplexMatchWeights)
            {
              paramLinearSystem.addEquality(localObject2.mListAnchors[(paramInt2 + 1)].mSolverVariable, localObject2.mListAnchors[paramInt2].mSolverVariable, 0, 4);
              break label918;
            }
            f1 = 1.0F;
          }
          if (f1 == 0.0F)
          {
            paramLinearSystem.addEquality(localObject2.mListAnchors[(paramInt2 + 1)].mSolverVariable, localObject2.mListAnchors[paramInt2].mSolverVariable, 0, 6);
          }
          else
          {
            if (localObject1 != null)
            {
              localObject4 = localObject1.mListAnchors[paramInt2].mSolverVariable;
              localObject1 = ((ConstraintWidget)localObject1).mListAnchors;
              n = paramInt2 + 1;
              localSolverVariable1 = localObject1[n].mSolverVariable;
              SolverVariable localSolverVariable2 = localObject2.mListAnchors[paramInt2].mSolverVariable;
              localObject5 = localObject2.mListAnchors[n].mSolverVariable;
              localObject1 = paramLinearSystem.createRow();
              ((ArrayRow)localObject1).createRowEqualMatchDimensions(f3, f2, f1, (SolverVariable)localObject4, localSolverVariable1, localSolverVariable2, (SolverVariable)localObject5);
              paramLinearSystem.addConstraint((ArrayRow)localObject1);
            }
            localObject1 = localObject2;
            f3 = f1;
          }
          i++;
        }
      }
    }
    if ((localConstraintWidget1 != null) && ((localConstraintWidget1 == localConstraintWidget4) || (i1 != 0)))
    {
      localObject2 = localConstraintWidget2.mListAnchors[paramInt2];
      paramConstraintWidgetContainer = localConstraintWidget3.mListAnchors;
      i = paramInt2 + 1;
      localObject1 = paramConstraintWidgetContainer[i];
      if (localConstraintWidget2.mListAnchors[paramInt2].mTarget != null) {
        paramConstraintWidgetContainer = localConstraintWidget2.mListAnchors[paramInt2].mTarget.mSolverVariable;
      } else {
        paramConstraintWidgetContainer = null;
      }
      if (localConstraintWidget3.mListAnchors[i].mTarget != null) {
        paramChainHead = localConstraintWidget3.mListAnchors[i].mTarget.mSolverVariable;
      } else {
        paramChainHead = null;
      }
      if (localConstraintWidget1 == localConstraintWidget4)
      {
        localObject2 = localConstraintWidget1.mListAnchors[paramInt2];
        localObject1 = localConstraintWidget1.mListAnchors[i];
      }
      if ((paramConstraintWidgetContainer != null) && (paramChainHead != null))
      {
        if (paramInt1 == 0) {
          f1 = ((ConstraintWidget)localObject3).mHorizontalBiasPercent;
        } else {
          f1 = ((ConstraintWidget)localObject3).mVerticalBiasPercent;
        }
        i = ((ConstraintAnchor)localObject2).getMargin();
        paramInt1 = ((ConstraintAnchor)localObject1).getMargin();
        paramLinearSystem.addCentering(((ConstraintAnchor)localObject2).mSolverVariable, paramConstraintWidgetContainer, i, f1, paramChainHead, ((ConstraintAnchor)localObject1).mSolverVariable, paramInt1, 5);
      }
    }
    else
    {
      if ((j != 0) && (localConstraintWidget1 != null))
      {
        if ((paramChainHead.mWidgetsMatchCount > 0) && (paramChainHead.mWidgetsCount == paramChainHead.mWidgetsMatchCount)) {
          n = 1;
        } else {
          n = 0;
        }
        paramChainHead = localConstraintWidget1;
        localObject3 = paramChainHead;
      }
      while (paramChainHead != null)
      {
        for (localObject1 = paramChainHead.mNextChainWidget[paramInt1]; (localObject1 != null) && (((ConstraintWidget)localObject1).getVisibility() == 8); localObject1 = localObject1.mNextChainWidget[paramInt1]) {}
        if ((localObject1 == null) && (paramChainHead != localConstraintWidget4)) {
          break label1717;
        }
        localObject4 = paramChainHead.mListAnchors[paramInt2];
        localSolverVariable1 = ((ConstraintAnchor)localObject4).mSolverVariable;
        if (((ConstraintAnchor)localObject4).mTarget != null) {
          localObject2 = ((ConstraintAnchor)localObject4).mTarget.mSolverVariable;
        } else {
          localObject2 = null;
        }
        if (localObject3 != paramChainHead)
        {
          paramConstraintWidgetContainer = localObject3.mListAnchors[(paramInt2 + 1)].mSolverVariable;
        }
        else
        {
          paramConstraintWidgetContainer = (ConstraintWidgetContainer)localObject2;
          if (paramChainHead == localConstraintWidget1)
          {
            paramConstraintWidgetContainer = (ConstraintWidgetContainer)localObject2;
            if (localObject3 == paramChainHead) {
              if (localConstraintWidget2.mListAnchors[paramInt2].mTarget != null) {
                paramConstraintWidgetContainer = localConstraintWidget2.mListAnchors[paramInt2].mTarget.mSolverVariable;
              } else {
                paramConstraintWidgetContainer = null;
              }
            }
          }
        }
        i1 = ((ConstraintAnchor)localObject4).getMargin();
        localObject2 = paramChainHead.mListAnchors;
        i2 = paramInt2 + 1;
        k = localObject2[i2].getMargin();
        if (localObject1 != null)
        {
          localObject5 = localObject1.mListAnchors[paramInt2];
          localObject2 = ((ConstraintAnchor)localObject5).mSolverVariable;
          localObject4 = paramChainHead.mListAnchors[i2].mSolverVariable;
        }
        else
        {
          localObject5 = localConstraintWidget3.mListAnchors[i2].mTarget;
          if (localObject5 != null) {
            localObject2 = ((ConstraintAnchor)localObject5).mSolverVariable;
          } else {
            localObject2 = null;
          }
          localObject4 = paramChainHead.mListAnchors[i2].mSolverVariable;
        }
        i = k;
        if (localObject5 != null) {
          i = k + ((ConstraintAnchor)localObject5).getMargin();
        }
        k = i1;
        if (localObject3 != null) {
          k = i1 + localObject3.mListAnchors[i2].getMargin();
        }
        if ((localSolverVariable1 != null) && (paramConstraintWidgetContainer != null) && (localObject2 != null) && (localObject4 != null))
        {
          if (paramChainHead == localConstraintWidget1) {
            k = localConstraintWidget1.mListAnchors[paramInt2].getMargin();
          }
          if (paramChainHead == localConstraintWidget4) {
            i = localConstraintWidget4.mListAnchors[i2].getMargin();
          }
          if (n != 0) {
            i1 = 6;
          } else {
            i1 = 4;
          }
          paramLinearSystem.addCentering(localSolverVariable1, paramConstraintWidgetContainer, k, 0.5F, (SolverVariable)localObject2, (SolverVariable)localObject4, i, i1);
        }
        label1717:
        if (paramChainHead.getVisibility() != 8) {
          localObject3 = paramChainHead;
        }
        paramChainHead = (ChainHead)localObject1;
        continue;
        if ((m != 0) && (localConstraintWidget1 != null))
        {
          if ((paramChainHead.mWidgetsMatchCount > 0) && (paramChainHead.mWidgetsCount == paramChainHead.mWidgetsMatchCount)) {
            i = 1;
          } else {
            i = 0;
          }
          paramChainHead = localConstraintWidget1;
          localObject1 = paramChainHead;
          while (paramChainHead != null)
          {
            for (paramConstraintWidgetContainer = paramChainHead.mNextChainWidget[paramInt1]; (paramConstraintWidgetContainer != null) && (paramConstraintWidgetContainer.getVisibility() == 8); paramConstraintWidgetContainer = paramConstraintWidgetContainer.mNextChainWidget[paramInt1]) {}
            if ((paramChainHead != localConstraintWidget1) && (paramChainHead != localConstraintWidget4) && (paramConstraintWidgetContainer != null))
            {
              if (paramConstraintWidgetContainer == localConstraintWidget4) {
                paramConstraintWidgetContainer = null;
              }
              localObject2 = paramChainHead.mListAnchors[paramInt2];
              localObject5 = ((ConstraintAnchor)localObject2).mSolverVariable;
              if (((ConstraintAnchor)localObject2).mTarget != null) {
                localObject3 = ((ConstraintAnchor)localObject2).mTarget.mSolverVariable;
              }
              localObject3 = ((ConstraintWidget)localObject1).mListAnchors;
              i1 = paramInt2 + 1;
              localSolverVariable1 = localObject3[i1].mSolverVariable;
              n = ((ConstraintAnchor)localObject2).getMargin();
              k = paramChainHead.mListAnchors[i1].getMargin();
              if (paramConstraintWidgetContainer != null)
              {
                localObject4 = paramConstraintWidgetContainer.mListAnchors[paramInt2];
                localObject3 = ((ConstraintAnchor)localObject4).mSolverVariable;
                if (((ConstraintAnchor)localObject4).mTarget != null) {
                  localObject2 = ((ConstraintAnchor)localObject4).mTarget.mSolverVariable;
                } else {
                  localObject2 = null;
                }
              }
              else
              {
                localObject4 = paramChainHead.mListAnchors[i1].mTarget;
                if (localObject4 != null) {
                  localObject3 = ((ConstraintAnchor)localObject4).mSolverVariable;
                } else {
                  localObject3 = null;
                }
                localObject2 = paramChainHead.mListAnchors[i1].mSolverVariable;
              }
              if (localObject4 != null) {
                k += ((ConstraintAnchor)localObject4).getMargin();
              }
              if (localObject1 != null) {
                n += localObject1.mListAnchors[i1].getMargin();
              }
              if (i != 0) {
                i1 = 6;
              } else {
                i1 = 4;
              }
              if ((localObject5 != null) && (localSolverVariable1 != null) && (localObject3 != null) && (localObject2 != null)) {
                paramLinearSystem.addCentering((SolverVariable)localObject5, localSolverVariable1, n, 0.5F, (SolverVariable)localObject3, (SolverVariable)localObject2, k, i1);
              }
            }
            if (paramChainHead.getVisibility() == 8) {
              paramChainHead = (ChainHead)localObject1;
            }
            localObject1 = paramChainHead;
            paramChainHead = paramConstraintWidgetContainer;
          }
          paramConstraintWidgetContainer = localConstraintWidget1.mListAnchors[paramInt2];
          paramChainHead = localConstraintWidget2.mListAnchors[paramInt2].mTarget;
          localObject1 = localConstraintWidget4.mListAnchors;
          paramInt1 = paramInt2 + 1;
          localObject1 = localObject1[paramInt1];
          localObject2 = localConstraintWidget3.mListAnchors[paramInt1].mTarget;
          if (paramChainHead != null) {
            if (localConstraintWidget1 != localConstraintWidget4) {
              paramLinearSystem.addEquality(paramConstraintWidgetContainer.mSolverVariable, paramChainHead.mSolverVariable, paramConstraintWidgetContainer.getMargin(), 5);
            } else if (localObject2 != null) {
              paramLinearSystem.addCentering(paramConstraintWidgetContainer.mSolverVariable, paramChainHead.mSolverVariable, paramConstraintWidgetContainer.getMargin(), 0.5F, ((ConstraintAnchor)localObject1).mSolverVariable, ((ConstraintAnchor)localObject2).mSolverVariable, ((ConstraintAnchor)localObject1).getMargin(), 5);
            }
          }
          if ((localObject2 != null) && (localConstraintWidget1 != localConstraintWidget4)) {
            paramLinearSystem.addEquality(((ConstraintAnchor)localObject1).mSolverVariable, ((ConstraintAnchor)localObject2).mSolverVariable, -((ConstraintAnchor)localObject1).getMargin(), 5);
          }
        }
      }
    }
    if (((j != 0) || (m != 0)) && (localConstraintWidget1 != null))
    {
      localObject1 = localConstraintWidget1.mListAnchors[paramInt2];
      paramConstraintWidgetContainer = localConstraintWidget4.mListAnchors;
      paramInt1 = paramInt2 + 1;
      localObject2 = paramConstraintWidgetContainer[paramInt1];
      if (((ConstraintAnchor)localObject1).mTarget != null) {
        paramChainHead = ((ConstraintAnchor)localObject1).mTarget.mSolverVariable;
      } else {
        paramChainHead = null;
      }
      if (((ConstraintAnchor)localObject2).mTarget != null) {
        paramConstraintWidgetContainer = ((ConstraintAnchor)localObject2).mTarget.mSolverVariable;
      } else {
        paramConstraintWidgetContainer = null;
      }
      if (localConstraintWidget3 != localConstraintWidget4)
      {
        paramConstraintWidgetContainer = localConstraintWidget3.mListAnchors[paramInt1];
        if (paramConstraintWidgetContainer.mTarget != null) {
          paramConstraintWidgetContainer = paramConstraintWidgetContainer.mTarget.mSolverVariable;
        } else {
          paramConstraintWidgetContainer = null;
        }
      }
      if (localConstraintWidget1 == localConstraintWidget4)
      {
        localObject1 = localConstraintWidget1.mListAnchors[paramInt2];
        localObject2 = localConstraintWidget1.mListAnchors[paramInt1];
      }
      if ((paramChainHead != null) && (paramConstraintWidgetContainer != null))
      {
        paramInt2 = ((ConstraintAnchor)localObject1).getMargin();
        if (localConstraintWidget4 == null) {
          localObject3 = localConstraintWidget3;
        } else {
          localObject3 = localConstraintWidget4;
        }
        paramInt1 = localObject3.mListAnchors[paramInt1].getMargin();
        paramLinearSystem.addCentering(((ConstraintAnchor)localObject1).mSolverVariable, paramChainHead, paramInt2, 0.5F, paramConstraintWidgetContainer, ((ConstraintAnchor)localObject2).mSolverVariable, paramInt1, 5);
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/constraint/solver/widgets/Chain.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */