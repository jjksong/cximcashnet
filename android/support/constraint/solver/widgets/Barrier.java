package android.support.constraint.solver.widgets;

import android.support.constraint.solver.LinearSystem;
import android.support.constraint.solver.Metrics;
import android.support.constraint.solver.SolverVariable;
import java.util.ArrayList;

public class Barrier
  extends Helper
{
  public static final int BOTTOM = 3;
  public static final int LEFT = 0;
  public static final int RIGHT = 1;
  public static final int TOP = 2;
  private boolean mAllowsGoneWidget = true;
  private int mBarrierType = 0;
  private ArrayList<ResolutionAnchor> mNodes = new ArrayList(4);
  
  public void addToSolver(LinearSystem paramLinearSystem)
  {
    this.mListAnchors[0] = this.mLeft;
    this.mListAnchors[2] = this.mTop;
    this.mListAnchors[1] = this.mRight;
    this.mListAnchors[3] = this.mBottom;
    for (int i = 0; i < this.mListAnchors.length; i++) {
      this.mListAnchors[i].mSolverVariable = paramLinearSystem.createObjectVariable(this.mListAnchors[i]);
    }
    i = this.mBarrierType;
    if ((i >= 0) && (i < 4))
    {
      ConstraintAnchor localConstraintAnchor = this.mListAnchors[this.mBarrierType];
      Object localObject1;
      int j;
      for (i = 0; i < this.mWidgetsCount; i++)
      {
        localObject1 = this.mWidgets[i];
        if ((this.mAllowsGoneWidget) || (((ConstraintWidget)localObject1).allowedInBarrier()))
        {
          j = this.mBarrierType;
          if (((j == 0) || (j == 1)) && (((ConstraintWidget)localObject1).getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT))
          {
            bool = true;
            break label209;
          }
          j = this.mBarrierType;
          if (((j == 2) || (j == 3)) && (((ConstraintWidget)localObject1).getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.MATCH_CONSTRAINT))
          {
            bool = true;
            break label209;
          }
        }
      }
      boolean bool = false;
      label209:
      i = this.mBarrierType;
      if ((i != 0) && (i != 1))
      {
        if (getParent().getVerticalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
          bool = false;
        }
      }
      else if (getParent().getHorizontalDimensionBehaviour() == ConstraintWidget.DimensionBehaviour.WRAP_CONTENT) {
        bool = false;
      }
      for (i = 0; i < this.mWidgetsCount; i++)
      {
        Object localObject2 = this.mWidgets[i];
        if ((this.mAllowsGoneWidget) || (((ConstraintWidget)localObject2).allowedInBarrier()))
        {
          localObject1 = paramLinearSystem.createObjectVariable(localObject2.mListAnchors[this.mBarrierType]);
          localObject2 = ((ConstraintWidget)localObject2).mListAnchors;
          j = this.mBarrierType;
          localObject2[j].mSolverVariable = ((SolverVariable)localObject1);
          if ((j != 0) && (j != 2)) {
            paramLinearSystem.addGreaterBarrier(localConstraintAnchor.mSolverVariable, (SolverVariable)localObject1, bool);
          } else {
            paramLinearSystem.addLowerBarrier(localConstraintAnchor.mSolverVariable, (SolverVariable)localObject1, bool);
          }
        }
      }
      i = this.mBarrierType;
      if (i == 0)
      {
        paramLinearSystem.addEquality(this.mRight.mSolverVariable, this.mLeft.mSolverVariable, 0, 6);
        if (!bool) {
          paramLinearSystem.addEquality(this.mLeft.mSolverVariable, this.mParent.mRight.mSolverVariable, 0, 5);
        }
      }
      else if (i == 1)
      {
        paramLinearSystem.addEquality(this.mLeft.mSolverVariable, this.mRight.mSolverVariable, 0, 6);
        if (!bool) {
          paramLinearSystem.addEquality(this.mLeft.mSolverVariable, this.mParent.mLeft.mSolverVariable, 0, 5);
        }
      }
      else if (i == 2)
      {
        paramLinearSystem.addEquality(this.mBottom.mSolverVariable, this.mTop.mSolverVariable, 0, 6);
        if (!bool) {
          paramLinearSystem.addEquality(this.mTop.mSolverVariable, this.mParent.mBottom.mSolverVariable, 0, 5);
        }
      }
      else if (i == 3)
      {
        paramLinearSystem.addEquality(this.mTop.mSolverVariable, this.mBottom.mSolverVariable, 0, 6);
        if (!bool) {
          paramLinearSystem.addEquality(this.mTop.mSolverVariable, this.mParent.mTop.mSolverVariable, 0, 5);
        }
      }
      return;
    }
  }
  
  public boolean allowedInBarrier()
  {
    return true;
  }
  
  public boolean allowsGoneWidget()
  {
    return this.mAllowsGoneWidget;
  }
  
  public void analyze(int paramInt)
  {
    if (this.mParent == null) {
      return;
    }
    if (!((ConstraintWidgetContainer)this.mParent).optimizeFor(2)) {
      return;
    }
    ResolutionAnchor localResolutionAnchor;
    switch (this.mBarrierType)
    {
    default: 
      return;
    case 3: 
      localResolutionAnchor = this.mBottom.getResolutionNode();
      break;
    case 2: 
      localResolutionAnchor = this.mTop.getResolutionNode();
      break;
    case 1: 
      localResolutionAnchor = this.mRight.getResolutionNode();
      break;
    case 0: 
      localResolutionAnchor = this.mLeft.getResolutionNode();
    }
    localResolutionAnchor.setType(5);
    paramInt = this.mBarrierType;
    if ((paramInt != 0) && (paramInt != 1))
    {
      this.mLeft.getResolutionNode().resolve(null, 0.0F);
      this.mRight.getResolutionNode().resolve(null, 0.0F);
    }
    else
    {
      this.mTop.getResolutionNode().resolve(null, 0.0F);
      this.mBottom.getResolutionNode().resolve(null, 0.0F);
    }
    this.mNodes.clear();
    for (paramInt = 0; paramInt < this.mWidgetsCount; paramInt++)
    {
      Object localObject = this.mWidgets[paramInt];
      if ((this.mAllowsGoneWidget) || (((ConstraintWidget)localObject).allowedInBarrier()))
      {
        switch (this.mBarrierType)
        {
        default: 
          localObject = null;
          break;
        case 3: 
          localObject = ((ConstraintWidget)localObject).mBottom.getResolutionNode();
          break;
        case 2: 
          localObject = ((ConstraintWidget)localObject).mTop.getResolutionNode();
          break;
        case 1: 
          localObject = ((ConstraintWidget)localObject).mRight.getResolutionNode();
          break;
        case 0: 
          localObject = ((ConstraintWidget)localObject).mLeft.getResolutionNode();
        }
        if (localObject != null)
        {
          this.mNodes.add(localObject);
          ((ResolutionAnchor)localObject).addDependent(localResolutionAnchor);
        }
      }
    }
  }
  
  public void resetResolutionNodes()
  {
    super.resetResolutionNodes();
    this.mNodes.clear();
  }
  
  public void resolve()
  {
    int i = this.mBarrierType;
    float f1 = Float.MAX_VALUE;
    ResolutionAnchor localResolutionAnchor1;
    switch (i)
    {
    default: 
      return;
    case 3: 
      localResolutionAnchor1 = this.mBottom.getResolutionNode();
      f1 = 0.0F;
      break;
    case 2: 
      localResolutionAnchor1 = this.mTop.getResolutionNode();
      break;
    case 1: 
      localResolutionAnchor1 = this.mRight.getResolutionNode();
      f1 = 0.0F;
      break;
    case 0: 
      localResolutionAnchor1 = this.mLeft.getResolutionNode();
    }
    int j = this.mNodes.size();
    ResolutionAnchor localResolutionAnchor2 = null;
    i = 0;
    Object localObject;
    for (float f2 = f1; i < j; f2 = f1)
    {
      localObject = (ResolutionAnchor)this.mNodes.get(i);
      if (((ResolutionAnchor)localObject).state != 1) {
        return;
      }
      int k = this.mBarrierType;
      if ((k != 0) && (k != 2))
      {
        f1 = f2;
        if (((ResolutionAnchor)localObject).resolvedOffset > f2)
        {
          f1 = ((ResolutionAnchor)localObject).resolvedOffset;
          localResolutionAnchor2 = ((ResolutionAnchor)localObject).resolvedTarget;
        }
      }
      else
      {
        f1 = f2;
        if (((ResolutionAnchor)localObject).resolvedOffset < f2)
        {
          f1 = ((ResolutionAnchor)localObject).resolvedOffset;
          localResolutionAnchor2 = ((ResolutionAnchor)localObject).resolvedTarget;
        }
      }
      i++;
    }
    if (LinearSystem.getMetrics() != null)
    {
      localObject = LinearSystem.getMetrics();
      ((Metrics)localObject).barrierConnectionResolved += 1L;
    }
    localResolutionAnchor1.resolvedTarget = localResolutionAnchor2;
    localResolutionAnchor1.resolvedOffset = f2;
    localResolutionAnchor1.didResolve();
    switch (this.mBarrierType)
    {
    default: 
      
    case 3: 
      this.mTop.getResolutionNode().resolve(localResolutionAnchor2, f2);
      break;
    case 2: 
      this.mBottom.getResolutionNode().resolve(localResolutionAnchor2, f2);
      break;
    case 1: 
      this.mLeft.getResolutionNode().resolve(localResolutionAnchor2, f2);
      break;
    case 0: 
      this.mRight.getResolutionNode().resolve(localResolutionAnchor2, f2);
    }
  }
  
  public void setAllowsGoneWidget(boolean paramBoolean)
  {
    this.mAllowsGoneWidget = paramBoolean;
  }
  
  public void setBarrierType(int paramInt)
  {
    this.mBarrierType = paramInt;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/constraint/solver/widgets/Barrier.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */