package android.support.constraint.solver;

import java.io.PrintStream;
import java.util.Arrays;

public class ArrayLinkedVariables
{
  private static final boolean DEBUG = false;
  private static final boolean FULL_NEW_CHECK = false;
  private static final int NONE = -1;
  private int ROW_SIZE = 8;
  private SolverVariable candidate = null;
  int currentSize = 0;
  private int[] mArrayIndices;
  private int[] mArrayNextIndices;
  private float[] mArrayValues;
  private final Cache mCache;
  private boolean mDidFillOnce;
  private int mHead;
  private int mLast;
  private final ArrayRow mRow;
  
  ArrayLinkedVariables(ArrayRow paramArrayRow, Cache paramCache)
  {
    int i = this.ROW_SIZE;
    this.mArrayIndices = new int[i];
    this.mArrayNextIndices = new int[i];
    this.mArrayValues = new float[i];
    this.mHead = -1;
    this.mLast = -1;
    this.mDidFillOnce = false;
    this.mRow = paramArrayRow;
    this.mCache = paramCache;
  }
  
  private boolean isNew(SolverVariable paramSolverVariable, LinearSystem paramLinearSystem)
  {
    int i = paramSolverVariable.usageInRowCount;
    boolean bool = true;
    if (i > 1) {
      bool = false;
    }
    return bool;
  }
  
  final void add(SolverVariable paramSolverVariable, float paramFloat, boolean paramBoolean)
  {
    if (paramFloat == 0.0F) {
      return;
    }
    int i = this.mHead;
    if (i == -1)
    {
      this.mHead = 0;
      localObject = this.mArrayValues;
      i = this.mHead;
      localObject[i] = paramFloat;
      this.mArrayIndices[i] = paramSolverVariable.id;
      this.mArrayNextIndices[this.mHead] = -1;
      paramSolverVariable.usageInRowCount += 1;
      paramSolverVariable.addToRow(this.mRow);
      this.currentSize += 1;
      if (!this.mDidFillOnce)
      {
        this.mLast += 1;
        i = this.mLast;
        paramSolverVariable = this.mArrayIndices;
        if (i >= paramSolverVariable.length)
        {
          this.mDidFillOnce = true;
          this.mLast = (paramSolverVariable.length - 1);
        }
      }
      return;
    }
    int j = 0;
    int m = -1;
    while ((i != -1) && (j < this.currentSize))
    {
      if (this.mArrayIndices[i] == paramSolverVariable.id)
      {
        localObject = this.mArrayValues;
        localObject[i] += paramFloat;
        if (localObject[i] == 0.0F)
        {
          if (i == this.mHead)
          {
            this.mHead = this.mArrayNextIndices[i];
          }
          else
          {
            localObject = this.mArrayNextIndices;
            localObject[m] = localObject[i];
          }
          if (paramBoolean) {
            paramSolverVariable.removeFromRow(this.mRow);
          }
          if (this.mDidFillOnce) {
            this.mLast = i;
          }
          paramSolverVariable.usageInRowCount -= 1;
          this.currentSize -= 1;
        }
        return;
      }
      if (this.mArrayIndices[i] < paramSolverVariable.id) {
        m = i;
      }
      i = this.mArrayNextIndices[i];
      j++;
    }
    i = this.mLast;
    if (this.mDidFillOnce)
    {
      localObject = this.mArrayIndices;
      if (localObject[i] != -1) {
        i = localObject.length;
      }
    }
    else
    {
      i++;
    }
    Object localObject = this.mArrayIndices;
    j = i;
    if (i >= localObject.length)
    {
      j = i;
      if (this.currentSize < localObject.length) {
        for (int k = 0;; k++)
        {
          localObject = this.mArrayIndices;
          j = i;
          if (k >= localObject.length) {
            break;
          }
          if (localObject[k] == -1)
          {
            j = k;
            break;
          }
        }
      }
    }
    localObject = this.mArrayIndices;
    i = j;
    if (j >= localObject.length)
    {
      i = localObject.length;
      this.ROW_SIZE *= 2;
      this.mDidFillOnce = false;
      this.mLast = (i - 1);
      this.mArrayValues = Arrays.copyOf(this.mArrayValues, this.ROW_SIZE);
      this.mArrayIndices = Arrays.copyOf(this.mArrayIndices, this.ROW_SIZE);
      this.mArrayNextIndices = Arrays.copyOf(this.mArrayNextIndices, this.ROW_SIZE);
    }
    this.mArrayIndices[i] = paramSolverVariable.id;
    this.mArrayValues[i] = paramFloat;
    if (m != -1)
    {
      localObject = this.mArrayNextIndices;
      localObject[i] = localObject[m];
      localObject[m] = i;
    }
    else
    {
      this.mArrayNextIndices[i] = this.mHead;
      this.mHead = i;
    }
    paramSolverVariable.usageInRowCount += 1;
    paramSolverVariable.addToRow(this.mRow);
    this.currentSize += 1;
    if (!this.mDidFillOnce) {
      this.mLast += 1;
    }
    i = this.mLast;
    paramSolverVariable = this.mArrayIndices;
    if (i >= paramSolverVariable.length)
    {
      this.mDidFillOnce = true;
      this.mLast = (paramSolverVariable.length - 1);
    }
  }
  
  SolverVariable chooseSubject(LinearSystem paramLinearSystem)
  {
    int j = this.mHead;
    Object localObject4 = null;
    int i = 0;
    Object localObject3 = null;
    float f5 = 0.0F;
    boolean bool4 = false;
    float f4 = 0.0F;
    boolean bool2;
    for (boolean bool3 = false; (j != -1) && (i < this.currentSize); bool3 = bool2)
    {
      float f2 = this.mArrayValues[j];
      SolverVariable localSolverVariable = this.mCache.mIndexedVariables[this.mArrayIndices[j]];
      float f1;
      if (f2 < 0.0F)
      {
        f1 = f2;
        if (f2 > -0.001F)
        {
          this.mArrayValues[j] = 0.0F;
          localSolverVariable.removeFromRow(this.mRow);
          f1 = 0.0F;
        }
      }
      else
      {
        f1 = f2;
        if (f2 < 0.001F)
        {
          this.mArrayValues[j] = 0.0F;
          localSolverVariable.removeFromRow(this.mRow);
          f1 = 0.0F;
        }
      }
      Object localObject1 = localObject4;
      Object localObject2 = localObject3;
      f2 = f5;
      boolean bool1 = bool4;
      float f3 = f4;
      bool2 = bool3;
      if (f1 != 0.0F) {
        if (localSolverVariable.mType == SolverVariable.Type.UNRESTRICTED)
        {
          if (localObject3 == null)
          {
            bool1 = isNew(localSolverVariable, paramLinearSystem);
            localObject1 = localObject4;
            localObject2 = localSolverVariable;
            f2 = f1;
            f3 = f4;
            bool2 = bool3;
          }
          else if (f5 > f1)
          {
            bool1 = isNew(localSolverVariable, paramLinearSystem);
            localObject1 = localObject4;
            localObject2 = localSolverVariable;
            f2 = f1;
            f3 = f4;
            bool2 = bool3;
          }
          else
          {
            localObject1 = localObject4;
            localObject2 = localObject3;
            f2 = f5;
            bool1 = bool4;
            f3 = f4;
            bool2 = bool3;
            if (!bool4)
            {
              localObject1 = localObject4;
              localObject2 = localObject3;
              f2 = f5;
              bool1 = bool4;
              f3 = f4;
              bool2 = bool3;
              if (isNew(localSolverVariable, paramLinearSystem))
              {
                bool1 = true;
                localObject1 = localObject4;
                localObject2 = localSolverVariable;
                f2 = f1;
                f3 = f4;
                bool2 = bool3;
              }
            }
          }
        }
        else
        {
          localObject1 = localObject4;
          localObject2 = localObject3;
          f2 = f5;
          bool1 = bool4;
          f3 = f4;
          bool2 = bool3;
          if (localObject3 == null)
          {
            localObject1 = localObject4;
            localObject2 = localObject3;
            f2 = f5;
            bool1 = bool4;
            f3 = f4;
            bool2 = bool3;
            if (f1 < 0.0F) {
              if (localObject4 == null)
              {
                bool2 = isNew(localSolverVariable, paramLinearSystem);
                localObject1 = localSolverVariable;
                localObject2 = localObject3;
                f2 = f5;
                bool1 = bool4;
                f3 = f1;
              }
              else if (f4 > f1)
              {
                bool2 = isNew(localSolverVariable, paramLinearSystem);
                localObject1 = localSolverVariable;
                localObject2 = localObject3;
                f2 = f5;
                bool1 = bool4;
                f3 = f1;
              }
              else
              {
                localObject1 = localObject4;
                localObject2 = localObject3;
                f2 = f5;
                bool1 = bool4;
                f3 = f4;
                bool2 = bool3;
                if (!bool3)
                {
                  localObject1 = localObject4;
                  localObject2 = localObject3;
                  f2 = f5;
                  bool1 = bool4;
                  f3 = f4;
                  bool2 = bool3;
                  if (isNew(localSolverVariable, paramLinearSystem))
                  {
                    bool2 = true;
                    f3 = f1;
                    bool1 = bool4;
                    f2 = f5;
                    localObject2 = localObject3;
                    localObject1 = localSolverVariable;
                  }
                }
              }
            }
          }
        }
      }
      j = this.mArrayNextIndices[j];
      i++;
      localObject4 = localObject1;
      localObject3 = localObject2;
      f5 = f2;
      bool4 = bool1;
      f4 = f3;
    }
    if (localObject3 != null) {
      return (SolverVariable)localObject3;
    }
    return (SolverVariable)localObject4;
  }
  
  public final void clear()
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      SolverVariable localSolverVariable = this.mCache.mIndexedVariables[this.mArrayIndices[j]];
      if (localSolverVariable != null) {
        localSolverVariable.removeFromRow(this.mRow);
      }
      j = this.mArrayNextIndices[j];
    }
    this.mHead = -1;
    this.mLast = -1;
    this.mDidFillOnce = false;
    this.currentSize = 0;
  }
  
  final boolean containsKey(SolverVariable paramSolverVariable)
  {
    int j = this.mHead;
    if (j == -1) {
      return false;
    }
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      if (this.mArrayIndices[j] == paramSolverVariable.id) {
        return true;
      }
      j = this.mArrayNextIndices[j];
    }
    return false;
  }
  
  public void display()
  {
    int j = this.currentSize;
    System.out.print("{ ");
    for (int i = 0; i < j; i++)
    {
      SolverVariable localSolverVariable = getVariable(i);
      if (localSolverVariable != null)
      {
        PrintStream localPrintStream = System.out;
        StringBuilder localStringBuilder = new StringBuilder();
        localStringBuilder.append(localSolverVariable);
        localStringBuilder.append(" = ");
        localStringBuilder.append(getVariableValue(i));
        localStringBuilder.append(" ");
        localPrintStream.print(localStringBuilder.toString());
      }
    }
    System.out.println(" }");
  }
  
  void divideByAmount(float paramFloat)
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      float[] arrayOfFloat = this.mArrayValues;
      arrayOfFloat[j] /= paramFloat;
      j = this.mArrayNextIndices[j];
    }
  }
  
  public final float get(SolverVariable paramSolverVariable)
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      if (this.mArrayIndices[j] == paramSolverVariable.id) {
        return this.mArrayValues[j];
      }
      j = this.mArrayNextIndices[j];
    }
    return 0.0F;
  }
  
  SolverVariable getPivotCandidate()
  {
    Object localObject1 = this.candidate;
    if (localObject1 == null)
    {
      int j = this.mHead;
      int i = 0;
      Object localObject2;
      for (localObject1 = null; (j != -1) && (i < this.currentSize); localObject1 = localObject2)
      {
        localObject2 = localObject1;
        if (this.mArrayValues[j] < 0.0F)
        {
          SolverVariable localSolverVariable = this.mCache.mIndexedVariables[this.mArrayIndices[j]];
          if (localObject1 != null)
          {
            localObject2 = localObject1;
            if (((SolverVariable)localObject1).strength >= localSolverVariable.strength) {}
          }
          else
          {
            localObject2 = localSolverVariable;
          }
        }
        j = this.mArrayNextIndices[j];
        i++;
      }
      return (SolverVariable)localObject1;
    }
    return (SolverVariable)localObject1;
  }
  
  SolverVariable getPivotCandidate(boolean[] paramArrayOfBoolean, SolverVariable paramSolverVariable)
  {
    int j = this.mHead;
    int i = 0;
    Object localObject2 = null;
    float f1;
    for (float f2 = 0.0F; (j != -1) && (i < this.currentSize); f2 = f1)
    {
      Object localObject1 = localObject2;
      f1 = f2;
      if (this.mArrayValues[j] < 0.0F)
      {
        SolverVariable localSolverVariable = this.mCache.mIndexedVariables[this.mArrayIndices[j]];
        if (paramArrayOfBoolean != null)
        {
          localObject1 = localObject2;
          f1 = f2;
          if (paramArrayOfBoolean[localSolverVariable.id] != 0) {}
        }
        else
        {
          localObject1 = localObject2;
          f1 = f2;
          if (localSolverVariable != paramSolverVariable) {
            if (localSolverVariable.mType != SolverVariable.Type.SLACK)
            {
              localObject1 = localObject2;
              f1 = f2;
              if (localSolverVariable.mType != SolverVariable.Type.ERROR) {}
            }
            else
            {
              float f3 = this.mArrayValues[j];
              localObject1 = localObject2;
              f1 = f2;
              if (f3 < f2)
              {
                localObject1 = localSolverVariable;
                f1 = f3;
              }
            }
          }
        }
      }
      j = this.mArrayNextIndices[j];
      i++;
      localObject2 = localObject1;
    }
    return (SolverVariable)localObject2;
  }
  
  final SolverVariable getVariable(int paramInt)
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      if (i == paramInt) {
        return this.mCache.mIndexedVariables[this.mArrayIndices[j]];
      }
      j = this.mArrayNextIndices[j];
    }
    return null;
  }
  
  final float getVariableValue(int paramInt)
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      if (i == paramInt) {
        return this.mArrayValues[j];
      }
      j = this.mArrayNextIndices[j];
    }
    return 0.0F;
  }
  
  boolean hasAtLeastOnePositiveVariable()
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      if (this.mArrayValues[j] > 0.0F) {
        return true;
      }
      j = this.mArrayNextIndices[j];
    }
    return false;
  }
  
  void invert()
  {
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      float[] arrayOfFloat = this.mArrayValues;
      arrayOfFloat[j] *= -1.0F;
      j = this.mArrayNextIndices[j];
    }
  }
  
  public final void put(SolverVariable paramSolverVariable, float paramFloat)
  {
    if (paramFloat == 0.0F)
    {
      remove(paramSolverVariable, true);
      return;
    }
    int i = this.mHead;
    if (i == -1)
    {
      this.mHead = 0;
      localObject = this.mArrayValues;
      i = this.mHead;
      localObject[i] = paramFloat;
      this.mArrayIndices[i] = paramSolverVariable.id;
      this.mArrayNextIndices[this.mHead] = -1;
      paramSolverVariable.usageInRowCount += 1;
      paramSolverVariable.addToRow(this.mRow);
      this.currentSize += 1;
      if (!this.mDidFillOnce)
      {
        this.mLast += 1;
        i = this.mLast;
        paramSolverVariable = this.mArrayIndices;
        if (i >= paramSolverVariable.length)
        {
          this.mDidFillOnce = true;
          this.mLast = (paramSolverVariable.length - 1);
        }
      }
      return;
    }
    int j = 0;
    int m = -1;
    while ((i != -1) && (j < this.currentSize))
    {
      if (this.mArrayIndices[i] == paramSolverVariable.id)
      {
        this.mArrayValues[i] = paramFloat;
        return;
      }
      if (this.mArrayIndices[i] < paramSolverVariable.id) {
        m = i;
      }
      i = this.mArrayNextIndices[i];
      j++;
    }
    i = this.mLast;
    if (this.mDidFillOnce)
    {
      localObject = this.mArrayIndices;
      if (localObject[i] != -1) {
        i = localObject.length;
      }
    }
    else
    {
      i++;
    }
    Object localObject = this.mArrayIndices;
    j = i;
    if (i >= localObject.length)
    {
      j = i;
      if (this.currentSize < localObject.length) {
        for (int k = 0;; k++)
        {
          localObject = this.mArrayIndices;
          j = i;
          if (k >= localObject.length) {
            break;
          }
          if (localObject[k] == -1)
          {
            j = k;
            break;
          }
        }
      }
    }
    localObject = this.mArrayIndices;
    i = j;
    if (j >= localObject.length)
    {
      i = localObject.length;
      this.ROW_SIZE *= 2;
      this.mDidFillOnce = false;
      this.mLast = (i - 1);
      this.mArrayValues = Arrays.copyOf(this.mArrayValues, this.ROW_SIZE);
      this.mArrayIndices = Arrays.copyOf(this.mArrayIndices, this.ROW_SIZE);
      this.mArrayNextIndices = Arrays.copyOf(this.mArrayNextIndices, this.ROW_SIZE);
    }
    this.mArrayIndices[i] = paramSolverVariable.id;
    this.mArrayValues[i] = paramFloat;
    if (m != -1)
    {
      localObject = this.mArrayNextIndices;
      localObject[i] = localObject[m];
      localObject[m] = i;
    }
    else
    {
      this.mArrayNextIndices[i] = this.mHead;
      this.mHead = i;
    }
    paramSolverVariable.usageInRowCount += 1;
    paramSolverVariable.addToRow(this.mRow);
    this.currentSize += 1;
    if (!this.mDidFillOnce) {
      this.mLast += 1;
    }
    if (this.currentSize >= this.mArrayIndices.length) {
      this.mDidFillOnce = true;
    }
    i = this.mLast;
    paramSolverVariable = this.mArrayIndices;
    if (i >= paramSolverVariable.length)
    {
      this.mDidFillOnce = true;
      this.mLast = (paramSolverVariable.length - 1);
    }
  }
  
  public final float remove(SolverVariable paramSolverVariable, boolean paramBoolean)
  {
    if (this.candidate == paramSolverVariable) {
      this.candidate = null;
    }
    int i = this.mHead;
    if (i == -1) {
      return 0.0F;
    }
    int k = 0;
    int j = -1;
    while ((i != -1) && (k < this.currentSize))
    {
      if (this.mArrayIndices[i] == paramSolverVariable.id)
      {
        if (i == this.mHead)
        {
          this.mHead = this.mArrayNextIndices[i];
        }
        else
        {
          int[] arrayOfInt = this.mArrayNextIndices;
          arrayOfInt[j] = arrayOfInt[i];
        }
        if (paramBoolean) {
          paramSolverVariable.removeFromRow(this.mRow);
        }
        paramSolverVariable.usageInRowCount -= 1;
        this.currentSize -= 1;
        this.mArrayIndices[i] = -1;
        if (this.mDidFillOnce) {
          this.mLast = i;
        }
        return this.mArrayValues[i];
      }
      int m = this.mArrayNextIndices[i];
      k++;
      j = i;
      i = m;
    }
    return 0.0F;
  }
  
  int sizeInBytes()
  {
    return this.mArrayIndices.length * 4 * 3 + 0 + 36;
  }
  
  public String toString()
  {
    String str = "";
    int j = this.mHead;
    for (int i = 0; (j != -1) && (i < this.currentSize); i++)
    {
      StringBuilder localStringBuilder = new StringBuilder();
      localStringBuilder.append(str);
      localStringBuilder.append(" -> ");
      str = localStringBuilder.toString();
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(str);
      localStringBuilder.append(this.mArrayValues[j]);
      localStringBuilder.append(" : ");
      str = localStringBuilder.toString();
      localStringBuilder = new StringBuilder();
      localStringBuilder.append(str);
      localStringBuilder.append(this.mCache.mIndexedVariables[this.mArrayIndices[j]]);
      str = localStringBuilder.toString();
      j = this.mArrayNextIndices[j];
    }
    return str;
  }
  
  final void updateFromRow(ArrayRow paramArrayRow1, ArrayRow paramArrayRow2, boolean paramBoolean)
  {
    int j = this.mHead;
    int i = 0;
    while ((j != -1) && (i < this.currentSize)) {
      if (this.mArrayIndices[j] == paramArrayRow2.variable.id)
      {
        float f = this.mArrayValues[j];
        remove(paramArrayRow2.variable, paramBoolean);
        ArrayLinkedVariables localArrayLinkedVariables = (ArrayLinkedVariables)paramArrayRow2.variables;
        j = localArrayLinkedVariables.mHead;
        for (i = 0; (j != -1) && (i < localArrayLinkedVariables.currentSize); i++)
        {
          add(this.mCache.mIndexedVariables[localArrayLinkedVariables.mArrayIndices[j]], localArrayLinkedVariables.mArrayValues[j] * f, paramBoolean);
          j = localArrayLinkedVariables.mArrayNextIndices[j];
        }
        paramArrayRow1.constantValue += paramArrayRow2.constantValue * f;
        if (paramBoolean) {
          paramArrayRow2.variable.removeFromRow(paramArrayRow1);
        }
        j = this.mHead;
        i = 0;
      }
      else
      {
        j = this.mArrayNextIndices[j];
        i++;
      }
    }
  }
  
  void updateFromSystem(ArrayRow paramArrayRow, ArrayRow[] paramArrayOfArrayRow)
  {
    int j = this.mHead;
    int i = 0;
    while ((j != -1) && (i < this.currentSize))
    {
      Object localObject = this.mCache.mIndexedVariables[this.mArrayIndices[j]];
      if (((SolverVariable)localObject).definitionId != -1)
      {
        float f = this.mArrayValues[j];
        remove((SolverVariable)localObject, true);
        ArrayRow localArrayRow = paramArrayOfArrayRow[localObject.definitionId];
        if (!localArrayRow.isSimpleDefinition)
        {
          localObject = (ArrayLinkedVariables)localArrayRow.variables;
          j = ((ArrayLinkedVariables)localObject).mHead;
          for (i = 0; (j != -1) && (i < ((ArrayLinkedVariables)localObject).currentSize); i++)
          {
            add(this.mCache.mIndexedVariables[localObject.mArrayIndices[j]], localObject.mArrayValues[j] * f, true);
            j = localObject.mArrayNextIndices[j];
          }
        }
        paramArrayRow.constantValue += localArrayRow.constantValue * f;
        localArrayRow.variable.removeFromRow(paramArrayRow);
        j = this.mHead;
        i = 0;
      }
      else
      {
        j = this.mArrayNextIndices[j];
        i++;
      }
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/support/constraint/solver/ArrayLinkedVariables.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */