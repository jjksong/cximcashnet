package android.arch.core.util;

public abstract interface Function<I, O>
{
  public abstract O apply(I paramI);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/arch/core/util/Function.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */