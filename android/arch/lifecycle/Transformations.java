package android.arch.lifecycle;

import android.arch.core.util.Function;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

public class Transformations
{
  @MainThread
  public static <X, Y> LiveData<Y> map(@NonNull LiveData<X> paramLiveData, @NonNull final Function<X, Y> paramFunction)
  {
    MediatorLiveData localMediatorLiveData = new MediatorLiveData();
    localMediatorLiveData.addSource(paramLiveData, new Observer()
    {
      public void onChanged(@Nullable X paramAnonymousX)
      {
        this.val$result.setValue(paramFunction.apply(paramAnonymousX));
      }
    });
    return localMediatorLiveData;
  }
  
  @MainThread
  public static <X, Y> LiveData<Y> switchMap(@NonNull LiveData<X> paramLiveData, @NonNull Function<X, LiveData<Y>> paramFunction)
  {
    final MediatorLiveData localMediatorLiveData = new MediatorLiveData();
    localMediatorLiveData.addSource(paramLiveData, new Observer()
    {
      LiveData<Y> mSource;
      
      public void onChanged(@Nullable X paramAnonymousX)
      {
        paramAnonymousX = (LiveData)this.val$func.apply(paramAnonymousX);
        LiveData localLiveData = this.mSource;
        if (localLiveData == paramAnonymousX) {
          return;
        }
        if (localLiveData != null) {
          localMediatorLiveData.removeSource(localLiveData);
        }
        this.mSource = paramAnonymousX;
        paramAnonymousX = this.mSource;
        if (paramAnonymousX != null) {
          localMediatorLiveData.addSource(paramAnonymousX, new Observer()
          {
            public void onChanged(@Nullable Y paramAnonymous2Y)
            {
              Transformations.2.this.val$result.setValue(paramAnonymous2Y);
            }
          });
        }
      }
    });
    return localMediatorLiveData;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/arch/lifecycle/Transformations.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */