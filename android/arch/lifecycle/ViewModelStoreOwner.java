package android.arch.lifecycle;

import android.support.annotation.NonNull;

public abstract interface ViewModelStoreOwner
{
  @NonNull
  public abstract ViewModelStore getViewModelStore();
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/arch/lifecycle/ViewModelStoreOwner.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */