package android.arch.lifecycle;

import android.support.annotation.RestrictTo;

@RestrictTo({android.support.annotation.RestrictTo.Scope.LIBRARY_GROUP})
public abstract interface GeneratedAdapter
{
  public abstract void callMethods(LifecycleOwner paramLifecycleOwner, Lifecycle.Event paramEvent, boolean paramBoolean, MethodCallsLogger paramMethodCallsLogger);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/arch/lifecycle/GeneratedAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */