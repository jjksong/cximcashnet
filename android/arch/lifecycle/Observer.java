package android.arch.lifecycle;

import android.support.annotation.Nullable;

public abstract interface Observer<T>
{
  public abstract void onChanged(@Nullable T paramT);
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/arch/lifecycle/Observer.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */