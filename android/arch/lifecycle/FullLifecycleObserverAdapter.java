package android.arch.lifecycle;

class FullLifecycleObserverAdapter
  implements GenericLifecycleObserver
{
  private final FullLifecycleObserver mObserver;
  
  FullLifecycleObserverAdapter(FullLifecycleObserver paramFullLifecycleObserver)
  {
    this.mObserver = paramFullLifecycleObserver;
  }
  
  public void onStateChanged(LifecycleOwner paramLifecycleOwner, Lifecycle.Event paramEvent)
  {
    switch (paramEvent)
    {
    default: 
      break;
    case ???: 
      throw new IllegalArgumentException("ON_ANY must not been send by anybody");
    case ???: 
      this.mObserver.onDestroy(paramLifecycleOwner);
      break;
    case ???: 
      this.mObserver.onStop(paramLifecycleOwner);
      break;
    case ???: 
      this.mObserver.onPause(paramLifecycleOwner);
      break;
    case ???: 
      this.mObserver.onResume(paramLifecycleOwner);
      break;
    case ???: 
      this.mObserver.onStart(paramLifecycleOwner);
      break;
    case ???: 
      this.mObserver.onCreate(paramLifecycleOwner);
    }
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/android/arch/lifecycle/FullLifecycleObserverAdapter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */