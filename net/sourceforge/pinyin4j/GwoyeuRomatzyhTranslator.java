package net.sourceforge.pinyin4j;

import com.hp.hpl.sparta.Document;
import com.hp.hpl.sparta.Element;
import com.hp.hpl.sparta.ParseException;

class GwoyeuRomatzyhTranslator
{
  private static String[] tones = { "_I", "_II", "_III", "_IV", "_V" };
  
  static String convertHanyuPinyinToGwoyeuRomatzyh(String paramString)
  {
    Object localObject2 = TextHelper.extractPinyinString(paramString);
    String str = TextHelper.extractToneNumber(paramString);
    Object localObject1 = null;
    try
    {
      paramString = new java/lang/StringBuffer;
      paramString.<init>();
      paramString.append("//");
      paramString.append(PinyinRomanizationType.HANYU_PINYIN.getTagName());
      paramString.append("[text()='");
      paramString.append((String)localObject2);
      paramString.append("']");
      paramString = paramString.toString();
      localObject2 = GwoyeuRomatzyhResource.getInstance().getPinyinToGwoyeuMappingDoc().xpathSelectElement(paramString);
      paramString = (String)localObject1;
      if (localObject2 != null)
      {
        paramString = new java/lang/StringBuffer;
        paramString.<init>();
        paramString.append("../");
        paramString.append(PinyinRomanizationType.GWOYEU_ROMATZYH.getTagName());
        paramString.append(tones[(Integer.parseInt(str) - 1)]);
        paramString.append("/text()");
        paramString = ((Element)localObject2).xpathSelectString(paramString.toString());
      }
    }
    catch (ParseException paramString)
    {
      paramString.printStackTrace();
      paramString = (String)localObject1;
    }
    return paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/GwoyeuRomatzyhTranslator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */