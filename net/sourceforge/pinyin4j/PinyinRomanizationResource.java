package net.sourceforge.pinyin4j;

import com.hp.hpl.sparta.Document;
import com.hp.hpl.sparta.ParseException;
import com.hp.hpl.sparta.Parser;
import java.io.FileNotFoundException;
import java.io.IOException;

class PinyinRomanizationResource
{
  private Document pinyinMappingDoc;
  
  private PinyinRomanizationResource()
  {
    initializeResource();
  }
  
  static PinyinRomanizationResource getInstance()
  {
    return PinyinRomanizationSystemResourceHolder.theInstance;
  }
  
  private void initializeResource()
  {
    try
    {
      setPinyinMappingDoc(Parser.parse("", ResourceHelper.getResourceInputStream("/pinyindb/pinyin_mapping.xml")));
    }
    catch (ParseException localParseException)
    {
      localParseException.printStackTrace();
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
    }
  }
  
  private void setPinyinMappingDoc(Document paramDocument)
  {
    this.pinyinMappingDoc = paramDocument;
  }
  
  Document getPinyinMappingDoc()
  {
    return this.pinyinMappingDoc;
  }
  
  private static class PinyinRomanizationSystemResourceHolder
  {
    static final PinyinRomanizationResource theInstance = new PinyinRomanizationResource(null);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/PinyinRomanizationResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */