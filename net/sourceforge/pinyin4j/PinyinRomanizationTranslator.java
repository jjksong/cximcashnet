package net.sourceforge.pinyin4j;

import com.hp.hpl.sparta.Document;
import com.hp.hpl.sparta.Element;
import com.hp.hpl.sparta.ParseException;

class PinyinRomanizationTranslator
{
  static String convertRomanizationSystem(String paramString, PinyinRomanizationType paramPinyinRomanizationType1, PinyinRomanizationType paramPinyinRomanizationType2)
  {
    String str2 = TextHelper.extractPinyinString(paramString);
    String str1 = TextHelper.extractToneNumber(paramString);
    Object localObject = null;
    try
    {
      paramString = new java/lang/StringBuffer;
      paramString.<init>();
      paramString.append("//");
      paramString.append(paramPinyinRomanizationType1.getTagName());
      paramString.append("[text()='");
      paramString.append(str2);
      paramString.append("']");
      paramString = paramString.toString();
      paramPinyinRomanizationType1 = PinyinRomanizationResource.getInstance().getPinyinMappingDoc().xpathSelectElement(paramString);
      paramString = (String)localObject;
      if (paramPinyinRomanizationType1 != null)
      {
        paramString = new java/lang/StringBuffer;
        paramString.<init>();
        paramString.append("../");
        paramString.append(paramPinyinRomanizationType2.getTagName());
        paramString.append("/text()");
        paramString = paramPinyinRomanizationType1.xpathSelectString(paramString.toString());
        paramPinyinRomanizationType1 = new java/lang/StringBuffer;
        paramPinyinRomanizationType1.<init>();
        paramPinyinRomanizationType1.append(paramString);
        paramPinyinRomanizationType1.append(str1);
        paramString = paramPinyinRomanizationType1.toString();
      }
    }
    catch (ParseException paramString)
    {
      paramString.printStackTrace();
      paramString = (String)localObject;
    }
    return paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/PinyinRomanizationTranslator.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */