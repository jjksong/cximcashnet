package net.sourceforge.pinyin4j;

import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;
import net.sourceforge.pinyin4j.format.exception.BadHanyuPinyinOutputFormatCombination;

class PinyinFormatter
{
  private static String convertToneNumber2ToneMark(String paramString)
  {
    paramString = paramString.toLowerCase();
    Object localObject = paramString;
    if (paramString.matches("[a-z]*[1-5]?"))
    {
      if (paramString.matches("[a-z]*[1-5]"))
      {
        int n = Character.getNumericValue(paramString.charAt(paramString.length() - 1));
        int k = 97;
        int j = paramString.indexOf('a');
        int m = paramString.indexOf('e');
        int i = paramString.indexOf("ou");
        if (-1 != j)
        {
          i = k;
        }
        else if (-1 != m)
        {
          j = m;
          i = 101;
        }
        else if (-1 != i)
        {
          k = "ou".charAt(0);
          j = i;
          i = k;
        }
        else
        {
          for (j = paramString.length() - 1; j >= 0; j--) {
            if (String.valueOf(paramString.charAt(j)).matches("[aeiouv]"))
            {
              i = paramString.charAt(j);
              break label161;
            }
          }
          i = 36;
          j = -1;
        }
        label161:
        localObject = paramString;
        if (36 != i)
        {
          localObject = paramString;
          if (-1 != j)
          {
            char c = "āáăàaēéĕèeīíĭìiōóŏòoūúŭùuǖǘǚǜü".charAt("aeiouv".indexOf(i) * 5 + (n - 1));
            localObject = new StringBuffer();
            ((StringBuffer)localObject).append(paramString.substring(0, j).replaceAll("v", "ü"));
            ((StringBuffer)localObject).append(c);
            ((StringBuffer)localObject).append(paramString.substring(j + 1, paramString.length() - 1).replaceAll("v", "ü"));
            localObject = ((StringBuffer)localObject).toString();
          }
        }
        return (String)localObject;
      }
      localObject = paramString.replaceAll("v", "ü");
    }
    return (String)localObject;
  }
  
  static String formatHanyuPinyin(String paramString, HanyuPinyinOutputFormat paramHanyuPinyinOutputFormat)
    throws BadHanyuPinyinOutputFormatCombination
  {
    if ((HanyuPinyinToneType.WITH_TONE_MARK == paramHanyuPinyinOutputFormat.getToneType()) && ((HanyuPinyinVCharType.WITH_V == paramHanyuPinyinOutputFormat.getVCharType()) || (HanyuPinyinVCharType.WITH_U_AND_COLON == paramHanyuPinyinOutputFormat.getVCharType()))) {
      throw new BadHanyuPinyinOutputFormatCombination("tone marks cannot be added to v or u:");
    }
    if (HanyuPinyinToneType.WITHOUT_TONE == paramHanyuPinyinOutputFormat.getToneType())
    {
      str = paramString.replaceAll("[1-5]", "");
    }
    else
    {
      str = paramString;
      if (HanyuPinyinToneType.WITH_TONE_MARK == paramHanyuPinyinOutputFormat.getToneType()) {
        str = convertToneNumber2ToneMark(paramString.replaceAll("u:", "v"));
      }
    }
    if (HanyuPinyinVCharType.WITH_V == paramHanyuPinyinOutputFormat.getVCharType()) {}
    for (paramString = "v";; paramString = "ü")
    {
      paramString = str.replaceAll("u:", paramString);
      break;
      paramString = str;
      if (HanyuPinyinVCharType.WITH_U_UNICODE != paramHanyuPinyinOutputFormat.getVCharType()) {
        break;
      }
    }
    String str = paramString;
    if (HanyuPinyinCaseType.UPPERCASE == paramHanyuPinyinOutputFormat.getCaseType()) {
      str = paramString.toUpperCase();
    }
    return str;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/PinyinFormatter.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */