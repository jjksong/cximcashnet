package net.sourceforge.pinyin4j;

import com.hp.hpl.sparta.Document;
import com.hp.hpl.sparta.ParseException;
import com.hp.hpl.sparta.Parser;
import java.io.FileNotFoundException;
import java.io.IOException;

class GwoyeuRomatzyhResource
{
  private Document pinyinToGwoyeuMappingDoc;
  
  private GwoyeuRomatzyhResource()
  {
    initializeResource();
  }
  
  static GwoyeuRomatzyhResource getInstance()
  {
    return GwoyeuRomatzyhSystemResourceHolder.theInstance;
  }
  
  private void initializeResource()
  {
    try
    {
      setPinyinToGwoyeuMappingDoc(Parser.parse("", ResourceHelper.getResourceInputStream("/pinyindb/pinyin_gwoyeu_mapping.xml")));
    }
    catch (ParseException localParseException)
    {
      localParseException.printStackTrace();
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
    }
  }
  
  private void setPinyinToGwoyeuMappingDoc(Document paramDocument)
  {
    this.pinyinToGwoyeuMappingDoc = paramDocument;
  }
  
  Document getPinyinToGwoyeuMappingDoc()
  {
    return this.pinyinToGwoyeuMappingDoc;
  }
  
  private static class GwoyeuRomatzyhSystemResourceHolder
  {
    static final GwoyeuRomatzyhResource theInstance = new GwoyeuRomatzyhResource(null);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/GwoyeuRomatzyhResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */