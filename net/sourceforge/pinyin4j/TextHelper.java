package net.sourceforge.pinyin4j;

class TextHelper
{
  static String extractPinyinString(String paramString)
  {
    return paramString.substring(0, paramString.length() - 1);
  }
  
  static String extractToneNumber(String paramString)
  {
    return paramString.substring(paramString.length() - 1);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/TextHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */