package net.sourceforge.pinyin4j.format;

public class HanyuPinyinVCharType
{
  public static final HanyuPinyinVCharType WITH_U_AND_COLON = new HanyuPinyinVCharType("WITH_U_AND_COLON");
  public static final HanyuPinyinVCharType WITH_U_UNICODE = new HanyuPinyinVCharType("WITH_U_UNICODE");
  public static final HanyuPinyinVCharType WITH_V = new HanyuPinyinVCharType("WITH_V");
  protected String name;
  
  protected HanyuPinyinVCharType(String paramString)
  {
    setName(paramString);
  }
  
  public String getName()
  {
    return this.name;
  }
  
  protected void setName(String paramString)
  {
    this.name = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/format/HanyuPinyinVCharType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */