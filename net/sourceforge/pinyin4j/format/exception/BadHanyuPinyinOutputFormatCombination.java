package net.sourceforge.pinyin4j.format.exception;

public class BadHanyuPinyinOutputFormatCombination
  extends Exception
{
  private static final long serialVersionUID = -8500822088036526862L;
  
  public BadHanyuPinyinOutputFormatCombination(String paramString)
  {
    super(paramString);
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/format/exception/BadHanyuPinyinOutputFormatCombination.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */