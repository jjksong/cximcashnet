package net.sourceforge.pinyin4j.format;

public final class HanyuPinyinOutputFormat
{
  private HanyuPinyinCaseType caseType;
  private HanyuPinyinToneType toneType;
  private HanyuPinyinVCharType vCharType;
  
  public HanyuPinyinOutputFormat()
  {
    restoreDefault();
  }
  
  public HanyuPinyinCaseType getCaseType()
  {
    return this.caseType;
  }
  
  public HanyuPinyinToneType getToneType()
  {
    return this.toneType;
  }
  
  public HanyuPinyinVCharType getVCharType()
  {
    return this.vCharType;
  }
  
  public void restoreDefault()
  {
    this.vCharType = HanyuPinyinVCharType.WITH_U_AND_COLON;
    this.caseType = HanyuPinyinCaseType.LOWERCASE;
    this.toneType = HanyuPinyinToneType.WITH_TONE_NUMBER;
  }
  
  public void setCaseType(HanyuPinyinCaseType paramHanyuPinyinCaseType)
  {
    this.caseType = paramHanyuPinyinCaseType;
  }
  
  public void setToneType(HanyuPinyinToneType paramHanyuPinyinToneType)
  {
    this.toneType = paramHanyuPinyinToneType;
  }
  
  public void setVCharType(HanyuPinyinVCharType paramHanyuPinyinVCharType)
  {
    this.vCharType = paramHanyuPinyinVCharType;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/format/HanyuPinyinOutputFormat.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */