package net.sourceforge.pinyin4j.format;

public class HanyuPinyinToneType
{
  public static final HanyuPinyinToneType WITHOUT_TONE = new HanyuPinyinToneType("WITHOUT_TONE");
  public static final HanyuPinyinToneType WITH_TONE_MARK = new HanyuPinyinToneType("WITH_TONE_MARK");
  public static final HanyuPinyinToneType WITH_TONE_NUMBER = new HanyuPinyinToneType("WITH_TONE_NUMBER");
  protected String name;
  
  protected HanyuPinyinToneType(String paramString)
  {
    setName(paramString);
  }
  
  public String getName()
  {
    return this.name;
  }
  
  protected void setName(String paramString)
  {
    this.name = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/format/HanyuPinyinToneType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */