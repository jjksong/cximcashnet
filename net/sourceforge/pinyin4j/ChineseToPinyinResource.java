package net.sourceforge.pinyin4j;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

class ChineseToPinyinResource
{
  private Properties unicodeToHanyuPinyinTable = null;
  
  private ChineseToPinyinResource()
  {
    initializeResource();
  }
  
  private String getHanyuPinyinRecordFromChar(char paramChar)
  {
    String str = Integer.toHexString(paramChar).toUpperCase();
    str = getUnicodeToHanyuPinyinTable().getProperty(str);
    if (!isValidRecord(str)) {
      str = null;
    }
    return str;
  }
  
  static ChineseToPinyinResource getInstance()
  {
    return ChineseToPinyinResourceHolder.theInstance;
  }
  
  private Properties getUnicodeToHanyuPinyinTable()
  {
    return this.unicodeToHanyuPinyinTable;
  }
  
  private void initializeResource()
  {
    try
    {
      Properties localProperties = new java/util/Properties;
      localProperties.<init>();
      setUnicodeToHanyuPinyinTable(localProperties);
      getUnicodeToHanyuPinyinTable().load(ResourceHelper.getResourceInputStream("/pinyindb/unicode_to_hanyu_pinyin.txt"));
    }
    catch (IOException localIOException)
    {
      localIOException.printStackTrace();
    }
    catch (FileNotFoundException localFileNotFoundException)
    {
      localFileNotFoundException.printStackTrace();
    }
  }
  
  private boolean isValidRecord(String paramString)
  {
    return (paramString != null) && (!paramString.equals("(none0)")) && (paramString.startsWith("(")) && (paramString.endsWith(")"));
  }
  
  private void setUnicodeToHanyuPinyinTable(Properties paramProperties)
  {
    this.unicodeToHanyuPinyinTable = paramProperties;
  }
  
  String[] getHanyuPinyinStringArray(char paramChar)
  {
    String str = getHanyuPinyinRecordFromChar(paramChar);
    if (str != null) {
      return str.substring(str.indexOf("(") + 1, str.lastIndexOf(")")).split(",");
    }
    return null;
  }
  
  private static class ChineseToPinyinResourceHolder
  {
    static final ChineseToPinyinResource theInstance = new ChineseToPinyinResource(null);
  }
  
  class Field
  {
    static final String COMMA = ",";
    static final String LEFT_BRACKET = "(";
    static final String RIGHT_BRACKET = ")";
    
    Field() {}
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/ChineseToPinyinResource.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */