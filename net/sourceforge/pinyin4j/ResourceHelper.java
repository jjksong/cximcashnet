package net.sourceforge.pinyin4j;

import java.io.BufferedInputStream;

class ResourceHelper
{
  static BufferedInputStream getResourceInputStream(String paramString)
  {
    Class localClass2 = class$net$sourceforge$pinyin4j$ResourceHelper;
    Class localClass1 = localClass2;
    if (localClass2 == null)
    {
      localClass1 = class$("net.sourceforge.pinyin4j.ResourceHelper");
      class$net$sourceforge$pinyin4j$ResourceHelper = localClass1;
    }
    return new BufferedInputStream(localClass1.getResourceAsStream(paramString));
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/ResourceHelper.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */