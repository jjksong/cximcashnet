package net.sourceforge.pinyin4j;

class PinyinRomanizationType
{
  static final PinyinRomanizationType GWOYEU_ROMATZYH = new PinyinRomanizationType("Gwoyeu");
  static final PinyinRomanizationType HANYU_PINYIN = new PinyinRomanizationType("Hanyu");
  static final PinyinRomanizationType MPS2_PINYIN;
  static final PinyinRomanizationType TONGYONG_PINYIN;
  static final PinyinRomanizationType WADEGILES_PINYIN = new PinyinRomanizationType("Wade");
  static final PinyinRomanizationType YALE_PINYIN;
  protected String tagName;
  
  static
  {
    MPS2_PINYIN = new PinyinRomanizationType("MPSII");
    YALE_PINYIN = new PinyinRomanizationType("Yale");
    TONGYONG_PINYIN = new PinyinRomanizationType("Tongyong");
  }
  
  protected PinyinRomanizationType(String paramString)
  {
    setTagName(paramString);
  }
  
  String getTagName()
  {
    return this.tagName;
  }
  
  protected void setTagName(String paramString)
  {
    this.tagName = paramString;
  }
}


/* Location:              /Users/hesk/bbgcoutput/cracx/diggy/pX7I4JUrPBWH5EjAl25JrrgACt9hpccOC4AjbGIQ_classes.jar!/net/sourceforge/pinyin4j/PinyinRomanizationType.class
 * Java compiler version: 6 (50.0)
 * JD-Core Version:       0.7.1
 */